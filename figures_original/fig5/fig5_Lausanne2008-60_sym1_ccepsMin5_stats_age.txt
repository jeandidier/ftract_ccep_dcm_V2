*** Lausanne2008-60 - Symmetric: 1
*** child pairs of parcels: 762
*** adult pairs of parcels: 1195
*** Common pairs of parcels: 638
Peak latencies: child=31.4279, adult=26.3421 (p=2.2483e-16)
Conduction delays: child=5.5535, adult=3.844 (p=9.097e-10)
Distances: child=33.2743, adult=36.5648 (p=0.00038618)
PeakLatency velocity: child=1.1221, adult=1.4884 (p=7.4337e-16)
Conduction delays velocity: child=20.0962, adult=33.1075 (p=0.00035282)
*** Common parcels: 58
synapticTe: child=4.9404, adult=4.003 (p=0.021184)
synapticTi: child=7.188, adult=6.8019 (p=0.074206)
