CCEPs between lh.superiortemporal_1 and lh.parsopercularis_1
All peak latencies: #21 = 42.3828      45.3125      29.6875      10.1562         6.25      30.6641      17.9688      25.7812       21.875      25.7812      14.0625      14.0625      43.3594      49.2188      51.1719      45.3125         6.25      8.20312      8.20312      54.1016      15.0391
Median peak latencies: 25.7812
All conduction delays: #21 = 23.5682      28.4802      7.34336    0.0786811     0.116987     0.431165     0.118936     0.643183     0.828895     0.724208      1.26313    0.0497524      13.1382      16.6433      12.2993     0.648865     0.565365    0.0737486    0.0928364      30.9936        2.295
Median conduction delays: 0.72421


CCEPs between lh.superiortemporal_1 and lh.parstriangularis_1
All peak latencies: #10 = 39.4531      27.7344      25.7812        68.75         37.5      38.4766      8.20312      33.5938      9.17969      43.3594
Median peak latencies: 35.5469
All conduction delays: #10 = 20.7576      8.83793      2.22921       39.622      15.5022      2.39635    0.0454779      15.7601     0.489863      27.4455
Median conduction delays: 12.1701


CCEPs between lh.parsopercularis_1 and lh.superiortemporal_1
All peak latencies: #16 = 23.8281      55.0781      8.20312      8.20312      10.1562       21.875      29.6875      49.2188         6.25      8.20312      2.34375      18.9453      34.5703      14.0625      42.3828      26.7578
Median peak latencies: 20.4102
All conduction delays: #16 = 4.79863      21.4043     0.210205     0.593616    0.0220027    0.0775338      2.71843      10.3271     0.112029    0.0513772     0.146937      3.20303       2.2528     0.140112      6.59438      1.22129
Median conduction delays: 0.90745


CCEPs between lh.parstriangularis_1 and lh.superiortemporal_1
All peak latencies: #6 = 25.7812      47.2656      43.3594      49.2188      29.6875      27.7344
Median peak latencies: 36.5234
All conduction delays: #6 = 6.45379      17.5801      19.0752      3.77405      1.06569      9.62859
Median conduction delays: 8.0412
