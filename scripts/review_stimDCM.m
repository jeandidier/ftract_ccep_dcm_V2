function review_stimDCM( DCMs )

if isempty( DCMs )
    bd      = baseDir();
    fname   = fullfile( bd, 'dcm/PATS__V1__DCMs.mat') ;
    DCMs    = load(fname) ;
    DCMs    = DCMs.DCMs ;
end


% peakLatencies = cell( length(DCMs), 1 ) ;
peakLatencies       = [] ;

accuracies          = [] ;
dcmPeakLatencies    = [] ;

parfor c=1:length(DCMs)
% for c=1:length(DCMs)

    for s=1:length(DCMs(c).stims)
        
        % to be removed when everything is computed   
        try
            DCMs(c).stims{s}.cceps ;
        catch
           continue 
        end
        
        
        tmp             = arrayfun( @(c) c.peakLat, DCMs(c).stims{s}.cceps ) ;
        peakLatencies 	= horzcat( peakLatencies, tmp ) ;
        
        tmp             = arrayfun( @(c) full(c.s2_acc), DCMs(c).stims{s}.cceps ) ;
        accuracies   	= horzcat( accuracies, tmp ) ;
                
        tmp             = arrayfun( @(c) full(c.s2_peak), DCMs(c).stims{s}.cceps ) ;
        dcmPeakLatencies = horzcat( dcmPeakLatencies, tmp ) ;

    end
    
end


peakLat_step    = 5 ;
maxPL           = 80 ;
pl_range        = 0:peakLat_step:maxPL ;


colors          = get(groot,'defaultAxesColorOrder') ;

figure ;
hold on ;
% histo of all fits
h_all = histogram( peakLatencies, pl_range, 'FaceColor', colors(1,:) );  h = h_all ; h.FaceAlpha = 1; 
% histo of accurate fits
h_acc = histogram( peakLatencies( accuracies > 70 & abs(dcmPeakLatencies-peakLatencies) < 5 ), pl_range, 'FaceColor', colors(3,:) );  h = h_acc ; h.FaceAlpha = 1; 


end



