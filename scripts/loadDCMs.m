function DCMs = loadDCMs

sigThreshold = 5 ;
dataDir = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/data/PATS__V1' ;
dcmDir  = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1' ;


% pF_T            = [ 8 16 ];
% pF_Te           = pF_T(1) ;
% pF_Ti           = pF_T(2) ;



DCMs    = struct ;
nb_crf  = 0 ;

pats    = dir(dcmDir) ;


% symmetric               = false ;
% MarsAtlasParcelsName    = getParcelsName( 'MarsAtlas', symmetric ) ;


for p=1:length(pats)
    pat = pats(p).name ;
    if startsWith( pat, '.' ), continue ; end

    fprintf( 1, [ pat ' ' num2str(p) ' / ' num2str(length(pats)) '\n' ] ) ;
    patDir  = [ dcmDir '/' pats(p).name '/SEEG' ] ;
    crfs    = dir( patDir ) ;
    
    for c=1:length(crfs)
        
        crf     = crfs(c).name ;
        if startsWith( crf, '.' ), continue ; end
        crfDir  = [ patDir '/' crf '/SEQ_BM_ACPCHIP_F_DCM' ] ;
        nb_crf  = nb_crf + 1 ;
        DCMs(nb_crf,1).crfDir = crfDir ;
        DCMs(nb_crf,1).stims  = {} ;
        
        stims_f     = dir( crfDir ) ;
        stims_f     = stims_f( arrayfun( @(s) ~startsWith( s.name, '.' ), stims_f ) ) ;
        crf_stims   = cell( size(stims_f) ) ; 
        
        crf_date        = strrep( crf, 'StimLF_', '' ) ;
%         fname_pos       = fullfile( dataDir, 'contacts_position', [ pat '_' crf_date ] ) ;
%         fname_pos_mni   = [ fname_pos '_mni.txt' ] ;
%         fname_pos_scan  = [ fname_pos '_scan.txt' ] ;
%         crf_pos_mni     = strsplit( fileread( fname_pos_mni ), '\n' ) ;
%         crf_pos_scan    = strsplit( fileread( fname_pos_scan ), '\n' ) ;
        fname_distances = fullfile( dataDir, 'contacts_distance', [ pat '_' crf_date '_scan.txt' ] ) ;
        tmp             = strsplit( fileread( fname_distances ), '\n' ) ;
        tmp             = tmp( cellfun(@(l) ~isempty(l),tmp) );
        crf_distances   = cell( length(tmp), 2 ) ;
        for cp=1:size(crf_distances,1)
            l=strsplit(tmp{cp},'\t');
            crf_distances{cp,1}=l{1};
            crf_distances{cp,2}=str2double(l{2});
        end
        
        parfor s=1:length(stims_f)
%        for s=1:length(stims_f)
            
%             s
            
            stim_f              = stims_f(s).name ;
            
            stimDataDir         = [ strrep( strrep( crfDir, dcmDir, dataDir ), 'SEQ_BM_ACPCHIP_F_DCM', 'SEQ_BM_ACPCHIP_F_ComputeAverage' ) '/' stim_f ] ;
            
            fd                  = fopen( [ stimDataDir '/stim.txt' ], 'r' ) ;
            stimInfo            = textscan( fd, '%s', 'delimiter', '\n' ) ;
            fclose(fd);
            
            peakDelay           = spm_eeg_load( [ stimDataDir '/PeakDelay_' stim_f '.mat' ] ) ;
            
            fd                  = fopen( [ stimDataDir '/recContactName.txt' ], 'r' ) ;
            recContactName      = textscan( fd, '%s', 'delimiter', '\n' ) ;
            fclose(fd);
            
%             fd                  = fopen( [ stimDataDir '/recContactPos.txt' ], 'r' ) ;
%             recContactPos       = textscan( fd, '%s', 'delimiter', '\n' ) ;
%             fclose(fd);
            
            fd                  = fopen( [ stimDataDir '/recContactParc.txt' ], 'r' ) ;
            recContactParc      = textscan( fd, '%s', 'delimiter', '\n' ) ;
            fclose(fd);
            
            fd                  = fopen( [ stimDataDir '/recContactSeg.txt' ], 'r' ) ;
            recContactSeg       = textscan( fd, '%s', 'delimiter', '\n' ) ;
            fclose(fd);

            
%             if ~isequal( size(recContactName{1}), size(recContactPos{1}) ) || ~isequal( size(recContactName{1}), size(recContactParc{1}) ) || ~isequal( size(recContactName{1}), size(recContactSeg{1}) )
            if ~isequal( size(recContactName{1}), size(recContactParc{1}) ) || ~isequal( size(recContactName{1}), size(recContactSeg{1}) )
                error( 'Inconsistent recContactName, recContactPos, recContactParc, recContactSeg' ) ; 
            end
            
            
%             stim_pos = stimInfo{1}{5} ;          
%             [ stim_pos_mni, stim_pos_scan ] = getPosition( stimInfo{1}{4}, stim_pos, crf_pos_mni, crf_pos_scan ) ;
            
            
            stim_seg = stimInfo{1}{7} ;
            if ~strcmp(stim_seg, 'GreyMatter'), error( [ 'Invalid seg:' stim_seg ] ) ; end
%             % this is done for future use
%             stim_parc = stimInfo{1}{6} ;
%             try
%                 stim_marsatlas      = parseAtlasParcel( stim_parc, 'MarsAtlas' ) ;
%                 stim_marsatlasInd   = find( cellfun( @(x) strcmp(x,stim_marsatlas), MarsAtlasParcelsName ) ) ;
%             catch ME
%                 if strcmp( ME.identifier, 'MATLAB:AtlasNotFound' ) || strcmp( ME.identifier, 'MATLAB:ParcelNotDefined' )
% %                     fprintf( 1, [ 'Stim ' ME.message '\n' ] ) ;
% %                     continue
%                     stim_marsatlasInd = 0 ;
%                 else
%                     rethrow(ME)
%                 end
% %                 stim_marsatlas = ME.message ;
% %                 
%                 
%             end
            
            
            
            stimDir             = [ crfDir '/' stim_f ] ;
            
            stim                = struct ;
            stim.info           = stimInfo{1} ;
            stim.crf            = stimInfo{1}{1} ;
            stim.age            = str2double( stimInfo{1}{2} ) ;
            stim.name           = stimInfo{1}{3} ;
            stim.contact        = stimInfo{1}{4} ;
%             stim.pos            = sscanf(stim_pos,'[%f, %f, %f]') ;
%             stim.pos_mni        = sscanf(stim_pos_mni,'[%f, %f, %f]') ;
%             stim.pos_scan       = sscanf(stim_pos_scan,'[%f, %f, %f]') ;
            
            
            stim.parc           = stimInfo{1}{6} ;
%             stim.marsatlasInd   = stim_marsatlasInd ;
            stim.seg            = stimInfo{1}{7} ;
            stim.dur            = str2double( stimInfo{1}{8} ) ;
            stim.freq           = str2double( stimInfo{1}{9} ) ;
            stim.int            = str2double( stimInfo{1}{10} ) ;
            stim.crop           = str2double( stimInfo{1}{11} ) ;
            stim.mode           = stimInfo{1}{12} ;
            
            
            s1                  = dir( [ stimDir '/step1' ] ) ;
            s1                  = s1( arrayfun( @(s) ~startsWith( s.name, '.' ) && ~endsWith( s.name, '.log' ), s1 ) ) ;
            s2                  = dir( [ stimDir '/step2' ] ) ;
            s2                  = s2( arrayfun( @(s) ~startsWith( s.name, '.' ) && ~endsWith( s.name, '.log' ), s2 ) ) ;
            
            
            if ~isequal( arrayfun( @(s) s.name, s1, 'UniformOutput', false ), arrayfun( @(s) s.name, s2, 'UniformOutput', false ) )
                error( [ 'Inconsistent s1/s2: ' stimDir ] )
                % maybe not yet done
%                 continue
            end
            
            
            
            stim_cceps          = cell( size(s1) ) ;
            for cc=1:length(s1)
                dcm             = s1(cc).name ;
%                 if startsWith( dcm, '.' ) || endsWith( dcm, '.log' ), continue ; end
                [ ~, name, ext ] = fileparts( dcm ) ;
                if ~strcmp( ext, '.mat' ), error( 'Invalid dcm fname' ) ; end
                
                tmp             = [ 'DCM_averaged_zs_' stim_f '_' ] ;
                if ~startsWith( name, tmp ), error( 'Invalid dcm fname' ) ; end
                contact         = name( length(tmp)+1:end ) ;
                
                spmContactId    = find( strcmp( contact, peakDelay.chanlabels ) ) ;
                if length(spmContactId) ~= 1, error( 'Contact not found in spm mat file' ) ; end
                txtContactId    = find( strcmp( contact, recContactName{1} ) ) ;
                if length(txtContactId) ~= 1, error( 'Contact not found in DB txt file' ) ; end
                
                
%                 rec_pos  = recContactPos{1}{txtContactId} ;
%                 [ rec_pos_mni, rec_pos_scan ] = getPosition( contact, rec_pos, crf_pos_mni, crf_pos_scan ) ;
                
                
%                 rec_seg = recContactSeg{1}{txtContactId} ;
%                 if ~strcmp(rec_seg, 'GreyMatter'), error( [ 'Invalid seg:' rec_seg ] ) ; end
%                 % this is done for future use
%                 rec_parc = recContactParc{1}{txtContactId} ;
% %                 rec_marsatlasInd = 0 ;
%                 try
%                     rec_marsatlas       = parseAtlasParcel( rec_parc, 'MarsAtlas' ) ;
%                     rec_marsatlasInd    = find( cellfun( @(x) strcmp(x,rec_marsatlas), MarsAtlasParcelsName ) ) ;
%                 catch ME
%                     if strcmp( ME.identifier, 'MATLAB:AtlasNotFound' ) || strcmp( ME.identifier, 'MATLAB:ParcelNotDefined' )
% %                         fprintf( 1, [ 'Rec ' ME.message '\n' ] ) ;
%     %                     continue
%                         rec_marsatlasInd = 0 ;
%                     else
%                         rethrow(ME)
%                     end
% %                     rec_marsatlas = ME.message ;
%                     
%                 end
                
                
                ccep                = struct ;
                ccep.contact        = contact ;
%                 ccep.pos            = sscanf(rec_pos,'[%f, %f, %f]') ;
%                 ccep.pos_mni        = sscanf(rec_pos_mni,'[%f, %f, %f]') ;
%                 ccep.pos_scan       = sscanf(rec_pos_scan,'[%f, %f, %f]') ;
                
                stim_ccep_name      = strjoin( sort( { stim.contact ccep.contact } ), ':' ) ;
                ccep.stim_ccep_dist = crf_distances{ strcmp( crf_distances(:,1), stim_ccep_name ), 2 } ;
                
                ccep.peakLat        = peakDelay( spmContactId, sigThreshold ) ;
                ccep.parc           = recContactParc{1}{txtContactId} ;
%                 ccep.marsatlasInd   = rec_marsatlasInd ;
                ccep.seg            = recContactSeg{1}{txtContactId} ;
                ccep.s1             = [ s1(cc).folder '/' s1(cc).name ] ;
                ccep.s2             = [ s2(cc).folder '/' s2(cc).name ] ;
                
                
                
                
                %% accuracy from DCM / step 1
                DCM                     = load( [ s1(cc).folder '/' s1(cc).name ] ) ;
                DCM                     = DCM.DCM ;
                [ ~, ~, ~, R2_c_m0 ] 	= compute_accuracy( DCM, [] ) ;
                ccep.s1_acc             = R2_c_m0 ;
                                
                % peak latency from DCM / step 1
                [~,locs]    = findpeaks(abs(DCM.H{1})) ;
                if ~isempty(locs)
                    s1_peak = DCM.xY.pst(locs(1)) ;
                else
                    s1_peak = nan ;
                end
                ccep.s1_peak   = s1_peak ;
                
                
                %% accuracy from DCM / step 2
                DCM                     = load( [ s2(cc).folder '/' s2(cc).name ] ) ;
                DCM                     = DCM.DCM ;
                [ ~, ~, ~, R2_c_m0 ] 	= compute_accuracy( DCM, [] ) ;
                ccep.s2_acc             = R2_c_m0 ;

                % peak latency from DCM / step 2
                [~,locs]    = findpeaks(abs(DCM.H{1})) ;
                if ~isempty(locs)
                    s2_peak = DCM.xY.pst(locs(1)) ;
                else
                    s2_peak = nan ;
                end
                ccep.s2_peak   = s2_peak ;

                
                % posteriors               
                [ cd, ste, sti ]        = get_posterior( DCM ) ;
                ccep.conductionDelay    = cd ;
                ccep.synapticTe         = ste ;
                ccep.synapticTi         = sti ;
                
       
                stim_cceps{cc}          = ccep ;
                
            end

%             continue
            
            stim.cceps    	= stim_cceps ;
            crf_stims{s}  	= stim ;
            
        end
        
        
        DCMs(nb_crf).stims  = crf_stims ;
        
    end
end

fname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1__DCMs.mat' ;
save( fname, 'DCMs') ;

end



% 
% function [ pos_mni, pos_scan ] = getPosition( contactName, previousPos, crf_pos_mni, crf_pos_scan )
% 
% pos_mni    = find( cellfun( @(pos) startsWith( pos, contactName ), crf_pos_mni ) ) ;
% pos_scan   = find( cellfun( @(pos) startsWith( pos, contactName ), crf_pos_scan ) ) ;
% if length(pos_mni) ~= 1 || ~isequal( pos_mni, pos_scan )
%     error( 'Invalid position' )
% end
% % check new mni positions is compatible with previous one
% tmp = strsplit( crf_pos_mni{pos_mni} ) ;
% if ~isequal( tmp{1}, contactName ), error( 'Incompatible contact name' ) ; end
% tmp = tmp(2:end) ;
% pos_mni = [ '[' strjoin( tmp, ', ' ) ']' ] ;
% if ~isequal( previousPos, pos_mni )
%     error( 'Incompatible mni position' )
% end
% % get scan position
% tmp = strsplit( crf_pos_scan{pos_scan} ) ;
% if ~isequal( tmp{1}, contactName ), error( 'Incompatible contact name' ) ; end
% tmp = tmp(2:end) ;
% pos_scan = [ '[' strjoin( tmp, ', ' ) ']' ] ;
% 
% end








