function cd_te = priors

cd_te = [] ;
for pl=1:80
    [ cd, te ] = cd_te_from_pl( pl ) ;
    fprintf( 1, [ 'pl: ' num2str(pl) ' cd: ' num2str(cd) ' te: ' num2str(te) '\n' ] ) ;
    cd_te = vertcat( cd_te, [ pl cd te ] ) ;
end

cd_te = unique( cd_te, 'rows' ) ;

end


function [ cd, te ] = cd_te_from_pl( pl )

if pl <= 10
    cd = 1 ;
else
    cd = pl / 2 ;
end

if pl <= 18
    te = 1 ;
elseif pl <= 26
    te = 2 ;
elseif pl <= 30
    te = 3 ;
elseif pl <= 36
    te = 4 ;
elseif pl <= 42
    te = 5 ;
elseif pl <= 48
    te = 6 ;
elseif pl <= 54
    te = 7 ;
else
    te = 8 ;
end

end
