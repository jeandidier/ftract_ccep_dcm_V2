


DCMs = loadDCMs ;



findGoodFit( DCMs ) / plotGoodFit




% plot distributions of peak latencies, conduction and synaptic delays (no
% parcellation)

% + stats sur nb de stims, cceps (sans parcellation)
% total: 318 crfs
%        9229 stims
%        90460 cceps
% en appliquant les criteres (sur les data)
%        peakLatMin  = 4 ;
%        => 85620 cceps 98%
% en appliquant les criteres (sur les fit)
%        accMin      = 70 ;
%        => 72994 cceps 80%
%        peakDiffMax = 5 ;
%        => 67559 cceps 75%

analyzeDCMs( DCMs )
% => fig2: distrib peak lat + example de fits
% => fig3: distrib des cond et synaptic delays


% plot distributions wrt parcellation: peakLat, cd, distance, velocity, and
% synapticTe/Ti
bd      = baseDir();
fname   = fullfile( bd, 'dcm/PATS__V1__DCMs.mat') ;
DCMs    = load(fname) ;
DCMs    = DCMs.DCMs ;

atlasId = 'Lausanne2008-60' ;
buildDCMsAtlas( DCMs, atlasId, 15, 100, 0 ) ;
buildDCMsAtlas( DCMs, atlasId, 15, 100, 1 ) ;
buildDCMsAtlas( DCMs, atlasId, 0, 15, 0 ) ;
buildDCMsAtlas( DCMs, atlasId, 0, 15, 1 ) ;

atlasId = 'HCP-MMP1' ;
buildDCMsAtlas( DCMs, atlasId, 15, 100, 0 ) ;


% => fig4: matrix of cond delays : symmetric version with Lausanne60
% => fig5: histo of velocities of cond delays
    + compare_age.m pour child versus adult


% => fig6: histo of synTe




createTextures.m




