function [ parcelName, parcelId ] = parseAtlasParcel( py_dict, atlasId, symmetric )
% example: atlasParcel( py_dic, 'MarsAtlas' )

if ~ ( isequal( py_dict(1), '{' ) && isequal( py_dict(end), '}' ) )
    error( 'Unrecognized token' )
end

atlasId     = strrep( atlasId, 'Brodmann', 'Broadmann' ) ;

parcels     = strsplit( py_dict(2:end-1), ', ' ) ;
atlas_ind   = find( startsWith( parcels, [ '"' atlasId '"' ] ) ) ;

if length(atlas_ind) ~= 1
    errorStruct             = struct ;
    errorStruct.message     = [ 'atlas ' atlasId ' not found in ' py_dict ] ;
    errorStruct.identifier  = 'MATLAB:AtlasNotFound' ;
    error( errorStruct ) ;
end

tmp         = strsplit( parcels{ 1, atlas_ind }, ': ' ) ;
parcelName  = tmp{1,2} ;
parcelName  = parcelName(1,2:end-1) ; % remove trailing "

if ( strcmp( atlasId, 'MarsAtlas' ) && ( strcmp( parcelName, 'not in a mars atlas parcel' ) || strcmp( parcelName, 'not calculated' ) ) ) || ...
   ( strcmp( atlasId, 'AALDilate' ) && ( strcmp( parcelName, 'not in a AALDilate parcel' ) ) ) || ...
   ( strcmp( atlasId, 'BroadmannDilate' ) && ( strcmp( parcelName, 'not in a Broadmann parcel' ) || strcmp(parcelName,'-25.0') || strcmp(parcelName,'-30.0') ) ) || ...
   ( strcmp( atlasId, 'Broadmann' ) && ( strcmp( parcelName, 'not in a Broadmann parcel' ) ) ) || ...
   ( strcmp( atlasId, 'HCP-MMP1' ) && ( strcmp( parcelName, 'not in a HCP-MMP1 parcel' ) ) ) || ...
   ( contains( atlasId, 'Lausanne2008' ) && ( strcmp( parcelName, 'not in a lausanne2008 parcel' ) ) )
        errorStruct             = struct ;
        errorStruct.message     = [ 'parcel ''' parcelName ''' not defined in atlas ' atlasId ] ;
        errorStruct.identifier  = 'MATLAB:ParcelNotDefined' ;
        error( errorStruct ) ;
end

atlasId             = strrep( atlasId, 'Broadmann', 'Brodmann' ) ;


% [ parcelsName, ~ ]  = atlasParcels_NameTexture(atlasId,symmetric) ;
[ parcelsName, ~ ] = getParcelsName( atlasId, symmetric ) ;
% if strcmp(atlasId,'Lausanne2008-60') && symmetric
% end
parcelId            = find( cellfun( @(x) strcmp( x, parcelName), parcelsName ) ) ;
if ~isequal( size(parcelId), [ 1 1 ] )
%     error( [ 'parcel ''' parcelName ''' not found in atlas ' atlasId ] )
    
    errorStruct             = struct ;
    errorStruct.message     = [ 'parcel ''' parcelName ''' not found in atlas ' atlasId ] ;
    errorStruct.identifier  = 'MATLAB:ParcelNotFound' ;
    error( errorStruct ) ;
    
end

end