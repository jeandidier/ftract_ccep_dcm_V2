function plot_distributions( conductionDelay, peakLatency, synapticTe, synapticTi, id, saveFigures )


% if saveFigures
% [ ~, dcmDir, ~ ]	= simuDirs() ;
% end


figD    = fullfile( figDir(), 'fig3' ) ;
system( [ 'mkdir -p ' figD ] ) ;



H = figure( 'Position', [1350 780 350 124] )
h = histogram( conductionDelay, 0:2.5:40 ) ;
h.FaceColor=[0.5 0.5 0.5] ;
xlim([0 40]) ;
ylim([0 10000]) ;
xlabel('conduction delays (ms)');

if saveFigures
    fname = [ figD '/histo_CD.txt' ] ;
    fprintf( 1, [ 'Save ' fname '\n' ] ) ;
    fd    = fopen( fname, 'wt' ) ;
    fprintf( fd, '%f ', h.Values ) ;
    fclose( fd ) ;
%     fname = [ dcmDir '/../screenshots/histo_CD_' strrep(id,' ','_') '.png' ] ;
%     fprintf( 1, [ 'Save ' fname '\n' ] ) ;
%     saveas( H, fname ) ;
    fname = [ figD '/fig3_2.svg' ] ;
    fprintf( 1, [ 'Save ' fname '\n' ] ) ;
    saveas( H, fname ) ;
end


H = figure( 'Position', [1706 780 350 124] ) ;
h = histogram( synapticTe, 0:1:20 ) ;
h.FaceColor=[0.5 0.5 0.5] ;
xlim([0 20]) ; 
ylim([0 10000]) ; 
% ylim([0 10000]) ; 
xlabel('synaptic excitatory delays (ms)'); 

if saveFigures
    fname = [ figD '/histo_SE.txt' ] ;
    fprintf( 1, [ 'Save ' fname '\n' ] ) ;
    fd    = fopen( fname, 'wt' ) ;
    fprintf( fd, '%f ', h.Values ) ;
    fclose( fd ) ;
%     fname = [ dcmDir '/../screenshots/histo_TE_' strrep(id,' ','_') '.png' ] ;
%     fprintf( 1, [ 'Save ' fname '\n' ] ) ;
%     saveas( H, fname ) ;
    fname = [ figD '/fig3_3.svg' ] ;
    fprintf( 1, [ 'Save ' fname '\n' ] ) ;
    saveas( H, fname ) ;
end


H = figure( 'Position', [2058 780 350 124] ) ;
h = histogram( synapticTi, 0:1:20 ) ;
h.FaceColor=[0.5 0.5 0.5] ;
xlim([0 20]) ; 
ylim([0 10000]) ; 
xlabel('synaptic inhibitory delays (ms)'); 

if saveFigures
    fname = [ figD '/histo_SI.txt' ] ;
    fprintf( 1, [ 'Save ' fname '\n' ] ) ;
    fd    = fopen( fname, 'wt' ) ;
    fprintf( fd, '%f ', h.Values ) ;
    fclose( fd ) ;
%     fname = [ dcmDir '/../screenshots/histo_TI_' strrep(id,' ','_') '.png' ] ;
%     fprintf( 1, [ 'Save ' fname '\n' ] ) ;
%     saveas( H, fname ) ;
    fname = [ figD '/fig3_4.svg' ] ;
    fprintf( 1, [ 'Save ' fname '\n' ] ) ;
    saveas( H, fname ) ;
end


% nanclr = [ 1 1 1 0 ] ;
% cm = [ nanclr; colormap ] ;
% colormap(cm)


H = figure( 'Position', [1350 900 350 500] ) ;
% subplot(1,3,1);
h2 = histogram2( conductionDelay, peakLatency, 0:2.5:40, 0:4:80, 'DisplayStyle', 'tile', 'FaceColor', 'flat' ) ;
xlim([0 40]) ; ylim([0 80]) ; 
yticks([])
% yticks(0:8:80)
% colorbar
caxis([0 300]) ;
% xlabel('conduction delays (ms)'); ylabel('peak latencies (ms)');
if saveFigures
    fname = [ figD '/fig3_A.svg' ] ;
    fprintf( 1, [ 'Save ' fname '\n' ] ) ;
    saveas( H, fname ) ;
end


H = figure( 'Position', [1706 900 350 500] ) ;
% subplot(1,3,2);
h2 = histogram2( synapticTe, peakLatency, 0:1:20, 0:4:80, 'DisplayStyle', 'tile', 'FaceColor', 'flat' ) ;
xlim([0 20]) ; ylim([0 80]) ; 
yticks([])
% yticks(0:8:80)
% colorbar
caxis([0 300]) ;
% xlabel('synaptic excitatory delays (ms)'); ylabel('peak latencies (ms)');
if saveFigures
    fname = [ figD '/fig3_B.svg' ] ;
    fprintf( 1, [ 'Save ' fname '\n' ] ) ;
    saveas( H, fname ) ;
end


H = figure( 'Position', [2058 900 350 500] ) ;
% subplot(1,3,3);
h2 = histogram2( synapticTi, peakLatency, 0:1:20, 0:4:80, 'DisplayStyle', 'tile', 'FaceColor', 'flat' ) ;
xlim([0 20]) ; ylim([0 80]) ; 
yticks([])
% yticks(0:8:80)
% colorbar
caxis([0 300]) ;
% xlabel('synaptic inhibitory delays (ms)'); ylabel('peak latencies (ms)');
if saveFigures
    fname = [ figD '/fig3_C.svg' ] ;
    fprintf( 1, [ 'Save ' fname '\n' ] ) ;
    saveas( H, fname ) ;
end

% if saveFigures
%     fname = [ dcmDir '/../screenshots/cd_te_ti_wrt_PL_' strrep(id,' ','_') '.png' ] ;
%     fprintf( 1, [ 'Save ' fname '\n' ] ) ;
%     saveas( H, fname ) ;
%     fname = [ dcmDir '/../screenshots/cd_te_ti_wrt_PL_' strrep(id,' ','_') '.svg' ] ;
%     fprintf( 1, [ 'Save ' fname '\n' ] ) ;
%     saveas( H, fname ) ;
% end


% for colorbar
H = figure( 'Position', [2265 900 300 500] ) ;
colorbar
caxis([0 300]) ;
if saveFigures
    fname = [ figD '/fig3_D.svg' ] ;
    fprintf( 1, [ 'Save ' fname '\n' ] ) ;
    saveas( H, fname ) ;
end



H = figure( 'Position', [1200 900 183 500] ) ;
h = histogram( peakLatency, 0:4:80, 'Orientation', 'horizontal' ) ; 
h.FaceColor=[0.5 0.5 0.5] ;
xticks(0:3000:10000); xlim([0 12000]);
yticks(0:8:80); ylim([0 80]) ; 

ylabel('peak latencies (ms)'); 
set(gca,'XDir','reverse')

if saveFigures
%     fname = [ dcmDir '/../screenshots/histo_PL_' strrep(id,' ','_') '.png' ] ;
%     fprintf( 1, [ 'Save ' fname '\n' ] ) ;
%     saveas( H, fname ) ;
    fname = [ figD '/fig3_1.svg' ] ;
    fprintf( 1, [ 'Save ' fname '\n' ] ) ;
    saveas( H, fname ) ;
end



% % conduction delay versus synaptic Te
% H = figure( 'Position', [1286 783 351 493] ) ;
% h = histogram2( conductionDelay, synapticTe, 0:2.5:40, 0:1:20, 'DisplayStyle', 'tile', 'FaceColor', 'flat' ) ;
% xlabel('conduction delays (ms)'); 
% ylabel('synaptic excitatory delays (ms)'); 
% caxis([0 300])
% colorbar
% suptitle( id ) ;
% 
% if saveFigures
%     fname = [ dcmDir '/../screenshots/cd_wrt_te_' strrep(id,' ','_') '.png' ] ;
%     fprintf( 1, [ 'Save ' fname '\n' ] ) ;
%     saveas( H, fname ) ;
%     fname = [ dcmDir '/../screenshots/cd_wrt_te_' strrep(id,' ','_') '.svg' ] ;
%     fprintf( 1, [ 'Save ' fname '\n' ] ) ;
%     saveas( H, fname ) ;
% end



end


