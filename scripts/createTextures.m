function createTextures( atlasId, ageMin, ageMax, symmetric, cceps_min )
% atlasId='MarsAtlas';
% atlasId='HCP-MMP1';
% atlasId='Lausanne2008-33';
% atlasId='Lausanne2008-60';
% atlasId='Lausanne2008-125';
% atlasId='Lausanne2008-250';
% atlasId='Lausanne2008-500';

% symmetric   = false ;


if strcmp( atlasId, 'MarsAtlas' )
    createMarsAtlasTextures( ageMin, ageMax, symmetric ) ;
end


bd          = baseDir() ;


if symmetric && strcmp( atlasId, 'Lausanne2008-60' )
    fname       = fullfile( bd, 'scripts/parcellations/anatomy_sorted', [ atlasId '-sym.txt' ] ) ;
    parcelsSym  = fileread(fname);
    parcelsSym  = strtrim(parcelsSym);
    parcelsSym  = strsplit(parcelsSym,'\n');
    parcelsSym( strcmp( parcelsSym, 'Brain-Stem' ) ) = [] ;
    parcelsSym( strcmp( parcelsSym, 'Unknown' ) )    = [] ;
    parcelsName = parcelsSym ;
else
    parcelsName = getParcelsName( atlasId, symmetric ) ;
end


% fname       = fullfile( bd, 'dcm', [ atlasId '_' num2str(ageMin) '-' num2str(ageMax) ], [ 'sym' num2str(symmetric) ], 'atlas.mat' ) ;
fname = fullfile( bd, 'dcm', [ atlasId '_' num2str(ageMin) '-' num2str(ageMax) ], [ 'sym' num2str(symmetric) ], [ 'cceps_min_' num2str(cceps_min) ], 'atlas.mat' ) ;
atlas       = load(fname);
atlas       = atlas.atlas ;


% conduction delays
textureOutputDir = fullfile( fileparts(fname), 'conductionDelays' );
system( [ 'mkdir -p ' textureOutputDir ] );

if symmetric && strcmp( atlasId, 'Lausanne2008-60' )
    
    parcelsSym = parcelsName ;
    H = length(parcelsSym) ;
    for s = 1:H
        stimParcels     = strsplit(parcelsSym{s},'\t\t') ;
        stimParcels_L   = strrep(stimParcels{1},' ','__') ;
%         stimParcels_R   = strrep(stimParcels{2},' ','__') ;
        
        fname = fullfile( textureOutputDir, [ stimParcels_L '.txt' ] ) ;
        fprintf( 1, [ '>> ' fname '\n' ] ) ;
        fd = fopen( fname, 'w' ) ;
        for r=1:H
            recParcels      = strsplit(parcelsSym{r},'\t\t') ;
            recParcels_L    = strrep(recParcels{1},' ','__') ;
            recParcels_R    = strrep(recParcels{2},' ','__') ;
            
            % ipsi
            fprintf( fd, '%s %f\n', recParcels_L, atlas.median_conduction_delays_parcels(s,r) ) ;
            % contra
            fprintf( fd, '%s %f\n', recParcels_R, atlas.median_conduction_delays_parcels(s,H+r) ) ;
        end
        fclose(fd);
    end
    return
    
else
    for s=1:length(parcelsName)
        fd = fopen( fullfile( textureOutputDir, [ parcelsName{s} '.txt' ] ), 'w' ) ;
        for r=1:length(parcelsName)
            fprintf( fd, '%s %f\n', parcelsName{r}, atlas.median_conduction_delays_parcels(s,r) ) ;
        end
        fclose(fd);
        if symmetric && ( s == length(parcelsName)/2 )
            break
        end
    end
end


% synaptic Te
textureOutputDir = fullfile( fileparts(fname), 'synapticTe' );
system( [ 'mkdir -p ' textureOutputDir ] );
% createTexture( atlas.median_synapticTe_parcels, textureOutputDir, 'synapticTe', symmetric ) ;
fd = fopen( fullfile( textureOutputDir, 'synapticTe.txt' ), 'w' ) ;
for r=1:length(parcelsName)
    fprintf( fd, '%s %f\n', parcelsName{r}, atlas.median_synapticTe_parcels(r) ) ;
    if symmetric && ( r == length(parcelsName)/2 )
        break
    end
end
fclose( fd ) ;


% synaptic Ti
textureOutputDir = fullfile( fileparts(fname), 'synapticTi' );
system( [ 'mkdir -p ' textureOutputDir ] );
% createTexture( atlas.median_synapticTi_parcels, textureOutputDir, 'synapticTi', symmetric ) ;
fd = fopen( fullfile( textureOutputDir, 'synapticTi.txt' ), 'w' ) ;
for r=1:length(parcelsName)
    fprintf( fd, '%s %f\n', parcelsName{r}, atlas.median_synapticTi_parcels(r) ) ;
    if symmetric && ( r == length(parcelsName)/2 )
        break
    end
end
fclose( fd ) ;



% stimulating density
if ~symmetric
    stiDensityOutputDir = fullfile( fileparts(fname), 'stiDensity' );
    system( [ 'mkdir -p ' stiDensityOutputDir ] );
    fd = fopen( fullfile( stiDensityOutputDir, 'stiDensity.txt' ), 'w' ) ;
    for s=1:length(parcelsName)
        fprintf( fd, '%s %d\n', parcelsName{s}, length( horzcat( atlas.peakLatency_parcels{s,:} ) ) ) ;
    end
    fclose( fd ) ;
end


% recording density
if ~symmetric
    recDensityOutputDir = fullfile( fileparts(fname), 'recDensity' );
    system( [ 'mkdir -p ' recDensityOutputDir ] );
    fd = fopen( fullfile( recDensityOutputDir, 'recDensity.txt' ), 'w' ) ;
    for r=1:length(parcelsName)
        
        fprintf( fd, '%s %d\n', parcelsName{r}, length( horzcat( atlas.peakLatency_parcels{:,r} ) ) ) ;
    end
    fclose( fd ) ;
end



end