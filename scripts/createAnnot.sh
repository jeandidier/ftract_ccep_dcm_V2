#! /bin/bash

module load FreeSurfer/6.0.0

gcs_dir="/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/scripts/cmp_nipype-master/cmtklib/data/colortable_and_gcs/my_atlas_gcs"
annot_dir="/home/jeandidier.lemarecha/mne_data/MNE-sample-data/subjects/fsaverage/label"

for resol in 36 60 125 250
do

for hemi in lh rh
do
mris_ca_label fsaverage ${hemi} sphere.reg ${gcs_dir}/myatlas_${resol}_${hemi}.gcs ${annot_dir}/${hemi}.Lausanne_${resol}.annot
done

done


