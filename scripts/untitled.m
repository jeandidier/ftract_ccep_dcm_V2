function findGoodFit( DCMs )

if isempty( DCMs )
    bd      = baseDir();
    fname   = fullfile( bd, 'dcm/PATS__V1__DCMs.mat') ;
    DCMs    = load(fname) ;
    DCMs    = DCMs.DCMs ;
end


% [ 10 15 ]
% dcmFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0001NAN/SEEG/StimLF_2015-08-31/SEQ_BM_ACPCHIP_F_DCM/W16-W17_1mA_1Hz_1050us_1/step2/DCM_averaged_zs_W16-W17_1mA_1Hz_1050us_1_W14-W15.mat' ;
% dcmFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0001NAN/SEEG/StimLF_2015-08-31/SEQ_BM_ACPCHIP_F_DCM/TMp09-TMp10_1mA_1Hz_1050us_1/step2/DCM_averaged_zs_TMp09-TMp10_1mA_1Hz_1050us_1_A04-A05.mat';
% dcmFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0001NAN/SEEG/StimLF_2015-08-31/SEQ_BM_ACPCHIP_F_DCM/B02-B03_1mA_1Hz_1050us_1/step2/DCM_averaged_zs_B02-B03_1mA_1Hz_1050us_1_C04-C05.mat';
% dcmFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0001NAN/SEEG/StimLF_2015-08-31/SEQ_BM_ACPCHIP_F_DCM/H02-H03_1mA_1Hz_1050us_1/step2/DCM_averaged_zs_H02-H03_1mA_1Hz_1050us_1_H04-H05.mat';
% dcmFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0001NAN/SEEG/StimLF_2015-08-31/SEQ_BM_ACPCHIP_F_DCM/TB02-TB03_1mA_1Hz_1050us_1/step2/DCM_averaged_zs_TB02-TB03_1mA_1Hz_1050us_1_TB09-TB10.mat';
% dcmFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0001NAN/SEEG/StimLF_2015-08-31/SEQ_BM_ACPCHIP_F_DCM/TB02-TB03_1mA_1Hz_1050us_1/step2/DCM_averaged_zs_TB02-TB03_1mA_1Hz_1050us_1_TM03-TM04.mat';


% [ 25 30 ]

% 40 45
% ok: dcmFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0001GRE/SEEG/StimLF_2014-11-25/SEQ_BM_ACPCHIP_F_DCM/Ip03-Ip04_3mA_1Hz_1000us_1/step2/DCM_averaged_zs_Ip03-Ip04_3mA_1Hz_1000us_1_ETp01-ETp02.mat' ;


interval = [ 60 70 ] ;
max_peak_dist = 2 ;
min_acc = 90 ;

for c=1:length(DCMs)
    for s=1:length(DCMs(c).stims)
        for d=1:length(DCMs(c).stims{s}.cceps)
            if interval(1) < DCMs(c).stims{s}.cceps(d).peakLat && ...
                    DCMs(c).stims{s}.cceps(d).peakLat < interval(2)  && ...
                    abs( DCMs(c).stims{s}.cceps(d).peakLat - DCMs(c).stims{s}.cceps(d).s2_peak ) < max_peak_dist && ...
                    DCMs(c).stims{s}.cceps(d).s2_acc > min_acc
%                 ccep_fname = strrep( DCMs(c).stims{s}.cceps(d).s2, 'step2/DCM_', 'cceps/' ) ;
%                 plotData( ccep_fname )
            
%                 dcmFname = 
                plotFit( dcmFname ) ;
            end
        end
    end
end



