function analyzeDCMs( DCMs )


bd      = baseDir();
    


if isempty( DCMs )
    fname   = fullfile( bd, 'dcm/PATS__V1__DCMs.mat') ;
    DCMs    = load(fname) ;
    DCMs    = DCMs.DCMs ;
end



crf_ages        = arrayfun( @(dcm) dcm.stims{1}.age, DCMs ) ;
young           = length(find(crf_ages<15));
old             = length(find(crf_ages>15));



age             = cell( size(DCMs) ) ;
peakLat         = cell( size(DCMs) ) ;
s1_acc          = cell( size(DCMs) ) ;
s1_peak         = cell( size(DCMs) ) ;
s2_acc          = cell( size(DCMs) ) ;
s2_peak         = cell( size(DCMs) ) ;
conductionDelay = cell( size(DCMs) ) ;
synapticTe      = cell( size(DCMs) ) ;
synapticTi      = cell( size(DCMs) ) ;


parfor crf=1:length(DCMs)
% for crf=1:length(DCMs)
    
    c_age               = [] ;
    c_peakLat           = [] ;
    c_s1_acc            = [] ;
    c_s1_peak           = [] ;
    c_s2_acc            = [] ;
    c_s2_peak           = [] ;
    c_conductionDelay   = [] ;
    c_synapticTe        = [] ;
    c_synapticTi        = [] ;
    
    for s=1:length(DCMs(crf).stims)
%         s
        % maybe not yet done
%         if isempty(DCMs(crf).stims{s})
%             continue
%         end
    
        tmp                 = repmat( DCMs(crf).stims{s}.age, size(DCMs(crf).stims{s}.cceps) ) ;
        c_age               = vertcat( c_age, tmp ) ;
        
        tmp                 = cellfun( @(ccep) ccep.peakLat, DCMs(crf).stims{s}.cceps ) ;
        c_peakLat           = vertcat( c_peakLat, tmp ) ;
        
        tmp                 = cellfun( @(ccep) full(ccep.s1_acc), DCMs(crf).stims{s}.cceps );
        c_s1_acc            = vertcat( c_s1_acc, tmp ) ;
        
        tmp                 = cellfun( @(ccep) ccep.s1_peak, DCMs(crf).stims{s}.cceps ) ;
        c_s1_peak           = vertcat( c_s1_peak, tmp ) ;
        
        tmp                 = cellfun( @(ccep) full(ccep.s2_acc), DCMs(crf).stims{s}.cceps );
        c_s2_acc            = vertcat( c_s2_acc, tmp ) ;
        
        tmp                 = cellfun( @(ccep) ccep.s2_peak, DCMs(crf).stims{s}.cceps ) ;
        c_s2_peak           = vertcat( c_s2_peak, tmp ) ;
        
        tmp                 = cellfun( @(ccep) ccep.conductionDelay, DCMs(crf).stims{s}.cceps ) ;
        c_conductionDelay   = vertcat( c_conductionDelay, tmp ) ;
        
        tmp                 = cellfun( @(ccep) ccep.synapticTe, DCMs(crf).stims{s}.cceps ) ;
        c_synapticTe        = vertcat( c_synapticTe, tmp ) ;
        
        tmp                 = cellfun( @(ccep) ccep.synapticTi, DCMs(crf).stims{s}.cceps ) ;
        c_synapticTi        = vertcat( c_synapticTi, tmp ) ;
        
    end
    
    age{crf}                = c_age ;
    peakLat{crf}            = c_peakLat ;
    s1_acc{crf}             = c_s1_acc ;
    s1_peak{crf}            = c_s1_peak ;
    s2_acc{crf}             = c_s2_acc ;
    s2_peak{crf}            = c_s2_peak ;
    conductionDelay{crf}    = c_conductionDelay ;
    synapticTe{crf}         = c_synapticTe ;
    synapticTi{crf}         = c_synapticTi ;
    
end


age             = vertcat( age{:} ) ;
peakLat         = vertcat( peakLat{:} ) ;
s1_acc          = vertcat( s1_acc{:} ) ;
s1_peak         = vertcat( s1_peak{:} ) ;
s2_acc          = vertcat( s2_acc{:} ) ;
s2_peak         = vertcat( s2_peak{:} ) ;
conductionDelay = vertcat( conductionDelay{:} ) ;
synapticTe      = vertcat( synapticTe{:} ) ;
synapticTi      = vertcat( synapticTi{:} ) ;






%% nb de crf, stim, ccep (sans condition)
% nb de patients
tmp = arrayfun( @(crf) fileparts( fileparts( crf.crfDir ) ), DCMs, 'UniformOutput', false ) ;
tmp = unique( tmp ) ;
length(tmp)

% nb crfs
length(DCMs)
% nb de stims
sum( arrayfun( @(d) length(d.stims), DCMs ) )
% nb de cceps
cceps=0;
for crf=1:length(DCMs)
    for s=1:length(DCMs(crf).stims)
        cceps=cceps+length(DCMs(crf).stims{s}.cceps);
    end
end
cceps




%% plot gof wrt peak lat



% plot_histo = false ;
plot_histo = true ;
if plot_histo
    
    bin=5;
        
    s1_gof_bin      = nan( 1, length(0:bin:80) - 1 ) ;
    s2_gof_bin      = nan( 1, length(0:bin:80) - 1 ) ;
    
%     gof_std = [] ;
    b = 1 ;
    for pl=0:bin:80
        if pl == 80, break ; end
        id      = find( peakLat>=pl & peakLat<=pl+bin ) ;
        %     id_good = find( peakLat>=pl & peakLat<=pl+bin & (s2_acc>accMin) & abs(peakLat-s2_peak)<peakDiffMax ) ;
        %     id_bad  = setdiff( id, id_good ) ;
        
        %     id_gof  = find( peakLat>=pl & peakLat<=pl+bin & (s2_acc>accMin) ) ;
        %     gof     = [ gof 100/length(id)*length(id_gof) ] ;
        
        s1_gof_bin(b)   = mean(s1_acc(id));
        s2_gof_bin(b)   = mean(s2_acc(id));
        
        %         gof     = [ gof mean(s2_acc(id)) ] ;
%         gof_std = [ gof_std std(s2_acc(id)) ] ;
        b = b + 1 ;
    end
    
    
    
    H = figure( 'Position', [2137 991 365 315] ) ;
    
    histogram( peakLat, 0:bin:80 ) ;
    hold on ;
    
    ylabel('number of cceps');
    xlabel('peak latency (ms)');
    
    % set(gca,'LineWidth',2);
    % set(gca,'FontWeight','bold');
    
    yticks( 0:2000:14000 ) ;
    ya = get( gca, 'YAxis' ) ;
    ya.Exponent = 3 ;
%     yticklabels( arrayfun( @(i) num2str(i), 0:2:14, 'UniformOutput', false) )
    
    yyaxis right
    plot( 0+bin/2:bin:80, s2_gof_bin,'LineWidth',2 ) ;
    plot( 0+bin/2:bin:80, s1_gof_bin,'LineWidth',2 ) ;
    
    
    ylim([0 100]);
    % ylim([0 110]);
    
    
    % set(gca,'LineWidth',2);
    % set(gca,'FontWeight','bold');
    
    ylabel( 'goodness of fit (%)' ) ;
    yticks( 0:20:100 ) ;
    yticklabels( arrayfun( @(i) num2str(i), 0:20:100, 'UniformOutput', false ) );
    %
    % line( get(gca,'XLim')', [ 70 70 ]', 'Color', 'k', 'LineStyle', '--' ) ;
    
    figD  = fullfile( figDir(), 'fig2' ) ;
    saveas( H, fullfile( figD, 'fig2_E.svg' ) ) ;
        
end



%% some additionnal stats
accMin      = 70 ;
peakDiffMax = 5 ;



fd = fopen( fullfile( figD, 'fig2_E.txt' ), 'w' ) ;
fprintf( fd, [ 'Pass1: mean gof = ' num2str(mean(s1_acc)) '%% and mean diff peak = ' num2str(mean(abs(peakLat-s1_peak), 'omitnan')) 'ms \n' ] ) ;
s1_good = (s1_acc>accMin) & (abs(peakLat-s1_peak)<peakDiffMax) ;
fprintf( fd, [ ' => after pass1 (gof>' num2str(accMin) '%% and diff(peak)<' num2str(peakDiffMax) 'ms ): ' num2str(length(find(s1_good))) ' cceps => ' num2str(100/length(s1_good)*length(find(s1_good))) '%%\n' ] ) ;

fprintf( fd, [ 'Pass2: mean gof = ' num2str(mean(s2_acc)) '%% and mean diff peak = ' num2str(mean(abs(peakLat-s2_peak), 'omitnan')) 'ms \n' ] ) ;
s2_good = (s2_acc>accMin) & (abs(peakLat-s2_peak)<peakDiffMax) ;
fprintf( fd, [ ' => after pass2 (gof>' num2str(accMin) '%% and diff(peak)<' num2str(peakDiffMax) 'ms ): ' num2str(length(find(s2_good))) ' cceps => ' num2str(100/length(s2_good)*length(find(s2_good))) '%%\n' ] ) ;

fclose(fd);






% 
% ageMin = 0 ;
% % ageMax = 14 ;
% % ageMin = 18 ;
% ageMax = 100 ;
% 
% 
% peakLatMin = 0 ;
% 
% 
% ind = (age>ageMin) & (age<ageMax) & (peakLat>peakLatMin) ;
% length(find(ind))
% 
% 
% 
% ind = (age>ageMin) & (age<ageMax) & (peakLat>peakLatMin) & (s2_acc>accMin);
% length(find(ind))
% 
% ind = (age>ageMin) & (age<ageMax) & (peakLat>peakLatMin) & (s2_acc>accMin) & abs(peakLat-s2_peak)<peakDiffMax ;
% length(find(ind))
% 
% ind = (s2_acc>accMin) & abs(peakLat-s2_peak)<peakDiffMax ;
% length(find(ind))
% length(s2_acc)
% 100/length(s2_acc)*length(find(ind))







id = 'None' ;
saveFigures = 1 ;
ind = s2_good ;
plot_distributions( conductionDelay(ind), peakLat(ind), synapticTe(ind), synapticTi(ind), id, saveFigures )




figD    = fullfile( figDir(), 'fig3' ) ;
fd = fopen( fullfile( figD, 'fig3.txt' ), 'w' ) ;
max_ax = 10 ;
p = 100/length(conductionDelay(ind))*length(find(conductionDelay(ind)<max_ax)) ;
fprintf( fd, [ 'ax delays below ' num2str(max_ax) 'ms: ' num2str(p) '%%\n' ] ) ;
fclose(fd);



end



