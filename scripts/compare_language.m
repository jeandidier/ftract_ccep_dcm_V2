function compare_language

bd = baseDir();
atlasId = 'Lausanne2008-60';
ageMin = 15 ;
ageMax = 100 ;
symmetric = false ;

fname = fullfile( bd, 'dcm', [ atlasId '_' num2str(ageMin) '-' num2str(ageMax) ], [ 'sym' num2str(symmetric) ], 'atlas.mat' ) ;
atlas = load(fname);
atlas = atlas.atlas ;


parcelsName     = getParcelsName( atlasId, false ) ;


broca = 'lh.parsopercularis_1' ;
wernicke = 'lh.superiortemporal_1' ;

id_broca    = find( strcmp( parcelsName, broca ) ) ;
id_wernicke = find( strcmp( parcelsName, wernicke ) ) ;


broca_wernicke_peakLatencies    = atlas.peakLatency_parcels{ id_broca, id_wernicke } ;
wernicke_broca_peakLatencies    = atlas.peakLatency_parcels{ id_wernicke, id_broca } ;
[ h, p ] = ttest2( broca_wernicke_peakLatencies, wernicke_broca_peakLatencies )


broca_wernicke_conductionDelays = atlas.conduction_delays_parcels{ id_broca, id_wernicke } ;
wernicke_broca_conductionDelays = atlas.conduction_delays_parcels{ id_wernicke, id_broca } ;
[ h, p ] = ttest2( broca_wernicke_conductionDelays, wernicke_broca_conductionDelays )


end