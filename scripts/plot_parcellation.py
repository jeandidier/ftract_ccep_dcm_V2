# %matplotlib qt



from surfer import Brain
import mayavi.mlab as mlab

import mne
import os
import os.path as op
import glob

import numpy as np
import scipy.stats
import subprocess

import matplotlib.pyplot as plt



bd='/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2'

#ageMin=15
#ageMax=100

red=( 255, 0, 0 )
pink=( 255, 20, 147 )
grey=( 127, 127, 127 )



parcellationIds = [ 'HCP-MMP1', 'Lausanne2008-33', 'Lausanne2008-60', 'Lausanne2008-125', 'Lausanne2008-250', 'Lausanne2008-500' ]

subjects_dir    = mne.datasets.sample.data_path() + '/subjects'



# colormap from BrainVisa
def read_brainvisa_cm() :
    cm_fname = op.join( bd,'scripts/Blue-Red-fusion.ima')
    fd = open( cm_fname, 'r' )
    lines = fd.readlines()
    fd.close()
    blue_red_fusion_cm = []
    for l in lines[1:] :
        l = l.strip().strip('(').strip(')')
        (r,g,b) = [ float(c) for c in l.split(',') ]
        blue_red_fusion_cm.append( (r,g,b) )
    return blue_red_fusion_cm




# colormap from matlab
def read_matlab_cm():
    cm_fname=op.join( bd,'scripts/matlab_cm.txt')
    fd = open( cm_fname, 'r' )
    lines = fd.readlines()
    fd.close()
    matlab_cm = []
    for l in lines :
        l = l.strip()
        (r,g,b) = [ float(c) for c in l.split() ]
        matlab_cm.append( [ 255*c for c in (r,g,b) ] )
    return matlab_cm



def read_parcellation( parcellationId, symmetric ) :
    
    if symmetric :
        fname = op.join(bd,'scripts','parcellations','anatomy_sorted',parcellationId+'-sym.txt')
    else :
        fname = op.join(bd,'scripts','parcellations','anatomy_sorted',parcellationId+'.txt')

    fd = open( fname, 'r' )
    lines = fd.readlines()
    fd.close()
    
    return [ p.strip() for p in lines ]    



def create_symmetric_parcellation( parcellationId ) :
    

    if parcellationId == 'Lausanne2008-60' :
        parcellation = Lausanne60_symmetric()
        fname = op.join(bd,'scripts','parcellations','anatomy_sorted',parcellationId+'-sym.txt')
        print( '>>', fname )
        fd = open( fname, 'w' )
        for p in parcellation :
            if len(p) == 2 :
                fd.write( '\t\t'.join( [ ' '.join( p2 ) for p2 in p ] ) + '\n' )
            elif len(p) == 1 :
                fd.write( p[0] + '\n' )
            else :
                raise Exception( 'unexpected' )            


            #assert len(p) == 2 or len(p) == 1
            #fd.write( 

        

        #fd.write( '\n'.join( [ '\t'.join( [ ' '.join( p2 ) for p2 in p ] ) for p in parcellation ] ) )
        #fd.close()
        return





    fd = open( op.join(bd,'scripts','parcellations','anatomy_sorted',parcellationId+'.txt'), 'r' )
    lines = fd.readlines()
    fd.close()
    
    # tested for Lausanne2008-60
    if parcellationId == 'Lausanne2008-33' :
        lh_pre = 'ctx-lh-'
        rh_pre = 'ctx-rh-'
    else :
        raise Exception( 'Not implemented (actually not validated)' )
        lh_pre = 'lh.'
        rh_pre = 'rh.'


    lh = [ p.strip() for p in lines if p.startswith(lh_pre) ]
    rh = [ p.strip() for p in lines if p.startswith(rh_pre) ]
    Left = [ p.strip() for p in lines if p.startswith('Left-') ]
    Right = [ p.strip() for p in lines if p.startswith('Right-') ]

    assert np.all( np.sort(lh+rh+Left+Right+['Unknown','Brain-Stem']) == np.sort( [ l.strip() for l in lines ] ) )
    
    lh = [ p.replace(lh_pre,'') for p in lh ]
    rh = [ p.replace(rh_pre,'') for p in rh ]
    Left = [ p.replace('Left-','') for p in Left ]
    Right = [ p.replace('Right-','') for p in Right ]


    symmetric_ctx   = [ p for p in lh if p in rh ]
    non_symmetric_lh = [ p for p in lh if p not in rh ]
    non_symmetric_rh = [ p for p in rh if p not in lh ]
    symmetric       = [ p for p in Left if p in Right ]
    
    
    fname = op.join(bd,'scripts','parcellations','anatomy_sorted',parcellationId+'-sym.txt')
    fd = open( fname, 'w' )
    for p in symmetric_ctx :
        fd.write( lh_pre + p + '\n' )
    for p in symmetric :
        fd.write( 'Left-' + p + '\n' )
    for p in symmetric_ctx :
        fd.write( rh_pre + p + '\n' )
    for p in symmetric :
        fd.write( 'Right-' + p + '\n' )
    fd.close()


    print( '>>', fname )
    



# compute correlation of synaptic Te between hemi
def compute_correlation( parcellationId ) :
    
    textureId='synapticTe'

    # read DCM texture    
    ageMin=15
    ageMax=100
    textureTxt  = op.join( bd, 'dcm', parcellationId+'_'+str(ageMin)+'-'+str(ageMax), 'sym0', 'cceps_min_5', textureId, textureId+'.txt' )
    fd          = open(textureTxt,'r')
    lines       = fd.readlines()
    fd.close()    

    hemi_left = dict()
    hemi_right = dict()
    for l in lines :
        l=l.strip()
        p=l.split()[0]
        v=float(l.split()[1])

        if parcellationId in [ 'MarsAtlas', 'HCP-MMP1' ]:
            if p.startswith('L_') :
                p = p[2:]
                hemi_left[p] = v
            elif p.startswith('R_') :
                p = p[2:]
                hemi_right[p] = v

        

        elif parcellationId == 'Lausanne2008-33' :
            if p.startswith('ctx-lh') :
                p = p[7:]
                hemi_left[p] = v
            elif p.startswith('ctx-rh') :
                p = p[7:]
                hemi_right[p] = v
    
        elif parcellationId in [ 'Lausanne2008-60', 'Lausanne2008-125', 'Lausanne2008-250', 'Lausanne2008-500' ] :
            if p.startswith('lh.') :
                p = p[3:]
                hemi_left[p] = v
            elif p.startswith('rh.') :
                p = p[3:]
                hemi_right[p] = v

        


    common_parcels = [ k for k in hemi_left.keys() if k in hemi_right.keys() ]
    diff_parcels   = [ 'lh.' + k for k in hemi_left.keys() if not k in hemi_right.keys() ] + [ 'rh.' + k for k in hemi_right.keys() if not k in hemi_left.keys() ]


    lh=[]
    rh=[]
    for k in common_parcels :
        l=hemi_left[k]
        r=hemi_right[k]
        if not ( np.isnan(l) or np.isnan(r) ) :
            lh.append(l)
            rh.append(r)
    

    x = np.array( [ lh , rh ] ) 
    np.corrcoef( x )
    

    lh = np.asarray( lh )
    rh = np.asarray( rh )

    li=np.where( lh > 11 )[0]
    ri=np.where( rh > 11 )[0]
    i=np.append(li,ri)

    lh=np.delete(lh,i,0)
    rh=np.delete(rh,i,0)


    (r,p) = scipy.stats.pearsonr( lh, rh )
    print( parcellationId, 'r='+str(r), 'p='+str(p) )


    fig, ax = plt.subplots()
    plt.scatter( lh, rh )
    ax.set_xlabel( 'left hemisphere' )
    ax.set_ylabel( 'right hemisphere' )
    ax.set_title( 'synaptic excitatory delays (ms)' )
    #ax.axis('equal')
    ax.set_aspect('equal', 'box')
    plt.xlim([0, 10])
    plt.ylim([0, 10])
    plt.show()


    slope, intercept, rvalue, pvalue, _ = scipy.stats.linregress( lh, rh )


    x=np.linspace(0,10,num=100)
    y=intercept+slope*x
    plt.plot( x, y, color='red', linestyle='dashed' )



    fname = op.join( bd, 'figures_original', 'fig6', 'fig6_' + parcellationId + '_' + str(ageMin) + '-' + str(ageMax) + '_' + 'sym0_cceps_min_5_' + textureId + '_correlation.svg' )
    plt.savefig( fname )

    fname = op.join( bd, 'figures_original', 'fig6', 'fig6_' + parcellationId + '_' + str(ageMin) + '-' + str(ageMax) + '_' + 'sym0_cceps_min_5_' + textureId + '_correlation.txt' )
    fd = open( fname, 'w' )
    fd.write( '\n'.join( [ 'slope: ' + str(slope), 
                           'intercept: ' + str(intercept),
                           'rvalue: ' + str(rvalue),
                            'pvalue: ' + str(pvalue) ] ) )
    fd.close()





'''
plot conduction and synaptic delays for all parcellations

'''
def plot_all() :

    for parcellationId in parcellationIds :
        # conduction delays
        plot_parcellation_conductionDelays( parcellationId )

        # synapticTe
        plot_parcellation( parcellationId, 'synapticTe', 'synapticTe' )
        # synapticTi
        plot_parcellation( parcellationId, 'synapticTi', 'synapticTi' )

        



'''
 create visu for all parcels (conductionDelays)
'''
def plot_parcellation_conductionDelays( parcellationId, symmetric ) :
    
    textureId = 'conductionDelays'
    textureDir = op.join(bd,'dcm',parcellationId+'_'+str(ageMin)+'-'+str(ageMax),'sym'+str(symmetric),textureId)
    files=glob.glob(op.join(textureDir,'*.txt'))
    for i,f in enumerate(files,1) :
        print( 'parcel', i,'/',len(files) )
        print( f )
        #if parcellationId == 'Lausanne2008-60' :
        p=op.basename(f)
        plot_parcellation( parcellationId, 'conductionDelays', op.splitext(p)[0], symmetric )
        mlab.close(all=True)


'''
create visu for one parcel:
      synaptic: 
        textureId='synapticTe' 
        parcelId='synapticTe'

      conduction: 
        textureId='conductionDelays'
        parcelId='lh.insula_1'



Parcel for article

Lausanne33
conduction delay: 
                lh-inferiortemporal
                lh-fusiform
                lh-insula
                lh-lateralorbitofrontal
                rh-inferiortemporal
                rh-insula
                Left-Amygdala
                Right-Amydala
                Right-Hippocampus



Lausanne60
conduction delay:
                


'''


# size=(2000,1400) # taille pour figures article
# size=(800,600) # taille pour explorer
# zoom for figures article


# fig6 : plot_parcellation.plot_parcellation( 'HCP-MMP1', 'synapticTe', 'synapticTe', 0, size=(2000,1400), zoom=True )
def plot_parcellation( parcellationId, textureId, parcelId, symmetric, cceps_min, cm=read_matlab_cm(), size=(800,600), zoom=False, stim_red=False ) :

    # parcellationId='HCP-MMP1'             # 362
    # parcellationId='Lausanne2008-33'      # 72 labels
    # parcellationId='Lausanne2008-60'      # 118               
    # parcellationId='Lausanne2008-125'     # 223
    # parcellationId='Lausanne2008-250'     # 452
    

    # textureId='conductionDelays'
    # parcelId='rh.inferiorparietal_1'
 
    # textureId='synapticTe'
    # parcelId='synapticTe'


    if parcellationId == 'Lausanne2008-60' and symmetric :
        raise Exception( 'use conduction_delays_Lausanne60_symmetric.py instead' )
    

    ageMin = 15
    ageMax = 100
    parcellationName = parcellationId+'_'+str(ageMin)+'-'+str(ageMax)



    # mapping between FS and FTRACT parcellation names
    annotId = parcellationId2annotId( parcellationId )



    # FS parcellations
    annotLabels     = mne.read_labels_from_annot('fsaverage', annotId, 'both', subjects_dir=subjects_dir)


    textureDir  = op.join( bd, 'dcm', parcellationName, 'sym'+str(symmetric), 'cceps_min_'+str(cceps_min), textureId )

    # read DCM texture
    textureTxt  = op.join(textureDir,parcelId+'.txt')
    fd          = open(textureTxt,'r')
    lines       = fd.readlines()
    fd.close()


    textures = []
    stim_label = None
    for l in lines :

        l=l.strip()
        p=l.split()[0]
        v=float(l.split()[1])        

        label = getLabel( parcellationId, p, annotLabels )
        if not label :
            continue

        textures.append( (label,v) )

        if p == parcelId :
            stim_label = label
    


    
    texMax = np.nanmax( [ t[1] for t in textures ] )
    texMin = np.nanmin( [ t[1] for t in textures ] )
    if textureId == 'synapticTe' :
        texMax = 10
        texMin = 0
    elif textureId == 'synapticTi' :
        texMax = 10
        texMin = 0
    elif textureId == 'conductionDelays' :
        texMax = 30
        texMin = 0


    #brain = Brain('fsaverage', 'split', 'inflated', title=annotId+'-'+textureId, size=(1200,450), subjects_dir=subjects_dir,
    #              cortex='low_contrast', background='white')

    #brain_lh = Brain('fsaverage', 'lh', 'inflated', title='lh - '+annotId+'-'+textureId, size=(600,450), subjects_dir=subjects_dir,
    #              cortex='low_contrast', background='white')
    #brain_rh = Brain('fsaverage', 'rh', 'inflated', title='rh - '+annotId+'-'+textureId, size=(600,450), subjects_dir=subjects_dir,
    #              cortex='low_contrast', background='white')





    txt = textureId
    if not parcelId.startswith('synapticT') :
        txt += ' '+parcelId




    # prepare labels
    for t in textures :
        label = t[0]
        if label == stim_label and stim_red : 
            label.color = [ c/255 for c in red ]+[1]
            continue
        if np.isnan(t[1]) : 
            col = grey
        elif t[1] > texMax : 
            col = cm[-1]
        else :
            c = round( (len(cm)-1)/(texMax-texMin)*t[1] )
            col = cm[c]
        label.color = ( col[0]/255, col[1]/255, col[2]/255, 1 )
      


    # 4 views to be sure screenshots are ok...
    screenshotsDir=op.join( textureDir, 'screenshots' )
    if not op.exists(screenshotsDir) : os.makedirs(screenshotsDir)
    for h in [ 'lh', 'rh' ] :

        brain = Brain('fsaverage', h, 'inflated', title=h+' - '+annotId+'-'+txt, size=size, subjects_dir=subjects_dir, cortex='low_contrast', background='white')

        if zoom :
            mlab.gcf().scene.camera.zoom(1.4)
            mlab.draw()


        #if stim_label :
        #    if stim_label.hemi == h :
        #        brain.add_label( stim_label )
        
        for t in textures :
            label = t[0]
            if label.hemi == h :
                brain.add_label( label )


        for v in [ 'lat', 'med' ] :
            brain.show_view(v)
            # save            
            fname=op.join( screenshotsDir, '-'.join( [ parcelId, h, v ] ) + '.png' )
            print( '>> Save', fname )
            brain.save_image(fname)

        brain.close()




def create_montage( parcellationId, textureId, symmetric ) :

    textureDir = op.join(bd,'dcm',parcellationId+'_'+str(ageMin)+'-'+str(ageMax),'sym'+str(symmetric),textureId)
    screenshotsDir = op.join( textureDir, 'screenshots' )
    montageDir = op.join( screenshotsDir, 'montage' )
    if not op.exists( montageDir ) : os.makedirs( montageDir )

    files=glob.glob(op.join(screenshotsDir,'*lh-lat.png'))
    for i,f in enumerate(files,1) :
        print( i, '/', len(files),':', f )
        f_rh_lat = str.replace( f, '-lh-lat.png', '-rh-lat.png' )
        f_lh_med = str.replace( f, '-lh-lat.png', '-lh-med.png' )
        f_rh_med = str.replace( f, '-lh-lat.png', '-rh-med.png' )
        if op.exists(f_rh_lat) and op.exists(f_lh_med) and op.exists(f_rh_med) :
            output=op.join(montageDir,str.replace(op.basename(f),'-lh-lat.png', '.png'))
            subprocess.call( [ 'montage', '-geometry', '+0+0', f, f_rh_lat, f_lh_med, f_rh_med, output ] )




def showParcel( parcellationId, parcelId ) :
    

    # mapping between FS and FTRACT parcellation names
    annotId = parcellationId2annotId( parcellationId )


    # FS parcellations
    subjects_dir    = mne.datasets.sample.data_path() + '/subjects'
    annotLabels     = mne.read_labels_from_annot('fsaverage', annotId, 'both', subjects_dir=subjects_dir)

    label = getLabel( parcellationId, parcelId, annotLabels )

    brain = Brain('fsaverage', 'split', 'inflated', subjects_dir=subjects_dir, title=parcelId )
    label.color = [ c/255 for c in red ]+[1]
    brain.add_label( label )

    return brain
    



def parcellationId2annotId( parcellationId ) :

    # mapping between FS and FTRACT parcellation names
    if parcellationId == 'HCP-MMP1' :
        annotId = 'HCPMMP1'
    elif parcellationId == 'Lausanne2008-33' :
       annotId = 'Lausanne_36'
    elif parcellationId == 'Lausanne2008-60' :
        annotId = 'Lausanne_60'
    elif parcellationId == 'Lausanne2008-125' :
        annotId = 'Lausanne_125'
    elif parcellationId == 'Lausanne2008-250' :
        annotId = 'Lausanne_250'
    else :
        raise Exception('invalid parcellationId')    
    return annotId



def getLabels( annotId ) :
    # FS parcellations
    subjects_dir    = mne.datasets.sample.data_path() + '/subjects'
    labels          = mne.read_labels_from_annot('fsaverage', annotId, 'both', subjects_dir=subjects_dir)
    return labels


def parcelId2annotLabel( parcellationId, parcelId ) :
    
    annotLabel    = parcelId

    if parcellationId == 'HCP-MMP1' :
        annotLabel += '_ROI-'
        if annotLabel.startswith('L_') :
            annotLabel += 'lh'
        elif annotLabel.startswith('R_') :
            annotLabel += 'rh'

    elif parcellationId == 'Lausanne2008-33' :
        if annotLabel.startswith('ctx-lh-') :
            annotLabel = annotLabel[7:] + '-lh'
        elif annotLabel.startswith('ctx-rh-') :
            annotLabel = annotLabel[7:] + '-rh'

    elif parcellationId in ['Lausanne2008-60','Lausanne2008-125','Lausanne2008-250'] :
        if annotLabel.startswith('lh.') :
            annotLabel = annotLabel[3:] + '-lh'
        elif annotLabel.startswith('rh.') :
            annotLabel = annotLabel[3:] + '-rh'      

    return annotLabel




def getLabel( parcellationId, parcelId, annotLabels ) :

    annotLabel = parcelId2annotLabel( parcellationId, parcelId )
    label = [ l for l in annotLabels if l.name == annotLabel ]
    if len(label) == 0 :
        print('Label not found:', annotLabel)
        return None
    elif len(label)>1 :
        raise('Unexpected multiple labels found:', annotLabel)
    
    label = label[0]    
    return label




def checkLausanne60() :


    parcellationId = 'Lausanne2008-33'
    annotId = plot_parcellation.parcellationId2annotId( parcellationId )
    annotLabels_33     = mne.read_labels_from_annot('fsaverage', annotId, 'both', subjects_dir=subjects_dir)
    left_33 = [ l for l in annotLabels_33 if l.name.endswith('-lh') ]
    right_33 = [ l for l in annotLabels_33 if l.name.endswith('-rh') ]

    left = [ l.name[0:-3] for l in left_33 ]
    right = [ l.name[0:-3] for l in right_33 ]
    assert left == right


    fname = op.join( bd, 'scripts/cmp_nipype-master/cmtklib/data/colortable_and_gcs/original_color_36_L.txt' )
    fd=open(fname,'r')
    lines=fd.readlines()
    fd.close()
    
    left_33_colors = dict()
    for l in lines :
        tmp = l.split()
        name = tmp[1]
        color = [ tmp[2], tmp[3], tmp[4] ]
        left_33_colors[name] = color

    fname = op.join( bd, 'scripts/cmp_nipype-master/cmtklib/data/colortable_and_gcs/original_color_36_R.txt' )
    fd=open(fname,'r')
    lines=fd.readlines()
    fd.close()

    right_33_colors = dict()
    for l in lines :
        tmp = l.split()
        name = tmp[1]
        color = [ tmp[2], tmp[3], tmp[4] ]
        right_33_colors[name] = color


    assert left_33_colors == right_33_colors
    


    brain = Brain('fsaverage', 'split', 'inflated', subjects_dir=subjects_dir,size=(1600,600)) #, title=parcelId )
    for l in left_33 :
        l.color = [ float(c)/255 for c in left_33_colors[l.name[0:-3]] ] +[1]
        brain.add_label( l )
    for l in right_33 :
        l.color = [ float(c)/255 for c in right_33_colors[l.name[0:-3]] ] +[1]
        brain.add_label( l )

    







    parcellationId = 'Lausanne2008-60'
    annotId = plot_parcellation.parcellationId2annotId( parcellationId )
    annotLabels_60     = mne.read_labels_from_annot('fsaverage', annotId, 'both', subjects_dir=subjects_dir)    
    left_60 = [ l for l in annotLabels_60 if l.name.endswith('-lh') ]
    right_60 = [ l for l in annotLabels_60 if l.name.endswith('-rh') ]
    
    
    

    fname = op.join( bd, 'scripts/cmp_nipype-master/cmtklib/data/colortable_and_gcs/original_color_60_L.txt' )
    fd=open(fname,'r')
    lines=fd.readlines()
    fd.close()
    
    left_60_colors = dict()
    for l in lines :
        tmp = l.split()
        name = tmp[1]
        color = [ tmp[2], tmp[3], tmp[4] ]
        left_60_colors[name] = color

    fname = op.join( bd, 'scripts/cmp_nipype-master/cmtklib/data/colortable_and_gcs/original_color_60_R.txt' )
    fd=open(fname,'r')
    lines=fd.readlines()
    fd.close()

    right_60_colors = dict()
    for l in lines :
        tmp = l.split()
        name = tmp[1]
        color = [ tmp[2], tmp[3], tmp[4] ]
        right_60_colors[name] = color


    brain_60 = Brain('fsaverage', 'split', 'inflated', subjects_dir=subjects_dir,size=(1600,600)) #, title=parcelId )
    for l in left_60 :
        l.color = [ float(c)/255 for c in left_60_colors[l.name[0:-3]] ] +[1]
        brain_60.add_label( l )
    for l in right_60 :
        l.color = [ float(c)/255 for c in right_60_colors[l.name[0:-3]] ] +[1]
        brain_60.add_label( l )



    brain_60_sym = Brain('fsaverage', 'split', 'inflated', subjects_dir=subjects_dir,size=(1600,600)) #, title=parcelId )
    parcellation_sym = read_parcellation( 'Lausanne2008-60', 1 )
    for p in parcellation_sym :
        if p.startswith('lh.') :
            p = p[3:] + '-lh'
        elif p.startswith('rh.') :
            p = p[3:] + '-rh'
        label = [ l for l in annotLabels_60 if l.name == p ]
        if len(label) == 1 :
            brain_60_sym.add_label(label[0])    
        


    # fix Lausanne
    # Lausanne_60 is not symmetric
    # only in lh: ['postcentral_3', 'precentral_4', 'rostralmiddlefrontal_3']
    # only in rh: ['lateraloccipital_3', 'inferiorparietal_3', 'medialorbitofrontal_2']
    # so these parcels will not be colored

    # 1) fusion de ces parcels avec des parcels existantes
    # 2) mapping lh <-> rh


    ## fusion rh lateraloccipital_3
    lateraloccipital_2_lh = [ l for l in left_60 if l.name == 'lateraloccipital_2-lh' ][0]
    lateraloccipital_2_lh.color = [ float(c)/255 for c in left_60_colors['lateraloccipital_2'] ] + [1]
    brain_tmp.add_label( lateraloccipital_2_lh )
    
    lateraloccipital_2_rh = [ l for l in right_60 if l.name == 'lateraloccipital_2-rh' ][0]
    lateraloccipital_2_rh.color = [ float(c)/255 for c in right_60_colors['lateraloccipital_2'] ] + [1]
    brain_tmp.add_label( lateraloccipital_2_rh )
    lateraloccipital_3_rh = [ l for l in right_60 if l.name == 'lateraloccipital_3-rh' ][0]
    lateraloccipital_3_rh.color = [ float(c)/255 for c in right_60_colors['lateraloccipital_3'] ] + [1]
    brain_tmp.add_label( lateraloccipital_3_rh )
    # lateraloccipital_2_lh = lateraloccipital_2_rh + lateraloccipital_3_rh


    lateraloccipital_1_lh = [ l for l in left_60 if l.name == 'lateraloccipital_1-lh' ][0]
    lateraloccipital_1_lh.color = [ float(c)/255 for c in left_60_colors['lateraloccipital_1'] ] + [1]
    brain_tmp.add_label( lateraloccipital_1_lh )

    lateraloccipital_1_rh = [ l for l in right_60 if l.name == 'lateraloccipital_1-rh' ][0]
    lateraloccipital_1_rh.color = [ float(c)/255 for c in right_60_colors['lateraloccipital_1'] ] + [1]
    brain_tmp.add_label( lateraloccipital_1_rh )
    # lateraloccipital_1_lh = lateraloccipital_1_rh





    ## fusion rh inferiorparietal_3
    inferiorparietal_2_lh = [ l for l in left_60 if l.name == 'inferiorparietal_2-lh' ][0]
    inferiorparietal_2_lh.color = [ float(c)/255 for c in left_60_colors['inferiorparietal_2'] ] + [1]
    brain_tmp.add_label( inferiorparietal_2_lh )
    
    inferiorparietal_2_rh = [ l for l in right_60 if l.name == 'inferiorparietal_2-rh' ][0]
    inferiorparietal_2_rh.color = [ float(c)/255 for c in right_60_colors['inferiorparietal_2'] ] + [1]
    brain_tmp.add_label( inferiorparietal_2_rh )
    inferiorparietal_1_rh = [ l for l in right_60 if l.name == 'inferiorparietal_1-rh' ][0]
    inferiorparietal_1_rh.color = [ float(c)/255 for c in right_60_colors['inferiorparietal_1'] ] + [1]
    brain_tmp.add_label( inferiorparietal_1_rh )
    # inferiorparietal_2_lh = inferiorparietal_2_rh + inferiorparietal_1_rh


    inferiorparietal_1_lh = [ l for l in left_60 if l.name == 'inferiorparietal_1-lh' ][0]
    inferiorparietal_1_lh.color = [ float(c)/255 for c in left_60_colors['inferiorparietal_1'] ] + [1]
    brain_tmp.add_label( inferiorparietal_1_lh )
    
    inferiorparietal_3_rh = [ l for l in right_60 if l.name == 'inferiorparietal_3-rh' ][0]
    inferiorparietal_3_rh.color = [ float(c)/255 for c in right_60_colors['inferiorparietal_3'] ] + [1]
    brain_tmp.add_label( inferiorparietal_3_rh )
    # inferiorparietal_1_lh = inferiorparietal_3_rh
    
    


    ## fusion rh medialorbitofrontal_2
    medialorbitofrontal_1_lh = [ l for l in left_60 if l.name == 'medialorbitofrontal_1-lh' ][0]
    medialorbitofrontal_1_lh.color = [ float(c)/255 for c in left_60_colors['medialorbitofrontal_1'] ] + [1]
    brain_tmp.add_label( medialorbitofrontal_1_lh )
    
    medialorbitofrontal_1_rh = [ l for l in right_60 if l.name == 'medialorbitofrontal_1-rh' ][0]
    medialorbitofrontal_1_rh.color = [ float(c)/255 for c in right_60_colors['medialorbitofrontal_1'] ] + [1]
    brain_tmp.add_label( medialorbitofrontal_1_rh )
    medialorbitofrontal_2_rh = [ l for l in right_60 if l.name == 'medialorbitofrontal_2-rh' ][0]
    medialorbitofrontal_2_rh.color = [ float(c)/255 for c in right_60_colors['medialorbitofrontal_2'] ] + [1]
    brain_tmp.add_label( medialorbitofrontal_2_rh )
    # medialorbitofrontal_1_lh = medialorbitofrontal_1_rh + medialorbitofrontal_2_rh





    ## fusion lh postcentral_3
    postcentral_1_lh = [ l for l in left_60 if l.name == 'postcentral_1-lh' ][0]
    postcentral_1_lh.color = [ float(c)/255 for c in left_60_colors['postcentral_1'] ] + [1]
    brain_tmp.add_label( postcentral_1_lh )    
    postcentral_2_lh = [ l for l in left_60 if l.name == 'postcentral_2-lh' ][0]
    postcentral_2_lh.color = [ float(c)/255 for c in left_60_colors['postcentral_1'] ] + [1]
    brain_tmp.add_label( postcentral_2_lh )    
    
    postcentral_2_rh = [ l for l in right_60 if l.name == 'postcentral_2-rh' ][0]
    postcentral_2_rh.color = [ float(c)/255 for c in right_60_colors['postcentral_2'] ] + [1]
    brain_tmp.add_label( postcentral_2_rh )    
    # postcentral_1_lh + postcentral_2_lh = postcentral_2_rh


    postcentral_3_lh = [ l for l in left_60 if l.name == 'postcentral_3-lh' ][0]
    postcentral_3_lh.color = [ float(c)/255 for c in left_60_colors['postcentral_3'] ] + [1]
    brain_tmp.add_label( postcentral_3_lh )    
    
    postcentral_1_rh = [ l for l in right_60 if l.name == 'postcentral_1-rh' ][0]
    postcentral_1_rh.color = [ float(c)/255 for c in right_60_colors['postcentral_1'] ] + [1]
    brain_tmp.add_label( postcentral_1_rh )    
    # postcentral_3_lh = postcentral_1_rh




    ## fusion lh precentral_4
    precentral_1_lh = [ l for l in left_60 if l.name == 'precentral_1-lh' ][0]
    precentral_1_lh.color = [ float(c)/255 for c in left_60_colors['precentral_1'] ] + [1]
    brain_tmp.add_label( precentral_1_lh )    
    precentral_2_lh = [ l for l in left_60 if l.name == 'precentral_2-lh' ][0]
    precentral_2_lh.color = [ float(c)/255 for c in left_60_colors['precentral_2'] ] + [1]
    brain_tmp.add_label( precentral_2_lh )
    
    precentral_3_rh = [ l for l in right_60 if l.name == 'precentral_3-rh' ][0]
    precentral_3_rh.color = [ float(c)/255 for c in right_60_colors['precentral_3'] ] + [1]
    brain_tmp.add_label( precentral_3_rh )    
    # precentral_1_lh + precentral_2_lh = precentral_3_rh


    precentral_3_lh = [ l for l in left_60 if l.name == 'precentral_3-lh' ][0]
    precentral_3_lh.color = [ float(c)/255 for c in left_60_colors['precentral_3'] ] + [1]
    brain_tmp.add_label( precentral_3_lh )
    
    precentral_2_rh = [ l for l in right_60 if l.name == 'precentral_2-rh' ][0]
    precentral_2_rh.color = [ float(c)/255 for c in right_60_colors['precentral_2'] ] + [1]
    brain_tmp.add_label( precentral_2_rh )    
    # precentral_3_lh = precentral_2_rh


    
    precentral_4_lh = [ l for l in left_60 if l.name == 'precentral_4-lh' ][0]
    precentral_4_lh.color = [ float(c)/255 for c in left_60_colors['precentral_4'] ] + [1]
    brain_tmp.add_label( precentral_4_lh )

    precentral_1_rh = [ l for l in right_60 if l.name == 'precentral_1-rh' ][0]
    precentral_1_rh.color = [ float(c)/255 for c in right_60_colors['precentral_1'] ] + [1]
    brain_tmp.add_label( precentral_1_rh )
    # precentral_4_lh = precentral_1_rh
    


    ## fusion lh rostralmiddlefrontal_3
    rostralmiddlefrontal_1_lh = [ l for l in left_60 if l.name == 'rostralmiddlefrontal_1-lh' ][0]
    rostralmiddlefrontal_1_lh.color = [ float(c)/255 for c in left_60_colors['rostralmiddlefrontal_1'] ] + [1]
    brain_tmp.add_label( rostralmiddlefrontal_1_lh )
    rostralmiddlefrontal_2_lh = [ l for l in left_60 if l.name == 'rostralmiddlefrontal_2-lh' ][0]
    rostralmiddlefrontal_2_lh.color = [ float(c)/255 for c in left_60_colors['rostralmiddlefrontal_2'] ] + [1]
    brain_tmp.add_label( rostralmiddlefrontal_2_lh )

    rostralmiddlefrontal_1_rh = [ l for l in right_60 if l.name == 'rostralmiddlefrontal_1-rh' ][0]
    rostralmiddlefrontal_1_rh.color = [ float(c)/255 for c in right_60_colors['rostralmiddlefrontal_1'] ] + [1]
    brain_tmp.add_label( rostralmiddlefrontal_1_rh )
    # rostralmiddlefrontal_1_lh + rostralmiddlefrontal_2_lh = rostralmiddlefrontal_1_rh

    

    rostralmiddlefrontal_3_lh = [ l for l in left_60 if l.name == 'rostralmiddlefrontal_3-lh' ][0]
    rostralmiddlefrontal_3_lh.color = [ float(c)/255 for c in left_60_colors['rostralmiddlefrontal_3'] ] + [1]
    brain_tmp.add_label( rostralmiddlefrontal_3_lh )

    rostralmiddlefrontal_2_rh = [ l for l in right_60 if l.name == 'rostralmiddlefrontal_2-rh' ][0]
    rostralmiddlefrontal_2_rh.color = [ float(c)/255 for c in right_60_colors['rostralmiddlefrontal_2'] ] + [1]
    brain_tmp.add_label( rostralmiddlefrontal_2_rh )
    # rostralmiddlefrontal_3_lh = rostralmiddlefrontal_2_rh



    ## verifier ensuite que toutes les parcels sont bien equivalentes droite / gauche






def Lausanne60_symmetric() :

    # liste de toutes les parcels avec couple gauche droite
    
    # exceptions:    
    # lateraloccipital_2_lh                                 = lateraloccipital_2_rh + lateraloccipital_3_rh
    # lateraloccipital_1_lh                                 = lateraloccipital_1_rh
    # inferiorparietal_2_lh                                 = inferiorparietal_2_rh + inferiorparietal_1_rh
    # inferiorparietal_1_lh                                 = inferiorparietal_3_rh
    # medialorbitofrontal_1_lh                              = medialorbitofrontal_1_rh + medialorbitofrontal_2_rh
    # postcentral_1_lh + postcentral_2_lh                   = postcentral_2_rh
    # postcentral_3_lh                                      = postcentral_1_rh
    # precentral_1_lh + precentral_2_lh                     = precentral_3_rh
    # precentral_3_lh                                       = precentral_2_rh
    # precentral_4_lh                                       = precentral_1_rh
    # rostralmiddlefrontal_1_lh + rostralmiddlefrontal_2_lh = rostralmiddlefrontal_1_rh
    # rostralmiddlefrontal_3_lh                             = rostralmiddlefrontal_2_rh
    # 
    # 'lh.precuneus_1'                                      = 'rh.precuneus_2'
    # 'lh.precuneus_2'                                      = 'rh.precuneus_1'
    # 'lh.supramarginal_1'                                  = 'rh.supramarginal_2'
    # 'lh.supramarginal_2'                                  = 'rh.supramarginal_1'


    parcellation = [
        ( [ 'lh.cuneus_1' ],                                                [ 'rh.cuneus_1' ] ), 
        ( [ 'lh.lateraloccipital_1' ],                                      [ 'rh.lateraloccipital_1' ] ), 
        ( [ 'lh.lateraloccipital_2' ],                                      [ 'rh.lateraloccipital_2', 'rh.lateraloccipital_3' ] ), 
        ( [ 'lh.lingual_1' ],                                               [ 'rh.lingual_1' ] ), 
        ( [ 'lh.lingual_2' ],                                               [ 'rh.lingual_2' ] ), 
        ( [ 'lh.pericalcarine_1' ],                                         [ 'rh.pericalcarine_1' ] ), 
        ( [ 'lh.bankssts_1' ],                                              [ 'rh.bankssts_1' ] ), 
        ( [ 'lh.entorhinal_1' ],                                            [ 'rh.entorhinal_1' ] ), 
        ( [ 'lh.fusiform_1' ],                                              [ 'rh.fusiform_1' ] ), 
        ( [ 'lh.fusiform_2' ],                                              [ 'rh.fusiform_2' ] ), 
        ( [ 'lh.inferiortemporal_1' ],                                      [ 'rh.inferiortemporal_1' ] ), 
        ( [ 'lh.inferiortemporal_2' ],                                      [ 'rh.inferiortemporal_2' ] ), 
        ( [ 'lh.middletemporal_1' ],                                        [ 'rh.middletemporal_1' ] ), 
        ( [ 'lh.middletemporal_2' ],                                        [ 'rh.middletemporal_2' ] ), 
        ( [ 'lh.parahippocampal_1' ],                                       [ 'rh.parahippocampal_1' ] ), 
        ( [ 'lh.superiortemporal_1' ],                                      [ 'rh.superiortemporal_1' ] ), 
        ( [ 'lh.superiortemporal_2' ],                                      [ 'rh.superiortemporal_2' ] ), 
        ( [ 'lh.temporalpole_1' ],                                          [ 'rh.temporalpole_1' ] ), 
        ( [ 'lh.transversetemporal_1' ],                                    [ 'rh.transversetemporal_1' ] ), 
        ( [ 'lh.inferiorparietal_1' ],                                      [ 'rh.inferiorparietal_3' ] ), 
        ( [ 'lh.inferiorparietal_2' ],                                      [ 'rh.inferiorparietal_1', 'rh.inferiorparietal_2' ] ), 
        ( [ 'lh.postcentral_1', 'lh.postcentral_2' ],                       [ 'rh.postcentral_2' ] ), 
        ( [ 'lh.postcentral_3' ],                                           [ 'rh.postcentral_1' ] ), 
        ( [ 'lh.precuneus_1' ],                                             [ 'rh.precuneus_2' ] ), 
        ( [ 'lh.precuneus_2' ],                                             [ 'rh.precuneus_1' ] ), 
        ( [ 'lh.superiorparietal_1' ],                                      [ 'rh.superiorparietal_1' ] ), 
        ( [ 'lh.superiorparietal_2' ],                                      [ 'rh.superiorparietal_2' ] ), 
        ( [ 'lh.superiorparietal_3' ],                                      [ 'rh.superiorparietal_3' ] ), 
        ( [ 'lh.supramarginal_1' ],                                         [ 'rh.supramarginal_2' ] ), 
        ( [ 'lh.supramarginal_2' ],                                         [ 'rh.supramarginal_1' ] ), 
        ( [ 'lh.caudalanteriorcingulate_1' ],                               [ 'rh.caudalanteriorcingulate_1' ] ), 
        ( [ 'lh.isthmuscingulate_1' ],                                      [ 'rh.isthmuscingulate_1' ] ), 
        ( [ 'lh.posteriorcingulate_1' ],                                    [ 'rh.posteriorcingulate_1' ] ), 
        ( [ 'lh.rostralanteriorcingulate_1' ],                              [ 'rh.rostralanteriorcingulate_1' ] ), 
        ( [ 'lh.paracentral_1' ],                                           [ 'rh.paracentral_1' ] ), 
        ( [ 'lh.caudalmiddlefrontal_1' ],                                   [ 'rh.caudalmiddlefrontal_1' ] ), 
        ( [ 'lh.frontalpole_1' ],                                           [ 'rh.frontalpole_1' ] ), 
        ( [ 'lh.parsopercularis_1' ],                                       [ 'rh.parsopercularis_1' ] ), 
        ( [ 'lh.parstriangularis_1' ],                                      [ 'rh.parstriangularis_1' ] ), 
        ( [ 'lh.precentral_1', 'lh.precentral_2' ],                         [ 'rh.precentral_3' ] ), 
        ( [ 'lh.precentral_3' ],                                            [ 'rh.precentral_2' ] ), 
        ( [ 'lh.precentral_4' ],                                            [ 'rh.precentral_1' ] ), 
        ( [ 'lh.rostralmiddlefrontal_1', 'lh.rostralmiddlefrontal_2' ],     [ 'rh.rostralmiddlefrontal_1' ] ), 
        ( [ 'lh.rostralmiddlefrontal_3' ],                                  [ 'rh.rostralmiddlefrontal_2' ] ), 
        ( [ 'lh.superiorfrontal_1' ],                                       [ 'rh.superiorfrontal_1' ] ), 
        ( [ 'lh.superiorfrontal_2' ],                                       [ 'rh.superiorfrontal_2' ] ), 
        ( [ 'lh.superiorfrontal_3' ],                                       [ 'rh.superiorfrontal_3' ] ), 
        ( [ 'lh.superiorfrontal_4' ],                                       [ 'rh.superiorfrontal_4' ] ), 
        ( [ 'lh.lateralorbitofrontal_1' ],                                  [ 'rh.lateralorbitofrontal_1' ] ), 
        ( [ 'lh.lateralorbitofrontal_2' ],                                  [ 'rh.lateralorbitofrontal_2' ] ), 
        ( [ 'lh.medialorbitofrontal_1' ],                                   [ 'rh.medialorbitofrontal_1', 'rh.medialorbitofrontal_2' ] ), 
        ( [ 'lh.parsorbitalis_1' ],                                         [ 'rh.parsorbitalis_1' ] ), 
        ( [ 'lh.insula_1' ],                                                [ 'rh.insula_1' ] ), 
        ( [ 'lh.insula_2' ],                                                [ 'rh.insula_2' ] ), 
        ( [ 'Left-Thalamus-Proper' ],                                       [ 'Right-Thalamus-Proper' ] ), 
        ( [ 'Left-Pallidum' ],                                              [ 'Right-Pallidum' ] ), 
        ( [ 'Left-Putamen' ],                                               [ 'Right-Putamen' ] ), 
        ( [ 'Left-Hippocampus' ],                                           [ 'Right-Hippocampus' ] ), 
        ( [ 'Left-Caudate' ],                                               [ 'Right-Caudate' ] ), 
        ( [ 'Left-Accumbens-area' ],                                        [ 'Right-Accumbens-area' ] ), 
        ( [ 'Left-Amygdala' ],                                              [ 'Right-Amygdala' ] ), 
        ( [ 'Brain-Stem' ] ), 
        ( [ 'Unknown' ] ), 
    ]

    return parcellation    







def plotLausanne60_symmetric() :


    textureId = 'conductionDelays'
    inversed = False
    
    parcellation = Lausanne60_symmetric()
    parcellation.remove(['Brain-Stem'])
    parcellation.remove(['Unknown'])

    for parcel in parcellation :
        parcelId = parcel[0][0]
        plot_parcellation_Lausanne2008_60_symmetric( textureId, parcelId, size=(2000,1400), zoom=True, inversed=inversed )

    




    
    



def addLausanne60_Label( brain_60, labelName, symmetric, left_60, right_60, left_60_colors, right_60_colors ) :
    
    parcellation = Lausanne60_symmetric()

    if symmetric :
        parcel_lh_rh    = [ p for p in parcellation if labelName in p[0] ]
        parcel_lh_rh    = parcel_lh_rh[0]
        parcel_lh       = parcel_lh_rh[0]
        parcel_rh       = parcel_lh_rh[1]
        for p in parcel_lh :
            p = p[3:]+'-lh'
            label       = [ l for l in left_60 if l.name == p ][0]
            label.color = [ float(c)/255 for c in left_60_colors[p[:-3]] ] + [1]
            brain_60.add_label( label )
        for p in parcel_rh :
            p = p[3:]+'-rh'
            label       = [ l for l in right_60 if l.name == p ][0]
            label.color = [ float(c)/255 for c in right_60_colors[p[:-3]] ] + [1]
            brain_60.add_label( label )
















# textureId = 'recDensity'
# textureId = 'stiDensity'
def plotRecDensity( textureId ) :
    

    parcellationId = 'HCP-MMP1'

    ageMin = 15
    ageMax = 100
    parcellationName = '_'.join( [ parcellationId, '-'.join( [ str(a) for a in [ ageMin, ageMax ] ] ) ] )


    textureDir = op.join( bd, 'dcm', parcellationName, 'sym0', textureId )
    parcelId = textureId
    fname = op.join( textureDir, parcelId + '.txt' )
    fd = open( fname, 'r' )
    lines = fd.readlines()
    fd.close()


    annotId = parcellationId2annotId( parcellationId )
    annotLabels = mne.read_labels_from_annot('fsaverage', annotId, 'both', subjects_dir=subjects_dir)

    
    

    
    labels = []
    for parcel in lines :
        ( parcelName, parcelDensity ) = parcel.strip().split()
        if parcelName.startswith('L_') : parcelName += '_ROI-lh'
        elif parcelName.startswith('R_') : parcelName += '_ROI-rh'
        else : raise Exception( 'unexpected parcelName: ' + parcelName )
        
        parcelLabel = [ l for l in annotLabels if l.name == parcelName ]
        if len(parcelLabel) != 1 : raise Exception( 'unexpected parcel Label: ' + parcelName )
        
        parcelDensity = int(parcelDensity)
        labels.append( ( parcelLabel[0], parcelDensity ) )
                 
    texMin = float( np.min( [ label[1] for label in labels ] ) )
    texMax = float( np.max( [ label[1] for label in labels ] ) )


    cm=read_matlab_cm()
    for label in labels :
        c = round( (len(cm)-1)/(texMax-texMin)*label[1] )
        col = cm[c]
        label[0].color = ( col[0]/255, col[1]/255, col[2]/255, 1 )


    brainSize = (800,600)
    txt = textureId
    screenshotsDir = op.join( textureDir, 'screenshots' )
    if not op.exists(screenshotsDir) : os.makedirs(screenshotsDir)

    for h in [ 'lh', 'rh' ] :

        brain = Brain('fsaverage', h, 'inflated', title=h+' - '+annotId+'-'+txt, size=brainSize, subjects_dir=subjects_dir, cortex='low_contrast', background='white')
    
        for label in labels :
            print( 'add', label[0].name )
            if label[0].hemi == h :
                brain.add_label( label[0] )
            
        for v in [ 'lat', 'med' ] :
            brain.show_view(v)
            # save            
            fname = op.join( screenshotsDir, '-'.join( [ parcelId, h, v ] ) + '.png' )
            brain.save_image( fname )

        brain.close()




def make_fig4( inversed = False ) :


    parcelId = 'Left-Amygdala'
    make_fig4_parcel( parcelId, symmetric=1 )
        
    parcelId = 'lh.insula_1'
    make_fig4_parcel( parcelId, symmetric=1 )
    parcelId = 'lh.insula_2'
    make_fig4_parcel( parcelId, symmetric=1 )
        
    parcelId = 'lh.parsopercularis_1'
    make_fig4_parcel( parcelId, symmetric=1 )
    make_fig4_parcel( parcelId, symmetric=0 )
    parcelId = 'lh.parstriangularis_1'
    make_fig4_parcel( parcelId, symmetric=1 )
    make_fig4_parcel( parcelId, symmetric=0 )
    parcelId = 'lh.superiortemporal_1'
    make_fig4_parcel( parcelId, symmetric=1 )
    
    


    # inversed
    parcelId = 'lh.insula_1'
    make_fig4_parcel( parcelId, symmetric=1, inversed=True )
    parcelId = 'lh.insula_2'
    make_fig4_parcel( parcelId, symmetric=1, inversed=True )



    # versus non symmetric
    parcelId = 'lh.superiortemporal_1'
    make_fig4_parcel( parcelId, symmetric=0 )
    parcelId = 'rh.superiortemporal_1'
    make_fig4_parcel( parcelId, symmetric=0 )
    parcelId = 'Left-Amygdala'
    make_fig4_parcel( parcelId, symmetric=0 )
    parcelId = 'Right-Amygdala'
    make_fig4_parcel( parcelId, symmetric=0 )
    



def make_fig4_parcel( parcelId, symmetric, cceps_min=5, inversed=False ) :

    textureId = 'conductionDelays'

    if symmetric :
        plot_parcellation_Lausanne2008_60_symmetric( textureId, parcelId, cceps_min, size=(2000,1400), zoom=True, inversed=inversed, close=False )
    else :
        plot_parcellation( 'Lausanne2008-60', textureId, parcelId, 0, size=(2000,1400), zoom=True ) 

    parcellationId = 'Lausanne2008-60'
    ageMin = 15
    ageMax = 100
    parcellationName = parcellationId+'_'+str(ageMin)+'-'+str(ageMax)
    textureDir = op.join( bd, 'dcm', parcellationName,'sym'+str(symmetric), 'cceps_min_'+str(cceps_min), textureId )
    screenshotsDir = op.join( textureDir, 'screenshots' )
  
    fig = 'fig4'
    for h in [ 'lh', 'rh' ] :
        for v in [ 'lat', 'med' ] :
            src = op.join( screenshotsDir, '-'.join( [ parcelId, h, v ] ) )
            dst = op.join( bd, 'figures_original', fig, fig+'_'+parcelId+'_sym'+str(symmetric)+'_15_100_'+h+'-'+v )
            if inversed :
                src += '_inversed'
                dst += '_inversed'
            src += '.png'
            dst += '.png'
            command = [ 'cp', src, dst ]
            print( '>>', ' '.join(command) )
            subprocess.call( command )




def make_fig6() :

    parcellationId = 'HCP-MMP1'
    make_fig6_parcellation( parcellationId )
       
    parcellationId = 'Lausanne2008-33'
    make_fig6_parcellation( parcellationId )
    
    parcellationId = 'Lausanne2008-60'
    make_fig6_parcellation( parcellationId )

    parcellationId = 'Lausanne2008-125'
    make_fig6_parcellation( parcellationId )

    parcellationId = 'Lausanne2008-250'
    make_fig6_parcellation( parcellationId )





def make_fig6_parcellation( parcellationId, cceps_min=5 ) :

    textureId = 'synapticTe'
    parcelId = 'synapticTe'
    symmetric = 0
    cceps_min = 5
    plot_parcellation( parcellationId, textureId, parcelId, symmetric, cceps_min, size=(2000,1400), zoom=True )

    fig = 'fig6'
    src = glob.glob( op.join( bd, 'dcm', parcellationId + '_15-100', 'sym' + str(symmetric), 'cceps_min_'+str(cceps_min), textureId, 'screenshots', '*.png' ) )
    dst = op.join( bd, 'figures_original', fig, parcellationId + '_15_100_sym' + str(symmetric) + '_ccepsMin' + str(cceps_min) + '__' + textureId )
    command = [ 'cp' ] + src + [ dst ]
    print( '>>', ' '.join(command) )
    subprocess.call( [ 'mkdir', '-p', dst ] )
    subprocess.call( command )











# size=(2000,1400) # taille pour figures article
# size=(800,600) # taille pour explorer
# zoom for figures article
# inversed: if True, plot conduction delay to parcelId (instead of from parcelId

def plot_parcellation_Lausanne2008_60_symmetric( textureId, parcelId, cceps_min, cm=read_matlab_cm(), size=(800,600), zoom=False, stim_red=False, inversed=False, close=False, texMin=0, texMax=30, saveFig=True ) :
    
        
    if parcelId.startswith('lh') :
        parcelSymId = parcelId.replace( 'lh', 'rh' )

    parcellationId='Lausanne2008-60'
    symmetric = 1

    ageMin = 15
    ageMax = 100
    parcellationName = parcellationId+'_'+str(ageMin)+'-'+str(ageMax)

    parcellation = Lausanne60_symmetric()
    parcellation.remove(['Brain-Stem'])
    parcellation.remove(['Unknown']) 


    

    # mapping between FS and FTRACT parcellation names
    annotId = parcellationId2annotId( parcellationId )
    # FS parcellations
    annotLabels = mne.read_labels_from_annot('fsaverage', annotId, 'both', subjects_dir=subjects_dir)


    Lausanne_60 = read_parcellation( parcellationId, 0 )
    Lausanne_60.remove('Brain-Stem')
    Lausanne_60.remove('Unknown')



    # mapping between parcel and label
    labels = dict()
    for p in Lausanne_60 :
        if p.startswith('lh.') : labName = p[3:]+'-lh'
        elif p.startswith('rh.') : labName = p[3:]+'-rh'
        else : continue
        label = [ lab for lab in annotLabels if lab.name == labName ]
        if len(label) == 1 :
            labels[p] = label[0]



    textureId = 'conductionDelays'
    textureDir = op.join( bd, 'dcm', parcellationName,'sym1', 'ccepsMin'+str(cceps_min), textureId )
    screenshotsDir = op.join( textureDir, 'screenshots' )
    if not op.exists(screenshotsDir) : os.makedirs(screenshotsDir)



    brains = []
    for parcelsSym in parcellation :

        if not parcelId in parcelsSym[0] :
            continue


        if not inversed :

            stimParcels = parcelsSym[0]
            stimParcelId = '__'.join( stimParcels )
            fname = op.join( textureDir, stimParcelId + '.txt' )
            fd = open( fname, 'r' )
            lines = fd.readlines()
            fd.close()

            # update texture for each rec label    
            for line in lines :
                ( parcels, value ) = line.strip().split()
                parcels = parcels.split('__')
                value = float(value)
                for p in parcels :
                    if p in labels.keys() :
                        label = labels[p]
                        if stim_red and p in stimParcels :
                            col = red
                        elif np.isnan(value) :
                            col = grey
                        elif value > texMax :
                            col = cm[-1]
                        else :
                            c = round( (len(cm)-1)/(texMax-texMin)*value )
                            col = cm[c]
                        label.color = ( col[0]/255, col[1]/255, col[2]/255, 1 )
          
        else :

            # read all txt files and get the line with parcelId or parcelSymId
            for parcelsSym in parcellation :
                stimParcels = parcelsSym[0]
                stimParcelId = '__'.join( stimParcels )
                stimParcelsSym = parcelsSym[1]

                if stimParcelId.startswith('Left-') or stimParcelId.startswith('Right-') :
                    # there is no corresponding label for this kind of parcels (Thalamus, Hippocampus, Amygdala, ...)
                    continue

                fname = op.join( textureDir, stimParcelId + '.txt' )
                fd = open( fname, 'r' )
                lines = fd.readlines()
                fd.close()

                for line in lines :
                    ( parcels, value ) = line.strip().split()
                    parcels = parcels.split('__')
                    value = float(value)
                    
                    if parcelId in parcels :
                        for p in stimParcels :
                            label = labels[p]
                            #print( label, value )
                            if np.isnan(value) :
                                col = grey 
                            elif value > texMax :
                                col = cm[-1]
                            else :
                                c = round( (len(cm)-1)/(texMax-texMin)*value )
                                col = cm[c]
                            label.color = ( col[0]/255, col[1]/255, col[2]/255, 1 )
                    if parcelSymId in parcels :
                        for p in stimParcelsSym :
                            label = labels[p]
                            #print( label, value )
                            if np.isnan(value) :
                                col = grey 
                            elif value > texMax :
                                col = cm[-1]
                            else :
                                c = round( (len(cm)-1)/(texMax-texMin)*value )
                                col = cm[c]
                            label.color = ( col[0]/255, col[1]/255, col[2]/255, 1 )

        




        txt = 'conduction delay - ' + stimParcelId + ' (inversed: ' + str(inversed) + ')'

        for h in [ 'lh', 'rh' ] :

            brain = Brain('fsaverage', h, 'inflated', title=h+' - '+annotId+'-'+txt, size=size, subjects_dir=subjects_dir, cortex='low_contrast', background='white')

            if zoom :
                mlab.gcf().scene.camera.zoom(1.4)
                mlab.draw()

            for label in labels.values() :
                print('add', label.name)
                if label.hemi == h :
                    brain.add_label(label)

            if saveFig :
                for v in [ 'lat', 'med' ] :
                    brain.show_view(v)
                    # save
                    fname = op.join( screenshotsDir, '-'.join( [ parcelId, h, v ] ) )
                    if inversed : fname += '_inversed'
                    fname += '.png'
                    print( '>> Save', fname )
                    brain.save_image( fname )

            if close :
                brain.close()
            else :
                brains.append(brain)

    return brains



















