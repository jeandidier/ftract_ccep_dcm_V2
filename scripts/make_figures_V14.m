function make_figures_V14

s1Selection_config  = 1 ;
s1Selection         = s1Selection_config_V14( s1Selection_config ) ;

V = 'V14' ;
[ peakLatency_all, s1_fnames, s1_acc_all, s1_peaks_all, s1_conductionDelay_all, s1_synapticTe_all, s1_synapticTi_all, s2_fnames, s2_acc_all, s2_peaks_all, s2_conductionDelay_all, s2_synapticTe_all, s2_synapticTi_all ] = eval( [ 'loadDCMs_' V '(s1Selection);' ] ) ;
[ dataDir, dcmDir, ~ ]	= simuDirs() ;

pF_D                    = [] ;
pC_D                    = [] ;
pE_T                    = [] ;
pE_G                    = [] ;
pC_T                    = [] ;
pC_G                    = [] ;
pC_S                    = [] ;
pC_H                    = [] ;
config_s2               = struct( 'step', 2, 'avg_mode', 'bpa_nocond' ) ;
config_s2.s1Selection   = s1Selection ;
fit_duration_id         = [] ;

for t=1:4
   
    if t == 1
        minPL   = 16 ;
        maxPL   = 20 ;
        % dataFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm/data/PATS__ArtefactCorrectionPCHIP/0001NAN/SEEG/StimLF_2015-08-31/SEQ_BM_ACPCHIP_F_ComputeAverage/B12_1mA_1Hz_1050us_2/cceps/B12_1mA_1Hz_1050us_2__b12b11.mat' ;
        dataFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm/data/PATS__ArtefactCorrectionPCHIP/0004NAN/SEEG/StimLF_2015-07-01/SEQ_BM_ACPCHIP_F_ComputeAverage/Qp45_4mA_1Hz_1050us_1/cceps/Qp45_4mA_1Hz_1050us_1__r''4r''3.mat' ;
%         dataFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm/data/PATS__ArtefactCorrectionPCHIP/0005GRE/SEEG/StimLF_2015-06-26/SEQ_BM_ACPCHIP_F_ComputeAverage/E12_3mA_1Hz_1000us_1/cceps/E12_3mA_1Hz_1000us_1__y5y4.mat' ;
        % dataFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm/data/PATS__ArtefactCorrectionPCHIP/0007LYO/SEEG/StimLF_2015-06-10/SEQ_BM_ACPCHIP_F_ComputeAverage/Yp1112_3mA_1Hz_1000us_1/cceps/Yp1112_3mA_1Hz_1000us_1__n''11n''10.mat' ;
        figpos = [2 881 312 277] ;
        
    elseif t == 2
        minPL   = 32 ;
        maxPL   = 36 ;
%         dataFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm/data/PATS__ArtefactCorrectionPCHIP/0001NAN/SEEG/StimLF_2015-08-31/SEQ_BM_ACPCHIP_F_ComputeAverage/O56_1mA_1Hz_1050us_1/cceps/O56_1mA_1Hz_1050us_1__tm12tm11.mat' ;
%         dataFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm/data/PATS__ArtefactCorrectionPCHIP/0005LYO/SEEG/StimLF_2015-05-26/SEQ_BM_ACPCHIP_F_ComputeAverage/D910_31mA_1Hz_1000us_1/cceps/D910_31mA_1Hz_1000us_1__b3b2.mat' ;
        dataFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm/data/PATS__ArtefactCorrectionPCHIP/0009ROT/SEEG/StimLF_2015-09-22/SEQ_BM_ACPCHIP_F_ComputeAverage/LP67_4mA_1Hz_1050us_1/cceps/LP67_4mA_1Hz_1050us_1__la8la7.mat' ;
        figpos = [2 615 312 277] ;
        
    elseif t == 3
        minPL   = 48 ;
        maxPL   = 52 ;
        dataFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm/data/PATS__ArtefactCorrectionPCHIP/0001LYO/SEEG/StimLF_2015-01-19/SEQ_BM_ACPCHIP_F_ComputeAverage/I23_3mA_1Hz_1000us_1/cceps/I23_3mA_1Hz_1000us_1__v2v1.mat' ;
        % dataFname: /network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm/data/PATS__ArtefactCorrectionPCHIP/0004NAN/SEEG/StimLF_2015-07-01/SEQ_BM_ACPCHIP_F_ComputeAverage/Cp1314_1mA_1Hz_1050us_2/cceps/Cp1314_1mA_1Hz_1050us_2__c'18c'17.mat
        % dataFname: /network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm/data/PATS__ArtefactCorrectionPCHIP/0011GRE/SEEG/StimLF_2013-11-19/SEQ_BM_ACPCHIP_F_ComputeAverage/Tp12_3mA_1Hz_1000us_1/cceps/Tp12_3mA_1Hz_1000us_1__b'2b'1.mat
        % dataFname: /network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm/data/PATS__ArtefactCorrectionPCHIP/0016GRE/SEEG/StimLF_2015-09-09/SEQ_BM_ACPCHIP_F_ComputeAverage/Ap12_3mA_1Hz_1000us_1/cceps/Ap12_3mA_1Hz_1000us_1__b'2b'1.mat
        figpos = [935 881 312 277] ;
        
    else
        minPL   = 64 ;
        maxPL   = 68 ;
        % dataFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm/data/PATS__ArtefactCorrectionPCHIP/0082GRE/SEEG/StimLF_2010-01-15/SEQ_BM_ACPCHIP_F_ComputeAverage/Np12_3mA_1Hz_1050us_1/cceps/Np12_3mA_1Hz_1050us_1__f''10f''9.mat' ;
        % dataFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm/data/PATS__ArtefactCorrectionPCHIP/0089GRE/SEEG/StimLF_2012-04-01/SEQ_BM_ACPCHIP_F_ComputeAverage/Ep56_3mA_1Hz_1050us_1/cceps/Ep56_3mA_1Hz_1050us_1__b''4b''3.mat' ;
        dataFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm/data/PATS__ArtefactCorrectionPCHIP/0103GRE/SEEG/StimLF_2016-06-16/SEQ_BM_ACPCHIP_F_ComputeAverage/O89_3mA_1Hz_1000us_1/cceps/O89_3mA_1Hz_1000us_1__o14o13.mat' ;
        figpos = [935 615 312 277] ;
    end
    
%     dcmFname        = buildDCMFname_V13( dataFname, pF_D, pC_D, pE_T, pE_G, pC_T, pC_G, pC_S, pC_H, config_s2, fit_duration_id ) ;
    dcmFname        = buildDCMFname_V14( dataFname, pF_D, pC_D, pE_T, pE_G, pC_T, pC_G, pC_S, pC_H, config_s2, fit_duration_id ) ;
    DCM             = load(dcmFname);
    DCM             = DCM.DCM ;
    d               = find( cellfun( @(d) strcmp(dcmFname, d), s2_fnames ) ) ;
    
    D = spm_eeg_load(dataFname) ;
    
    [~,locs] = findpeaks(abs(DCM.H{1})) ;
    signe = sign(DCM.H{1}(locs(1))) ;
    
    H = figure( 'Position', figpos ) ;
    hold on ;
    plot( D.time * 1000, signe * D(:), 'LineWidth', 2 ) ;
    plot( DCM.xY.pst, signe * DCM.H{1} / DCM.xY.scale, 'LineWidth', 2, 'LineStyle', '--'  ) ;
    xlim([0 100]) ;
%     gcf
%     set(gca,'LineWidth',2);
%     set(gca,'FontWeight','bold');
    
    grid on
    
    suptitle( sprintf( '(#%d) PL: %d ms - Ax: %d ms, Te: %d ms, Ti: %d ms', d, round(s2_peaks_all(d)), round(s2_conductionDelay_all(d)), round(s2_synapticTe_all(d)), round(s2_synapticTi_all(d)) ) ) ;
    legend({'obs.' 'pred.'})
    
    saveFigures= true ;
    if saveFigures
        fname = [ dcmDir '/../screenshots/data_fitted_' num2str(minPL) '_' num2str(maxPL) '_' num2str(d) '.txt' ] ;
        fd = fopen(fname, 'wt') ;
        fprintf(fd, sprintf('%s %.1f\n', '#d', d) );
        fprintf(fd, sprintf('%s %.1f\n', 'peak latency', round(s2_peaks_all(d)) ) );
        fprintf(fd, sprintf('%s %.1f\n', 'axonal delay', round(s2_conductionDelay_all(d)) ) );
        fprintf(fd, sprintf('%s %.1f\n', 'synapticTe', round(s2_synapticTe_all(d)) ) );
        fprintf(fd, sprintf('%s %.1f\n', 'synapticTi', round(s2_synapticTi_all(d)) ) );
        fclose(fd);
        fname = [ dcmDir '/../screenshots/data_fitted_' num2str(minPL) '_' num2str(maxPL) '_' num2str(d) '.png' ] ;
        saveas( H, fname ) ;
        fname = [ dcmDir '/../screenshots/data_fitted_' num2str(minPL) '_' num2str(maxPL) '_' num2str(d) '.svg' ] ;
        saveas( H, fname ) ;
        fprintf(1, [ 'Save ' fname '\n' ] ) ;
    end
    
    
end


end




function cd_vs_te()

% 1) 1 < cd < 2 and 1 < Te < 2
% 2) 1 < cd < 2 and 10 < Te < 12


s1Selection_config  = 1 ;
s1Selection         = s1Selection_config_V14( s1Selection_config ) ;

V = 'V14' ;
[ peakLatency_all, s1_fnames, s1_acc_all, s1_peaks_all, s1_conductionDelay_all, s1_synapticTe_all, s1_synapticTi_all, s2_fnames, s2_acc_all, s2_peaks_all, s2_conductionDelay_all, s2_synapticTe_all, s2_synapticTi_all ] = eval( [ 'loadDCMs_' V '(s1Selection);' ] ) ;
[ dataDir, dcmDir, ~ ]	= simuDirs() ;

doi1=[];
doi2=[];
for d=1:length(peakLatency_all)
    if s2_acc_all(d) < 90 || abs( s2_peaks_all(d) - peakLatency_all(d) ) > 5
        continue
    end
    if isnan(s2_peaks_all(d))
        continue
    end
    if s2_conductionDelay_all(d) > 1 && s2_conductionDelay_all(d) < 2 
        if s2_synapticTe_all(d) > 1 && s2_synapticTe_all(d) < 2
            doi1(end+1)=d;
        elseif s2_synapticTe_all(d) > 10 && s2_synapticTe_all(d) < 12
            doi2(end+1)=d;
        end
    end
end


% 
doi = doi1 ; % 5036
doi = doi2 ; % 21807 13913        6422 9390 12116 

for i=1:length(doi)
    d=doi(i+190);
    
    DCM = load(s2_fnames{d});
    DCM = DCM.DCM ;
    
    D = spm_eeg_load(DCM.xY.Dfile) ;
    
    [~,locs] = findpeaks(abs(DCM.H{1})) ;
    
    signe = sign(DCM.H{1}(locs(1))) ;
    
    figpos = [62 793 678 497] ; % [935 615 312 277] ;
    H = figure( 'Position', figpos ) ;
    hold on ;
    plot( D.time * 1000, signe * D(:), 'LineWidth', 2 ) ;
    plot( DCM.xY.pst, signe * DCM.H{1} / DCM.xY.scale, 'LineWidth', 2, 'LineStyle', '--'  ) ;
    xlim([0 100]) ;
    %     gcf
    %     set(gca,'LineWidth',2);
    %     set(gca,'FontWeight','bold');
    
    grid on
    
    suptitle( sprintf( '(#%d) PL: %d ms - Ax: %d ms, Te: %d ms, Ti: %d ms', d, round(s2_peaks_all(d)), round(s2_conductionDelay_all(d)), round(s2_synapticTe_all(d)), round(s2_synapticTi_all(d)) ) ) ;
    legend({'obs.' 'pred.'})
    
    if i>20
        break
    end
end



end








