function dxdt = spm_fx_erp_ddefun_lags_opt(t,x,Z,P,M,u,lags_ind)
% t    in seconds
% x    = [ x11 x21 x12 x22 x13 x23 ... x19 x29 ]' ;
% Z    = [ f(x(t-De(1,1))) f(x(t-De(2,1))) ... f(x(t-De(n,1))) ... f(x(t-De(1,n))) f(x(t-De(2,n))) ... f(x(t-De(n,n))) f(x(t-Di)) ];
% dxdt = [ d(x11 x21 x12 x22 x13 x23 ... x19 x29)dt ]' ;
% u is the input
% lags_ind : list of connexion indices using each lag / Z

% get dimensions and configure state variables
%--------------------------------------------------------------------------
x  = spm_unvec(x,M.x);      % neuronal states
n  = size(x,1);             % number of sources

% [default] fixed parameters
%--------------------------------------------------------------------------
E = [1 1/2 1/8]*32;         % extrinsic rates (forward, backward, lateral)
% C = 1 ;                     % extrinsic input C
G = [1 4/5 1/4 1/4]*128;    % intrinsic rates (g1 g2 g3 g4)
% D = [2 16];                 % propogation delays (intrinsic, extrinsic)       % defined & used in gen_erp_dde23_lags_opt
H = [4 32];                 % receptor densities (excitatory, inhibitory)
T = [8 16];                 % synaptic constants (excitatory, inhibitory)
R = [2 1]/3;                % parameters of static nonlinearity

% [specified] fixed parameters
%--------------------------------------------------------------------------
if isfield(M,'pF')
    try, E = M.pF.E; end
%     try, C = M.pF.C; end
    try, G = M.pF.H; end
%     try, D = M.pF.D; end      % defined & used in gen_erp_dde23_lags_opt
    try, H = M.pF.G; end
    try, T = M.pF.T; end
    try, R = M.pF.R; end
end


% test for free parameters on intrinsic connections
%--------------------------------------------------------------------------
try
    G = G.*exp(P.H);
end
G     = ones(n,1)*G;

% exponential transform to ensure positivity constraints
%--------------------------------------------------------------------------
if n > 1
    A{1} = exp(P.A{1})*E(1);
    A{2} = exp(P.A{2})*E(2);
    A{3} = exp(P.A{3})*E(3);
else
    A = {0,0,0};
end
C     = exp(P.C);

% intrinsic connectivity and parameters
%--------------------------------------------------------------------------
Te    = T(1)/1000*exp(P.T(:,1));         % excitatory time constants
Ti    = T(2)/1000*exp(P.T(:,2));         % inhibitory time constants
He    = H(1)*exp(P.G(:,1));              % excitatory receptor density
Hi    = H(2)*exp(P.G(:,2));              % inhibitory receptor density
% Te    = T(:,1)/1000.*exp(P.T(:,1));         % excitatory time constants
% Ti    = T(:,2)/1000.*exp(P.T(:,2));         % inhibitory time constants
% He    = H(:,1).*exp(P.G(:,1));              % excitatory receptor density
% Hi    = H(:,2).*exp(P.G(:,2));              % inhibitory receptor density

% pre-synaptic inputs: s(V)
%--------------------------------------------------------------------------
R     = full(R.*exp(P.S));
S     = @(v) 1./(1 + exp(-R(1)*(v - R(2)))) - 1./(1 + exp(R(1)*R(2))) ;
% for all delays: n^2 (extrinsic) + 1 (intrinsic)
%Zr    = reshape( Z, n, 9, n^2+1 );
%S_Zr  = S(Zr);

% init with zero lag
Zfull = repmat( x(:), [ 1 n*n ] );
% update with nonzero lag
for l=1:length(lags_ind)            % all (unique) delays considered
    for i=1:length(lags_ind{l,1})   % all connexion indices using this delay
        Zfull(:,lags_ind{l,1}(i,1))=Z(:,l) ;
    end
end
Zr      = reshape( full(Zfull), n, 9, n, n ) ;
S_Zr    = S(Zr);





% input
%==========================================================================
if isfield(M,'u')
    
    error( 'not implemented' ) ;
    % endogenous input
    %----------------------------------------------------------------------
    U = u(:)*64;
   
else
    % exogenous input
    %----------------------------------------------------------------------
    U = C*u(t)*2;
    
    % warning: in some cases (large t in particular), U is NaN 
    % because of a division by zero in spm_erp_u :
    % U      = exp(-(t - delay).^2/(2*scale^2)); 
    % => U = 0 for t=350; delay=50; scale=5;
    % followed by division by zero 
    % U      = prop*cumsum(U)/sum(U) + U*(1 - prop);
    % Nan is replaced by zero in this case:
    if ~isempty(find(isnan(U)))
        U(find(isnan(U))) = 0 ;
    end
end


% State: f(x)
%==========================================================================

% Supragranular layer (inhibitory interneurons): Voltage & depolarizing current
%--------------------------------------------------------------------------
f(:,7) = x(:,8);
% f(:,8) = (He.*((A{2} + A{3})*S(:,9) + G(:,3).*S(:,9)) - 2*x(:,8) - x(:,7)./Te)./Te;
for s=1:n
    % delayed outputs (x9) of each source using their own intrinsic delay
    Si = zeros(n,1); 
    % delayed outputs (x9) of each source using extrinsic delay to source s 
    Se = zeros(n,1);
    % delayed states for connection s2 to s
    for s2=1:n
        if s2==s
            % intrinsic
            Si(s2,1) = S_Zr(s2,9,s,s2);
        else
            % extrinsic
            Se(s2,1) = S_Zr(s2,9,s,s2);
        end
    end
    tmp     = (He.*((A{2} + A{3})*Se(:) + G(:,3).*Si(:)) - 2*x(:,8) - x(:,7)./Te)./Te ;
    f(s,8)  = tmp(s,1);
end

% Granular layer (spiny stellate cells): Voltage & depolarizing current
%--------------------------------------------------------------------------
f(:,1) = x(:,4);
% f(:,4) = (He.*((A{1} + A{3})*S(:,9) + G(:,1).*S(:,9) + U) - 2*x(:,4) - x(:,1)./Te)./Te;
for s=1:n
    % delayed outputs (x9) of each source using their own intrinsic delay
    Si = zeros(n,1); 
    % delayed outputs (x9) of each source using extrinsic delay to source s 
    Se = zeros(n,1);
    % delayed states for connection s2 to s
    for s2=1:n
        if s2==s
            % intrinsic
            Si(s2,1) = S_Zr(s2,9,s,s2);
        else
            % extrinsic
            Se(s2,1) = S_Zr(s2,9,s,s2) ;
        end
    end
    tmp     = (He.*((A{1} + A{3})*Se + G(:,1).*Si + U) - 2*x(:,4) - x(:,1)./Te)./Te ;
    f(s,4)  = tmp(s,1);
end

% Infra-granular layer (pyramidal cells): depolarizing current
%--------------------------------------------------------------------------
f(:,2) = x(:,5);
% f(:,5) = (He.*((A{2} + A{3})*S(:,9) + G(:,2).*S(:,1)) - 2*x(:,5) - x(:,2)./Te)./Te;
for s=1:n
    % delayed outputs (x9) of each source using their own intrinsic delay
    Si = zeros(n,1); 
    % delayed outputs (x9) of each source using extrinsic delay to source s 
    Se = zeros(n,1);
    % delayed states for connection s2 to s
    for s2=1:n
        if s2==s
            % intrinsic
            Si(s2,1) = S_Zr(s2,1,s,s2);
        else
            % extrinsic
            Se(s2,1) = S_Zr(s2,9,s,s2) ;
        end
    end
    tmp     = (He.*((A{2} + A{3})*Se + G(:,2).*Si) - 2*x(:,5) - x(:,2)./Te)./Te ;
    f(s,5)  = tmp(s,1);   
end

% Infra-granular layer (pyramidal cells): hyperpolarizing current
%--------------------------------------------------------------------------
f(:,3) = x(:,6);
% f(:,6) = (Hi.*G(:,4).*S(:,7) - 2*x(:,6) - x(:,3)./Ti)./Ti;
Si = zeros(n,1);
for s=1:n, Si(s,1)=S_Zr(s,7,s,s); end
f(:,6)  = (Hi.*G(:,4).*Si - 2*x(:,6) - x(:,3)./Ti)./Ti  ;

% Infra-granular layer (pyramidal cells): Voltage
%--------------------------------------------------------------------------
f(:,9) = x(:,5) - x(:,6);

dxdt   = spm_vec(f);

end