function [y] = gen_erp_dde23_lags_opt(P,M,U)
% integrates a MIMO nonlinear system using dde23


u = @(t) spm_erp_u(t,P,M) ;



% propagation delays (intrinsic, extrinsic)
% [specified] fixed parameters
% try
%     Die = M.pF.Die ;
%     Di  = eye(size(P.D)) .* Die ;
%     De  = ( ones(size(P.D)) - eye(size(P.D)) ) .* Die ;
% catch
%     try Di = M.pF.Di; catch, Di = 2 ; end
%     try De = M.pF.De; catch, De = 16 ; end
%     if isscalar( Di ), Di = eye(size(P.D)) * Di ; end ;
%    	if isscalar( De ), De = ( ones(size(P.D)) - eye(size(P.D)) ) * De ; end ;
% end

D = [2 16];                 % propogation delays (intrinsic, extrinsic)
if isfield(M,'pF')
    try, D = M.pF.D; end
end

n 	= size(P.A{1},1); % number of sources
Q 	= sparse(n,n);
for i = 1:length(P.A)
%       A{i} = ~~A{i};
%     E.A{i} = A{i}*N - N;                          % forward
%     V.A{i} = A{i}/16;                             % backward
    Q      = Q | exp(P.A{i});                            % and lateral connections
end

De = D(2) * Q .* exp(P.D)/1000; 
Di = D(1) * eye(n,n)     /1000 ;

Die = De + Di ;
% lags are strictly positive values of Die
[c,~,ic]    = unique( Die );   % we want lags to be unique
c_ind       = find(c) ;        % indices of strictly positive lags

lags_ind    = cell(length(c_ind),1) ; % list of connexion indices for each non-zero lag
for l=1:length(c_ind)
     lags_ind{l,1} = find( ic == c_ind(l) ) ;
end
lags = c(c_ind) ;
% fprintf( 1, [ 'Using ' num2str(length(lags)) ' lagged states (optimized)\n' ] ) ;


% check: rebuild Die from lags and lags_ind
Die_check = zeros(size(Die)) ;
for l=1:length(lags)
    Die_check( lags_ind{l,1} ) = lags(l);
end
if ~isequal( Die, Die_check )
   error( 'Invalid lags / lags_ind' ) ; 
end


ddefun  = @(t,x,Z) spm_fx_erp_ddefun_lags_opt(t,x,Z,P,M,u,lags_ind) ;



n       = size(P.A{1},1); % number of sources
m       = size(M.x,2);    % number of states for each neuronal source

history = zeros(n*m,1) ;  % maybe to be replaced with M.x ???


% possible to split the integration on many intervals
if M.dde23_options.tspan == 0
    interval_samples = M.ns ;
else
    error( 'not implemented' ) ;
    % ddefun and lags are the same
    % history and tspan are different
end

options = ddeset('RelTol',M.dde23_options.reltol,'AbsTol',M.dde23_options.abstol) ;

% fprintf( 1, [ 'Using dde23 with: \n' ] ) ;
% fprintf( 1, [ '\ttspan samples  = ' num2str(interval_samples) '\n' ] ) ;
% fprintf( 1, [ '\trel tol        = ' num2str( ddeget(options,'RelTol') ) '\n' ] ) ;
% fprintf( 1, [ '\tabs tol        = ' num2str( ddeget(options,'AbsTol') ) '\n' ] ) ;


history_interval    = history ;
% tspan               = [ 1 M.ns ] * U.dt ;
tspan               = [ 0 M.ns-1 ] * U.dt ;
% tic
sol                 = dde23(ddefun,lags,history_interval,tspan, options);
% toc

save_sol = 0 ;
if save_sol
    save( 'sol.mat', 'sol' ) ;
end
plot_x_histo = 0 ;
if plot_x_histo
    figure ;
    histogram(sol.x, 50) ;
end


% sol is evaluated at (1:M.ns) * U.dt ;
% y = deval( sol, (1:M.ns) * U.dt );
y = deval( sol, (0:M.ns-1) * U.dt );
y = y' ;


end
