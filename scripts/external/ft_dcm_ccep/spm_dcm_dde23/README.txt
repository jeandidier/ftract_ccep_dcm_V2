
Modifications for silent source for spatial LFP
(try to apply the same strategy as for other spatial models, ECD or IMG)
	spm_L_priors.m
		adjust the size of L to the true number of sources: n instead of m
		set the gain of the silent source to 0 and fix it (null covariance) 
	spm_erp_L.m 
		adjust L to take into account LFP silent source


spm_dcm_eeg_channelmodes.m : add option ‘q’ to spm_diff to fit single ccep

spm_erp_priors.m : set -log of absent (null) connections to 1000
 
Example:
	generate_data.m
	fit_data.m



possibility to set different priors for each source using M.pF
