function txt = IS_as_txt( IS )

if isequal( IS.function, @spm_gen_erp )
    txt = [ 'spm_gen_erp' ] ;
elseif isequal( IS.function, @spm_gen_erp_dde23 )
    txt = [ 'spm_gen_erp_dde23__reltol_' num2str(IS.options.reltol) '_abstol_' num2str(IS.options.abstol) ] ;
    try lags_opt=IS.options.lags_opt; catch, lags_opt=false; end
    txt = [ txt '_lagsopt_' num2str(lags_opt)  ] ;
else
    error('Invalid IS');
end

end
