function [ fit_IS_TA, fit_IS_RK_reltol_1em3, fit_IS_RK_reltol_1em2 ] = integration_schemes()

ta_options                  = struct( 'name','spm_gen_erp') ;
fit_IS_TA                   = struct( 'function', @spm_gen_erp, 'options', ta_options ) ;

dde23_options               = struct( 'name','spm_gen_erp_dde23','tspan',0,'reltol',1e-3,'abstol',1e-6 ) ;
fit_IS_RK_reltol_1em3       = struct( 'function', @spm_gen_erp_dde23, 'options', dde23_options ) ;

dde23_options               = struct( 'name','spm_gen_erp_dde23','tspan',0,'reltol',1e-2,'abstol',1e-6 ) ;
fit_IS_RK_reltol_1em2       = struct( 'function', @spm_gen_erp_dde23, 'options', dde23_options ) ;

end
