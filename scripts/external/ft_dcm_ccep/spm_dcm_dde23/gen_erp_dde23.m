function [y] = gen_erp_dde23(P,M,U)
% integrates a MIMO nonlinear system using dde23


u = @(t) spm_erp_u(t,P,M) ;

plot_input = 0 ;
if plot_input
    tmp = feval(M.fu,(1:M.ns)*U.dt,P,M);
    figure ; plot( (1:M.ns)*U.dt, tmp ) ;
end




try dde23_lags_opt = M.dde23_options.lags_opt ; catch, dde23_lags_opt = false; end

if dde23_lags_opt
    [y] = gen_erp_dde23_lags_opt(P,M,U) ;
    return
end




ddefun  = @(t,x,Z) spm_fx_erp_ddefun(t,x,Z,P,M,u) ;



% in this implementation, lags = [ spm_vec(De) ; Di ]; (see below)
% it means that n*n + 1 lagged states are computed

% in the optimal implementation (gen_erp_dde23_lags_opt), lagged values are computed only for strictly positive lags
% present in the Die matrix

% Die is not implemented here because here Di is expected to be the same
% for all sources

% Di is one single value (the same for all sources and different from zero)
% De can be a single value or a matrix ( again none value can be zero )

% propagation delays (intrinsic, extrinsic)
% [specified] fixed parameters
try Di = M.pF.Di; catch, Di = 2 ; end
try De = M.pF.De; catch, De = 16 ; end

De      = De.*exp(P.D)/1000; 
Di      = Di/1000;

lags    = [ spm_vec(De) ; Di ]; 
% fprintf( 1, [ 'Using ' num2str(length(lags)) ' lagged states\n' ] ) ;


n       = size(P.A{1},1); % number of sources
m       = size(M.x,2);    % number of states for each neuronal source

history = zeros(n*m,1) ;  % maybe to be replaced with M.x ???


% possible to split the integration on many intervals
if M.dde23_options.tspan == 0
    interval_samples = M.ns ;
else
    error( 'not implemented' ) ;
    % ddefun and lags are the same
    % history and tspan are different
end

options = ddeset('RelTol',M.dde23_options.reltol,'AbsTol',M.dde23_options.abstol) ;

% fprintf( 1, [ 'Using dde23 with: \n' ] ) ;
% fprintf( 1, [ '\ttspan samples  = ' num2str(interval_samples) '\n' ] ) ;
% fprintf( 1, [ '\trel tol        = ' num2str( ddeget(options,'RelTol') ) '\n' ] ) ;
% fprintf( 1, [ '\tabs tol        = ' num2str( ddeget(options,'AbsTol') ) '\n' ] ) ;


history_interval    = history ;
% tspan               = [ 1 M.ns ] * U.dt ;
tspan               = [ 0 M.ns-1 ] * U.dt ;
% tic
sol                 = dde23(ddefun,lags,history_interval,tspan, options);
% toc

save_sol = 0 ;
if save_sol
    save( 'sol.mat', 'sol' ) ;
end
plot_x_histo = 0 ;
if plot_x_histo
    figure ;
    histogram(sol.x, 50) ;
end


% sol is evaluated at (1:M.ns) * U.dt ;
% y = deval( sol, (1:M.ns) * U.dt );
y = deval( sol, (0:M.ns-1) * U.dt );
y = y' ;


end
