    function [Delay, DelayOnset, Ampl, Slope, Probability]=PeaksCharac(threshold, MaxPeak, ResponseZSp, PeakDelayOut, OnsetDelayOut, AmplOut, SlopeOut, ProbabilityOut)

    % Extracts the characteristics of significant peaks - to be applied on
    % preprocessed averaged data 
    %
    % pathIn : path linking to the meeg file processed
    % threshold : significativity threshold
    % Responsep : path linking to the MEEG signal averaged and z-scored
    %
    % Delay : delay of each significant peak for each channel (ms)
    % Ampl : amplitude of each significant peak for each channel (std)
    % Slope : slope of the peak, in (std/ms)
    % Delay and Ampl are saved as MEEG files
    
    if threshold==0
        threshold=[1;2;3;4;5;6;7];
    end

    RespZS=spm_eeg_load(ResponseZSp);
    offset=indsample(RespZS,0);
    endAnalysis = size(RespZS,2);
    
    ResponseZS=RespZS(:,offset:endAnalysis);
    ResponseZSAbs=abs(ResponseZS);
    Ampl=NaN*zeros(size(ResponseZSAbs,1),length(threshold));
    Delay=NaN*zeros(size(ResponseZSAbs,1),length(threshold));
    DelayOnset=NaN*zeros(size(ResponseZSAbs,1),length(threshold));
    Slope=NaN*zeros(size(ResponseZSAbs,1),length(threshold));
    Time=RespZS.time;

    for i1=1:size(ResponseZSAbs,1)
        
        for i2=1:length(threshold)
            locs=local_max(ResponseZSAbs(i1,:));
            tmp1=locs(locs~=1);
            tmp2=tmp1(find(ResponseZSAbs(i1,tmp1)>threshold(i2)));
            
            if numel(tmp2)>0
                IndPeak(i1,i2)=tmp2(1);
                Ampl(i1,i2)=ResponseZSAbs(i1,IndPeak(i1,i2));
%                 t=IndPeak(i1,i2)-1;
%                 Delay(i1,i2)=1000*t/RespZS.fsample;
                Delay(i1,i2)=1000*(RespZS.time(offset-1+IndPeak(i1,i2)));
                d=[zeros(size(ResponseZSAbs,1),1) diff(ResponseZSAbs,1,2)];
                n=1:size(d,2)-1;
                h = find( (d(i2,n)<0 & d(i2,n+1)>0) | (d(i2,n)>0 & d(i2,n+1)<0) );
                
                if numel(max(h(find(h-IndPeak(i1,i2)<0))))>0
                    IndStart(i1,i2)=max(h(find(h-IndPeak(i1,i2)<0)));
                else IndStart(i1,i2)=1;
                end
                
                DelayOnset(i1,i2)=1000*(IndStart(i1,i2)-1)/RespZS.fsample;
                Slope(i1,i2)=(ResponseZSAbs(i1,IndPeak(i1,i2))-ResponseZSAbs(i1,IndStart(i1,i2)))/(Delay(i1,i2)-DelayOnset(i1,i2));
            end
        end
        
    end
    

    Probability=zeros(RespZS.nchannels,1);
    for i1=1:RespZS.nchannels
        for i2=1:length(threshold)
            if ~isnan(Ampl(i1,i2))
                Probability(i1,i2)=1;
            end
        end
    end

    D1=clone(RespZS, PeakDelayOut, [RespZS.nchannels size(Delay,2) 1]);
    D1(:,:,:)=Delay;
    D1=fsample(D1,1);
    D1=timeonset(D1,0);
    save(D1);

    D2=clone(RespZS, AmplOut, [RespZS.nchannels size(Ampl,2) 1]);
    D2(:,:,:)=Ampl;
    D2=fsample(D2,1);
    D2=timeonset(D2,0);
    save(D2);

    D3=clone(RespZS, OnsetDelayOut, [RespZS.nchannels size(DelayOnset,2) 1]);
    D3(:,:,:)=DelayOnset;
    D3=fsample(D3,1);
    D3=timeonset(D3,0);
    save(D3);

    D4=clone(RespZS, SlopeOut, [RespZS.nchannels size(Slope,2) 1]);
    D4(:,:,:)=Slope;
    D4=fsample(D4,1);
    D4=timeonset(D4,0);
    save(D4);
    
    D5=clone(RespZS, ProbabilityOut, [RespZS.nchannels size(Probability,2) 1]);
    D5(:,:,:)=Probability;
    D5=fsample(D5,1);
    D5=timeonset(D5,0);
    save(D5);

    end
