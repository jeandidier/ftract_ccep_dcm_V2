function fname = dcmFname( dataFname, config )

dcmDir = [ config.outputDir '/step' num2str(config.step) ] ;
%  '/step' num2str(config.step) ] 
[ ~, name ] = fileparts( dataFname ) ;
dcmId       = [ 'DCM_' name ] ;
fname   	= [ dcmDir '/' dcmId '.mat' ] ;

end
