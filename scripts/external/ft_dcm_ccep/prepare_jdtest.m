function prepare_jdtest( ccepsInd, steps, peakLatencyMax, threshold, fitDurationMode, computationMode, stimFname, probabilityFname, peakLatencyFname, durationFname, dcmOutputDir )


fprintf( 1, [ 'Entering prepare_jdtest\n' ] ) ;
ccepsInd
steps
peakLatencyMax
threshold
fitDurationMode
computationMode
stimFname
probabilityFname
peakLatencyFname
durationFname
dcmOutputDir

if ischar(ccepsInd)
    if strcmp(ccepsInd, 'significant')
        ccepsInd=[];
    else
        ccepsInd=strsplit(ccepsInd,'__');
        ccepsInd=cellfun( @(c) str2num(c), ccepsInd );
    end    
end

if ischar(steps)
    if strcmp(steps, 'all')
        steps=[ 1 2 ];
    else
        steps=strsplit(steps,'__');
        steps=cellfun( @(s) str2num(s), steps );
    end
end

if ischar(peakLatencyMax)
    peakLatencyMax = str2num(peakLatencyMax);
end
if ischar(threshold)
    threshold = str2num(threshold);
end







set_final_status('OK');

close all

end

