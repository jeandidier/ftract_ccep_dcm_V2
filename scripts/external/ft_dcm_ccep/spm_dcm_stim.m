function [ dcmFnames_step1, dcmFnames_step2 ] = spm_dcm_stim( ccepsFnames, peakLatencies, fitDurations, dcmOutputDir, compute_mode, steps )

% compute_mode = 'serial' ;
% compute_mode = 'parallel' ;


% create output dirs
fprintf( 1, [ 'Creating ' dcmOutputDir '/step1' '\n' ] ) ;
mkdir( [ dcmOutputDir '/step1' ] ) ;
fprintf( 1, [ 'Creating ' dcmOutputDir '/step2' '\n' ] ) ;
mkdir( [ dcmOutputDir '/step2' ] ) ;


% priors for variances
pC      = struct ;
pC.D    = [ 0 0 ; 1 0 ] ;
pC.T    = [ 1 1 ; 1 1 ] ;
pC.G    = [ 1 1 ; 1 1 ] ;
pC.S    = [ 1/16 1/16 ] ;
pC.H    = [ 1/16 1/16 1/16 1/16 ] ;


%% step 1
do_step1 = ~isempty(find(steps==1));
dcmFnames_step1 = {};
if do_step1
    
    config              = struct( 'step', 1, 'compute_mode', compute_mode ) ;
    config.outputDir    = dcmOutputDir ;
    config.pC           = pC ;
    
    dcmFnames_step1     = fitCceps( ccepsFnames, peakLatencies, fitDurations, config ) ;

end



%% step 2
do_step2 = ~isempty(find(steps==2));
dcmFnames_step2 = {};
if do_step2
    
    % average individuals fits
    DCM_acc             = struct([]) ;
    config              = struct( 'step', 1 ) ;
    config.outputDir    = dcmOutputDir ;
    for c=1:length(ccepsFnames)
        
        fname        = dcmFname( ccepsFnames{c}, config ) ;
        if ~exist(fname,'file'), error([fname ': DCM not found']); end
        DCM                         = load(fname);
        DCM                         = DCM.DCM;
        [ ~, ~, ~, R2_c_m0_s1_full ]= compute_accuracy( DCM, [] ) ;
        DCM_acc(c).dcmFname         = fname ;
        DCM_acc(c).R2_c_m0_s1_full  = full(R2_c_m0_s1_full) ;
        DCM_acc(c).Ep               = DCM.Ep ;
        
        [~,locs]                   	= findpeaks(abs(DCM.H{1})) ;
        if ~isempty(locs)
            DCM_acc(c).s1_peak    	= DCM.xY.pst(locs(1)) ;
        else
            DCM_acc(c).s1_peak    	= nan ;
        end
        DCM_acc(c).peakLatency    	= peakLatencies(c) ;
        
    end
    
    
    % only accurate fits are averaged
    s1_selected         = 1:length(ccepsFnames) ;
    
    s1Selection         = struct ;
%     s1Selection.minPL   = 0 ;
%     s1Selection.maxPL   = peakLatencyMax ;
    s1Selection.R2      = nan ;
    s1Selection.MPD     = nan ;
    
%     s1_selected = ( s1Selection.minPL <= peakLatencies ) & ( peakLatencies <= s1Selection.maxPL ) ;
    if ~isnan(s1Selection.R2)
        s1_selected = s1_selected & arrayfun( @(d) ( d.R2_c_m0_s1_full > s1Selection.R2 ), DCM_acc ) ;
    end
    if ~isnan(s1Selection.MPD)
        s1_selected = s1_selected & arrayfun( @(d) ( abs( d.s1_peak - d.peakLatency ) < s1Selection.MPD ), DCM_acc ) ;
    end

    cceps_acc_peak_to_avg = find( s1_selected ) ; 
    if ~isempty(cceps_acc_peak_to_avg)
        
        config              = struct( 'step', 2, 'avg_mode', 'bpa_nocond', 'compute_mode', compute_mode ) ;
        config.outputDir    = dcmOutputDir ;
        config.pC           = pC ;
        
        if strcmp(config.avg_mode, 'bpa_nocond')
            P_acc       = arrayfun( @(d) d.dcmFname, DCM_acc(cceps_acc_peak_to_avg), 'UniformOutput', false ) ;    
            name        = '';
            nocond      = strcmp( config.avg_mode, 'bpa_nocond' ) ;
            graphics    = 0 ;
            DCM_avg_ffx = spm_dcm_average(P_acc,name,nocond,graphics) ;
        elseif strcmp(config.avg_mode, 'median')
            DCM_acc = DCM_acc(cceps_acc_peak_to_avg) ;
            T = nan(length(DCM_acc),2,2) ;
            G = nan(length(DCM_acc),2,2) ;
            S = nan(length(DCM_acc),2) ;
            C = nan(length(DCM_acc),2) ;
            H = nan(length(DCM_acc),4) ;
            for d=1:length(DCM_acc)
                T(d,:,:)    = full(DCM_acc(d).Ep.T) ;
                G(d,:,:)    = full(DCM_acc(d).Ep.G) ;
                S(d,:)      = full(DCM_acc(d).Ep.S) ;
                C(d,:)      = full(DCM_acc(d).Ep.C) ;
                H(d,:)      = full(DCM_acc(d).Ep.H) ;
            end
            DCM_avg_ffx      = struct ;
            DCM_avg_ffx.Ep.T = squeeze(median(T,1)) ;
            DCM_avg_ffx.Ep.G = squeeze(median(G,1)) ;
            DCM_avg_ffx.Ep.S = squeeze(median(S,1)) ;
            DCM_avg_ffx.Ep.C = squeeze(median(C,1)) ;
            DCM_avg_ffx.Ep.H = squeeze(median(H,1)) ;
            
        end
        
        config.DCM_avg_ffx  = DCM_avg_ffx ;
        config.s1Selection  = s1Selection ;
        
        % all cceps are fitted again except if none were accurate
        dcmFnames_step2     = fitCceps( ccepsFnames, peakLatencies, fitDurations, config ) ;
        
    end
        
end


end




function [ cd, te ] = cd_te_from_pl( pl )

if pl <= 10
    cd = 1 ;
else
    cd = pl / 2 ;
end

if pl <= 18
    te = 1 ;
elseif pl <= 26
    te = 2 ;
elseif pl <= 30
    te = 3 ;
elseif pl <= 36
    te = 4 ;
elseif pl <= 42
    te = 5 ;
elseif pl <= 48
    te = 6 ;
elseif pl <= 54
    te = 7 ;
else
    te = 8 ;
end

end



function dcmFnames = fitCceps( ccepsFnames, peakLatencies, fitDurations, config )

Te      = 8 ;
k1      = 8 ;

dcmFnames = cell( size(ccepsFnames) ) ;
for c=1:length(ccepsFnames)
    
    [ cd, te ]          = cd_te_from_pl( peakLatencies(c) ) ;
    
    k2                  = Te/te ; % te = Te / k2 => k2 = Te / te
    pE_T                = [ -log(k1) -log(k1); -log(k2) -log(k2) ] ;
    pE_G                = [  log(k1)  log(k1);  log(k2)  log(k2) ] ;
    
    config.dataFname    = ccepsFnames{c} ;
    config.pF           = struct( 'D', [ 0 cd ] ) ;
    config.pE           = struct( 'T', pE_T, 'G', pE_G ) ;
    config.fit_duration = fitDurations(c) ;
    
    dcmFnames{c} = dcmFname( config.dataFname, config ) ;
    
    if strcmp(config.compute_mode,'parallel')
        f(c) = parfeval( @stimDCM_V14, 0, config ) ;
    else
        stimDCM_V14( config ) ;
    end
    
end
    
if strcmp(config.compute_mode,'parallel')
    for c=1:length(ccepsFnames)
        f(c).wait();
        fname               = dcmFname( ccepsFnames{c}, config ) ;
        if ~exist(fname,'file'), error( [ fname ': DCM not found' ] ) ; end
        [ pathstr,name,~ ]  = fileparts(fname);
        logFname            = [ pathstr '/' name '__par.log' ] ;
        fd = fopen(logFname,'w');
        fprintf(fd,f(c).Diary);
        fclose(fd);
    end
end

end




