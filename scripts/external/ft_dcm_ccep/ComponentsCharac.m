function [Duration, LatStart, Integral]=ComponentsCharac(Hthreshold, Lthreshold, gap, MaxComp, Responsep, DurationOut, LatStartOut, IntegralOut)

% Extracts the characteristics of the component corresponding to the significant peaks - to be applied on
% preprocessed averaged data
%
% path : path linking to the meeg file processed
% Hthreshold : high threshold
% Lthreshold : low threshold
% gap : permitted time gap (s) under which the signals stays below the high
% threshold but above the low threshold
% Responsep : path linking to the MEEG signal averaged and z-scored
%
% IndStart : first sample with a significative probability corresponding to
% each significant peak for each channel
% IndEnd : last sample with a significative probability corresponding to
% each significant peak for each channel
% LatStart : latency corresponding to IndStart
% LatEnd : latency corresponding to IndEnd
% Duration : duration of each component corresponding to each significant
% peak for each channel (LatEnd-Start)
% Integral : signal integral between the start and the end of each
% component
% Duration, LatStart and Integral are saved as MEEG files

Resp=spm_eeg_load(Responsep);
offset=indsample(Resp,0);
Response=abs(Resp(:,offset:size(Resp,2)));

LatStart=NaN(size(Response,1), MaxComp);
LatEnd=NaN(size(Response,1), MaxComp);
Duration=NaN(size(Response,1), MaxComp);
Integral=NaN(size(Response,1), MaxComp);
gapI=gap*Resp.fsample;


for i1=1:size(Response,1)

    tmpH=find(Response(i1,:)>Hthreshold);
    tmpL=find(Response(i1,:)>Lthreshold);
    Components=[];
    
    % Algorithm to find the components : 
    % - look for the samples above a high threshold
    % - if the signals gets down under the high threshold, check if it
    % stays above a low threshold for a short time window
    % - if then it goes back above the high threshold, the whole part is
    % considered to belong to the same component
    % - if the part of the signal between the two threshold exceeds the
    % time gap, the component corresponds to the part above the high
    % threshold
    % This allows to avoid uncertainties concerning the chosen threshold.
    
    if ~isempty(tmpH)
        cH=ComputeClusters(tmpH);   % create clusters with the consecutive samples
        ComponentsTmp=cH;
        
        if ~isempty(tmpL)
            cL=ComputeClusters(tmpL);
            
            nb_cl=0;
            gap_tmp=1;
            i2=1;
            
            while i2<=size(cH,1) && nb_cl<5
                
                newComp=ComponentsTmp(i2,find(ComponentsTmp(i2,:)));    %default value of the new component: the next cluster among the values above the high threshold
                Stop=0;
                
                if (newComp(end)+gap_tmp >= size(Response,2))   %if the components ends up at the end of the signal, no need to continue
                    nb_cl=nb_cl+1;
                    Components(nb_cl,1:length(newComp))=newComp;
                    i2=i2+1;
                else
                    while ~Stop
                        [bl, numcl] = BelongTo(cL,newComp(end)+gap_tmp);    % tests if the value of the sample, which is in the uncertainty gap, is above the low threshold
                        [bh, numch] = BelongTo(cH,newComp(end)+gap_tmp);    % tests if the value of the sample, which is in the uncertainty gap, is above the high threshold
                        
                        if ~bl && ~bh    % the signal gets down under the two threshold: we don't go any further - the component is the newComponent
                            nb_cl=nb_cl+1;
                            Components(nb_cl,1:length(newComp))=newComp;
                            Stop=1;
                            i2=i2+1;
                        else if bl && ~bh     % the signals stays below the high threshold but above the low threshold
                                if gap_tmp+1 < gapI     % the allowed gap has not been overstepped
                                    gap_tmp=gap_tmp+1;
                                    Stop=0;
                                else              % we exceed the allowed gap: we don't go any further - the component is the newComponent
                                    nb_cl=nb_cl+1;
                                    Components(nb_cl,1:length(newComp))=newComp;
                                    Stop=1;
                                    i2=i2+1;
                                end
                            else if bl && bh    % the signals gets back above the high threshold: we add the part between the threshold to the newComponent and so concatenate the two clusters
                                    comp1=ComponentsTmp(i2,find(ComponentsTmp(i2,:)));
                                    if size(ComponentsTmp,1)>=i2+1
                                        comp2=ComponentsTmp(i2+1,find(ComponentsTmp(i2+1,:)));
                                        newComp=cat(2,comp1,comp1(end):comp2(1),comp2);
                                        gap_tmp=1;
                                        i2=i2+2;
                                    else
                                        i2=i2+1;
                                        Stop=1;
                                    end
                                end
                            end
                        end
                    end
                end
                
            end
        end
    end
    
    if ~isempty(Components)
        nb_comp=0;
        for i2=1:size(Components,1)
            comp=Components(i2,find(Components(i2,:)));
            if length(comp)>1 && nb_comp<MaxComp
                nb_comp=nb_comp+1;
                LatStart(i1,nb_comp)=1000*comp(1)/Resp.fsample;
                LatEnd(i1,nb_comp)=1000*comp(end)/Resp.fsample;
                Duration(i1,nb_comp)=LatEnd(i1,nb_comp)-LatStart(i1,nb_comp);
                Integral(i1,nb_comp)=trapz(Response(i1,comp(1):comp(end)));
            end
        end
    end
    
end


% Duration(find(Duration==0))=NaN;
% Integral(find(Integral==0))=NaN;
% LatStart(find(LatStart==0))=NaN;


D1=clone(Resp, DurationOut, [Resp.nchannels size(Duration,2) 1]);
D1(:,:,:)=Duration;
D1=fsample(D1,1);
D1=timeonset(D1,0);
save(D1);

D2=clone(Resp, LatStartOut, [Resp.nchannels size(LatStart,2) 1]);
D2(:,:,:)=LatStart;
D2=fsample(D2,1);
D2=timeonset(D2,0);
save(D2);


D3=clone(Resp, IntegralOut, [Resp.nchannels size(Integral,2) 1]);
D3(:,:,:)=Integral;
D3=fsample(D3,1);
D3=timeonset(D3,0);
save(D3);



    function lc=ComputeClusters(l)
        
        lc=[];
        newC=1;
        for i3=1:length(l)
            
            if newC
                lc(size(lc,1)+1,1)=l(i3);
                newC=0;
                ind=1;
            else if l(i3)==l(i3-1)+1
                    ind=ind+1;
                    lc(size(lc,1),ind)=l(i3);
                else newC=1;
                end
            end
        end
    end
            

    function [b, numc]=BelongTo(l,e)
        
        b=0;
        numc=NaN;
        for i4=1:size(l,1)
            for i5=1:size(l,2)
                if e==l(i4,i5)
                    b=1;
                    numc=i4;
                end
            end
        end
        
    end

end
