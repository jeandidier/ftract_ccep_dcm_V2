addpath('/gin/data/software/matlab/spm12.7219');
spm('defaults','eeg');

bd                  = fileparts( mfilename('fullpath') ) ;
addpath( bd );
addpath( [ bd '/spm_dcm_dde23'] );


% ccepsRecNames       = 'significant' ;
% steps            	= 'all' ;
% peakLatencyMax      = 80 ; 
% threshold        	= 5 ;
% fitDurationMode     = 'm20_PLp2D_M40' ;
% % fitDurationMode     = 'm20_PLp2D_M80' ;
% % fitDurationMode     = 'm20_PLp40' ;
% % computationMode     = 'parallel' ;
% computationMode     = 'serial' ;
% 
% % dd                  = '/gin/data/database/04-processed/0022ROT/SEEG/StimLF_2014-07-06' ;
% % stimId              = 'FA1011_2mA_1Hz_1050us_1' ;
% dd                  = '/gin/data/database/04-processed/0023ROT/SEEG/StimLF_2015-02-01' ;
% stimId              = 'PS1617_2mA_1Hz_1050us_1' ;
% 
% stimFname           = [ dd '/SEQ_BM_ACPCHIP_F_ComputeAverage/averaged_zs_' stimId '.mat' ] ;
% probabilityFname    = [ dd '/SEQ_BM_ACPCHIP_F_PeaksCharac/Probability_' stimId '.mat' ] ;
% peakLatencyFname    = [ dd '/SEQ_BM_ACPCHIP_F_PeaksCharac/PeakDelay_' stimId '.mat' ] ;
% durationFname       = [ dd '/SEQ_BM_ACPCHIP_F_ComponentsCharac/Duration_' stimId '.mat' ] ;
% 
% dcmOutputDir        = [ '/tmp/DCM' ] ;
% dcmOutputListFname = [ dcmOutputDir '/cceps_significant' ] ;
% 
% prepare_spm_dcm_stim( ccepsRecNames, steps, peakLatencyMax, threshold, fitDurationMode, computationMode, stimFname, probabilityFname, peakLatencyFname, durationFname, dcmOutputDir, dcmOutputListFname ) ;


ccepsRecNames   = 'significant' ;
steps           = 'all' ;
peakLatencyMax  = 80 ;
threshold       = 5 ;
fitDurationMode = 'm20_PLp2D_M40' ;
computationMode = 'serial' ;

stimFname='/gin/data/database/04-processed/0022ROT/SEEG/StimLF_2014-07-06/SEQ_BM_ACPCHIP_F_ComputeAverage/averaged_zs_FA01-FA02_1mA_1Hz_1050us_1.mat' ;
probabilityFname='/gin/data/database/04-processed/0022ROT/SEEG/StimLF_2014-07-06/SEQ_BM_ACPCHIP_F_PeaksCharac/Probability_FA01-FA02_1mA_1Hz_1050us_1.mat' ;
peakLatencyFname='/gin/data/database/04-processed/0022ROT/SEEG/StimLF_2014-07-06/SEQ_BM_ACPCHIP_F_PeaksCharac/PeakDelay_FA01-FA02_1mA_1Hz_1050us_1.mat' ;
durationFname='/gin/data/database/04-processed/0022ROT/SEEG/StimLF_2014-07-06/SEQ_BM_ACPCHIP_F_ComponentsCharac/Duration_FA01-FA02_1mA_1Hz_1050us_1.mat' ;
recContactNameFname='/gin/data/database/04-processed/0022ROT/SEEG/StimLF_2014-07-06/SEQ_BM_ACPCHIP_F_DCM/FA01-FA02_1mA_1Hz_1050us_1/recContactName.txt' ;
recContactSegFname='/gin/data/database/04-processed/0022ROT/SEEG/StimLF_2014-07-06/SEQ_BM_ACPCHIP_F_DCM/FA01-FA02_1mA_1Hz_1050us_1/recContactSeg.txt' ;

stimFname='/gin/data/database/04-processed/0022ROT/SEEG/StimLF_2014-07-06/SEQ_BM_ACPCHIP_F_ComputeAverage/averaged_zs_FA03-FA04_4mA_1Hz_1050us_1.mat' ;
probabilityFname='/gin/data/database/04-processed/0022ROT/SEEG/StimLF_2014-07-06/SEQ_BM_ACPCHIP_F_PeaksCharac/Probability_FA03-FA04_4mA_1Hz_1050us_1.mat' ;
peakLatencyFname='/gin/data/database/04-processed/0022ROT/SEEG/StimLF_2014-07-06/SEQ_BM_ACPCHIP_F_PeaksCharac/PeakDelay_FA03-FA04_4mA_1Hz_1050us_1.mat' ;
durationFname='/gin/data/database/04-processed/0022ROT/SEEG/StimLF_2014-07-06/SEQ_BM_ACPCHIP_F_ComponentsCharac/Duration_FA03-FA04_4mA_1Hz_1050us_1.mat' ;
recContactNameFname='/gin/data/database/04-processed/0022ROT/SEEG/StimLF_2014-07-06/SEQ_BM_ACPCHIP_F_DCM/FA03-FA04_4mA_1Hz_1050us_1/recContactName.txt' ;
recContactSegFname='/gin/data/database/04-processed/0022ROT/SEEG/StimLF_2014-07-06/SEQ_BM_ACPCHIP_F_DCM/FA03-FA04_4mA_1Hz_1050us_1/recContactSeg.txt' ;


% dcmOutputDir='/gin/data/database/04-processed/0022ROT/SEEG/StimLF_2014-07-06/SEQ_BM_ACPCHIP_F_DCM/FA03-FA04_2mA_1Hz_1050us_1' ;
% dcmOutputListFname='/gin/data/database/04-processed/0022ROT/SEEG/StimLF_2014-07-06/SEQ_BM_ACPCHIP_F_DCM/FA03-FA04_2mA_1Hz_1050us_1/cceps_significant' ;
dcmOutputDir        = [ '/tmp/DCM' ] ;
dcmOutputListFname = [ dcmOutputDir '/cceps_significant' ] ;

prepare_spm_dcm_stim( ccepsRecNames, steps, peakLatencyMax, threshold, fitDurationMode, computationMode, stimFname, probabilityFname, peakLatencyFname, durationFname, recContactNameFname, recContactSegFname, dcmOutputDir, dcmOutputListFname ) ;

