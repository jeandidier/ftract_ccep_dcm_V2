function [ lognormal_mean, lognormal_mode ] = lognormal( normal_mean, normal_var )

lognormal_mean = exp( normal_mean + normal_var / 2 ) ;
lognormal_mode = exp( normal_mean - normal_var ) ;

end
