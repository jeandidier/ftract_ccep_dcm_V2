function prepare_spm_dcm_stim( ccepsRecNames, steps, peakLatencyMax, threshold, fitDurationMode, computationMode, stimFname, probabilityFname, peakLatencyFname, durationFname, recContactNameFname, recContactSegFname, dcmOutputDir, dcmOutputListFname )

% cf modification de PeaksCharac.m pour tomber sur bon sample


%fprintf( 1, [ 'Initial path\n' ] ) ;
%path


if isdeployed
    spm('defaults','eeg');
    
    ft_dcm_dir = fileparts( mfilename('fullpath') ) ;
    addpath( [ ft_dcm_dir ] ) ;
    addpath( [ ft_dcm_dir '/spm_dcm_dde23' ] ) ;
end


fprintf( 1, [ 'Working path\n' ] ) ;
path


fprintf( 1, [ 'Entering prepare_spm_dcm_stim\n' ] ) ;



if strcmp(steps, 'all')
    steps=[ 1 2 ];
elseif strcmp(steps, '1')
    steps=[ 1 ];
elseif strcmp(steps, '2')
    steps=[ 2 ];    
else
    error('invalid steps parameter');
end


if ischar(peakLatencyMax)
    peakLatencyMax = str2num(peakLatencyMax);
end
if ischar(threshold)
    threshold = str2num(threshold);
end

ccepsRecNames
steps
peakLatencyMax
threshold
fitDurationMode
computationMode
stimFname
probabilityFname
peakLatencyFname
durationFname
recContactNameFname
recContactSegFname
dcmOutputDir
dcmOutputListFname


% cceps to fit

if strcmp(ccepsRecNames, 'significant')
    ccepsInd    = ccepsIndexes( peakLatencyMax, threshold, probabilityFname, peakLatencyFname, recContactNameFname, recContactSegFname ) ;
else
    ccepsRecNames=strsplit(ccepsRecNames,'____');
    D              = spm_eeg_load( stimFname ) ;
    ccepsInd = zeros(size(D.chanlabels)) ;
    for c=1:length(ccepsInd)
        ccepsInd(c) = ~isempty( find( cellfun( @(r) strcmp( D.chanlabels{c}, r ), ccepsRecNames ) ) ) ;
    end 
    ccepsInd = find( ccepsInd ) ;
end


ccepsInd
% D.chanlabels(find(ccepsInd))



% create dcmOutputDir
fprintf( 1, [ 'Creating ' dcmOutputDir '\n' ] ) ;
mkdir( [ dcmOutputDir ] ) ;

fprintf( 1, [ 'Write ' dcmOutputListFname '.txt' '\n' ] ) ;
fd = fopen( [ dcmOutputListFname '.txt' ], 'w' ) ;
if ~isempty(ccepsInd)
    D  = spm_eeg_load( stimFname ) ;
%     fprintf( fd, '%s\n', D.chanlabels(ccepsInd) ) ;
    tmp = D.chanlabels(ccepsInd) ;
    fprintf( fd, '%s\n', tmp{:} ) ;
end
fclose( fd ) ;

if isempty(ccepsInd)
    fprintf( 1, [ 'ccepsInd is empty => nothing to do' '\n' ] ) ;
    set_final_status('OK');
    close all
    return
end


% create individual ccep
fprintf( 1, [ 'Creating ' dcmOutputDir '/cceps' '\n' ] ) ;
mkdir( [ dcmOutputDir '/cceps' ] ) ;
ccepsFnames = createCceps( stimFname, ccepsInd, dcmOutputDir ) ;


fprintf( 1, '%s\n', ccepsFnames{:} )


% peak latencies
D_peak 	= spm_eeg_load( peakLatencyFname ) ;
% significance durations
D_dur   = spm_eeg_load( durationFname ) ;

peakLatencies  	= D_peak(ccepsInd,threshold);
sigDurations    = D_dur(ccepsInd);



% A DISCUTER !!!
% peakLatency + 2 * duration avec un maximum de 40 ms
% minimum de 20ms
if strcmp( fitDurationMode, 'm20_PLp2D_M40' )
    fitDurations = max( 20, peakLatencies + min( 2 * sigDurations, 40 ) ) ;
elseif strcmp( fitDurationMode, 'm20_PLp2D_M80' )
    fitDurations = max( 20, peakLatencies + min( 2 * sigDurations, 80 ) ) ;
elseif strcmp( fitDurationMode, 'm20_PLp40' )
    fitDurations = max( 20, peakLatencies + 40 ) ;
else
    error( 'Invalid fit duration mode' ) ;
end


% plot ccep
% plot_cceps = true ;
plot_cceps = false ;
if plot_cceps
   plotCceps( stimFname, ccepsInd, peakLatencies, fitDurations ) ;
end




[ dcmFnames_step1, dcmFnames_step2 ] = spm_dcm_stim( ccepsFnames, peakLatencies, fitDurations, dcmOutputDir, computationMode, steps ) ;

do_step1 = ~isempty(find(steps==1));
if do_step1
    fd = fopen( [ dcmOutputListFname '_step1.txt' ], 'w' ) ;
    fprintf( fd, '%s\n', dcmFnames_step1{:} ) ;
    fclose( fd ) ;
end
do_step2 = ~isempty(find(steps==2));
if do_step2
    fd = fopen( [ dcmOutputListFname '_step2.txt' ], 'w' ) ;
    fprintf( fd, '%s\n', dcmFnames_step2{:} ) ;
    fclose( fd ) ;
end


set_final_status('OK');

close all

end


function ccepsInd = ccepsIndexes( peakLatencyMax, threshold, probabilityFname, peakLatencyFname, recContactNameFname, recContactSegFname )

D_prob      = spm_eeg_load( probabilityFname ) ;
D_peak      = spm_eeg_load( peakLatencyFname ) ;
if ( size(D_prob,2) < threshold )
    ccepsInd = [] ;
    return
end
ccepsInd    = find( D_prob(:,threshold) & ( D_peak(:,threshold) < peakLatencyMax ) ) ;

% remove bad channels from ccepsInd !!!
ccepsInd    = setdiff(ccepsInd,D_prob.badchannels) ;

% keep contacts in grey matter only
fd          = fopen(recContactNameFname,'r');
name        = textscan(fd,'%s');
name        = name{1};
fclose(fd);
fd          = fopen(recContactSegFname,'r');
seg         = textscan(fd,'%s');
seg         = seg{1};
fclose(fd);

ccepsInd    = ccepsInd( arrayfun( @(n) strcmp(seg(strcmp(n,name)),'GreyMatter'), D_prob.chanlabels(ccepsInd) ) );

end


function ccepsFnames = createCceps( stimFname, ccepsInd, dcmOutputDir )

ccepsFnames      = {} ;
D               = spm_eeg_load( stimFname ) ;
% Following is removed because isempty(ccepsInd) means that cceps should
% not been created
% if isempty(ccepsInd)
%     ccepsInd    = setdiff( 1:D.nchannels, D.badchannels ) ;
% end
[ ~, name, ~ ]  = fileparts( stimFname ) ;
for i=1:length(ccepsInd)
   ind          = ccepsInd(i);
   chname       = D.chanlabels{ind};
   fnamedat     = [ dcmOutputDir '/cceps/' name '_' chname '.mat' ] ;
   ccep         = clone( D, fnamedat, [ 1 D.nsamples D.ntrials ] );
   ccep         = chanlabels( ccep, 1, chname );
   ccep(:,:,:)  = D(ind,:,:) ;
   save(ccep);
   ccepsFnames  = [ ccepsFnames fnamedat ] ;
end

end


function plotCceps( stimFname, ccepsInd, peak, fitDurations )

D = spm_eeg_load( stimFname ) ;
for i=1:length(ccepsInd)
    ccepInd = ccepsInd(i);
    
    startSample = indsample( D, -20 * 1e-3 ) ;
    endSample   = indsample( D, 120 * 1e-3 ) ;
%     endSample   = indsample( D, ( fitDurations(i) + 20 ) * 1e-3 ) ;
    H = figure ;
    plot( D.time(startSample:endSample), D(ccepInd,startSample:endSample) );
    hold on ;
    line( [ -20e-3 120e-3 ]', [ 0 0 ]', 'Color', 'k' ) ;
    line( [ -20e-3 120e-3 ]', [ -5 -5 ]', 'Color', 'r' ) ;
    line( [ -20e-3 120e-3 ]', [ 5 5 ]', 'Color', 'r' ) ;
    ylim = get( gca, 'YLim' ) ;
    line( [ 0 0 ]', ylim', 'Color', 'g' ) ;
    line( [ peak(i) peak(i) ]'*1e-3, ylim', 'Color', 'b' ) ;
    line( [ fitDurations(i) fitDurations(i) ]'*1e-3, ylim', 'Color', 'g' ) ;
    
    title( D.chanlabels{ccepInd} );
%     close (H);
end

end


