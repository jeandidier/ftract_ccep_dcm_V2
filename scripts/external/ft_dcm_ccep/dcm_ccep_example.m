addpath( '/Users/jd/Desktop/untitled folder/spm_software/spm/spm12_r6732/spm12' );
spm( 'defaults', 'eeg' );

bd                  = fileparts( mfilename('fullpath') ) ;
addpath( bd );
addpath( [ bd '/spm_dcm_dde23'] );


ccepsRecNames       = 'significant' ;
steps            	= 'all' ;
peakLatencyMax      = 80 ; 
threshold           = 5 ;
fitDurationMode     = 'm20_PLp2D_M40' ;
% fitDurationMode     = 'm20_PLp2D_M80' ;
% fitDurationMode     = 'm20_PLp40' ;
% computationMode     = 'parallel' ;
computationMode     = 'serial' ;
% computationMode     = '' ;



stimPat             = '0022ROT' ;
stimDate            = '2014-07-06' ;

stimDir             = [ '/Users/jd/these/F-TRACT/ftract_ccep_dcm_V2/data/PATS__V1/' stimPat '/SEEG/StimLF_' stimDate '/SEQ_BM_ACPCHIP_F_ComputeAverage' ] ;
% stimId              = 'FA01-FA02_1mA_1Hz_1050us_1' ;
stimId              = 'FA03-FA04_4mA_1Hz_1050us_1' ;

stimFname           = [ stimDir '/' stimId '/averaged_zs_' stimId '.mat' ] ;
probabilityFname    = [ stimDir '/' stimId '/Probability_' stimId '.mat' ] ;
peakLatencyFname    = [ stimDir '/' stimId '/PeakDelay_' stimId '.mat' ] ;
durationFname       = [ stimDir '/' stimId '/Duration_' stimId '.mat' ] ; 
recContactNameFname = [ stimDir '/' stimId '/recContactName.txt' ] ; 
recContactSegFname  = [ stimDir '/' stimId '/recContactSeg.txt' ] ; 

dcmOutputDir        = [ '/Users/jd/these/F-TRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/' stimPat '/SEEG/StimLF_' stimDate '/SEQ_BM_ACPCHIP_F_DCM/' stimId ];
dcmOutputListFname  = [ dcmOutputDir '/cceps_significant' ] ;

prepare_spm_dcm_stim( ccepsRecNames, steps, peakLatencyMax, threshold, fitDurationMode, computationMode, stimFname, probabilityFname, peakLatencyFname, durationFname, recContactNameFname, recContactSegFname, dcmOutputDir, dcmOutputListFname )



%% generate characs if needed
mkdir( [ bd '/data/tmp' ] ) ;

threshold       = 0 ;
MaxPeak         = nan ;
ResponseZSp     = stimFname ;
PeakDelayOut    = [ bd '/data/tmp/PeakDelayOut.mat' ] ;
OnsetDelayOut   = [ bd '/data/tmp/OnsetDelayOut.mat' ] ;
AmplOut         = [ bd '/data/tmp/AmplOut.mat' ] ;
SlopeOut        = [ bd '/data/tmp/SlopeOut.mat' ] ;
ProbabilityOut  = [ bd '/data/tmp/ProbabilityOut.mat' ] ;

[Delay, DelayOnset, Ampl, Slope, Probability]=PeaksCharac(threshold, MaxPeak, ResponseZSp, PeakDelayOut, OnsetDelayOut, AmplOut, SlopeOut, ProbabilityOut) ;


Hthreshold      = 5 ;
Lthreshold      = 4 ;
gap             = 0.005 ;
MaxComp         = 1 ;
Responsep       = stimFname ;
DurationOut  	= [ bd '/data/tmp/DurationOut.mat' ] ;
LatStartOut     = [ bd '/data/tmp/LatStartOut.mat' ] ;
IntegralOut     = [ bd '/data/tmp/IntegralOut.mat' ] ;

[Duration, LatStart, Integral]=ComponentsCharac(Hthreshold, Lthreshold, gap, MaxComp, Responsep, DurationOut, LatStartOut, IntegralOut) ;
