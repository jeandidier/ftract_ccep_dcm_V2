function [ R2, R2_c, R2_m0, R2_c_m0 ] = compute_accuracy( DCM, time_span_ms )

% FORCE MEAN = 0

y   = DCM.H ;
r   = DCM.R ;
Nt  = size(y,2) ;
    
% reduce the computation to time_span
if ~isempty(time_span_ms)
    [~,start_sample]=min(abs(DCM.xY.pst - time_span_ms(1)));
    [~,end_sample]=min(abs(DCM.xY.pst - time_span_ms(2)));   
    
    for i=1:Nt
        y{i} = y{i}(start_sample:end_sample,:) ;
        r{i} = r{i}(start_sample:end_sample,:) ;
    end
end



% Assess accuracy; signal to noise (over sources), SSE and log-evidence
%----------------------------------------------------------------------
for i = 1:Nt % sum over trials
    SSR(i) = sum(var(r{i}));
    SST(i) = sum(var(y{i} + r{i}));
    
    SSR_c(i,:) = (var(r{i}));
    SST_c(i,:) = (var(y{i} + r{i}));
    
    
    % force the mean to be zero for observed and predicted
%     fprintf( 1, [ 'Mean observed: ' num2str(mean(y{i} + r{i})) ' - Mean predicted: ' num2str(mean(y{i})) '\n' ] ) ;
        
    SSR_m0(i) = sum( sum( r{i}.^2)          / (length(r{i})-1) );
    SST_m0(i) = sum( sum( (y{i}+r{i}).^2 )  / (length(r{i})-1) );
    
    SSR_c_m0(i,:) = sum(r{i}.^2)            / (length(r{i})-1) ;
    SST_c_m0(i,:) = sum((y{i} + r{i}).^2)   / (length(r{i})-1) ;
end
R2      = 100*(sum(SST - SSR))/sum(SST);
R2_c    = 100*(sum(SST_c - SSR_c,1))./sum(SST_c,1);
R2_m0   = 100*(sum(SST_m0 - SSR_m0))/sum(SST_m0);
R2_c_m0 = 100*(sum(SST_c_m0 - SSR_c_m0,1))./sum(SST_c_m0,1);
end
