function DCM = stimDCM_V14( config )
% function DCM = stimDCM_V14( dataFname, pF_D, pC_D, pE_T, pE_G, pC_T, pC_G, pC_S, pC_H, config, fit_duration )

V               = 'V14' ;

[ ~, ~, fit_IS_RK_reltol_1em2 ] = integration_schemes() ;
fit_IS                          = fit_IS_RK_reltol_1em2 ;
fit_IS.options.lags_opt         = 1 ;
pyr_only                        = 1 ; 



DCM         = buildDCM( config.dataFname, fit_IS, config.fit_duration ) ; %, Nc ) ; % , stimulation ) ;
% dcmFname    = eval( [ 'buildDCMFname_' V '( dataFname, pF_D, pC_D, pE_T, pE_G, pC_T, pC_G, pC_S, pC_H, config, fit_duration.id ) ;' ] ) ;
DCM.name  	= dcmFname( config.dataFname, config ) ;




%% specify fixed parameters
% pF          = struct ;
% pF.D        = pF_D.pF_D ;
% DCM.M.pF 	= pF ;
DCM.M.pF 	= config.pF ;


%% setup neural priors
[ pE, pC ]      = spm_dcm_neural_priors( DCM.A, DCM.B, DCM.C, 'ERP' );
pE.T = config.pE.T ; %     pE_T ;
pE.G = config.pE.G ; % pE_G ;
pC.T = config.pC.T ; % pC_T ;
pC.G = config.pC.G ; % pC_G ;
pC.S = config.pC.S ; % pC_S ;
pC.H = config.pC.H ; % pC_H ;
pC.D = config.pC.D ; % pC_D ;
pC.R = [ 0 0 ] ;

DCM.M.pE = pE ;
DCM.M.pC = pC ;



if isfield(config,'step')
    if config.step == 2
        DCM.M.pE.G(1,:) = config.DCM_avg_ffx.Ep.G(1,:);
        DCM.M.pC.G(1,:) = 0 ;
        
        DCM.M.pE.T(1,:) = config.DCM_avg_ffx.Ep.T(1,:);
        DCM.M.pC.T(1,:) = 0 ;
        
        DCM.M.pE.S      = config.DCM_avg_ffx.Ep.S ;
        DCM.M.pC.S(:)   = 0 ;
        
        DCM.M.pE.C(1)   = config.DCM_avg_ffx.Ep.C(1) ;
        DCM.M.pC.C(1)   = 0 ;
        
        DCM.M.pE.H      = config.DCM_avg_ffx.Ep.H ;
        DCM.M.pC.H(:)   = 0 ;
    end
end


% spatial priors
[ gE, gC ]  = spm_L_priors(DCM.M.dipfit);
if pyr_only
    gC.J(:) = 0 ;
end
DCM.M.gE    = gE ;
DCM.M.gC    = gC ;




%% fit the data
[ dcmDir, dcmId ]   = fileparts( DCM.name ) ;
diaryFile           = [ dcmDir '/' dcmId '.log' ] ;
system( [ 'rm -f ' diaryFile ] ) ;
diary( diaryFile );

fprintf( 1, [ 'Input : ' config.dataFname '\n' ] );
fprintf( 1, [ 'Output : ' DCM.name '\n' ] );
fprintf( 1, [ 'Integration scheme: ' IS_as_txt( fit_IS ) '\n' ] );

which('spm_dcm_erp')
tic
DCM = spm_dcm_erp( DCM );
toc

diary off ;

[ ~, ~, ~, R2_c_m0 ] = compute_accuracy( DCM, [] ) ;
fprintf( 1, [ '\t=> Accuracy (R2) = ' num2str(R2_c_m0) ' %% \n' ] ) ;
fprintf( 1, [ '\t=> ' DCM.name '\n\n'  ] ) ;
fprintf( 1, [ 'stimDCM_' V ' FINAL STATUS : OK' '\n' ] ) ;



end






function DCM = buildDCM( dataFname, fit_IS, fit_duration ) % , Nrec ) % , stimulation )

D                       = spm_eeg_load( dataFname ) ;
Nc                      = length(D.chanlabels) ;
Ns                      = Nc + 1 ;


DCM                     = struct ;

DCM.xY.Dfile            = dataFname ;

DCM.options.analysis    = 'ERP' ;
DCM.options.model       = 'ERP' ;
DCM.options.spatial     = 'LFP' ;
DCM.options.trials      = 1 ;
DCM.options.Tdcm(1)     = 0 ;
DCM.options.Tdcm(2)     = fit_duration ;

DCM.options.h           = 0 ;       
DCM.options.onset       = 0 ; 
DCM.options.dur         = 2 ; 
DCM.options.D           = 1 ;
DCM.options.han         = 0 ;
DCM.options.location    = 0 ;
DCM.options.symmetry    = 0 ;
DCM.options.lock        = 0 ;
DCM.options.Nmax        = 512 ;
% DCM.options.Fdcm        = [4 48] ;

% silent stimulation source
A{1}            = zeros(Ns,Ns) ;
A{1}(2:end,1)   = 1 ;
A{2}            = zeros(Ns,Ns) ;
A{3}            = zeros(Ns,Ns) ;
B{1}            = zeros(Ns,Ns) ;
C               = zeros(Ns,1) ;
C(1,1)          = 1 ;
    
DCM.A           = A ;
DCM.B           = B ;
DCM.C           = C ;



% WARNING: if the name contains silent, it is silent (nonsilent becomes
% silent...)
% DCM.Sname  	= [ 'stim_silent' arrayfun( @(x) { [ 'ccep' num2str(x) ] }, 1:Nc ) ] ;
DCM.Sname  	= [ 'stim_silent' D.chanlabels ] ;



if isequal( fit_IS.function, @spm_gen_erp )
    M.IS        = 'spm_gen_erp' ;
elseif isequal( fit_IS.function, @spm_gen_erp_dde23 )
    M.IS        = 'spm_gen_erp_dde23' ;
    M.dde23_options = fit_IS.options ;
else
    error('invalid IS') ;
end


%% spatial
spatial_model   = 'LFP' ;
M.dipfit.model  = 'ERP' ;
M.dipfit.type   = spatial_model ;
M.dipfit.Ns     = Ns;   % number of sources
M.dipfit.Nc     = Nc;   % number of channels
%
M.dipfit.silent_source      = strfind(DCM.Sname,'silent');
M.dipfit.silent_source{1,1} = 1 ; 


DCM.M   = M ;

end

