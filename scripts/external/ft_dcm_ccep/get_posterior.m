function [ conductionDelay, synapticTe, synapticTi ] = get_posterior( DCM )


pF_T            = [ 8 16 ];
pF_Te           = pF_T(1) ;
pF_Ti           = pF_T(2) ;


Cp                      = spm_unvec(diag(DCM.Cp),DCM.Ep) ;
[ ~, lognormal_mode ]   = lognormal( DCM.Ep.D(2,1) + log( DCM.M.pF.D(2) ), Cp.D(2,1) ) ;
conductionDelay         = lognormal_mode ;
[ ~, lognormal_mode ] 	= lognormal( DCM.Ep.T(2,1) + log( pF_Te ), Cp.T(2,1) ) ;
synapticTe              = lognormal_mode ;
[ ~, lognormal_mode ]  	= lognormal( DCM.Ep.T(2,2) + log( pF_Ti ), Cp.T(2,2) ) ;
synapticTi              = lognormal_mode ;

end