function buildDCMsAtlas( DCMs, atlasId, ageMin, ageMax, symmetric, cceps_min )
% atlasId='MarsAtlas';
% atlasId='HCP-MMP1';
% atlasId='Lausanne2008-33';
% atlasId='Lausanne2008-60';
% atlasId='Lausanne2008-125';
% atlasId='Lausanne2008-250';
% atlasId='Lausanne2008-500';


% doClustering=true ;
doClustering=false ;


only_plot = true ;
% only_plot = false ;
if only_plot
    plot_results( atlasId, ageMin, ageMax, symmetric, cceps_min ) ;
    return
end


bd = baseDir();





if doClustering

    if isempty( DCMs )
        fname   = fullfile( bd, 'dcm/PATS__V1__DCMs.mat') ;
        DCMs    = load(fname) ;
        DCMs    = DCMs.DCMs ;
    end



    % minPL       = 4 ;
    minPL       = 0 ;
    maxPL       = 80 ;
    accMin      = 70 ;
    peakDiffMax = 5 ;

    parcelsName     = getParcelsName( atlasId, false ) ;
    parcelsCount    = length(parcelsName) ;

    peakLatency_parcels         = cell(parcelsCount,parcelsCount) ;
    conduction_delays_parcels   = cell(parcelsCount,parcelsCount) ;
    synapticTe_parcels          = cell(parcelsCount,parcelsCount) ;
    synapticTi_parcels          = cell(parcelsCount,parcelsCount) ;
    distance_parcels            = cell(parcelsCount,parcelsCount) ;

    for crf=1:length(DCMs)
        fprintf( 1, [ num2str(crf) '/' num2str(length(DCMs)) '\n' ] ) ;

        for s=1:length(DCMs(crf).stims)

            if ~ ( ageMin <= DCMs(crf).stims{s}.age && DCMs(crf).stims{s}.age <= ageMax ), continue ; end

            try
                [ ~, stim_parcelId ] = parseAtlasParcel( DCMs(crf).stims{s}.parc, atlasId, false ) ;
            catch ME
                if strcmp( ME.identifier, 'MATLAB:ParcelNotDefined' )
                    fprintf( 1, [ 'Stim ' ME.message '\n' ] ) ;
                    continue
                elseif symmetric && strcmp( ME.identifier, 'MATLAB:ParcelNotFound' )
                    fprintf( 1, [ 'Stim ' ME.message '\n' ] ) ;
                    continue
                else
                    rethrow(ME) ;
                end
            end


            for cc=1:length(DCMs(crf).stims{s}.cceps)

                if ~ ( minPL <= DCMs(crf).stims{s}.cceps{cc}.peakLat && DCMs(crf).stims{s}.cceps{cc}.peakLat <= maxPL ), continue ; end
                if DCMs(crf).stims{s}.cceps{cc}.s2_acc < accMin, continue ; end
                if abs(DCMs(crf).stims{s}.cceps{cc}.peakLat-DCMs(crf).stims{s}.cceps{cc}.s2_peak)>peakDiffMax, continue ; end

                try
                    [ ~, rec_parcelId ] = parseAtlasParcel( DCMs(crf).stims{s}.cceps{cc}.parc, atlasId, false ) ;
                catch ME
                    if strcmp( ME.identifier, 'MATLAB:ParcelNotDefined' )
                        fprintf( 1, [ 'Rec ' ME.message '\n' ] ) ;
                        continue
                    elseif symmetric && strcmp( ME.identifier, 'MATLAB:ParcelNotFound' )
                        fprintf( 1, [ 'Rec ' ME.message '\n' ] ) ;
                        continue
                    else
                        rethrow(ME) ;
                    end
                end


    %             fprintf( 1, [ 'avant ' num2str(stim_parcelId) ' ' num2str(rec_parcelId) '\n' ] ) ;
    %             if symmetric && stim_parcelId > parcelsCount/2
    %                 stim_parcelId = stim_parcelId - parcelsCount/2 ;
    %                 if rec_parcelId > parcelsCount/2
    %                     rec_parcelId = rec_parcelId - parcelsCount/2 ;
    %                 else
    %                     rec_parcelId = rec_parcelId + parcelsCount/2 ;
    %                 end
    %                 continue
    %             end
    %             fprintf( 1, [ 'apres ' num2str(stim_parcelId) ' ' num2str(rec_parcelId) '\n' ] ) ;


                peakLatency_parcels{stim_parcelId,rec_parcelId}(end+1)          = DCMs(crf).stims{s}.cceps{cc}.peakLat ;
                conduction_delays_parcels{stim_parcelId,rec_parcelId}(end+1) 	= DCMs(crf).stims{s}.cceps{cc}.conductionDelay ;
                synapticTe_parcels{stim_parcelId,rec_parcelId}(end+1)           = DCMs(crf).stims{s}.cceps{cc}.synapticTe ;
                synapticTi_parcels{stim_parcelId,rec_parcelId}(end+1)           = DCMs(crf).stims{s}.cceps{cc}.synapticTi ;
    %             distance_parcels{stim_parcelId,rec_parcelId}(end+1)             = norm( DCMs(crf).stims{s}.pos - DCMs(crf).stims{s}.cceps{cc}.pos ) ;
    %             distance_parcels{stim_parcelId,rec_parcelId}(end+1)             = norm( DCMs(crf).stims{s}.pos_scan - DCMs(crf).stims{s}.cceps{cc}.pos_scan ) ;
                distance_parcels{stim_parcelId,rec_parcelId}(end+1)             = DCMs(crf).stims{s}.cceps{cc}.stim_ccep_dist ;

            end
        end
    end


    if symmetric 

        if strcmp( atlasId, 'Lausanne2008-60' )

            fname       = fullfile( bd, 'scripts/parcellations/anatomy_sorted', [ atlasId '-sym.txt' ] ) ;
            parcelsSym  = fileread(fname);
            parcelsSym  = strtrim(parcelsSym);
            parcelsSym  = strsplit(parcelsSym,'\n');
            parcelsSym(find( strcmp( parcelsSym, 'Brain-Stem' ) ) ) = [] ;
            parcelsSym(find( strcmp( parcelsSym, 'Unknown' ) ) )    = [] ;


            % on fabrique les matrices symmetriques
            H = length(parcelsSym) ;

            sym_peakLatency_parcels         = cell( H, H + H ) ;
            sym_conduction_delays_parcels   = cell( H, H + H ) ;
            sym_distance_parcels            = cell( H, H + H ) ;
            sym_synapticTe_parcels          = cell( H, H + H ) ;
            sym_synapticTi_parcels          = cell( H, H + H ) ;

            sr_pairs                        = zeros( size( peakLatency_parcels ) ) ;

            for stimParcel = 1:H

                stimParcels     = strsplit(parcelsSym{stimParcel},'\t\t') ;
                stimParcels_L   = strsplit(stimParcels{1}) ;
                stimParcels_R   = strsplit(stimParcels{2}) ;

                % fprintf( 1, [ strjoin( stimParcels_L, ' ' ) ' AND ' strjoin( stimParcels_R, ' ' ) '\n' ] ) ;

                for recParcel = 1:H

                    recParcels      = strsplit(parcelsSym{recParcel},'\t\t') ;
                    recParcels_L    = strsplit(recParcels{1}) ;
                    recParcels_R    = strsplit(recParcels{2}) ;

                    ipsi_sym_peakLatency_parcels            = [] ;
                    contra_sym_peakLatency_parcels          = [] ;
                    ipsi_sym_conduction_delays_parcels      = [] ;
                    contra_sym_conduction_delays_parcels    = [] ;
                    ipsi_sym_distance_parcels               = [] ;
                    contra_sym_distance_parcels             = [] ;
                    ipsi_sym_synapticTe_parcels             = [] ;
                    contra_sym_synapticTe_parcels           = [] ;
                    ipsi_sym_synapticTi_parcels             = [] ;
                    contra_sym_synapticTi_parcels           = [] ;


                    % stim left
                    for sti = 1:length(stimParcels_L)
                        s = find( strcmp( parcelsName, stimParcels_L{sti} ) ) ;
                        % ipsi - rec left
                        for rec = 1:length(recParcels_L)
                            r                                       = find( strcmp( parcelsName, recParcels_L{rec} ) ) ;
                            ipsi_sym_peakLatency_parcels            = horzcat( ipsi_sym_peakLatency_parcels, peakLatency_parcels{s,r} ) ;
                            ipsi_sym_conduction_delays_parcels      = horzcat( ipsi_sym_conduction_delays_parcels, conduction_delays_parcels{s,r} ) ;
                            ipsi_sym_distance_parcels               = horzcat( ipsi_sym_distance_parcels, distance_parcels{s,r} ) ;
                            ipsi_sym_synapticTe_parcels             = horzcat( ipsi_sym_synapticTe_parcels, synapticTe_parcels{s,r} ) ;
                            ipsi_sym_synapticTi_parcels             = horzcat( ipsi_sym_synapticTi_parcels, synapticTi_parcels{s,r} ) ;
                            sr_pairs(s,r)                           = 1 ;
                        end
                        % contra - rec right
                        for rec = 1:length(recParcels_R)
                            r                                       = find( strcmp( parcelsName, recParcels_R{rec} ) ) ;
                            contra_sym_peakLatency_parcels          = horzcat( contra_sym_peakLatency_parcels, peakLatency_parcels{s,r} ) ;
                            contra_sym_conduction_delays_parcels  	= horzcat( contra_sym_conduction_delays_parcels, conduction_delays_parcels{s,r} ) ;
                            contra_sym_distance_parcels             = horzcat( contra_sym_distance_parcels, distance_parcels{s,r} ) ;
                            contra_sym_synapticTe_parcels           = horzcat( contra_sym_synapticTe_parcels, synapticTe_parcels{s,r} ) ;
                            contra_sym_synapticTi_parcels           = horzcat( contra_sym_synapticTi_parcels, synapticTi_parcels{s,r} ) ;
                            sr_pairs(s,r)                           = 1 ;
                        end
                    end

                    % stim right
                    for sti = 1:length(stimParcels_R)
                        s = find( strcmp( parcelsName, stimParcels_R{sti} ) ) ;
                        % ipsi - rec right
                        for rec = 1:length(recParcels_R)
                            r                                       = find( strcmp( parcelsName, recParcels_R{rec} ) ) ;
                            ipsi_sym_peakLatency_parcels            = horzcat( ipsi_sym_peakLatency_parcels, peakLatency_parcels{s,r} ) ;
                            ipsi_sym_conduction_delays_parcels      = horzcat( ipsi_sym_conduction_delays_parcels, conduction_delays_parcels{s,r} ) ;
                            ipsi_sym_distance_parcels               = horzcat( ipsi_sym_distance_parcels, distance_parcels{s,r} ) ;
                            ipsi_sym_synapticTe_parcels             = horzcat( ipsi_sym_synapticTe_parcels, synapticTe_parcels{s,r} ) ;
                            ipsi_sym_synapticTi_parcels             = horzcat( ipsi_sym_synapticTi_parcels, synapticTi_parcels{s,r} ) ;
                            sr_pairs(s,r)                           = 1 ;
                        end
                        % contra - rec left
                        for rec = 1:length(recParcels_L)
                            r       = find( strcmp( parcelsName, recParcels_L{rec} ) ) ;
                            contra_sym_peakLatency_parcels          = horzcat( contra_sym_peakLatency_parcels, peakLatency_parcels{s,r} ) ;
                            contra_sym_conduction_delays_parcels  	= horzcat( contra_sym_conduction_delays_parcels, conduction_delays_parcels{s,r} ) ;
                            contra_sym_distance_parcels             = horzcat( contra_sym_distance_parcels, distance_parcels{s,r} ) ;
                            contra_sym_synapticTe_parcels           = horzcat( contra_sym_synapticTe_parcels, synapticTe_parcels{s,r} ) ;
                            contra_sym_synapticTi_parcels           = horzcat( contra_sym_synapticTi_parcels, synapticTi_parcels{s,r} ) ;
                            sr_pairs(s,r)                           = 1 ;
                        end
                    end

                    sym_peakLatency_parcels{ stimParcel, recParcel }            = ipsi_sym_peakLatency_parcels ;
                    sym_peakLatency_parcels{ stimParcel, H + recParcel }        = contra_sym_peakLatency_parcels ;

                    sym_conduction_delays_parcels{ stimParcel, recParcel }   	= ipsi_sym_conduction_delays_parcels ;
                    sym_conduction_delays_parcels{ stimParcel, H + recParcel } 	= contra_sym_conduction_delays_parcels ;

                    sym_distance_parcels{ stimParcel, recParcel }               = ipsi_sym_distance_parcels ;
                    sym_distance_parcels{ stimParcel, H + recParcel }           = contra_sym_distance_parcels ;

                    sym_synapticTe_parcels{ stimParcel, recParcel }             = ipsi_sym_synapticTe_parcels ;
                    sym_synapticTe_parcels{ stimParcel, H + recParcel }         = contra_sym_synapticTe_parcels ;

                    sym_synapticTi_parcels{ stimParcel, recParcel }             = ipsi_sym_synapticTi_parcels ;
                    sym_synapticTi_parcels{ stimParcel, H + recParcel }         = contra_sym_synapticTi_parcels ;

                end

            end


            tmp_synTe = cell( 1, H ) ;
            tmp_synTi = cell( 1, H ) ;
            for rec = 1:H
                tmp_synTe{rec} = horzcat( sym_synapticTe_parcels{:,rec}, sym_synapticTe_parcels{:,H+rec} ) ;
                tmp_synTi{rec} = horzcat( sym_synapticTi_parcels{:,rec}, sym_synapticTi_parcels{:,H+rec} ) ;
            end
            sym_synapticTe_parcels = tmp_synTe ;
            sym_synapticTi_parcels = tmp_synTi ;



            % check sym_peakLatency_parcels
            tmp_all = peakLatency_parcels(1:128,1:128) ;
            tmp_all = horzcat( tmp_all{:} ) ;
            tmp_sym = horzcat( sym_peakLatency_parcels{:} ) ;
            if numel( tmp_all ) ~= numel( tmp_sym ) || ...
                max( abs( sort(tmp_all) - sort(tmp_sym) ) ) ~= 0 || ...
                ~isempty( find(sr_pairs(1:128,1:128)==0) )
                error( 'Invalid sym_peakLatency_parcels') ;
            end
            fprintf( 1, [ 'Mean sym_peakLatency_parcels: ' num2str(mean( tmp_sym )) ' (#' num2str(numel( tmp_sym )) ')\n' ] ) ;


            % check sym_conduction_delays_parcels
            tmp_all = conduction_delays_parcels(1:128,1:128) ;
            tmp_all = horzcat( tmp_all{:} ) ;
            tmp_sym = horzcat( sym_conduction_delays_parcels{:} ) ;
            if numel( tmp_all ) ~= numel( tmp_sym ) || ...
                max( abs( sort(tmp_all) - sort(tmp_sym) ) ) ~= 0 || ...
                ~isempty( find(sr_pairs(1:128,1:128)==0) )
                error( 'Invalid sym_conduction_delays_parcels') ;
            end
            fprintf( 1, [ 'Mean sym_conduction_delays_parcels: ' num2str(mean( tmp_sym )) ' (#' num2str(numel( tmp_sym )) ')\n' ] ) ;


            % check sym_synapticTe_parcels
            tmp_all = synapticTe_parcels(1:128,1:128) ;
            tmp_all = horzcat( tmp_all{:} ) ;
            tmp_sym = horzcat( sym_synapticTe_parcels{:} ) ;
            if numel( tmp_all ) ~= numel( tmp_sym ) || ...
                max( abs( sort(tmp_all) - sort(tmp_sym) ) ) ~= 0 || ...
                ~isempty( find(sr_pairs(1:128,1:128)==0) )
                error( 'Invalid sym_synapticTe_parcels') ;
            end
            fprintf( 1, [ 'Mean sym_synapticTe_parcels: ' num2str(mean( tmp_sym )) ' (#' num2str(numel( tmp_sym )) ')\n' ] ) ;


            % check sym_synapticTi_parcels
            tmp_all = synapticTi_parcels(1:128,1:128) ;
            tmp_all = horzcat( tmp_all{:} ) ;
            tmp_sym = horzcat( sym_synapticTi_parcels{:} ) ;
            if numel( tmp_all ) ~= numel( tmp_sym ) || ...
                max( abs( sort(tmp_all) - sort(tmp_sym) ) ) ~= 0 || ...
                ~isempty( find(sr_pairs(1:128,1:128)==0) )
                error( 'Invalid sym_synapticTi_parcels') ;
            end
            fprintf( 1, [ 'Mean sym_synapticTi_parcels: ' num2str(mean( tmp_sym )) ' (#' num2str(numel( tmp_sym )) ')\n' ] ) ;


        else

            sym_peakLatency_parcels         = cell( parcelsCount/2, parcelsCount ) ;
            sym_conduction_delays_parcels   = cell( parcelsCount/2, parcelsCount ) ;
            sym_distance_parcels            = cell( parcelsCount/2, parcelsCount ) ;
            sym_synapticTe_parcels          = cell( 1, parcelsCount/2 ) ;
            sym_synapticTi_parcels          = cell( 1, parcelsCount/2 ) ;

            for s=1:parcelsCount/2
                for r=1:parcelsCount/2
                    sym_peakLatency_parcels{s,r}        = horzcat( peakLatency_parcels{s,r},        peakLatency_parcels         {s+parcelsCount/2,r+parcelsCount/2} ) ;
                    sym_conduction_delays_parcels{s,r}  = horzcat( conduction_delays_parcels{s,r},  conduction_delays_parcels   {s+parcelsCount/2,r+parcelsCount/2} ) ;
                    sym_distance_parcels{s,r}           = horzcat( distance_parcels{s,r},           distance_parcels            {s+parcelsCount/2,r+parcelsCount/2} ) ;
                end
                for r=parcelsCount/2+1:parcelsCount
                    sym_peakLatency_parcels{s,r}        = horzcat( peakLatency_parcels{s,r},        peakLatency_parcels         {s+parcelsCount/2,r-parcelsCount/2} ) ;
                    sym_conduction_delays_parcels{s,r}  = horzcat( conduction_delays_parcels{s,r},  conduction_delays_parcels   {s+parcelsCount/2,r-parcelsCount/2} ) ;
                    sym_distance_parcels{s,r}           = horzcat( distance_parcels{s,r},           distance_parcels            {s+parcelsCount/2,r-parcelsCount/2} ) ;
                end

            end

            for r=1:parcelsCount/2
                sym_synapticTe_parcels{r}               = horzcat( synapticTe_parcels{:,r},         synapticTe_parcels          {:,r+parcelsCount/2} ) ;
                sym_synapticTi_parcels{r}               = horzcat( synapticTi_parcels{:,r},         synapticTi_parcels          {:,r+parcelsCount/2} ) ;
            end

        end

        peakLatency_parcels         = sym_peakLatency_parcels ;
        conduction_delays_parcels   = sym_conduction_delays_parcels ;
        distance_parcels            = sym_distance_parcels ;
        synapticTe_parcels          = sym_synapticTe_parcels ;
        synapticTi_parcels          = sym_synapticTi_parcels ;

    end
    
    
    % save the clustering
    clustering                                          = struct ;
    clustering.id                                       = atlasId ;
    clustering.age                                      = [ ageMin ageMax ] ;
    clustering.peakLatency_parcels                      = peakLatency_parcels ;
    clustering.conduction_delays_parcels                = conduction_delays_parcels ;
    clustering.distance_parcels                         = distance_parcels ;
    clustering.synapticTe_parcels                       = synapticTe_parcels ;
    clustering.synapticTi_parcels                       = synapticTi_parcels ;

    fname = fullfile( bd, 'dcm', [ atlasId '_' num2str(ageMin) '-' num2str(ageMax) ], [ 'sym' num2str(symmetric) ], 'clustering.mat' ) ;
    system( [ 'mkdir -p ' fileparts(fname) ] );
    fprintf( 1, [ 'Save ' fname '\n' ] ) ;
    save(fname,'clustering');

else
    
    fname = fullfile( bd, 'dcm', [ atlasId '_' num2str(ageMin) '-' num2str(ageMax) ], [ 'sym' num2str(symmetric) ], 'clustering.mat' ) ;
    fprintf( 1, [ 'Load ' fname '\n' ] ) ;
    clustering = load(fname);
    clustering = clustering.clustering ;
    
    peakLatency_parcels         = clustering.peakLatency_parcels ;
    conduction_delays_parcels   = clustering.conduction_delays_parcels ;
    distance_parcels            = clustering.distance_parcels ;
    synapticTe_parcels          = clustering.synapticTe_parcels ;
    synapticTi_parcels          = clustering.synapticTi_parcels ;
    
end





parcel_pairs_invalid                                                = cellfun( @(x) length(x) < cceps_min, conduction_delays_parcels ) ;

median_peakLatency_parcels                                          = cellfun( @(x) median(full(x)), peakLatency_parcels ) ;
median_peakLatency_parcels( parcel_pairs_invalid )                  = nan ;
    
median_conduction_delays_parcels                                    = cellfun( @(x) median(full(x)), conduction_delays_parcels ) ;
median_conduction_delays_parcels( parcel_pairs_invalid )            = nan ;

median_distance_parcels                                             = cellfun( @(x) median(full(x)), distance_parcels ) ;
median_distance_parcels( parcel_pairs_invalid )                     = nan ;

median_peakLatency_velocity_parcels                                 = cellfun( @(d,pl) median( d./pl ), distance_parcels, peakLatency_parcels ) ;
median_peakLatency_velocity_parcels     ( parcel_pairs_invalid )    = nan ;

median_conductionDelay_velocity_parcels                             = cellfun( @(d,cd) median( d./cd ), distance_parcels, conduction_delays_parcels ) ;
median_conductionDelay_velocity_parcels ( parcel_pairs_invalid )    = nan ;




median_synapticTe_parcels           = nan( 1, size(synapticTe_parcels,2) ) ;
median_synapticTi_parcels           = nan( 1, size(synapticTe_parcels,2) ) ;
for r=1:length(median_synapticTe_parcels)
    median_synapticTe_parcels(r)    = median( horzcat( synapticTe_parcels{:,r} ) );
    median_synapticTi_parcels(r)    = median( horzcat( synapticTi_parcels{:,r} ) );
end


atlas                                           = struct ;
atlas.id                                        = atlasId ;
atlas.age                                       = [ ageMin ageMax ] ;
atlas.peakLatency_parcels                       = peakLatency_parcels ;
atlas.conduction_delays_parcels                 = conduction_delays_parcels ;
atlas.median_distance_parcels                   = median_distance_parcels ;
atlas.median_peakLatency_parcels                = median_peakLatency_parcels ;
atlas.median_peakLatency_velocity_parcels       = median_peakLatency_velocity_parcels ;
atlas.median_conduction_delays_parcels        	= median_conduction_delays_parcels ;
atlas.median_conductionDelay_velocity_parcels   = median_conductionDelay_velocity_parcels ;
atlas.median_synapticTe_parcels                 = median_synapticTe_parcels ;
atlas.median_synapticTi_parcels                 = median_synapticTi_parcels ;


% if symmetric
%     fname = fullfile( bd, [ 'dcm/' atlasId '_' num2str(ageMin) '-' num2str(ageMax) '/sym/atlas.mat' ] ) ;
% else
%     fname = fullfile( bd, [ 'dcm/' atlasId '_' num2str(ageMin) '-' num2str(ageMax) '/atlas.mat' ] ) ;
% end
fname = fullfile( bd, 'dcm', [ atlasId '_' num2str(ageMin) '-' num2str(ageMax) ], [ 'sym' num2str(symmetric) ], [ 'cceps_min_' num2str(cceps_min) ], 'atlas.mat' ) ;
system( [ 'mkdir -p ' fileparts(fname) ] );
fprintf( 1, [ 'Save ' fname '\n' ] ) ;
save(fname,'atlas');



end






function plot_results( atlasId, ageMin, ageMax, symmetric, cceps_min )

bd = baseDir();

fname = fullfile( bd, 'dcm', [ atlasId '_' num2str(ageMin) '-' num2str(ageMax) ], [ 'sym' num2str(symmetric) ], [ 'cceps_min_' num2str(cceps_min) ], 'atlas.mat' ) ;
atlas = load(fname);
atlas = atlas.atlas ;


median_distance_parcels                 = atlas.median_distance_parcels ;
median_peakLatency_parcels           	= atlas.median_peakLatency_parcels ;
median_peakLatency_velocity_parcels   	= atlas.median_peakLatency_velocity_parcels ;
median_conduction_delays_parcels       	= atlas.median_conduction_delays_parcels ;
median_conductionDelay_velocity_parcels = atlas.median_conductionDelay_velocity_parcels ;
median_synapticTe_parcels               = atlas.median_synapticTe_parcels ;
median_synapticTi_parcels               = atlas.median_synapticTi_parcels ;


saveFig = true ;
% saveFig = false ;


if strcmp(atlasId,'Lausanne2008-60')
    
    if ~symmetric
        % remove the last two parcels  Brain-Stem and Unknown
        median_conduction_delays_parcels = median_conduction_delays_parcels( 1:128, 1:128 ) ;
        
        total        = numel(median_conduction_delays_parcels) ;
        estimated    = length(find(~isnan(median_conduction_delays_parcels))) ;
        estimated_ipsi   = length(find(~isnan(median_conduction_delays_parcels(1:64,1:64)))) + length(find(~isnan(median_conduction_delays_parcels(65:end,65:end)))) ;
        estimated_contra = length(find(~isnan(median_conduction_delays_parcels(1:64,65:end)))) + length(find(~isnan(median_conduction_delays_parcels(65:end,1:64)))) ;
        
    else 
       % Brain-stem and Unknown have already been removed previously (no need to remove them here)
       
       total        = numel(median_conduction_delays_parcels) ;
       estimated    = length(find(~isnan(median_conduction_delays_parcels))) ;
       sym_parcels  = size(median_conduction_delays_parcels,1) ;
       estimated_ipsi   = length(find(~isnan(median_conduction_delays_parcels(:,1:sym_parcels)))) ;
       estimated_contra = length(find(~isnan(median_conduction_delays_parcels(:,sym_parcels+1:end)))) ;
        
       
       fname       = fullfile( bd, 'scripts/parcellations/anatomy_sorted', [ atlasId '-sym.txt' ] ) ;
       parcelsSym  = fileread(fname);
       parcelsSym  = strtrim(parcelsSym);
       parcelsSym  = strsplit(parcelsSym,'\n');
       parcelsSym( strcmp(parcelsSym,'Brain-Stem') ) = [] ;
       parcelsSym( strcmp(parcelsSym,'Unknown') )    = [] ;
       H = length(parcelsSym) ;
       
       parcelsLeft = cell(H,1) ;
       for p=1:H
           pl = strsplit(parcelsSym{p},'\t\t');
           pl = strsplit(pl{1});
           pl = pl{1};
           parcelsLeft{p}=pl;
       end
       
       
       % get the values (and how many) for one connection
       fname 	= fullfile( figDir(), 'fig4', [ 'fig4_' atlasId '_' num2str(ageMin) '_' num2str(ageMax) '_sym' num2str(symmetric) '_ccepsMin' num2str(cceps_min) '_connections.txt' ] ) ;
       fprintf( 1, [ '>> Save ' fname '\n' ] ) ;
       fd       = fopen( fname, 'w' ) ;
        
       
       parcelStimName='lh.superiortemporal_1';
       parcelRecName='lh.parsopercularis_1';
       parcelLeft_Stim_id = find(strcmp(parcelsLeft,parcelStimName)) ;
       parcelLeft_Rec_id = find(strcmp(parcelsLeft,parcelRecName)) ;
       
       fprintf( fd, [ 'CCEPs between ' parcelStimName ' and ' parcelRecName '\n' ] ) ;
       tmp = atlas.peakLatency_parcels{parcelLeft_Stim_id,parcelLeft_Rec_id} ;
       fprintf( fd, [ 'All peak latencies: #' num2str(length(tmp)) ' = ' num2str(tmp) '\n' ] ) ;
       fprintf( fd, [ 'Median peak latencies: ' num2str(median(tmp)) '\n' ] ) ;
       tmp = atlas.conduction_delays_parcels{parcelLeft_Stim_id,parcelLeft_Rec_id} ;
       fprintf( fd, [ 'All conduction delays: #' num2str(length(tmp)) ' = ' num2str(tmp) '\n' ] ) ;
       fprintf( fd, [ 'Median conduction delays: ' num2str(median(tmp)) '\n' ] ) ;
       
       
       fprintf( fd, [ '\n\n' ] ) ;
       
       
       parcelStimName='lh.superiortemporal_1';
       parcelRecName='lh.parstriangularis_1';
       parcelLeft_Stim_id = find(strcmp(parcelsLeft,parcelStimName)) ;
       parcelLeft_Rec_id = find(strcmp(parcelsLeft,parcelRecName)) ;
       
       fprintf( fd, [ 'CCEPs between ' parcelStimName ' and ' parcelRecName '\n' ] ) ;
       tmp = atlas.peakLatency_parcels{parcelLeft_Stim_id,parcelLeft_Rec_id} ;
       fprintf( fd, [ 'All peak latencies: #' num2str(length(tmp)) ' = ' num2str(tmp) '\n' ] ) ;
       fprintf( fd, [ 'Median peak latencies: ' num2str(median(tmp)) '\n' ] ) ;
       tmp = atlas.conduction_delays_parcels{parcelLeft_Stim_id,parcelLeft_Rec_id} ;
       fprintf( fd, [ 'All conduction delays: #' num2str(length(tmp)) ' = ' num2str(tmp) '\n' ] ) ;
       fprintf( fd, [ 'Median conduction delays: ' num2str(median(tmp)) '\n' ] ) ;
       
       
       fprintf( fd, [ '\n\n' ] ) ;
       
       
       parcelStimName='lh.parsopercularis_1';
       parcelRecName='lh.superiortemporal_1';
       parcelLeft_Stim_id = find(strcmp(parcelsLeft,parcelStimName)) ;
       parcelLeft_Rec_id = find(strcmp(parcelsLeft,parcelRecName)) ;
       
       fprintf( fd, [ 'CCEPs between ' parcelStimName ' and ' parcelRecName '\n' ] ) ;
       tmp = atlas.peakLatency_parcels{parcelLeft_Stim_id,parcelLeft_Rec_id} ;
       fprintf( fd, [ 'All peak latencies: #' num2str(length(tmp)) ' = ' num2str(tmp) '\n' ] ) ;
       fprintf( fd, [ 'Median peak latencies: ' num2str(median(tmp)) '\n' ] ) ;
       tmp = atlas.conduction_delays_parcels{parcelLeft_Stim_id,parcelLeft_Rec_id} ;
       fprintf( fd, [ 'All conduction delays: #' num2str(length(tmp)) ' = ' num2str(tmp) '\n' ] ) ;
       fprintf( fd, [ 'Median conduction delays: ' num2str(median(tmp)) '\n' ] ) ;
       
       
       fprintf( fd, [ '\n\n' ] ) ;
       
       
       parcelStimName='lh.parstriangularis_1';
       parcelRecName='lh.superiortemporal_1';
       parcelLeft_Stim_id = find(strcmp(parcelsLeft,parcelStimName)) ;
       parcelLeft_Rec_id = find(strcmp(parcelsLeft,parcelRecName)) ;
       
       fprintf( fd, [ 'CCEPs between ' parcelStimName ' and ' parcelRecName '\n' ] ) ;
       tmp = atlas.peakLatency_parcels{parcelLeft_Stim_id,parcelLeft_Rec_id} ;
       fprintf( fd, [ 'All peak latencies: #' num2str(length(tmp)) ' = ' num2str(tmp) '\n' ] ) ;
       fprintf( fd, [ 'Median peak latencies: ' num2str(median(tmp)) '\n' ] ) ;
       tmp = atlas.conduction_delays_parcels{parcelLeft_Stim_id,parcelLeft_Rec_id} ;
       fprintf( fd, [ 'All conduction delays: #' num2str(length(tmp)) ' = ' num2str(tmp) '\n' ] ) ;
       fprintf( fd, [ 'Median conduction delays: ' num2str(median(tmp)) '\n' ] ) ;
         
       fclose(fd);
       
       
              
       

       
       
       
       
       
       
    end
    
    fname 	= fullfile( figDir(), 'fig4', [ 'fig4_A_' atlasId '_' num2str(ageMin) '_' num2str(ageMax) '_sym' num2str(symmetric) '_ccepsMin' num2str(cceps_min) '.txt' ] ) ;
    fd      = fopen( fname, 'w' ) ;
    fprintf( fd, [ 'Total: ' num2str(total) '\n' ] ) ;
    fprintf( fd, [ 'Estimated: ' num2str(estimated) ' = ' num2str(100/total*estimated) '%% of total\n' ] ) ;
    fprintf( fd, [ 'Estimated ipsi: ' num2str(estimated_ipsi) ' = ' num2str(100/total*estimated_ipsi) '%% of total\n' ] ) ;
    fprintf( fd, [ 'Estimated contra: ' num2str(estimated_contra) ' = ' num2str(100/total*estimated_contra) '%% of total\n' ] ) ;
    fclose( fd ) ;
    
end










% H = figure( 'Position', [77 986 1078 521] ) ;
H = figure( 'Position', [77 986 1078 1303] ) ;
imagescwithnan( median_conduction_delays_parcels, colormap, [0.6 0.6 0.6], 0, 30 ) ; title( [ 'median conduction delays parcels - ccepsMin' num2str(cceps_min) ] ) ;
axis equal
xlim([0.5 size(median_conduction_delays_parcels,2)+.5]) ;
ylim([0.5 size(median_conduction_delays_parcels,1)+.5]) ;
suptitle( [ atlasId ' sym' num2str(symmetric) ' ' num2str(ageMin) '-' num2str(ageMax) 'yo ccepsMin' num2str(cceps_min) ] ) ;

if saveFig
    % fig4_A
    figD  = fullfile( figDir(), 'fig4' ) ;
    system( [ 'mkdir -p ' figD ] ) ;
    fname = fullfile( figD, [ 'fig4_A_' atlasId '_' num2str(ageMin) '_' num2str(ageMax) '_sym' num2str(symmetric) '_ccepsMin' num2str(cceps_min) '.svg' ] ) ;
    fprintf( 1, [ 'Save ' fname '\n' ] ) ;
    saveas( H, fname ) ;
end


plot_variability( atlas, atlasId, ageMin, ageMax, symmetric, cceps_min )





% normalization = 'count' ; % default
% normalization = 'probability' ;


maison = true ;
maison = false ;
if maison
    H = figure( 'Position', [95 79 800 400] );
else
    H = figure( 'Position', [48 446 1165 725] );     
end

subplot(4,3,[1 4]) ;
% h = histogram( median_peakLatency_parcels, 0:4:80 ) ; %, 'Normalization', normalization ) ;
h = histogram( median_peakLatency_parcels, 0:80/15:80 ) ; %, 'Normalization', normalization ) ;
h.FaceColor = [0.5 0.5 0.5] ;
xlabel( 'peak latency (ms)') ;
ylabel( 'parcels pairs' ) ;

subplot(4,3,[7 10]);
% h = histogram( median_peakLatency_velocity_parcels, 0:.2:4 ) ;
% h = histogram( median_peakLatency_velocity_parcels, 0:6/20:6 ) ;
h = histogram( median_peakLatency_velocity_parcels, 0:6/15:6 ) ;
h.FaceColor = [0.5 0.5 0.5] ;
xlabel( 'velocity (m/s)') ;
ylabel( 'parcels pairs' ) ;

subplot(4,3,[5 8]) ;
% h = histogram( median_distance_parcels, 0:120/15:120 ) ;
% h = histogram( median_distance_parcels, 0:130/15:130 ) ;
% h = histogram( median_distance_parcels, 0:140/15:140 ) ;
h = histogram( median_distance_parcels, 0:150/15:150 ) ;
h.FaceColor = [0.5 0.5 0.5] ;
if strcmp( atlasId, 'MarsAtlas' ), ylim([0 180]);
end
xlabel( 'distance between parcels (mm)') ;
ylabel( 'parcels pairs' ) ;

subplot(4,3,[3 6]) ;
% h = histogram( median_conduction_delays_parcels, 0:2:30 ) ;
h = histogram( median_conduction_delays_parcels, 0:30/15:30 ) ;
h.FaceColor = [0.5 0.5 0.5] ;
if strcmp( atlasId, 'MarsAtlas' )
elseif strcmp( atlasId, 'HCP-MMP1' )
end
% xlim([0 30]);
xlabel( 'conduction delay (ms)') ;
ylabel( 'parcels pairs' ) ;

subplot(4,3,[9 12]) ;
h = histogram( median_conductionDelay_velocity_parcels, 0:4:80 ) ;
% h = histogram( median_conductionDelay_velocity_parcels, 0:6:90 ) ;
% h = histogram( median_conductionDelay_velocity_parcels, 0:100/15:100 ) ;
% h = histogram( median_conductionDelay_velocity_parcels, 0:90/15:90 ) ;
% h = histogram( median_conductionDelay_velocity_parcels, 0:80/15:80 ) ;
% h = histogram( median_conductionDelay_velocity_parcels, 0:70/15:70 ) ;
h = histogram( median_conductionDelay_velocity_parcels, 0:60/15:60 ) ;


h.FaceColor = [0.5 0.5 0.5] ;
if strcmp( atlasId, 'MarsAtlas' ), ylim([0 180]);
end
xlabel( 'velocity (m/s)') ;
ylabel( 'parcels pairs' ) ;


% subplot(6,3,[13 16]) ; 
% h = histogram( median_synapticTe_parcels, 0:10 ) ; 
% h.FaceColor = [0.5 0.5 0.5] ;
% if strcmp( atlasId, 'MarsAtlas' ), ylim([0 35]);
% elseif strcmp( atlasId, 'HCP-MMP1' ), ylim([0 90]);
% elseif strcmp( atlasId, 'Lausanne2008-33' ), ylim([0 20]);
% elseif strcmp( atlasId, 'Lausanne2008-60' ), ylim([0 30]);
% elseif strcmp( atlasId, 'Lausanne2008-250' ), ylim([0 90]);
% end
% xlabel( 'synapticTe (ms)') ;
% ylabel( '#parcels' ) ;
% 
% subplot(6,3,[14 17]) ; 
% h = histogram( median_synapticTi_parcels, 0:10 ) ; 
% h.FaceColor = [0.5 0.5 0.5] ;
% if strcmp( atlasId, 'MarsAtlas' ), ylim([0 35]);
% elseif strcmp( atlasId, 'HCP-MMP1' ), ylim([0 90]);
% elseif strcmp( atlasId, 'Lausanne2008-33' ), ylim([0 30]);
% end
% xlabel( 'synapticTi (ms)') ;
% ylabel( '#parcels' ) ;

suptitle( [ atlasId ' sym' num2str(symmetric) ' '  num2str(ageMin) '-' num2str(ageMax) 'yo ccepsMin' num2str(cceps_min) ] ) ;

if saveFig
    % fig5
    figD  = fullfile( figDir(), 'fig5' ) ;
    system( [ 'mkdir -p ' figD ] ) ;
    
    txt = [     'Mean peak latency: ' num2str(mean(median_peakLatency_parcels(:),'omitnan')) '\n' ] ;
    txt = [ txt 'Median peak latency: ' num2str(median(median_peakLatency_parcels(:),'omitnan')) '\n\n' ] ;
    
    txt = [ txt 'Mean peak latency velocity: ' num2str(mean(median_peakLatency_velocity_parcels(:),'omitnan')) '\n' ] ;
    txt = [ txt 'Median peak latency velocity: ' num2str(median(median_peakLatency_velocity_parcels(:),'omitnan')) '\n\n' ] ;
    
    txt = [ txt 'Mean distance: ' num2str(mean(median_distance_parcels(:),'omitnan')) '\n' ] ;
    txt = [ txt 'Median distance: ' num2str(median(median_distance_parcels(:),'omitnan')) '\n\n' ] ;
    
    txt = [ txt 'Mean conduction delay: ' num2str(mean(median_conduction_delays_parcels(:),'omitnan')) '\n' ] ;
    txt = [ txt 'Median conduction delay: ' num2str(median(median_conduction_delays_parcels(:),'omitnan')) '\n\n' ] ;
    
    txt = [ txt 'Mean conduction delay velocity: ' num2str(mean(median_conductionDelay_velocity_parcels(:),'omitnan')) '\n' ] ;
    txt = [ txt 'Median conduction delay velocity: ' num2str(median(median_conductionDelay_velocity_parcels(:),'omitnan')) '\n\n' ] ;
    
    
    pairs_estimated = find( ~isnan(median_peakLatency_parcels) ) ;
    txt = [ txt 'Total pairs: ' num2str(numel(median_peakLatency_parcels)) '\n' ] ;
    txt = [ txt 'Estimated pairs: ' num2str(length(pairs_estimated)) '\n' ] ;
    txt = [ txt '%% Estimated cond delays > 10 ms: ' num2str(100/length(pairs_estimated)*length(find(median_conduction_delays_parcels>10))) '%% \n' ] ;
    txt = [ txt '%% Estimated cond delays > 20 ms: ' num2str(100/length(pairs_estimated)*length(find(median_conduction_delays_parcels>20))) '%% \n\n' ] ;
    
    
    fprintf( 1, txt ) ;
    
    fname = fullfile( figD, [ 'fig5_' atlasId '_' num2str(ageMin) '_' num2str(ageMax) '_sym' num2str(symmetric) '_ccepsMin' num2str(cceps_min) '.txt' ] ) ;
    fprintf( 1, [ 'Save ' fname '\n' ] ) ;
    fd = fopen( fname, 'w' ) ;
    fprintf( fd, txt ) ;
    fclose(fd);
    
    
    fname = fullfile( figD, [ 'fig5_' atlasId '_' num2str(ageMin) '_' num2str(ageMax) '_sym' num2str(symmetric) '_ccepsMin' num2str(cceps_min) '.svg' ] ) ;
    fprintf( 1, [ 'Save ' fname '\n' ] ) ;
    saveas( H, fname ) ;
end




% plot  synTe et synTi
H = figure( 'Position', [343 1140 644 352] ) ;
subplot( 1, 2, 1 ) ;
h = histogram( median_synapticTe_parcels, 0:10 ) ; 
h.FaceColor = [0.5 0.5 0.5] ;
if strcmp( atlasId, 'MarsAtlas' ), ylim([0 35]);
elseif strcmp( atlasId, 'HCP-MMP1' )
    if ~symmetric 
        ylim([0 90]);
    end
elseif strcmp( atlasId, 'Lausanne2008-33' )
    if ~symmetric
        ylim([0 30]);
    end
elseif strcmp( atlasId, 'Lausanne2008-60' )
    if symmetric
        ylim([0 30]);
    else
        ylim([0 45]);
    end
elseif strcmp( atlasId, 'Lausanne2008-125' )
    if ~symmetric
        ylim([0 80]);
    end
elseif strcmp( atlasId, 'Lausanne2008-250' )
    if ~symmetric 
        ylim([0 110]);
    end
end
xlabel( 'excitatory (ms)') ;
ylabel( 'parcels' ) ;
% title( [ atlasId ' ' num2str(ageMin) '-' num2str(ageMax) 'yo' ] ) ;

subplot( 1, 2, 2 ) ;
h = histogram( median_synapticTi_parcels, 0:10 ) ; 
h.FaceColor = [0.5 0.5 0.5] ;
if strcmp( atlasId, 'MarsAtlas' ), ylim([0 35]);
elseif strcmp( atlasId, 'HCP-MMP1' )
    if ~symmetric 
        ylim([0 90]);
    end
elseif strcmp( atlasId, 'Lausanne2008-33' )
    if ~symmetric
        ylim([0 30]);
    end
elseif strcmp( atlasId, 'Lausanne2008-60' )
    if symmetric
        ylim([0 30]);
    else
        ylim([0 45]);
    end
elseif strcmp( atlasId, 'Lausanne2008-125' )
    if ~symmetric
        ylim([0 80]);
    end
elseif strcmp( atlasId, 'Lausanne2008-250' )
    if ~symmetric 
        ylim([0 110]);
    end
end
xlabel( 'inhibitory (ms)') ;
ylabel( 'parcels' ) ;
% title( [ atlasId ' ' num2str(ageMin) '-' num2str(ageMax) 'yo' ] ) ;

suptitle( [ atlasId ' sym' num2str(symmetric) ' '  num2str(ageMin) '-' num2str(ageMax) 'yo ccepsMin' num2str(cceps_min) ] ) ;


saveFig = true ;
if saveFig 
    % fig6
    figD    = fullfile( figDir(), 'fig6' ) ;
    system( [ 'mkdir -p ' figD ] ) ;
    
    txt = [     'Mean synaptic Te: ' num2str(mean(median_synapticTe_parcels(:),'omitnan')) '\n' ] ;
    txt = [ txt 'Median synaptic Te: ' num2str(median(median_synapticTe_parcels(:),'omitnan')) '\n\n' ] ;
    
    txt = [ txt 'Mean synaptic Ti: ' num2str(mean(median_synapticTi_parcels(:),'omitnan')) '\n' ] ;
    txt = [ txt 'Median synaptic Ti: ' num2str(median(median_synapticTi_parcels(:),'omitnan')) '\n\n' ] ;
    
    fprintf( 1, txt ) ;
    
    fname = fullfile( figD, [ 'fig6_' atlasId '_' num2str(ageMin) '_' num2str(ageMax) '_sym' num2str(symmetric) '_ccepsMin' num2str(cceps_min) '.txt' ] ) ;
    fd = fopen( fname, 'w' ) ;
    fprintf( fd, txt ) ;
    fclose(fd);
    
    
    fname = [ figD '/fig6_ab_' atlasId '_' num2str(ageMin) '_' num2str(ageMax) '_sym' num2str(symmetric) '_ccepsMin' num2str(cceps_min) '.svg' ] ;
    fprintf( 1, [ 'Save ' fname '\n' ] ) ;
    saveas( H, fname ) ;
end


end





function plot_variability( atlas, atlasId, ageMin, ageMax, symmetric, cceps_min )

% parcels not computed / invalid
parcel_pairs_invalid = isnan(atlas.median_peakLatency_parcels) ;
% parcel_pairs_invalid = cellfun( @(x) length(x) < 10, atlas.conduction_delays_parcels ) ;

%% peak latencies
peaklat_ecart_median = cellfun( @(x) mean(abs(x-median(x))), atlas.peakLatency_parcels ) ;
peaklat_ecart_median(parcel_pairs_invalid) = nan ;
cm_extrema = [ 0 60 ] ;
figure( 'Position', [27 59 1053 1439] ) ;
subplot(3,1,1);
imagescwithnan( atlas.median_peakLatency_parcels, colormap, [0.6 0.6 0.6], cm_extrema(1), cm_extrema(2) ) ;
title( 'median' )
subplot(3,1,2);
imagescwithnan( peaklat_ecart_median, colormap, [0.6 0.6 0.6], cm_extrema(1), cm_extrema(2) ) ;
title( 'ecart median' )
subplot(3,1,3);
imagescwithnan( 100 * peaklat_ecart_median ./ atlas.median_peakLatency_parcels, colormap, [0.6 0.6 0.6], 0, 100 ) ;
title( 'ecart median relatif' )
suptitle( 'peak latencies' )
       
figure ;
scatter( atlas.median_peakLatency_parcels(:), peaklat_ecart_median(:) ) ;
% xlim([0 40]);
% ylim([0 40]);
xlabel('median peakLatency parcels') ;
ylabel('peaklat ecart median') ;


       
%% conduction delay
conddel_ecart_median = cellfun( @(x) mean(abs(x-median(x))), atlas.conduction_delays_parcels ) ;
conddel_ecart_median(parcel_pairs_invalid) = nan ;
cm_extrema = [ 0 30 ] ;
figure( 'Position', [27 59 1053 1439] ) ;
subplot(3,1,1);
imagescwithnan( atlas.median_conduction_delays_parcels, colormap, [0.6 0.6 0.6], cm_extrema(1), cm_extrema(2) ) ;
title( 'median' )
subplot(3,1,2);
imagescwithnan( conddel_ecart_median, colormap, [0.6 0.6 0.6], cm_extrema(1), cm_extrema(2) ) ;
title( 'ecart median' )
subplot(3,1,3);
imagescwithnan( 100 * conddel_ecart_median ./ atlas.median_conduction_delays_parcels, colormap, [0.6 0.6 0.6], 0, 100 ) ;
title( 'ecart median relatif' )
suptitle( 'cond delays' )


% same figure for article
H = figure( 'Position', [77 986 1078 1303] ) ;
cm_extrema = [ 0 30 ] ;
imagescwithnan( conddel_ecart_median, colormap, [0.6 0.6 0.6], cm_extrema(1), cm_extrema(2) ) ;
title( [ 'ecart median conduction delays parcels - ccepsMin' num2str(cceps_min) ] ) ;
axis equal
xlim([0.5 size(conddel_ecart_median,2)+.5]) ;
ylim([0.5 size(conddel_ecart_median,1)+.5]) ;
suptitle( [ atlasId ' sym' num2str(symmetric) ' ' num2str(ageMin) '-' num2str(ageMax) 'yo ccepsMin' num2str(cceps_min) ] ) ;

saveFig=1 ;
if saveFig
    % fig4_A
    figD  = fullfile( figDir(), 'fig4' ) ;
    system( [ 'mkdir -p ' figD ] ) ;
    fname = fullfile( figD, [ 'fig4_A_ecartmedian_' atlasId '_' num2str(ageMin) '_' num2str(ageMax) '_sym' num2str(symmetric) '_ccepsMin' num2str(cceps_min) '.svg' ] ) ;
    fprintf( 1, [ 'Save ' fname '\n' ] ) ;
    saveas( H, fname ) ;
end






figure ;
scatter( atlas.median_conduction_delays_parcels(:), conddel_ecart_median(:) ) ;
xlim([0 40]);
ylim([0 40]);
xlabel('median conduction delays parcels') ;
ylabel('conddel ecart median') ;



%% correlation peak lat versus cond del
figure ;
scatter( atlas.median_peakLatency_parcels(:), atlas.median_conduction_delays_parcels(:) ) ;
xlabel('median peakLatency parcels');
ylabel('median conduction delays parcels');
       


%% est-ce que ecart diminue quand le nombre de data
nb_data = cellfun( @(p) numel(p), atlas.peakLatency_parcels ) ;
figure ;
histogram( nb_data(nb_data>0), 'BinWidth', 1 ) ;
xlabel( 'nb data' )
ylabel( 'nb parcels' )
title( 'nb data par parcel' )
       


ecart_median = cell( 100, 1 ) ;
for nb=5:100
    id = find(nb_data==nb) ;
    for i=1:length(id)
        values = atlas.peakLatency_parcels{id(i)} ;
        ecart_median{nb} = horzcat( ecart_median{nb}, mean( abs(values-median(values)) ) ) ;
    end
end

mean_ecart_median = cellfun( @(e) mean(e), ecart_median ) ;

figure ; 
scatter( 1:100, mean_ecart_median ) ;




       
end




