function H = plotFit( dcmFname, figPos )

DCM             = load(dcmFname);
DCM             = DCM.DCM ;

D = spm_eeg_load(DCM.xY.Dfile) ;

[~,locs] = findpeaks(abs(DCM.H{1})) ;
signe = sign(DCM.H{1}(locs(1))) ;
    

H = figure( 'Position', figPos ) ; 

hold on ;
plot( D.time * 1000, signe * D(:), 'LineWidth', 2 ) ;
plot( DCM.xY.pst, signe * DCM.H{1} / DCM.xY.scale, 'LineWidth', 2, 'LineStyle', '--'  ) ;
xlim([0 100]) ;
%     gcf
% set(gca,'LineWidth',2);
% set(gca,'FontWeight','bold');
    
grid on
set( gca, 'GridAlpha', .3)

[ ~, name ] = fileparts( dcmFname ) ;
tmp         = strsplit( name, '_' ) ;

dataFname           = fileparts( fileparts( strrep( strrep( dcmFname, '/dcm/', '/data/' ), 'SEQ_BM_ACPCHIP_F_DCM', 'SEQ_BM_ACPCHIP_F_ComputeAverage' ) ) ) ;
dataFname           = fullfile( dataFname, [ strjoin(tmp(2:end-1),'_') '.mat' ] ) ;
D                   = spm_eeg_load( dataFname ) ;

peakLatencyFname    = fileparts( fileparts( strrep( strrep( dcmFname, '/dcm/', '/data/' ), 'SEQ_BM_ACPCHIP_F_DCM', 'SEQ_BM_ACPCHIP_F_ComputeAverage' ) ) ) ;
peakLatencyFname    = fullfile( peakLatencyFname, [ 'PeakDelay_' strjoin(tmp(4:8),'_') '.mat' ] ) ;
peakLatency         = spm_eeg_load( peakLatencyFname ) ;
recChName           = tmp{end} ;
pl                  = peakLatency( indchannel( peakLatency, recChName ), 5 ) ;
% pl                  = round(D.time(round(D.indsample(pl/1000)))*1000);


[~,locs]    = findpeaks(abs(DCM.H{1})) ;
s2_peak     = DCM.xY.pst(locs(1)) ;


[ conductionDelay, synapticTe, synapticTi ] = get_posterior( DCM ) ;
suptitle( sprintf( 'Pl: %d ms, Ax: %d ms, Se: %d ms, Si: %d ms', round(s2_peak), round(conductionDelay), round(synapticTe), round(synapticTi) ) ) ;



colors = get(gca,'ColorOrder') ;
line( [ pl pl ]', get( gca, 'YLim' )', 'Color', colors(1,:), 'LineStyle', '--', 'LineWidth', 2 ) ;
line( [ s2_peak s2_peak ]', get( gca, 'YLim' )', 'Color', colors(2,:), 'LineStyle', '--', 'LineWidth', 2 ) ;

legend({'obs.' 'pred.'})


return
    
    
    
    
    
    saveFigures= true ;
    if saveFigures
        fname = [ dcmDir '/../screenshots/data_fitted_' num2str(minPL) '_' num2str(maxPL) '_' num2str(d) '.txt' ] ;
        fd = fopen(fname, 'wt') ;
        fprintf(fd, sprintf('%s %.1f\n', '#d', d) );
        fprintf(fd, sprintf('%s %.1f\n', 'peak latency', round(s2_peaks_all(d)) ) );
        fprintf(fd, sprintf('%s %.1f\n', 'axonal delay', round(s2_conductionDelay_all(d)) ) );
        fprintf(fd, sprintf('%s %.1f\n', 'synapticTe', round(s2_synapticTe_all(d)) ) );
        fprintf(fd, sprintf('%s %.1f\n', 'synapticTi', round(s2_synapticTi_all(d)) ) );
        fclose(fd);
        fname = [ dcmDir '/../screenshots/data_fitted_' num2str(minPL) '_' num2str(maxPL) '_' num2str(d) '.png' ] ;
        saveas( H, fname ) ;
        fname = [ dcmDir '/../screenshots/data_fitted_' num2str(minPL) '_' num2str(maxPL) '_' num2str(d) '.svg' ] ;
        saveas( H, fname ) ;
        fprintf(1, [ 'Save ' fname '\n' ] ) ;
    end
    

















end