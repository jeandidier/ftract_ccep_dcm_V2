function compare_age


bd = baseDir();

% axonal delays
% atlasId='Lausanne2008-60';
% symmetric = 1 ;

% synaptic delays
atlasId='HCP-MMP1';
symmetric = 0 ;



cceps_min = 5 ;

txt = [ '*** ' atlasId ' - Symmetric: ' num2str(symmetric) '\n' ] ;



ageMin      = 0 ;
ageMax      = 15 ;
fname       = fullfile( bd, 'dcm', [ atlasId '_' num2str(ageMin) '-' num2str(ageMax) ], [ 'sym' num2str(symmetric) ], [ 'cceps_min_' num2str(cceps_min) ], 'atlas.mat' ) ;
atlas       = load(fname);
atlas_child = atlas.atlas ;


ageMin      = 15 ;
ageMax      = 100 ;
fname       = fullfile( bd, 'dcm', [ atlasId '_' num2str(ageMin) '-' num2str(ageMax) ], [ 'sym' num2str(symmetric) ], [ 'cceps_min_' num2str(cceps_min) ], 'atlas.mat' ) ;
atlas       = load(fname);
atlas_adult = atlas.atlas ;




% common pair of parcels
child_parcels   = ~isnan( atlas_child.median_peakLatency_parcels );
txt = [ txt '*** child pairs of parcels: ' num2str(length( find(child_parcels) )) '\n' ] ;

adult_parcels   = ~isnan( atlas_adult.median_peakLatency_parcels );
txt = [ txt '*** adult pairs of parcels: ' num2str(length( find(adult_parcels) )) '\n' ] ;

common_parcels  = child_parcels & adult_parcels ;
txt = [ txt '*** Common pairs of parcels: ' num2str(length( find(common_parcels) )) '\n' ] ;


% compare peak latencies
child = atlas_child.median_peakLatency_parcels(common_parcels) ;
adult = atlas_adult.median_peakLatency_parcels(common_parcels) ;
[h,p] = ttest2( child, adult ) ;
txt = [ txt 'Peak latencies: child=' num2str(mean(child)) ', adult=' num2str(mean(adult)) ' (p=' num2str(p) ')\n' ] ;

% compare conduction delays
child = atlas_child.median_conduction_delays_parcels(common_parcels) ;
adult = atlas_adult.median_conduction_delays_parcels(common_parcels) ;
[h,p] = ttest2( child, adult ) ;
txt = [ txt 'Conduction delays: child=' num2str(mean(child)) ', adult=' num2str(mean(adult)) ' (p=' num2str(p) ')\n' ] ;

% compare distance
child = atlas_child.median_distance_parcels(common_parcels) ;
adult = atlas_adult.median_distance_parcels(common_parcels) ;
[h,p] = ttest2( child, adult ) ;
txt = [ txt 'Distances: child=' num2str(mean(child)) ', adult=' num2str(mean(adult)) ' (p=' num2str(p) ')\n' ] ;

% compare peakLatency velocity
child = atlas_child.median_peakLatency_velocity_parcels(common_parcels) ;
adult = atlas_adult.median_peakLatency_velocity_parcels(common_parcels) ;
[h,p] = ttest2( child, adult ) ;
txt = [ txt 'PeakLatency velocity: child=' num2str(mean(child)) ', adult=' num2str(mean(adult)) ' (p=' num2str(p) ')\n' ] ;

% compare conduction delays velocity
child = atlas_child.median_conductionDelay_velocity_parcels(common_parcels) ;
adult = atlas_adult.median_conductionDelay_velocity_parcels(common_parcels) ;
[h,p] = ttest2( child, adult ) ;
txt = [ txt 'Conduction delays velocity: child=' num2str(mean(child)) ', adult=' num2str(mean(adult)) ' (p=' num2str(p) ')\n' ] ;






% common pair of parcels
child_parcels   = ~isnan( atlas_child.median_synapticTe_parcels );
length( find(child_parcels) ) ;
adult_parcels   = ~isnan( atlas_adult.median_synapticTe_parcels );
length( find(adult_parcels) ) ;
common_parcels  = child_parcels & adult_parcels ;
length( find(common_parcels) ) ;
txt = [ txt '*** Common parcels: ' num2str(length( find(common_parcels) )) '\n' ] ;

% compare synapticTe
child = atlas_child.median_synapticTe_parcels(common_parcels) ;
adult = atlas_adult.median_synapticTe_parcels(common_parcels) ;
[h,p] = ttest2( child, adult ) ;
txt = [ txt 'synapticTe: child=' num2str(mean(child, 'omitnan')) ', adult=' num2str(mean(adult, 'omitnan')) ' (p=' num2str(p) ')\n' ] ;

% compare synapticTi
child = atlas_child.median_synapticTi_parcels(common_parcels) ;
adult = atlas_adult.median_synapticTi_parcels(common_parcels) ;
[h,p] = ttest2( child, adult ) ;
txt = [ txt 'synapticTi: child=' num2str(mean(child, 'omitnan')) ', adult=' num2str(mean(adult, 'omitnan')) ' (p=' num2str(p) ')\n' ] ;


fprintf( 1, txt ) ;

figD  = fullfile( figDir(), 'fig5' ) ;
system( [ 'mkdir -p ' figD ] ) ;
fname = fullfile( figD, [ 'fig5_' atlasId '_sym' num2str(symmetric) '_ccepsMin' num2str(cceps_min) '_stats_age.txt' ] ) ;
fd = fopen( fname, 'w' ) ;
fprintf( fd, txt ) ;
fclose( fd ) ;


end




