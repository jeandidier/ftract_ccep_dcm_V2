import os
import os.path as op
import tempfile
import numpy as np
import pickle

import crfs_status
import create_jobs



baseDir             = crfs_status.baseDir

ccepsRecNames       = 'significant'
steps            	= 'all'
peakLatencyMax      = 80
threshold           = 5
fitDurationMode     = 'm20_PLp2D_M40'
computationMode     = 'serial'




def create_jobs_params( center, create=True ) :

 
    output_directory = op.join(baseDir,'scripts','jobs',center)
    if not op.exists(output_directory): os.makedirs( output_directory )
    output_directory = tempfile.mkdtemp( dir=output_directory )

    jobs_dir         = op.join(output_directory,'jobs')
    os.mkdir( jobs_dir )


    pats = crfs_status.get_crfs_on_disk( center = center, write = False )

    # dans le fichier log, il y a les patients deja calcules
    #centerLog = op.join(baseDir,'dcm',center+'.log')
    #if op.exists(centerLog) : 
    #    # il faut enlever les patients qui ont deja ete calcules
    #    raise Exception( 'IMPLEMENT' )


    # fabriquer les commandes matlab
    commands = []
    j=1
    for p in pats :
        path = op.join(baseDir,'data','PATS__V1',p,'SEEG')
        crfs = os.listdir(path)
        for crf in crfs :
            crfDir = op.join(path,crf,'SEQ_BM_ACPCHIP_F_ComputeAverage')
            stims = os.listdir(crfDir)
            for stim in stims :
                stimDir             = op.join(crfDir,stim)
                stimFname           = op.join(stimDir, 'averaged_zs_'+stim+'.mat')
                probabilityFname    = op.join(stimDir, 'Probability_'+ stim+ '.mat')
                peakLatencyFname    = op.join(stimDir, 'PeakDelay_' +stim+ '.mat')
                durationFname       = op.join(stimDir, 'Duration_' +stim +'.mat')
                recContactNameFname = op.join(stimDir, 'recContactName.txt')
                recContactSegFname  = op.join(stimDir, 'recContactSeg.txt')
                files               = [ stimFname, probabilityFname, peakLatencyFname, 
                                        durationFname, recContactNameFname, recContactSegFname ]
                assert np.all( [ op.exists(f) for f in files ] )

                dcmOutputDir        = op.join(baseDir,'dcm','PATS__V1',p,'SEEG',crf,'SEQ_BM_ACPCHIP_F_DCM',stim)
                dcmOutputListFname  = op.join(dcmOutputDir,'cceps_significant')
                command             = "prepare_spm_dcm_stim( '"+ccepsRecNames+"', '"+steps+"', "+str(peakLatencyMax)+", "+str(threshold)+", '"+fitDurationMode+"', '"+computationMode+"', '"+stimFname+"', '"+probabilityFname+"', '"+peakLatencyFname+"', '"+durationFname+"', '"+recContactNameFname+"', '"+recContactSegFname+"', '"+dcmOutputDir+"', '"+dcmOutputListFname+"' );"
                #commands.append(c)

                mFname              = op.join(jobs_dir,'j'+str(j)+'.m')
                print('write', mFname)
                fd                  = open( mFname, 'w' )
                fd.write( '\n'.join( [ 'addpath(\'/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/scripts/external/spm12_r6732/spm12\') ;', 
                                       'spm(\'defaults\',\'eeg\') ;',
                                       'addpath(genpath(\'/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/scripts/external/ft_dcm_ccep\')) ;',
                                       command ] ) )
                fd.close()
                
                command             = '\n'.join( [ 'module load MATLAB/R2017b', 
                                                   'matlab -nodesktop -nodisplay -nosoftwareopengl -singleCompThread -r "addpath(\''+jobs_dir+'\'); j'+str(j)+'; quit();"', ] )
                commands.append( command ) ;
                j += 1


    params                      = dict()
    params['job_name']          = center+'_'+op.basename(output_directory)
    params['output_directory']  = output_directory
    params['commands']          = commands
    params['cluster_queue']     = 'normal'
    params['cpus_per_task']     = 1
    params['mem_G']             = 4 # Go
    params['walltime']          = '72:00:00'


    params_fname                = op.join(output_directory,'parameters.pkl')
    print('Save',params_fname)
    pickle.dump(params,open(params_fname,'wb'))
    

    if create :
        create_jobs.create_jobs( params )


    return params, params_fname



