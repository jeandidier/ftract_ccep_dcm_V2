function review_stimDCM_V14( update_DCMs, s1Selection_config )

onlyCheck = true ;
% onlyCheck = false ;
if onlyCheck
%     write_analyze = [ 1 1 ] ;
%     update_DCMs = true ;
%     update_DCMs = false ;
    
    s1Selection     = s1Selection_config_V14( s1Selection_config ) ;
    
    s2Selection     = struct ;
    s2Selection.R2  = 70 ;
    s2Selection.MPD = 5 ;

    checkStimDCM( update_DCMs, s1Selection, s2Selection ) ;
    return
end



close all

sequence_id = 'ArtefactCorrectionPCHIP' ; 
stims_valid = validStims_V12( sequence_id ) ;


%% histogram of peak latency
% peak_latency = horzcat(stims_valid.peak_latency);
% figure ; histogram( peak_latency ) ; xlabel( 'peak latency' )


rng('shuffle') ;
s = round((length(stims_valid)-1)*rand+1);
s = 1 ;
[ DCM_s1_acc, DCM_s2_median_acc, DCM_s2_bpa_nocond_acc ] = review_stimDCM_stim( stims_valid, s ) ;


return 


%% below is the code used to compare bpa and median for 150 stimulations


max_stims = 150 ;
rng('default') ;
p           = randperm(length(stims_valid),max_stims) ;
% stims_valid = stims_valid(p) ;
% s = round((length(stims_valid)-1)*rand+1);
rng('shuffle') ;
s = p(round((max_stims-1)*rand+1));



% n=10000000;
% tmp=nan(1,n);
% for i=1:n
%     tmp(i)=round((length(stims_valid)-1)*rand+1);
% end
% figure, histogram(tmp,0.5:1:length(stims_valid)+0.5)

% s = 1 ;
% s = 2 ;
% s = 5 ; 
% s = 6 ; 
% s = 7 ; 
% s = 8 ; 
% s = 9 ;
% s = 2048 ; % bcp de ccep = 39
% s = 353 ;

% s = 10 ;
% stim        = stims_valid(s);
% s = 2569 ; % bcp de cceps

% s = 1831 ; % why not ?
% [ DCM_s1_acc, DCM_s2_median_acc, DCM_s2_bpa_nocond_acc ] = review_stimDCM_V6_stim( stims_valid, s ) ;


acc_th      = 80 ;
% stims_valid = stims_valid(p) ;

total_cceps_valid               = 0 ;
total_cceps_acc_s1              = 0 ;
total_cceps_acc_s2_median       = 0 ;
total_cceps_acc_s2_bpa_nocond   = 0 ;

skipStep2Median = true ;
parfor s=1:length(stims_valid)
% for s=1:length(stims_valid)
    try 
        [ DCM_s1_acc, DCM_s2_median_acc, DCM_s2_bpa_nocond_acc ] = review_stimDCM_V6_stim( stims_valid, s ) ;
     
%         fprintf( 1, [ 'Stim' num2str(s) ' - Valid cceps: ' num2str(length(cceps_valid)) '\n' ] ) ;
%         fprintf( 1, [ 'Stim' num2str(s) ' - Valid cceps: ' num2str(length(DCM_s1_acc)) '\n' ] ) ;
        idx_acc = arrayfun( @(d) d.R2_c_m0_10_55 > acc_th, DCM_s1_acc ) ;
%         fprintf( 1, [ '\t' ' - Step1: Acc cceps: ' num2str(length(find(idx_acc))) '\n' ] ) ;

        if ~skipStep2Median
                idx_median_acc = arrayfun( @(d) d.R2_c_m0_10_55 > acc_th, DCM_s2_median_acc(idx_acc) ) ;
        %         fprintf( 1, [ '\t' ' - Step2 Median: Acc cceps: ' num2str(length(find(idx_median_acc))) '\n' ] ) ;
                total_cceps_acc_s2_median       = total_cceps_acc_s2_median     + length(find(idx_median_acc)) ;
        end

        idx_bpanocond_acc = arrayfun( @(d) d.R2_c_m0_10_55 > acc_th, DCM_s2_bpa_nocond_acc(idx_acc) ) ;
%         fprintf( 1, [ '\t' ' - Step2 BPA nocond: Acc cceps: ' num2str(length(find(idx_bpanocond_acc))) '\n' ] ) ;

        total_cceps_valid               = total_cceps_valid             + length(DCM_s1_acc) ;
        total_cceps_acc_s1              = total_cceps_acc_s1            + length(find(idx_acc)) ;
        
        total_cceps_acc_s2_bpa_nocond   = total_cceps_acc_s2_bpa_nocond + length(find(idx_bpanocond_acc)) ;
        
    catch
        fprintf( 1, [ 'DCM not found?' '\n' ] ) ;
    end
end
    
total_cceps_valid               % 1260
total_cceps_acc_s1              % 801
total_cceps_acc_s2_median       % 567
total_cceps_acc_s2_bpa_nocond   % 552



end



function [ DCM_s1_acc, DCM_s2_median_acc, DCM_s2_bpa_nocond_acc ] = review_stimDCM_stim( stims_valid, s )

fprintf( 1, [ 'stim' num2str(s)  '\n' ] ) ;

verbose = true ;
verbose = false ;
if verbose
    fprintf( 1, [ 'review stim' num2str(s) '\n' ] ) ;
end

DCM_s1_acc              = struct([]) ;
DCM_s2_median_acc       = struct([]) ; 
DCM_s2_bpa_nocond_acc 	= struct([]) ; 

% icm_screen = false ;
icm_screen  = true ;

acc_th          = 80 ;
max_diff_peak   = 10 ;


sequence_id = 'ArtefactCorrectionPCHIP' ; 
stim        = stims_valid(s);

[ stimData, cceps_valid ]   = loadStim_V12( sequence_id, stim.patient, stim.crf, stim.stimContact ) ;
if ~isequal( cceps_valid, stim.cceps_valid )
   error( 'inconsistency in stim cceps_valid' ) 
end
if isempty(cceps_valid)
%     error('cceps_valid empty')
    if verbose
        fprintf( 1, [ 'stim' num2str(s)  ': cceps_valid empty\n' ] ) ;
    end
    return
end



pF_D            = '1_PLhalf' ;
pC_D            = [ 0 0 ; 1 0 ] ;
k1              = 8 ;
k2              = 2 ;
pE_T            = [ -log(k1) 0 ; -log(k2) 0 ] ;
pE_G            = [  log(k1) 0 ;  log(k2) 0 ] ;
pC_T            = [ 1 1/16 ; 1 1/16 ] ;
pC_G            = [ 1 1/16 ; 1 1/16 ] ;
pC_S            = [ 1/16 1/16 ] ;
pC_H            = [ 1/16 1/16 1/16 1/16 ] ;
fit_duration_id = 'PLp2D_M40' ;




[ dataDir, ~, ~ ]           = simuDirs() ;
patientsDir                 = [ dataDir '/PATS__'  sequence_id ] ;
% sequence_avg_dir            = [ sequence_id 'ComputeAverage' ] ;
if strcmp( sequence_id, 'ArtefactCorrectionPCHIP' )
    sequence_avg_dir    = 'SEQ_BM_ACPCHIP_F_ComputeAverage' ;
end


stimDir          	= [ patientsDir '/' stim.patient '/SEEG/' stim.crf '/' sequence_avg_dir '/' stim.stimContact ] ;
ccepsDir            = [ stimDir '/cceps' ] ;

ccepContactNames    = stimData.sensorName(cceps_valid) ;
stimParcels         = stimData.stimParcel(cceps_valid) ;
ccepParcels         = stimData.sensorParcel(cceps_valid) ;
lat_start           = stimData.mat_lat_start(cceps_valid) ;
onset_delay         = stimData.mat_onset_delay(cceps_valid) ;
peak_latency        = stimData.mat_delay(cceps_valid) ;
duration            = stimData.mat_duration(cceps_valid) ;

atlasId         = 'AALDilate' ;
try
    stimParcelName  = strrep( parseAtlasParcel( stimParcels{1}, atlasId ), '_', '\_' ) ;
catch
    stimParcelName  = 'stimParcelUnknown' ;
end

plot_DCM        = true ;
% plot_DCM        = false ;
plot_DCM_post   = false ;

plot_step1      = true ;
% plot_step1      = false ;

% skipStep2       = false ;
skipStep2       = true ;
plot_step2      = true ;
% plot_step2      = false ;

skipBPA_nocond  = false ;
% skipBPA_nocond  = true ;
plot_BPA_nocond = true ;


% minPL = 66 ;
% maxPL = 80 ;

for c=1:length(cceps_valid)
    
%     if peak_latency{c}(5) < minPL, continue ; end
%     if peak_latency{c}(5) > maxPL, continue ; end
    
    dataFname       = [ ccepsDir '/' stim.stimContact '__' ccepContactNames{c} '.mat' ] ;
    
    try
        ccepParcelName  = strrep( parseAtlasParcel( ccepParcels{c}, atlasId ), '_', '\_' ) ;
    catch
        ccepParcelName  = 'ccepParcelUnknown' ;
    end
    
    config      = struct( 'step', 1, 'compute_mode', '' ) ;
    dcmFname    = buildDCMFname_V12( dataFname, pF_D, pC_D, pE_T, pE_G, pC_T, pC_G, pC_S, pC_H, config, fit_duration_id ) ;
%     if ~exist(dcmFname,'file'), fprintf( 1, [ dcmFname ': DCM not found' '\n' ] ) ; continue ; end
    if ~exist(dcmFname,'file'), error( [ dcmFname ': DCM not found' ] ) ; end
        
    DCM_s1      = load( dcmFname ) ;
    DCM_s1   	= DCM_s1.DCM ;
    [ ~, ~, ~, R2_c_m0_s1_full ]    = compute_accuracy( DCM_s1, [] ) ;
    DCM_s1_acc(c).dcmFname          = dcmFname ;
%     DCM_s1_acc(c).R2_c_m0_10_55 = full(R2_c_m0_s1) ;
    
    [~,locs]                      	= findpeaks(abs(DCM_s1.H{1})) ;
    if ~isempty(locs)
        s1_peak          = DCM_s1.xY.pst(locs(1)) ;
    else
        s1_peak          = nan ;
    end
            

    if plot_step1
        
        if icm_screen
            H1=figure('Position', [672 1141 560 289]) ;
            H2=figure('Position', [1234 1141 560 289]) ;
            H12=figure('Position', [1796 1141 560 289]);
        else
            H1=figure('Position', [198 699 470 178]) ;
            H2=figure('Position', [673 699 460 178]) ;
            H12=figure('Position', [1139 699 451 178]);
        end
        
        figure(H1);spm_dcm_erp_results(DCM_s1,'Response',H1);          suptitle( [ 's' num2str(s) ' c' num2str(c) ' s1 - R2: ' num2str(R2_c_m0_s1_full) ' LS ' num2str(lat_start{c}) ' OD ' num2str(onset_delay{c}(5)) ' PL ' num2str(peak_latency{c}(5)) ' DU ' num2str(duration{c}) ] ) ;
        figure(H2);spm_dcm_erp_results(DCM_s1,'ERPs (sources)',H2);    suptitle( [ 's' num2str(s) ' c' num2str(c) ' s1 - R2: ' num2str(R2_c_m0_s1_full) ] ) ;
        figure(H12);bar([DCM_s1.Ep.T(1,1) DCM_s1.Ep.T(1,2) DCM_s1.Ep.G(1,1) DCM_s1.Ep.G(1,2) DCM_s1.Ep.S(1) DCM_s1.Ep.S(2) DCM_s1.Ep.C(1) DCM_s1.Ep.H(1) DCM_s1.Ep.H(2) DCM_s1.Ep.H(3) DCM_s1.Ep.H(4) ]) ; ylim( [-3.5 3.5] ) ;
        
    end
    
    
    % ALL CCEPS are fitted in step 2 
    
    
%     if R2_c_m0_s1_full < acc_th || abs( s1_peak - peak_latency{c}(5) ) > max_diff_peak
%         continue
%     end
    
    valid_s1 = ~ ( R2_c_m0_s1_full < acc_th || abs( s1_peak - peak_latency{c}(5) ) > max_diff_peak ) ;
        
%     if ~skipStep2
%         config      = struct( 'step', 2, 'avg_mode', 'median' ) ;
%         dcmFname    = buildDCMFname_V6( dataFname, pF_D, pE_T, pE_G, pC_T, pC_G, pC_S, pC_H, pC_D, config ) ;
%         if ~exist(dcmFname,'file')
%             fprintf( 1, [ dcmFname ': DCM not found' '\n' ] ) ; 
% %             error( [ dcmFname ': not found' ] ) ; 
%         end
%         DCM_s2_median   = load( dcmFname ) ;
%         DCM_s2_median   = DCM_s2_median.DCM ;
%         [ ~, R2_c_median, ~, R2_c_m0_median ]     = compute_accuracy( DCM_s2_median, [10 55] ) ;
%         DCM_s2_median_acc(c).dcmFname            = dcmFname ;
%         DCM_s2_median_acc(c).R2_c_m0_10_55       = full(R2_c_m0_median) ;
%         
%         if plot_step2
%             
%             if icm_screen
%                 H3=figure('Position', [672 758 560 302]) ;
%                 H4=figure('Position', [1234 758 560 302]) ;
%                 H34=figure('Position', [1796 758 560 302]);
%             else
%                 H3=figure('Position', [197 466 471 147]);
%                 H4=figure('Position', [673 465 460 150]) ;
%                 H34=figure('Position', [1140 465 450 146]);
%             end
%             
%             figure(H3);spm_dcm_erp_results(DCM_s2_median,'Response',H3);          suptitle( [ 's' num2str(s) ' c' num2str(c) ' s2 ' config.avg_mode ' - R2 10-55: ' num2str(R2_c_median) '- m0: ' num2str(R2_c_m0_median) ] ) ;
%             figure(H4);spm_dcm_erp_results(DCM_s2_median,'ERPs (sources)',H4);    suptitle( [ 's' num2str(s) ' c' num2str(c) ' s2 ' config.avg_mode ' - R2 10-55: ' num2str(R2_c_median) '- m0: ' num2str(R2_c_m0_median) ] ) ;
%             figure(H34);bar([DCM_s2_median.Ep.T(1,1) DCM_s2_median.Ep.T(1,2) DCM_s2_median.Ep.G(1,1) DCM_s2_median.Ep.G(1,2) DCM_s2_median.Ep.S(1) DCM_s2_median.Ep.S(2) DCM_s2_median.Ep.C(1) DCM_s2_median.Ep.H(1) DCM_s2_median.Ep.H(2) DCM_s2_median.Ep.H(3) DCM_s2_median.Ep.H(4) ]) ; ylim( [-3.5 3.5] ) ;
%             
%         end
%     end
    
    
    if ~skipBPA_nocond
        config      = struct( 'step', 2, 'avg_mode', 'bpa_nocond' ) ;
        dcmFname    = buildDCMFname_V12( dataFname, pF_D, pC_D, pE_T, pE_G, pC_T, pC_G, pC_S, pC_H, config, fit_duration_id ) ;
        if ~exist(dcmFname,'file')
%             fprintf( 1, [ dcmFname ': DCM not found' '\n' ] ) ; 
%             continue
            error( [ dcmFname ': not found' ] ) ; 
        end
        DCM_s2_bpa_nocond         = load( dcmFname ) ;
        DCM_s2_bpa_nocond         = DCM_s2_bpa_nocond.DCM ;
        [ ~, ~, ~, R2_c_m0_bpa_nocond ]     = compute_accuracy( DCM_s2_bpa_nocond, [] ) ;
        DCM_s2_bpa_nocond_acc(c).dcmFname            = dcmFname ;
%         DCM_s2_bpa_nocond_acc(c).R2_c_m0_10_55       = full(R2_c_m0_bpa_nocond) ;


        [~,locs]                      	= findpeaks(abs(DCM_s2_bpa_nocond.H{1})) ;
        if ~isempty(locs)
            s2_peak          = DCM_s2_bpa_nocond.xY.pst(locs(1)) ;
        else
            s2_peak          = nan ;
        end


%         if R2_c_m0_bpa_nocond < acc_th || abs( s2_peak - peak_latency{c}(5) ) > max_diff_peak
%             continue
%         end
        valid_s2 = ~ ( R2_c_m0_bpa_nocond < acc_th || abs( s2_peak - peak_latency{c}(5) ) > max_diff_peak ) ;
    
        fprintf( 1, [ 'valid_s1: ' num2str(valid_s1) ' ; valid_s2: ' num2str(valid_s2) '\n'  ] ) ;
        
        if plot_step2
            
            if icm_screen
                H5=figure('Position', [672 758 560 302]) ;
                H6=figure('Position', [1234 758 560 302]) ;
                %H56=figure('Position', [1796 758 560 302]);
            else
                H5=figure('Position', [198 231 471 147]);
                H6=figure('Position', [674 231 464 148]) ;
                %H56=figure('Position', [1140 233 450 146]);
            end
            
%             figure(H5);spm_dcm_erp_results(DCM_s2_bpa_nocond,'Response',H5);          suptitle( [ 's' num2str(s) ' c' num2str(c) ' s2 ' config.avg_mode ' - R2: ' num2str(R2_c_m0_bpa_nocond) ] ) ;
%             figure(H6);spm_dcm_erp_results(DCM_s2_bpa_nocond,'ERPs (sources)',H6);    suptitle( [ 's' num2str(s) ' c' num2str(c) ' s2 ' config.avg_mode ' - R2: ' num2str(R2_c_m0_bpa_nocond) ] ) ;
%             figure(H56);bar([DCM_s2_bpa_nocond.Ep.T(1,1) DCM_s2_bpa_nocond.Ep.T(1,2) DCM_s2_bpa_nocond.Ep.G(1,1) DCM_s2_bpa_nocond.Ep.G(1,2) DCM_s2_bpa_nocond.Ep.S(1) DCM_s2_bpa_nocond.Ep.S(2) DCM_s2_bpa_nocond.Ep.C(1) DCM_s2_bpa_nocond.Ep.H(1) DCM_s2_bpa_nocond.Ep.H(2) DCM_s2_bpa_nocond.Ep.H(3) DCM_s2_bpa_nocond.Ep.H(4) ]) ; ylim( [-3.5 3.5] ) ;
            figure(H5);spm_dcm_erp_results(DCM_s2_bpa_nocond,'Response',H5);          suptitle( [ 's' num2str(s) ' ' stimParcelName ' - ' ccepParcelName ] ) ;
            figure(H6);spm_dcm_erp_results(DCM_s2_bpa_nocond,'ERPs (sources)',H6);    suptitle( [ 's' num2str(s) ' c' num2str(c) ' s2 ' config.avg_mode ' - R2: ' num2str(R2_c_m0_bpa_nocond) ] ) ;
            %figure(H56);bar([DCM_s2_bpa_nocond.Ep.T(1,1) DCM_s2_bpa_nocond.Ep.T(1,2) DCM_s2_bpa_nocond.Ep.G(1,1) DCM_s2_bpa_nocond.Ep.G(1,2) DCM_s2_bpa_nocond.Ep.S(1) DCM_s2_bpa_nocond.Ep.S(2) DCM_s2_bpa_nocond.Ep.C(1) DCM_s2_bpa_nocond.Ep.H(1) DCM_s2_bpa_nocond.Ep.H(2) DCM_s2_bpa_nocond.Ep.H(3) DCM_s2_bpa_nocond.Ep.H(4) ]) ; ylim( [-3.5 3.5] ) ;
            
        end
    
    end

    
end

return

idx_acc = arrayfun( @(d) d.R2_c_m0_10_55 > acc_th, DCM_s1_acc ) ;


verbose = false ;
if verbose
    fprintf( 1, [ 'Stim' num2str(s) ' - Valid cceps: ' num2str(length(cceps_valid)) '\n' ] ) ;
    
    fprintf( 1, [ '\t' ' - Step1: Acc cceps: ' num2str(length(find(idx_acc))) '\n' ] ) ;
    if ~skipStep2
        idx_median_acc = arrayfun( @(d) d.R2_c_m0_10_55 > acc_th, DCM_s2_median_acc(idx_acc) ) ;
        fprintf( 1, [ '\t' ' - Step2 Median: Acc cceps: ' num2str(length(find(idx_median_acc))) '\n' ] ) ;
    end
    idx_bpanocond_acc = arrayfun( @(d) d.R2_c_m0_10_55 > acc_th, DCM_s2_bpa_nocond_acc(idx_acc) ) ;
    fprintf( 1, [ '\t' ' - Step2 BPA nocond: Acc cceps: ' num2str(length(find(idx_bpanocond_acc))) '\n' ] ) ;
end


if isempty(idx_acc), return ; end



%% average parameters
average_param = false ;
% average_param = true ;
if ~average_param, return ; end



% BPA with all DCM
P_all       = arrayfun( @(d) d.dcmFname, DCM_s1_acc, 'UniformOutput', false ) ;
name        = '' ;
nocond      = 0 ;
graphics    = 0 ;
[ DCM_all_cond ]       = spm_dcm_average(P_all,name,nocond,graphics) ;

nocond      = 1 ;
graphics    = 0 ;
[ DCM_all_nocond ] = spm_dcm_average(P_all,name,nocond,graphics) ;
plot_average_param_bpa = false ;
if plot_average_param_bpa
    figure ;
    subplot( 2,3,1 ) ; bar( [ DCM_all_cond.Ep.T(1,:)' DCM_all_nocond.Ep.T(1,:)' ] ); title( 'Ep.T(1,:)' ) ;
    subplot( 2,3,2 ) ; bar( [ DCM_all_cond.Ep.G(1,:)' DCM_all_nocond.Ep.G(1,:)' ] ); title( 'Ep.G(1,:)' ) ;
    subplot( 2,3,3 ) ; bar( [ DCM_all_cond.Ep.S(:)    DCM_all_nocond.Ep.S(:) ] ); title( 'Ep.S' ) ;
    subplot( 2,3,4 ) ; bar( [ DCM_all_cond.Ep.C(1)    DCM_all_nocond.Ep.C(1) ; 0 0 ] ); title( 'Ep.C(1)' ) ;
    subplot( 2,3,5 ) ; bar( [ DCM_all_cond.Ep.H(:)    DCM_all_nocond.Ep.H(:) ] ); title( 'Ep.H' ) ;
    % subplot( 2,3,6 ) ; bar( [ DCM.Ep.D(:) DCM_nocond.Ep.D(:) ] ); title( 'Ep.D' ) ;
    suptitle( [ 'stim' num2str(s) ': ' num2str(length(P_all)) ' cceps valid' ] ) ;
end


% BPA with acc DCM
P_acc       = arrayfun( @(d) d.dcmFname, DCM_s1_acc(idx_acc), 'UniformOutput', false ) ;
nocond      = 0 ;
graphics    = 0 ;
[DCM_acc_cond]  = spm_dcm_average(P_acc,name,nocond,graphics) ;

nocond      = 1 ;
graphics    = 0 ;
[DCM_acc_nocond] = spm_dcm_average(P_acc,name,nocond,graphics) ;

if plot_average_param_bpa
    figure ;
    subplot( 2,3,1 ) ; bar( [ DCM_acc_cond.Ep.T(1,:)' DCM_acc_nocond.Ep.T(1,:)' ] ); title( 'Ep.T(1,:)' ) ;
    subplot( 2,3,2 ) ; bar( [ DCM_acc_cond.Ep.G(1,:)' DCM_acc_nocond.Ep.G(1,:)' ] ); title( 'Ep.G(1,:)' ) ;
    subplot( 2,3,3 ) ; bar( [ DCM_acc_cond.Ep.S(:)    DCM_acc_nocond.Ep.S(:) ] ); title( 'Ep.S' ) ;
    subplot( 2,3,4 ) ; bar( [ DCM_acc_cond.Ep.C(1)    DCM_acc_nocond.Ep.C(1) ; 0 0 ] ); title( 'Ep.C(1)' ) ;
    subplot( 2,3,5 ) ; bar( [ DCM_acc_cond.Ep.H(:)    DCM_acc_nocond.Ep.H(:) ] ); title( 'Ep.H' ) ;
    % subplot( 2,3,6 ) ; bar( [ DCM.Ep.D(:) DCM_nocond.Ep.D(:) ] ); title( 'Ep.D' ) ;
    suptitle( [ 'stim' num2str(s) ': ' num2str(length(P_acc)) ' cceps accurate' ] ) ;
end
    




%% mean / median of parameters
T = nan(length(P_acc),2,2) ;
G = nan(length(P_acc),2,2) ;
S = nan(length(P_acc),2) ;
C = nan(length(P_acc),2) ;
H = nan(length(P_acc),4) ;
for d=1:length(P_acc)
    tmp         = load( P_acc{d} ) ;
    T(d,:,:)    = full(tmp.DCM.Ep.T) ;
    G(d,:,:)    = full(tmp.DCM.Ep.G) ;
    S(d,:)      = full(tmp.DCM.Ep.S) ;
    C(d,:)      = full(tmp.DCM.Ep.C) ;
    H(d,:)      = full(tmp.DCM.Ep.H) ;
end

T_mean   = squeeze(mean(T,1)) ;
T_median = squeeze(median(T,1)) ;
G_mean   = squeeze(mean(G,1)) ;
G_median = squeeze(median(G,1)) ;
S_mean   = squeeze(mean(S,1)) ;
S_median = squeeze(median(S,1)) ;
C_mean   = squeeze(mean(C,1)) ;
C_median = squeeze(median(C,1)) ;
H_mean   = squeeze(mean(H,1)) ;
H_median = squeeze(median(H,1)) ;


figure( 'Position', [209 39 1314 332] ) ;
subplot( 2, 6, 1 ) ; bar( T(:,1,1) ) ;  hold on ; bar( length(P_acc)+1, T_mean(1,1), 'r' ) ; bar( length(P_acc)+2, T_median(1,1), 'g' ) ; bar( length(P_acc)+3, DCM_acc_cond.Ep.T(1,1), 'y' ) ;  bar( length(P_acc)+4, DCM_acc_nocond.Ep.T(1,1), 'm' ) ;  title( 'Te' ) ;
subplot( 2, 6, 2 ) ; bar( T(:,1,2) ) ;  hold on ; bar( length(P_acc)+1, T_mean(1,2), 'r' ) ; bar( length(P_acc)+2, T_median(1,2), 'g' ) ; bar( length(P_acc)+3, DCM_acc_cond.Ep.T(1,2), 'y' ) ;  bar( length(P_acc)+4, DCM_acc_nocond.Ep.T(1,2), 'm' ) ;  title( 'Ti' ) ;

subplot( 2, 6, 3 ) ; bar( G(:,1,1) ) ;  hold on ; bar( length(P_acc)+1, G_mean(1,1), 'r' ) ; bar( length(P_acc)+2, G_median(1,1), 'g' ) ; bar( length(P_acc)+3, DCM_acc_cond.Ep.G(1,1), 'y' ) ;  bar( length(P_acc)+4, DCM_acc_nocond.Ep.G(1,1), 'm' ) ;  title( 'Ge' ) ;
subplot( 2, 6, 4 ) ; bar( G(:,1,2) ) ;  hold on ; bar( length(P_acc)+1, G_mean(1,2), 'r' ) ; bar( length(P_acc)+2, G_median(1,2), 'g' ) ; bar( length(P_acc)+3, DCM_acc_cond.Ep.G(1,2), 'y' ) ;  bar( length(P_acc)+4, DCM_acc_nocond.Ep.G(1,2), 'm' ) ;  title( 'Gi' ) ;

subplot( 2, 6, 5 ) ; bar( S(:,1) ) ;    hold on ; bar( length(P_acc)+1, S_mean(1,1), 'r' ) ; bar( length(P_acc)+2, S_median(1,1), 'g' ) ; bar( length(P_acc)+3, DCM_acc_cond.Ep.S(1,1), 'y' ) ;  bar( length(P_acc)+4, DCM_acc_nocond.Ep.S(1,1), 'm' ) ;  title( 'S1' ) ;
subplot( 2, 6, 6 ) ; bar( S(:,2) ) ;    hold on ; bar( length(P_acc)+1, S_mean(1,2), 'r' ) ; bar( length(P_acc)+2, S_median(1,2), 'g' ) ; bar( length(P_acc)+3, DCM_acc_cond.Ep.S(1,2), 'y' ) ;  bar( length(P_acc)+4, DCM_acc_nocond.Ep.S(1,2), 'm' ) ;  title( 'S2' ) ;

subplot( 2, 6, 7 ) ; bar( C(:,1) ) ;    hold on ; bar( length(P_acc)+1, C_mean(1,1), 'r' ) ; bar( length(P_acc)+2, C_median(1,1), 'g' ) ; bar( length(P_acc)+3, DCM_acc_cond.Ep.C(1,1), 'y' ) ;  bar( length(P_acc)+4, DCM_acc_nocond.Ep.C(1,1), 'm' ) ;  title( 'C(1)' ) ;

subplot( 2, 6, 8 ) ; bar( H(:,1) ) ;    hold on ; bar( length(P_acc)+1, H_mean(1,1), 'r' ) ; bar( length(P_acc)+2, H_median(1,1), 'g' ) ; bar( length(P_acc)+3, DCM_acc_cond.Ep.H(1,1), 'y' ) ;  bar( length(P_acc)+4, DCM_acc_nocond.Ep.H(1,1), 'm' ) ;  title( 'H1' ) ;
subplot( 2, 6, 9 ) ; bar( H(:,2) ) ;    hold on ; bar( length(P_acc)+1, H_mean(1,2), 'r' ) ; bar( length(P_acc)+2, H_median(1,2), 'g' ) ; bar( length(P_acc)+3, DCM_acc_cond.Ep.H(1,2), 'y' ) ;  bar( length(P_acc)+4, DCM_acc_nocond.Ep.H(1,2), 'm' ) ;  title( 'H2' ) ;
subplot( 2, 6, 10 ) ; bar( H(:,3) ) ;   hold on ; bar( length(P_acc)+1, H_mean(1,3), 'r' ) ; bar( length(P_acc)+2, H_median(1,3), 'g' ) ; bar( length(P_acc)+3, DCM_acc_cond.Ep.H(1,3), 'y' ) ;  bar( length(P_acc)+4, DCM_acc_nocond.Ep.H(1,3), 'm' ) ;  title( 'H3' ) ;
subplot( 2, 6, 11 ) ; bar( H(:,4) ) ;   hold on ; bar( length(P_acc)+1, H_mean(1,4), 'r' ) ; bar( length(P_acc)+2, H_median(1,4), 'g' ) ; bar( length(P_acc)+3, DCM_acc_cond.Ep.H(1,4), 'y' ) ;  bar( length(P_acc)+4, DCM_acc_nocond.Ep.H(1,4), 'm' ) ;  title( 'H4' ) ;




end





function checkStimDCM( update_DCMs, s1Selection, s2Selection )

% icm_screen = false ;
icm_screen = true ;
saveFigures = true ;

V                 	= 'V14' ;

% acc_th_s1           = 80 ;
% acc_th_s1           = 70 ;
% acc_th_s1           = 60 ;
% acc_th_s1           = 50 ;
% acc_th_s1           = 0 ;
% acc_th_s1           = nan ;
% acc_th_s1           = s1Selection.R2 ;

% max_diff_peak_s1    = 10 ;
% max_diff_peak_s1    = nan ;

% acc_th_s2           = 80 ;
% max_diff_peak_s2    = 5 ;



% [ validSuffix, applyGreyMatterFilter, applySamplingRateFilter, applyMinPeakLatencyFilter, applyMaxPeakLatencyFilter, applyValidDuration ] = eval( [ 'validStimParams_' V '() ; ' ] ) ;
% minPL               = applyMinPeakLatencyFilter ;
% % minPL               = 12 ; 
% maxPL               = applyMaxPeakLatencyFilter ;
minPL       	= s1Selection.minPL ;
maxPL        	= s1Selection.maxPL ;


sequence_id = 'ArtefactCorrectionPCHIP' ; 
stims_valid = eval( [ 'validStims_' V '( sequence_id )' ] );


pF_T            = [ 8 16 ];
pF_Te           = pF_T(1) ;
pF_Ti           = pF_T(2) ;

% % pF_D            = '1_PLhalf' ;
% pC_D            = [ 0 0 ; 1 0 ] ;
% k1              = 8 ;
% % k2              = 2 ;
% % pE_T            = [ -log(k1) -log(k1); -log(k2) -log(k2) ] ;
% % pE_G            = [  log(k1)  log(k1);  log(k2)  log(k2) ] ;
% pC_T            = [ 1 1 ; 1 1 ] ;
% pC_G            = [ 1 1 ; 1 1 ] ;
% pC_S            = [ 1/16 1/16 ] ;
% pC_H            = [ 1/16 1/16 1/16 1/16 ] ;
% fit_duration_id = 'm20_PLp2D_M40' ;
% pF_D                    = [] ;
% pC_D                    = [] ;
% pE_T                    = [] ;
% pE_G                    = [] ;
% pC_T                    = [] ;
% pC_G                    = [] ;
% pC_S                    = [] ;
% pC_H                    = [] ;
% config_s2               = struct( 'step', 2, 'avg_mode', 'bpa_nocond' ) ;
% config_s2.s1Selection   = s1Selection ;
% fit_duration_id         = [] ;





[ dataDir, dcmDir, ~ ]	= simuDirs() ;
patientsDir             = [ dataDir '/PATS__'  sequence_id ] ;
if strcmp( sequence_id, 'ArtefactCorrectionPCHIP' )
    sequence_avg_dir    = 'SEQ_BM_ACPCHIP_F_ComputeAverage' ;
end


config_s1 	= struct( 'step', 1 ) ;
config_s2 	= struct( 'step', 2, 'avg_mode', 'bpa_nocond' ) ;
config_s2.s1Selection = s1Selection ;

s1Selection_txt = [ 'minPL' num2str(s1Selection.minPL) '_maxPL' num2str(s1Selection.maxPL) '_R2' num2str(s1Selection.R2) '_MPD' num2str(s1Selection.MPD) ] ; 
s2Selection_txt = [ 'R2' num2str(s2Selection.R2) '_MPD' num2str(s2Selection.MPD) ] ; 
  
if update_DCMs
    
    peakLatency_all = cell(size(stims_valid)) ;
    % H1=figure('Position', [672 1141 560 289]) ;
    DCMs = cell(size(stims_valid)) ;
    
    DCMsNotFound_s1 = cell(size(stims_valid)) ;
    DCMsError_s1    = cell(size(stims_valid)) ;
    
    DCMsNotFound_s2 = cell(size(stims_valid)) ;
    DCMsError_s2    = cell(size(stims_valid)) ;
    
    
    parfor s=1:length(stims_valid)
%     for s=1:length(stims_valid)
        
        s
        %     if s > 100
        %         break
        %     end
        
        stim = stims_valid(s);
        [ stimData, cceps_valid ] = loadStim_V14( sequence_id, stim.patient, stim.crf, stim.stimContact ) ;
        stimDir          	= [ patientsDir '/' stim.patient '/SEEG/' stim.crf '/' sequence_avg_dir '/' stim.stimContact ] ;
        ccepsDir            = [ stimDir '/cceps' ] ;
        ccepContactNames    = stimData.sensorName(cceps_valid) ;
        ccepPeakLatency     = stimData.mat_delay(cceps_valid) ;
        
        peakLatency_all{s} 	= nan(size(cceps_valid));
        DCMs{s}             = cell(size(cceps_valid));
        DCMsNotFound_s1{s}  = cell(size(cceps_valid));
        DCMsError_s1{s}     = cell(size(cceps_valid));
        
%         config          = struct( 'step', 1 ) ;
        for c=1:length(cceps_valid)
            
            peakLatency_all{s}(c)       = ccepPeakLatency{c}(5) ;
            dataFname       = [ ccepsDir '/' stim.stimContact '__' ccepContactNames{c} '.mat' ] ;
            
%             [ cd, te ]      = cd_te_from_pl( peakLatency_all{s}(c) ) ;
%             pF_D            = struct( 'id', '0_ADJ', 'pF_D', [ 0 cd ] ) ;
%             k2              = pF_Te/te ; % te = Te / k2 => k2 = Te / te
%             pE_T            = [ -log(k1) -log(k1); -log(k2) -log(k2) ] ;
%             pE_G            = [  log(k1)  log(k1);  log(k2)  log(k2) ] ;
    
%             dcmFname        = buildDCMFname_V14( dataFname, pF_D, pC_D, pE_T, pE_G, pC_T, pC_G, pC_S, pC_H, config_s1, fit_duration_id ) ;
            dcmFname        = buildDCMFname_V14( dataFname, [], [], [], [], [], [], [], [], config_s1, [] ) ;
            if ~exist(dcmFname,'file')
%                 fprintf( 1, [ dcmFname ': DCM not found' '\n'] ) ;
                DCMsNotFound_s1{s}{c} = dcmFname ;
%                 continue ;
                error( [ dcmFname ': DCM not found' ] ) ;
            end
            
            try
                DCM                             = load(dcmFname) ;
            catch
                fprintf( 1, [ dcmFname ': DCM on error' '\n'] ) ;
                DCMsError_s1{s}{c}              = dcmFname ;
                continue
            end
            
            DCM                             = DCM.DCM ;
            
            DCMs{s}{c}.s1_dcmFname          = dcmFname ;
            
            DCMs{s}{c}.s1_peakLatency       = ccepPeakLatency{c}(5) ;
            DCMs{s}{c}.Tdcm                 = DCM.options.Tdcm ;
            [ ~, ~, ~, R2_c_m0_s1_full ]    = compute_accuracy( DCM, [] ) ;
            DCMs{s}{c}.s1_acc_full          = R2_c_m0_s1_full ;
            
            [~,locs]                      	= findpeaks(abs(DCM.H{1})) ;
            if ~isempty(locs)
                DCMs{s}{c}.s1_peak          = DCM.xY.pst(locs(1)) ;
            else
                DCMs{s}{c}.s1_peak          = nan ;
            end
            
            Cp                              = spm_unvec(diag(DCM.Cp),DCM.Ep) ;
            [ ~, lognormal_mode ]           = lognormal( DCM.Ep.D(2,1) + log( DCM.M.pF.D(2) ), Cp.D(2,1) ) ;
            DCMs{s}{c}.s1_conductionDelay   = lognormal_mode ;
            
            [ ~, lognormal_mode ]           = lognormal( DCM.Ep.T(2,1) + log( pF_Te ), Cp.T(2,1) ) ;
            DCMs{s}{c}.s1_synapticTe        = lognormal_mode ;
            
            [ ~, lognormal_mode ]           = lognormal( DCM.Ep.T(2,2) + log( pF_Ti ), Cp.T(2,2) ) ;
            DCMs{s}{c}.s1_synapticTi        = lognormal_mode ;
            
        end

        if ~isempty( find( cellfun( @(c) ~isempty(c), DCMsNotFound_s1{s} ) ) ) || ~isempty( find( cellfun( @(c) ~isempty(c), DCMsError_s1{s} ) ) )
            continue
        end
        
%         do_s2 = false ;
        
%         for c=1:length(cceps_valid)
%             if DCMs{s}{c}.s1_acc_full > acc_th_s1 && abs( peakLatency_all{s}(c) - DCMs{s}{c}.s1_peaks ) < max_diff_peak_s1
%                do_s2 = true ;
%                break ;
%             end
%         end
        
     	s1_selected = ( s1Selection.minPL <= peakLatency_all{s} ) & ( peakLatency_all{s} <= s1Selection.maxPL ) ;
        if ~isnan(s1Selection.R2)
            s1_selected = s1_selected & cellfun( @(d) ( full(d.s1_acc_full) > s1Selection.R2 ), DCMs{s} ) ;
        end
        if ~isnan(s1Selection.MPD)
            s1_selected = s1_selected & cellfun( @(d) ( abs( d.s1_peak  - d.s1_peakLatency ) < s1Selection.MPD ), DCMs{s} ) ;
        end
        
        if isempty(find(s1_selected))
            continue
        end
           
        
%         config      = struct( 'step', 2, 'avg_mode', 'bpa_nocond' ) ;
        % ALL CCEPS are refitted in step 2 !!!!
        for c=1:length(cceps_valid)
           
            dataFname  	= [ ccepsDir '/' stim.stimContact '__' ccepContactNames{c} '.mat' ] ;
            
%             [ cd, te ]      = cd_te_from_pl( peakLatency_all{s}(c) ) ;
%             pF_D            = struct( 'id', '0_ADJ', 'pF_D', [ 0 cd ] ) ;
%             k2              = pF_Te/te ; % te = Te / k2 => k2 = Te / te
%             pE_T            = [ -log(k1) -log(k1); -log(k2) -log(k2) ] ;
%             pE_G            = [  log(k1)  log(k1);  log(k2)  log(k2) ] ;
    
%             dcmFname                = buildDCMFname_V14( dataFname, pF_D, pC_D, pE_T, pE_G, pC_T, pC_G, pC_S, pC_H, config_s2, fit_duration_id ) ;
            dcmFname                = buildDCMFname_V14( dataFname, [], [], [], [], [], [], [], [], config_s2, [] ) ;
            DCMs{s}{c}.s2_dcmFname          = dcmFname ;
            
            if ~exist(dcmFname,'file')
%                 fprintf( 1, [ dcmFname ': DCM not found' '\n'] ) ;
                DCMsNotFound_s2{s}{c} = dcmFname ;
                continue ;
%                 error( [ dcmFname ': DCM not found' ] ) ;
            end
            
            try
                DCM                             = load(dcmFname) ;
            catch
%                 fprintf( 1, [ dcmFname ': DCM on error' '\n'] ) ;
                DCMsError_s2{s}{c}              = dcmFname ;
                continue
            end
            
            DCM                             = DCM.DCM ;

            DCMs{s}{c}.s2_peakLatency       = ccepPeakLatency{c}(5) ;
            if ~isequal( DCM.options.Tdcm, DCMs{s}{c}.Tdcm ), error( 'invalid Tdcm') ; end
            [ ~, ~, ~, R2_c_m0_s2_full ]    = compute_accuracy( DCM, [] ) ;
            DCMs{s}{c}.s2_acc_full          = R2_c_m0_s2_full ;
            
            [~,locs]                     	= findpeaks(abs(DCM.H{1})) ;
            if ~isempty(locs)
                DCMs{s}{c}.s2_peak          = DCM.xY.pst(locs(1)) ;
            else
                DCMs{s}{c}.s2_peak          = nan ;
            end

            Cp                              = spm_unvec(diag(DCM.Cp),DCM.Ep) ;
            [ ~, lognormal_mode ]           = lognormal( DCM.Ep.D(2,1) + log( DCM.M.pF.D(2) ), Cp.D(2,1) ) ;
            DCMs{s}{c}.s2_conductionDelay   = lognormal_mode ;
            
            [ ~, lognormal_mode ]           = lognormal( DCM.Ep.T(2,1) + log( pF_Te ), Cp.T(2,1) ) ;
            DCMs{s}{c}.s2_synapticTe        = lognormal_mode ;
                
            [ ~, lognormal_mode ]           = lognormal( DCM.Ep.T(2,2) + log( pF_Ti ), Cp.T(2,2) ) ;
            DCMs{s}{c}.s2_synapticTi        = lognormal_mode ;
            
        end
        
    end
    
    save( [ 'peakLatency_all_' V '.mat' ], 'peakLatency_all' ) ;
    save( [ 'DCMs_' V '_' s1Selection_txt '.mat' ], 'DCMs' ) ;
    
    fd = fopen( [ 'review_stimDCM_' V '_' s1Selection_txt '_DCMsNotFound_s1.txt' ], 'wt' ) ;
    StimNotFound_s1 = []  ;
    for s=1:length(DCMsNotFound_s1)
        for c=1:length(DCMsNotFound_s1{s})
            tmp = DCMsNotFound_s1{s}{c} ;
            if ~isempty(tmp)
%                 fprintf(1,[ 'DCM NOT FOUND ' num2str(s) ' ' tmp '\n' ]);
                fprintf(fd,[ 'DCM NOT FOUND ' num2str(s) ' ' tmp '\n' ]);
                StimNotFound_s1(end+1) = s ;
            end
        end
    end
    unique(StimNotFound_s1)
    fprintf(fd, [ 'unique(StimNotFound_s1): ' num2str(unique(StimNotFound_s1)) '\n' ]);
    fprintf(fd, [ 'unique(StimNotFound_s1): #' num2str(length(unique(StimNotFound_s1))) '\n' ]);
    fclose( fd ) ;
    
    fd = fopen( [ 'review_stimDCM_' V '_' s1Selection_txt '_DCMsError_s1.txt' ], 'wt' ) ;
    StimError_s1 = []  ;
    for s=1:length(DCMsError_s1)
        for c=1:length(DCMsError_s1{s})
            tmp = DCMsError_s1{s}{c} ;
            if ~isempty(tmp)
%                 fprintf(1,[ 'DCM NOT FOUND ' num2str(s) ' ' tmp '\n' ]);
                fprintf(fd,[ 'DCM ERROR ' num2str(s) ' ' tmp '\n' ]);
                StimError_s1(end+1) = s ;
            end
        end
    end
    unique(StimError_s1)
    fprintf(fd, [ 'unique(StimError_s1): ' num2str(unique(StimError_s1)) '\n' ]);
    fprintf(fd, [ 'unique(StimError_s1): #' num2str(length(unique(StimError_s1))) '\n' ]);
    fclose( fd ) ;
    
    
    fd = fopen( [ 'review_stimDCM_' V '_' s1Selection_txt '_DCMsNotFound_s2.txt' ], 'wt' ) ;
    StimNotFound_s2 = []  ;
    for s=1:length(DCMsNotFound_s2)
        for c=1:length(DCMsNotFound_s2{s})
            tmp = DCMsNotFound_s2{s}{c} ;
            if ~isempty(tmp)
%                 fprintf(1,[ 'DCM NOT FOUND ' num2str(s) ' ' tmp '\n' ]);
                fprintf(fd,[ 'DCM NOT FOUND ' num2str(s) ' ' tmp '\n' ]);
                StimNotFound_s2(end+1) = s ;
            end
        end
    end
    unique(StimNotFound_s2)
    fprintf(fd, [ 'unique(StimNotFound_s2): ' num2str(unique(StimNotFound_s2)) '\n' ]);
    fclose( fd ) ;
    
    fd = fopen( [ 'review_stimDCM_' V '_' s1Selection_txt '_DCMsError_s2.txt' ], 'wt' ) ;
    StimError_s2 = []  ;
    for s=1:length(DCMsError_s2)
        for c=1:length(DCMsError_s2{s})
            tmp = DCMsError_s2{s}{c} ;
            if ~isempty(tmp)
%                 fprintf(1,[ 'DCM NOT FOUND ' num2str(s) ' ' tmp '\n' ]);
                fprintf(fd,[ 'DCM ERROR ' num2str(s) ' ' tmp '\n' ]);
                StimError_s2(end+1) = s ;
            end
        end
    end
    unique(StimError_s2)
    fprintf(fd, [ 'unique(StimError_s2): ' num2str(unique(StimError_s2)) '\n' ]);
    fprintf(fd, [ 'unique(StimError_s2): #' num2str(length(unique(StimError_s2))) '\n' ]);
    fclose( fd ) ;
    
    return
end



[ peakLatency_all, s1_fnames, s1_acc_all, s1_peak_all, s1_conductionDelay_all, s1_synapticTe_all, s1_synapticTi_all, s2_fnames, s2_acc_all, s2_peak_all, s2_conductionDelay_all, s2_synapticTe_all, s2_synapticTi_all ] = eval( [ 'loadDCMs_' V '( config_s2.s1Selection ) ; ' ] ) ;


PL              = ( minPL <= peakLatency_all & peakLatency_all < maxPL ); 

s1_total        = length( find( PL ) ); 
s1_done         = length( find( PL & ~isnan(s1_acc_all) ) ) ;

s1_acc_peak     = PL & ~isnan(s1_acc_all) ;
if ~isnan(s1Selection.R2)
    s1_acc_peak = s1_acc_peak & ( s1_acc_all > s1Selection.R2 ) ;
end
if ~isnan(s1Selection.MPD)
    s1_acc_peak = s1_acc_peak & ( abs(peakLatency_all-s1_peak_all) < s1Selection.MPD ) ;
end
s1_no_acc_peak  = PL & ~isnan(s1_acc_all) & ~s1_acc_peak ;

s1_acc_peak     = find( s1_acc_peak ) ;
s1_no_acc_peak	= find( s1_no_acc_peak ) ;



s2_total        = length( find( PL & cellfun( @(s) ~isempty(s), s2_fnames ) ) ) ; 
s2_done         = length( find( PL & cellfun( @(s) ~isempty(s), s2_fnames ) & ~isnan(s2_acc_all) ) ) ;

s2_acc_peak     = PL & cellfun( @(s) ~isempty(s), s2_fnames ) & ~isnan(s2_acc_all) ;
if ~isnan(s2Selection.R2)
    s2_acc_peak = s2_acc_peak & ( s2_acc_all > s2Selection.R2 ) ;
end
if ~isnan(s2Selection.MPD)
    s2_acc_peak = s2_acc_peak & ( abs(peakLatency_all-s2_peak_all) < s2Selection.MPD ) ;
end
s2_no_acc_peak  = PL & cellfun( @(s) ~isempty(s), s2_fnames ) & ~isnan(s2_acc_all) & ~s2_acc_peak ;

s2_acc_peak     = find( s2_acc_peak ) ;
s2_no_acc_peak	= find( s2_no_acc_peak ) ;





peakLatency_s1_acc_peak  	= peakLatency_all(s1_acc_peak);
peakLatency_s1_no_acc_peak  = peakLatency_all(s1_no_acc_peak);

peakLatency_s2_acc_peak     = peakLatency_all(s2_acc_peak);
peakLatency_s2_no_acc_peak  = peakLatency_all(s2_no_acc_peak);

conductionDelay_s1_acc_peak = s1_conductionDelay_all(s1_acc_peak) ;
conductionDelay_s2_acc_peak = s2_conductionDelay_all(s2_acc_peak) ;

synapticTe_s1_acc_peak      = s1_synapticTe_all(s1_acc_peak) ;
synapticTe_s2_acc_peak   	= s2_synapticTe_all(s2_acc_peak) ;

synapticTi_s1_acc_peak    	= s1_synapticTi_all(s1_acc_peak) ;
synapticTi_s2_acc_peak     	= s2_synapticTi_all(s2_acc_peak) ;



%%
txt   = [ 'Total cceps: ' num2str(s1_total) '\n' ...
    'Perc of peakLatency (initial) below 60ms: ' num2str(100/length(peakLatency_all)*length(find(peakLatency_all<60))) '%%\n' ...
    'Step 1: done = '  num2str(s1_done) '/' num2str(s1_total) ' ' sprintf('%.3f', (100/s1_total*s1_done)) '%%\n' ...
    '\t acc abs : ' num2str(length(s1_acc_peak)) '/' num2str(s1_total) ' ' sprintf('%.3f', (100/s1_total*length(s1_acc_peak))) '%%\n' ...
    '\t acc rel : ' num2str(length(s1_acc_peak)) '/' num2str(s1_done) ' ' sprintf('%.3f', (100/s1_done*length(s1_acc_peak))) '%%\n' ...
    'Step 2: done = '  num2str(s2_done) '/' num2str(s2_total) ' ' sprintf('%.3f', (100/s2_total*s2_done)) '%%\n' ...
    '\t acc abs : '  num2str(length(s2_acc_peak)) '/' num2str(s2_total) ' ' sprintf('%.3f', (100/s2_total*length(s2_acc_peak))) '%%\n' ...
    '\t acc rel : '  num2str(length(s2_acc_peak)) '/' num2str(s2_done) ' ' sprintf('%.3f', (100/s2_done*length(s2_acc_peak))) '%%\n' ...
    'Perc of peakLatency (final) below 60ms: ' num2str(100/length(peakLatency_s2_acc_peak)*length(find(peakLatency_s2_acc_peak<60))) '%%\n' ] ;
    



fprintf( 1, txt ) ;

id = sprintf( '%s %s %s', V, s1Selection_txt, s2Selection_txt ) ;
fd = fopen( [ 'review_stimDCM_' strrep(id,' ','_') '.txt' ], 'wt' ) ;
fprintf( fd, txt ) ;
fclose( fd ) ;







%% compute average R2 in each pl bin
binWidth    = 4 ;
pl_range    = 0:binWidth:maxPL ;
NumBins     = length(pl_range)-1 ;

R2_s1_mean  = zeros(1,NumBins) ;
R2_s1_std   = zeros(1,NumBins) ;
R2_s2_mean  = zeros(1,NumBins) ;
R2_s2_std   = zeros(1,NumBins) ;
for bin=1:NumBins
    tmp             = ( pl_range(bin) <= peakLatency_all & peakLatency_all < pl_range(bin+1) ); 
    R2_s1_mean(bin) = mean(s1_acc_all(tmp),'omitnan') ;
    R2_s1_std(bin)  = std(s1_acc_all(tmp),'omitnan') ;
    R2_s2_mean(bin) = mean(s2_acc_all(tmp),'omitnan') ;
    R2_s2_std(bin)  = std(s2_acc_all(tmp),'omitnan') ;
end




%% number of cceps wrt peak latency
% maxPL = 200 ;
if icm_screen
    H = figure( 'Position', [252 573 722 632] );
else
    H = figure( 'Position', [86 76 1120 725] ) ;
end

colors  = get(groot,'defaultAxesColorOrder') ;
leg     = {};

hold on ;
h_all       	= histogram( peakLatency_all, pl_range, 'FaceColor', colors(1,:) );  h = h_all ; h.FaceAlpha = 1; %h.LineWidth=2;
edges        	= h_all.BinEdges(1:end-1) + h_all.BinWidth/2 ;
leg{end+1}      = 'initial' ;

% display the average R2 in each pl bin
yyaxis right ;
plot( edges, R2_s1_mean, 'Color', colors(1,:), 'LineWidth', 2 ) ;
% ylim([0 100]);




plot_s1_histo = false ;
if plot_s1_histo
    h_s1_acc_peak 	= histogram( peakLatency_s1_acc_peak,  0:hs:maxPL, 'FaceColor', colors(2,:) );  h = h_s1_acc_peak ; h.FaceAlpha=1; %h.LineWidth=2;
    leg{end+1}      = 's1 acc' ;
end

% display the average R2 in each pl bin
yyaxis right ;
plot( edges, R2_s2_mean, 'Color', colors(2,:), 'LineStyle', '-', 'LineWidth', 2 ) ;
% ylim([0 100]);

if ~isnan(s2Selection.R2) || ~isnan(s2Selection.MPD)
    yyaxis left ;
    h_s2_acc_peak 	= histogram( peakLatency_s2_acc_peak, pl_range, 'FaceColor', colors(3,:) );  h = h_s2_acc_peak ; h.FaceAlpha=1; %h.LineWidth=2;
    leg{end+1}      = 'after step2 + th' ;
end





% h_s1_acc_peaks    = histogram( peakLatency_s1_acc_peaks, 0:hs:maxPL );  h = h_s1_acc_peaks  ; h.LineWidth=2; h.FaceAlpha=1;
% h_s2_acc_peaks    = histogram( peakLatency_s2_acc_peaks, 0:hs:maxPL );  h = h_s2_acc_peaks  ; h.LineWidth=2; h.FaceAlpha=1;
% set(gca,'LineWidth',2);
xticks(0:binWidth*2:maxPL); xlim([minPL,maxPL]);
xlabel('peak latency (ms)'); ylabel('number of cceps');
legend( leg );
title('ccep total and fitted')

% H_unused    = figure ;
% h_all       = histogram();
% edges               = h_all.BinEdges(1:end-1) + h_all.BinWidth/2 ;
h_val               = h_all.Values ;
% if plot_s1_histo
%     h_s1_acc_peak_val 	= h_s1_acc_peak.Values ;
% end
h_s2_acc_peak_val 	= h_s2_acc_peak.Values ;
% close(H_unused);

% H_unused                = figure ;
% % h_s1_acc_peaks        	= histogram( peakLatency_s1_acc_peaks, minPL:hs:maxPL ); 
% % h_s1_acc_peaks_val    	= h_s1_acc_peaks.Values ;
% h_s1_no_acc_peak              = histogram( peakLatency_s1_no_acc_peak, minPL:hs:maxPL ); 
% h_s1_no_acc_peak_val          = h_s1_no_acc_peak.Values ;
% h_s2_no_acc_peak              = histogram( peakLatency_s2_no_acc_peak, minPL:hs:maxPL ); 
% h_s2_no_acc_peak_val          = h_s2_no_acc_peak.Values ;
% % h_s2_acc_peaks        	= histogram( peakLatency_s2_acc_peaks, 0:hs:maxPL ); 
% % h_s2_acc_peaks_val    	= h_s2_acc_peaks.Values ;
% % % h_s2_unacc              = histogram( peakLatency_s2_unacc, 0:hs:maxPL ); 
% % % h_s2_unacc_val          = h_s2_unacc.Values ;
% close(H_unused);

% subplot(2,1,2) ; 
yyaxis right ;
hold on ;
% colors = get(groot,'defaultAxesColorOrder') ;
% absolute
% plot_s1_absolute = true ;
plot_s1_absolute = false ;
if plot_s1_absolute
%     plot(edges,100./h_val.* h_s1_acc_val, 'LineWidth', 2, 'LineStyle', '-', 'Color', colors(2,:) ) ; 
    plot(edges,100./h_val.* h_s1_acc_peak_val, 'LineWidth', 2, 'LineStyle', '-', 'Color', colors(2,:) ) ; 
%     plot(edges,100./h_val.* h_s1_acc_peaks_val,  'LineStyle', '-', 'Color', colors(2,:) ) ; 
end
% relative
plot_s1_relative = false ;
% plot_s1_relative = true ;
if plot_s1_relative
%     plot(edges,100./(h_s1_acc_val+h_s1_unacc_val).* h_s1_acc_val, 'LineWidth', 2, 'LineStyle', '-', 'Color', colors(2,:) ) ; 
    plot(edges,100./(h_s1_acc_peak_val+h_s1_no_acc_peak_val).* h_s1_acc_peak_val, 'LineWidth', 2, 'LineStyle', '--', 'Color', colors(2,:) ) ; 
end

% plot_s2_absolute = true ;
plot_s2_absolute = false ;
if plot_s2_absolute
%     plot(edges,100./h_val.* h_s2_acc_val, 'LineWidth', 2, 'LineStyle', '-', 'Color', colors(3,:) ) ; 
    plot(edges,100./h_val.* h_s2_acc_peak_val, 'LineWidth', 2, 'LineStyle', '-', 'Color', colors(3,:) ) ; 
%     plot(edges,100./h_val.* h_s2_acc_peaks_val, 'LineStyle', '-', 'Color', colors(3,:) ) ; 
end

% plot_s2_relative = true ;
plot_s2_relative = false ;
if plot_s2_relative
%     plot(edges,100./(h_s2_acc_val+h_s2_unacc_val).* h_s2_acc_val, 'LineWidth', 2, 'LineStyle', '-', 'Color', colors(3,:) ) ;
    plot(edges,100./(h_s2_acc_peak_val+h_s2_no_acc_peak_val).* h_s2_acc_peak_val, 'LineWidth', 2, 'LineStyle', '--', 'Color', colors(3,:) ) ; 
end

% plot(edges,100./h_s1_val.* h_s2_val, 'LineWidth', 2, 'Color', colors(3,:) ) ; 
% xlim([0 100]); 
% line([0;maxPL], [50;50], 'Color','k','LineStyle','--','LineWidth', 1, 'Color', [0.3 0.3 0.3] );
% line([0;maxPL], [50;50], 'Color','k','LineWidth', 1, 'LineStyle','--', 'Color', [0.3 0.3 0.3] );
line([0;maxPL], [s2Selection.R2;s2Selection.R2], 'Color','k','LineWidth', 2, 'LineStyle','--', 'Color', colors(3,:) );

ylim([0 100]);
% set(gca,'LineWidth',2);
% set(gca,'FontWeight','bold');
set(gca,'YColor', [0 0 0] );
xlabel('peak latency (ms)'); ylabel('% cceps'); 
ylabel('accuracy'); 
% legend({'s1/all' 's2/all'} ) ; %  's2/s1'}); 
% title('perc fitted ccep accurate')






suptitle( strrep(id,'_',' ') ) ;



if saveFigures
    fname = [ dcmDir '/../screenshots/histo_fits_all_s1_s2_versus_peakLatency_' strrep(id,' ','_') '.png' ] ;
    saveas( H, fname ) ;
    fname = [ dcmDir '/../screenshots/histo_fits_all_s1_s2_versus_peakLatency_' strrep(id,' ','_') '.svg' ] ;
    saveas( H, fname ) ;
    fprintf(1, [ 'Save ' fname '\n' ] ) ;
end








txt   = [ 'Step 1:  \n' ...
    '- Goodness of fit R2: \n' ...
    '\t mean(s1_acc_all(:)) : ' num2str( mean(s1_acc_all(:)) ) '\n' ...
    '\t R2_s1_mean: '           num2str( R2_s1_mean ) '\n' ...
    '\t mean(R2_s1_mean): ' 	num2str( mean(R2_s1_mean) ) '\n' ...
    '- Difference in peak latency: \n' ...
    '\t mean( abs(peakLatency_all-s1_peak_all) ): ' num2str( mean( abs(peakLatency_all-s1_peak_all), 'omitnan' ) ) ' ms\n\n' ...
    ...
    'Step 2:  \n' ...
    '- Goodness of fit R2: \n' ...
    '\t mean(s2_acc_all(:)) : ' num2str( mean(s2_acc_all(:)) ) '\n' ...
    '\t R2_s2_mean: '           num2str( R2_s2_mean ) '\n' ...
    '\t mean(R2_s2_mean): ' 	num2str( mean(R2_s2_mean) ) '\n' ...
    '- Difference in peak latency: \n' ...
    '\t mean( abs(peakLatency_all-s1_peak_all) ): ' num2str( mean( abs(peakLatency_all-s2_peak_all), 'omitnan' ) ) ' ms\n' ...
    '\t length( find( ( s2_acc_all > 70 ) ) ): ' num2str(length( find( ( s2_acc_all > 70 ) ) )) '\n' ...
    '\t\t => ' num2str( 100 / length(s2_acc_all) * length( find( ( s2_acc_all > 70 ) ) ) ) ' %%\n' ...
    '\t length( find( ( abs( peakLatency_all-s2_peak_all ) < 5 ) ) ): ' num2str(length( find( ( abs( peakLatency_all-s2_peak_all ) < 5 ) ) )) '\n' ...
    '\t\t => ' num2str( 100 / length(s2_acc_all) * length( find( ( abs( peakLatency_all-s2_peak_all ) < 5 ) ) ) ) ' %%\n' ...
    '\t length( find( ( s2_acc_all > 70 ) & ( abs( peakLatency_all-s2_peak_all ) < 5 ) ) ): ' num2str(length( find( ( s2_acc_all > 70 ) & ( abs( peakLatency_all-s2_peak_all ) < 5 ) ) )) '\n' ...
    '\t\t => ' num2str( 100 / length(s2_acc_all) * length( find( ( s2_acc_all > 70 ) & ( abs( peakLatency_all-s2_peak_all ) < 5 ) ) ) ) ' %%\n' ...
    ] ;



fprintf( 1, txt ) ;

fd = fopen( [ 'review_stimDCM_' strrep(id,' ','_') '.txt' ], 'a' ) ;
fprintf( fd, txt ) ;
fclose( fd ) ;












% error( 'IMPLEMENT make_figures_V14' ) ;
% make_figures_V14() ;






% id = sprintf( '%s s1accpeaks R2 %dpc MAX PK DIF %d minPL %d', V, acc_th_s1, max_diff_peak_s1, minPL ) ;
% plot_distributions( conductionDelay_s1_acc_peaks, peakLatency_s1_acc_peaks, synapticTe_s1_acc_peaks, synapticTi_s1_acc_peaks, id, saveFigures ) ;
% id = sprintf( '%s s2accpeaks R2 %d MPD %d minPL %d', V, s2Selection.R2, s2Selection.MPD, s1Selection.minPL ) ;
plot_distributions( conductionDelay_s2_acc_peak, peakLatency_s2_acc_peak, synapticTe_s2_acc_peak, synapticTi_s2_acc_peak, id, saveFigures ) ;



return




H = figure( 'Position', [466 655 1685 801] ) ;
% subplot(2,3,1); histogram(fsample_all, 0:10:1050); title( 'fsample all' )
% subplot(2,3,2); histogram(fsample_all(s1_acc), 0:10:1050); title( 'fsample acc' )
% subplot(2,3,3); histogram(fsample_all(s1_unacc), 0:10:1050); title( 'fsample unacc' )
subplot(2,3,1); histogram(peakLatency_all, 0:4:80); ylim([0 12000]); title( 'peak latency all' )
subplot(2,3,2); histogram(peakLatency_s1_acc_peak, 0:4:80); ylim([0 12000]); title( 's1 peak latency acc' )
subplot(2,3,3); histogram(peakLatency_s1_no_acc_peak, 0:4:80); ylim([0 12000]); title( 's1 peak latency unacc' )
% subplot(2,3,4); histogram(s1_peakLatency_all, 0:4:80); ylim([0 12000]); title( 'peak latency all' )
subplot(2,3,5); histogram(peakLatency_s2_acc_peak, 0:4:80); ylim([0 12000]); title( 's2 peak latency acc' )
subplot(2,3,6); histogram(peakLatency_s2_no_acc_peak, 0:4:80); ylim([0 12000]); title( 's2 peak latency unacc' )

suptitle( V ) ;

if saveFigures
    fname = [ dcmDir '/../screenshots/histo_all_acc_unacc_wrt_PL' V '.png' ] ;
    saveas( H, fname ) ;
end









%% histo of conductionDelay and synapticTe
% figure ;
% subplot(1,2,1); histogram(s1_conductionDelay_all,0:1:40); title('s1 conductionDelay all');
% subplot(1,2,2); histogram(s1_synapticTe_all,0:1:40) ;title('s1 synapticTe all');
H = figure( 'Position', [119 915 772 389] ) ;
subplot(1,2,1); histogram( conductionDelay_s1_acc_peak, 0:1:40 ); xlabel('conduction delay'); ylabel('#fits'); ylim([0 13000]);
subplot(1,2,2); histogram( synapticTe_s1_acc_peak,      0:1:40 ) ; xlabel('synaptic Te'); ylabel('#fits'); ylim([0 5200]);
suptitle( [ V ' s1 acc' ] );

fname = [ dcmDir '/../screenshots/histo_CD_TE_s1' V '.png' ] ;
saveas( H, fname ) ;


% figure ;
% subplot(1,2,1); histogram(s1_conductionDelay_acc_10_55,0:1:40); title('s1 conductionDelay acc 10 55');
% subplot(1,2,2); histogram(s1_synapticTe_acc_10_55,0:1:40) ;title('s1 synapticTe acc 10 55');
% figure ;
% subplot(1,2,1); histogram(s2_conductionDelay_all,0:1:40); title('s2 conductionDelay all');
% subplot(1,2,2); histogram(s2_synapticTe_all,0:1:40) ;title('s2 synapticTe all');
H = figure('Position', [894 917 775 389] );
subplot(1,2,1); histogram( conductionDelay_s2_acc_peak, 0:1:40 ); xlabel('conduction delay'); ylabel('#fits'); ylim([0 13000]);
subplot(1,2,2); histogram( synapticTe_s2_acc_peak,      0:1:40 ) ;xlabel('synaptic Te'); ylabel('#fits'); ylim([0 5200]);
suptitle( [ V ' s2 acc' ] );

fname = [ dcmDir '/../screenshots/histo_CD_TE_s2' V '.png' ] ;
saveas( H, fname ) ;

% figure ;
% subplot(1,2,1); histogram(s2_conductionDelay_acc_10_55,0:1:40); title('s2 conductionDelay acc 10 55');
% subplot(1,2,2); histogram(s2_synapticTe_acc_10_55,0:1:40) ;title('s2 synapticTe acc 10 55');






% histo of conductionDelay and synaptic Te versus pl
hs = 4 ;
figure( 'Position', [246 382 1976 1061] ) ; p = 1 ;
maxCD = 40 ;
for minPL=0:hs:maxPL-hs
    id = minPL <= peakLatency_all & peakLatency_all < minPL + hs & ~isnan(s1_acc_all) ;
    if ~isnan(s1Selection.R2)
        id = id & ( s1_acc_all > s1Selection.R2 ) ;
    end
    if ~isnan(s1Selection.MPD)
        id = id & ( abs(peakLatency_all-s1_peak_all) < s1Selection.MPD ) ;
    end
    id = find(id) ; 
%     id = find( minPL <= peakLatency_all & peakLatency_all < minPL + hs & ~isnan(s1_acc_all) &  ( s1_acc_all > s1Selection.R2 & abs(peakLatency_all-s1_peak_all) < s1Selection.MPD ) ) ;
    subplot(5,4,p); 
    histogram(s1_conductionDelay_all(id),0:maxCD);
    xlim( [ 0 maxCD ] ) ;
    title( [ num2str(minPL) '-' num2str(minPL+hs) ] );
    p = p + 1 ;
end
suptitle( [ 'conductionDelay s1' ] )

figure( 'Position', [246 382 1976 1061] ) ; p = 1 ;
for minPL=0:hs:maxPL-hs
    id = minPL <= peakLatency_all & peakLatency_all < minPL + hs & cellfun( @(s) ~isempty(s), s2_fnames ) & ~isnan(s2_acc_all) ;
    if ~isnan(s2Selection.R2)
     id = id & ( s2_acc_all > s2Selection.R2 ) ; 
    end
    if ~isnan(s2Selection.MPD)
        id = id & ( abs(peakLatency_all-s2_peak_all) < s2Selection.MPD ) ;
    end
    id = find(id) ; 
%     id = find( minPL <= peakLatency_all & peakLatency_all < minPL + hs & cellfun( @(s) ~isempty(s), s2_fnames ) & ~isnan(s2_acc_all) &  ( s2_acc_all > acc_th_s2 & abs(peakLatency_all-s2_peaks_all) < max_diff_peak_s2 ) ) ;
    subplot(5,4,p); 
    histogram(s2_conductionDelay_all(id),0:maxCD);
    xlim( [ 0 maxCD ] ) ;
    title( [ num2str(minPL) '-' num2str(minPL+hs) ] );
    p = p + 1 ;
end
suptitle( [ 'conductionDelay s2' ] )



figure( 'Position', [246 382 1976 1061] ) ; p = 1 ;
for minPL=0:hs:maxPL-hs
    id = minPL <= peakLatency_all & peakLatency_all < minPL + hs & ~isnan(s1_acc_all) ;
    if ~isnan(s1Selection.R2)
        id = id & ( s1_acc_all > s1Selection.R2 ) ;
    end
    if ~isnan(s1Selection.MPD)
        id = id & ( abs(peakLatency_all-s1_peak_all) < s1Selection.MPD ) ;
    end
    id = find(id) ;     
%     id = find( minPL <= peakLatency_all & peakLatency_all < minPL + hs & ~isnan(s1_acc_all) &  ( s1_acc_all > acc_th_s1 & abs(peakLatency_all-s1_peaks_all) < max_diff_peak_s1 ) ) ;
    subplot(5,4,p); 
    histogram(s1_synapticTe_all(id),0:20);
    xlim( [ 0 20 ] ) ;
    title( [ num2str(minPL) '-' num2str(minPL+hs) ] );
    p = p + 1 ;
end
suptitle( [ 'synaptic Te s1' ] )


figure( 'Position', [246 382 1976 1061] ) ; p = 1 ;
for minPL=0:hs:maxPL-hs
    id = minPL <= peakLatency_all & peakLatency_all < minPL + hs & cellfun( @(s) ~isempty(s), s2_fnames ) & ~isnan(s2_acc_all) ;
    if ~isnan(s2Selection.R2)
     id = id & ( s2_acc_all > s2Selection.R2 ) ; 
    end
    if ~isnan(s2Selection.MPD)
        id = id & ( abs(peakLatency_all-s2_peak_all) < s2Selection.MPD ) ;
    end
    id = find(id) ; 
%     id = find( peakLatency_all >= minPL & peakLatency_all < minPL + hs & cellfun( @(s) ~isempty(s), s2_fnames ) & ~isnan(s2_acc_all) &  ( s2_acc_all > acc_th_s2 & abs(peakLatency_all-s2_peaks_all) < max_diff_peak_s2 ) ) ;
    subplot(5,4,p); 
    histogram(s2_synapticTe_all(id),0:20);
    xlim( [ 0 20 ] ) ;
    title( [ num2str(minPL) '-' num2str(minPL+hs) ] );
    p = p + 1 ;
end
suptitle( [ 'synaptic Te s2' ] )



return




%% correlation between accuracy (step 1) and peakLatency
% figure ; 
% scatter( s1_accuracy_all, peakLatency_all ) ;
% xlabel('accuracy') ; ylabel('peak latency')
figure ; 
subplot(2,2,1);
scatter( s1_peakLatency_all, s1_acc_full_all ) ; ylim([-200 100]);
xlabel('peak latency') ; ylabel('s1 acc full') ; title('s1 acc full') ;
subplot(2,2,2); 
scatter( s2_peakLatency_all, s2_acc_full_all ) ; ylim([-200 100]);
xlabel('peak latency') ; ylabel('s2 acc full') ; title('s2 acc full') ;
subplot(2,2,3);
scatter( s1_peakLatency_all, s1_acc_10_55_all ) ; ylim([-200 100]);
xlabel('peak latency') ; ylabel('s1 acc 10 55') ; title('s1 acc 10 55') ;
subplot(2,2,4); 
scatter( s2_peakLatency_all, s2_acc_10_55_all ) ; ylim([-200 100]);
xlabel('peak latency') ; ylabel('s2 acc 10 55') ; title('s2 acc 10 55') ;


% percentage of DCM accurate for each peakLatency after s1
s1_peakLatency_count    = [] ;        % number of DCM at each pl bin
s1_acc_full_PL_count    = [];  % number of accurate DCM after s1
s1_acc_full_PL_perc     = [];   % perc of accurate DCM after s1
% same as above but considering accuracy 10 55
s1_acc_10_55_PL_count   = [];  % number of accurate DCM after s1
s1_acc_10_55_PL_perc    = [];   % perc of accurate DCM after s1

s2_peakLatency_count    = [] ;        % number of DCM at each pl bin
s2_acc_full_PL_count    = [];  % number of accurate DCM after s1
s2_acc_full_PL_perc     = [];   % perc of accurate DCM after s1
% same as above but considering accuracy 10 55
s2_acc_10_55_PL_count   = [];  % number of accurate DCM after s1
s2_acc_10_55_PL_perc    = [];   % perc of accurate DCM after s1

pl_step = 5 ;
tmp_check= [] ;
% for pl=5:pl_step:200
for pl=2.5:pl_step:200
    tmp = find( ( s1_peakLatency_all > pl-pl_step/2 ) & ( s1_peakLatency_all <= pl+pl_step/2 ) ) ;
    tmp_check(end+1)                = length(tmp) ;
    s1_peakLatency_count(end+1)     = length(tmp);
    s1_acc_full_PL_count(end+1)     = length( find( s1_acc_full_all(tmp) > acc_th ) );
    s1_acc_full_PL_perc(end+1)      = 100 / length(tmp) * length( find( s1_acc_full_all(tmp) > acc_th ) );
    s1_acc_10_55_PL_count(end+1)    = length( find( s1_acc_10_55_all(tmp) > acc_th ) );
    s1_acc_10_55_PL_perc(end+1)     = 100 / length(tmp) * length( find( s1_acc_10_55_all(tmp) > acc_th ) );
    
    tmp = find( ( s2_peakLatency_all > pl-pl_step/2 ) & ( s2_peakLatency_all < pl+pl_step/2 ) ) ;
    s2_peakLatency_count(end+1)     = length(tmp);
    s2_acc_full_PL_count(end+1)     = length( find( s2_acc_full_all(tmp) > acc_th ) );
    s2_acc_full_PL_perc(end+1)      = 100 / length(tmp) * length( find( s2_acc_full_all(tmp) > acc_th ) );
    s2_acc_10_55_PL_count(end+1)    = length( find( s2_acc_10_55_all(tmp) > acc_th ) );
    s2_acc_10_55_PL_perc(end+1)     = 100 / length(tmp) * length( find( s2_acc_10_55_all(tmp) > acc_th ) );
    
end

isequal( length( find( ( s1_peakLatency_all > 0 ) & ( s1_peakLatency_all <= 200 ) ) ), sum(tmp_check) )
isequal( h_val(1:40), tmp_check )
    
    
    
figure ; 
subplot(2,2,1); hold on
plot( 5:5:200, s1_acc_full_PL_perc ) ;
plot( 5:5:200, s2_acc_full_PL_perc ) ;
xlim([1 200]);
ylim([1 100]);
xlabel( 'peak latency' ) ; ylabel( 'perc of acc full DCM' ) ; title( 'perc of acc full after s1 and s2' )
legend( { 'after s1' 'after s2' } );

subplot(2,2,2); hold on
plot( 5:5:200, s1_acc_full_PL_count ) ; 
plot( 5:5:200, s2_acc_full_PL_count ) ; 
plot( 5:5:200, s1_peakLatency_count ) ; 
% plot( 5:5:200, s2_peakLatency_count ) ; 
% xlim([1 100]);
xlabel( 'peak latency' ) ; ylabel( 'total number of DCM' ) ; title( 'count of acc full after s1 and s2' )
legend( { 'after s1' 'after s2' 'total s1' } );
% suptitle( 'accuracy computed on full Tdcm' )

% figure ; 
subplot(2,2,3); hold on
plot( 5:5:200, s1_acc_10_55_PL_perc ) ;
plot( 5:5:200, s2_acc_10_55_PL_perc ) ;
xlim([1 200]);
ylim([1 100]);
xlabel( 'peak latency' ) ; ylabel( 'perc  of acc 10 55 DCM' ) ; title( 'perc of acc 10 55 after s1 and s2' )
legend( { 'after s1' 'after s2' } );

subplot(2,2,4); hold on
plot( 5:5:200, s1_acc_10_55_PL_count ) ; 
plot( 5:5:200, s2_acc_10_55_PL_count ) ; 
plot( 5:5:200, s1_peakLatency_count ) ; 
% xlim([1 100]);
xlabel( 'peak latency' ) ; ylabel( 'total number of DCM' ) ;title( 'count of acc 10 55 after s1 and s2' )
legend( { 'after s1' 'after s2' 'total s1' } );
% suptitle( 'accuracy computed on 10-55' )


end













