function findGoodFit( DCMs )

found = true ;
% found = false ;
if found
    plotGoodFit()
   return 
end

if isempty( DCMs )
    bd      = baseDir();
    fname   = fullfile( bd, 'dcm/PATS__V1__DCMs.mat') ;
    DCMs    = load(fname) ;
    DCMs    = DCMs.DCMs ;
end


% [ 10 15 ]
% dcmFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0001NAN/SEEG/StimLF_2015-08-31/SEQ_BM_ACPCHIP_F_DCM/W16-W17_1mA_1Hz_1050us_1/step2/DCM_averaged_zs_W16-W17_1mA_1Hz_1050us_1_W14-W15.mat' ;
% dcmFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0001NAN/SEEG/StimLF_2015-08-31/SEQ_BM_ACPCHIP_F_DCM/TMp09-TMp10_1mA_1Hz_1050us_1/step2/DCM_averaged_zs_TMp09-TMp10_1mA_1Hz_1050us_1_A04-A05.mat';
% dcmFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0001NAN/SEEG/StimLF_2015-08-31/SEQ_BM_ACPCHIP_F_DCM/B02-B03_1mA_1Hz_1050us_1/step2/DCM_averaged_zs_B02-B03_1mA_1Hz_1050us_1_C04-C05.mat';
% dcmFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0001NAN/SEEG/StimLF_2015-08-31/SEQ_BM_ACPCHIP_F_DCM/H02-H03_1mA_1Hz_1050us_1/step2/DCM_averaged_zs_H02-H03_1mA_1Hz_1050us_1_H04-H05.mat';
% dcmFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0001NAN/SEEG/StimLF_2015-08-31/SEQ_BM_ACPCHIP_F_DCM/TB02-TB03_1mA_1Hz_1050us_1/step2/DCM_averaged_zs_TB02-TB03_1mA_1Hz_1050us_1_TB09-TB10.mat';
% dcmFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0001NAN/SEEG/StimLF_2015-08-31/SEQ_BM_ACPCHIP_F_DCM/TB02-TB03_1mA_1Hz_1050us_1/step2/DCM_averaged_zs_TB02-TB03_1mA_1Hz_1050us_1_TM03-TM04.mat';


% [ 25 30 ]

% 40 45
% ok: dcmFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0001GRE/SEEG/StimLF_2014-11-25/SEQ_BM_ACPCHIP_F_DCM/Ip03-Ip04_3mA_1Hz_1000us_1/step2/DCM_averaged_zs_Ip03-Ip04_3mA_1Hz_1000us_1_ETp01-ETp02.mat' ;


interval = [ 60 70 ] ;
min_cd = 30 ;

interval = [ 50 60 ] ;
min_cd = 20 ;

interval = [ 30 40 ] ;
bsl_e = 0.01 ;
min_cd = 10 ;

interval = [ 20 30 ] ;
bsl_e = 0.01 ;
min_cd = 5 ;

interval = [ 10 20 ] ;
bsl_e = 0.005 ;
min_cd = 1 ;

interval = [ 8 12 ] ;
bsl_e = 0.005 ;
min_cd = 1 ;



max_peak_dist = 2 ;
min_acc = 99 ;
min_acc = 95 ;
% min_acc = 90 ;

min_fs = 512 ;

max_bsl_z = 0.5 ;


allDCMs  = struct ;
a = 1 ;

% dcmFnames = {} ;
for c=1:length(DCMs)
    fprintf( 1, [ num2str(c) '/' num2str(length(DCMs)) '\n' ] ) ;
    for s=1:length(DCMs(c).stims)
        for d=1:length(DCMs(c).stims{s}.cceps)
            
            allDCMs(a).dcmFname         = DCMs(c).stims{s}.cceps{d}.s2 ;
            allDCMs(a).peakLat          = DCMs(c).stims{s}.cceps{d}.peakLat ;
            allDCMs(a).s2_peak          = DCMs(c).stims{s}.cceps{d}.s2_peak ;
            allDCMs(a).s2_acc           = full(DCMs(c).stims{s}.cceps{d}.s2_acc) ;
            allDCMs(a).conductionDelay  = DCMs(c).stims{s}.cceps{d}.conductionDelay ;
            
            
            
%             if interval(1) < DCMs(c).stims{s}.cceps{d}.peakLat && ...
%                     DCMs(c).stims{s}.cceps{d}.peakLat < interval(2)  && ...
%                     abs( DCMs(c).stims{s}.cceps{d}.peakLat - DCMs(c).stims{s}.cceps{d}.s2_peak ) < max_peak_dist && ...
%                     DCMs(c).stims{s}.cceps{d}.s2_acc > min_acc && ...
%                     DCMs(c).stims{s}.cceps{d}.conductionDelay > min_cd
%                 
%             dcmFname                    = DCMs(c).stims{s}.cceps{d}.s2 ;
%             DCM                         = load(dcmFname);
%             DCM                         = DCM.DCM ;
%             D                           = spm_eeg_load(DCM.xY.Dfile);
%             allDCMs(a).D                = D ;
            
            a                           = a+1 ;
                                 

%                 if D.fsample > min_fs && ...
%                         max(abs(D(indsample(D,0):indsample(D,bsl_e)))) < max_bsl_z
% %                     plotFit( dcmFname ) ;
%                     dcmFnames = vertcat( dcmFnames, dcmFname ) ;
%                 end
%                 
%             end
        end
    end
end


% fprintf( 1, [ num2str(length(dcmFnames)) '\n' ] ) ;

interval=[10 15]; 
%           '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0001GRE/SEEG/StimLF_2014-11-25/SEQ_BM_ACPCHIP_F_DCM/Ep02-Ep03_3mA_1Hz_1000us_1/step2/DCM_averaged_zs_Ep02-Ep03_3mA_1Hz_1000us_1_Gp07-Gp08.mat'

interval=[25 30]; 
% 2  :  ++   '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0001GRE/SEEG/StimLF_2014-11-25/SEQ_BM_ACPCHIP_F_DCM/Cp07-Cp08_3mA_1Hz_1000us_1/step2/DCM_averaged_zs_Cp07-Cp08_3mA_1Hz_1000us_1_ETp07-ETp08.mat'
% 27 : +   '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0001NAN/SEEG/StimLF_2015-08-31/SEQ_BM_ACPCHIP_F_DCM/F10-F11_1mA_1Hz_1050us_2/step2/DCM_averaged_zs_F10-F11_1mA_1Hz_1050us_2_F06-F07.mat'
% 71 :   +  '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0002ROT/SEEG/StimLF_2013-09-09/SEQ_BM_ACPCHIP_F_DCM/HA03-HA04_4mA_1Hz_1050us_1/step2/DCM_averaged_zs_HA03-HA04_4mA_1Hz_1050us_1_NA01-NA02.mat'
% 73 :     '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0002ROT/SEEG/StimLF_2013-09-09/SEQ_BM_ACPCHIP_F_DCM/PO01-PO02_3mA_1Hz_1050us_1/step2/DCM_averaged_zs_PO01-PO02_3mA_1Hz_1050us_1_PO12-PO13.mat'
% 83 :     '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0003GRE/SEEG/StimLF_2015-05-12/SEQ_BM_ACPCHIP_F_DCM/T06-T07_3mA_1Hz_1000us_1/step2/DCM_averaged_zs_T06-T07_3mA_1Hz_1000us_1_I05-I06.mat'

interval=[40 45];
% 8         '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0001NAN/SEEG/StimLF_2015-08-31/SEQ_BM_ACPCHIP_F_DCM/B02-B03_1mA_1Hz_1050us_1/step2/DCM_averaged_zs_B02-B03_1mA_1Hz_1050us_1_C01-C02.mat'
% 16        '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0002BUC/SEEG/StimLF_2014-09-02/SEQ_BM_ACPCHIP_F_DCM/I01-I02_2mA_1Hz_3000us_1/step2/DCM_averaged_zs_I01-I02_2mA_1Hz_3000us_1_Q05-Q06.mat'
% 37        '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0004ROT/SEEG/StimLF_2013-11-05/SEQ_BM_ACPCHIP_F_DCM/IP01-IP02_4mA_1Hz_1050us_1/step2/DCM_averaged_zs_IP01-IP02_4mA_1Hz_1050us_1_OP03-OP04.mat'
% 80        '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0010ROT/SEEG/StimLF_2014-09-08/SEQ_BM_ACPCHIP_F_DCM/PT01-PT02_4mA_1Hz_1050us_1/step2/DCM_averaged_zs_PT01-PT02_4mA_1Hz_1050us_1_HA04-HA05.mat'
% 96 ++   	'/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0013LIL/SEEG/StimLF_2016-12-15/SEQ_BM_ACPCHIP_F_DCM/CR13-CR14_2mA_1Hz_1050us_1/step2/DCM_averaged_zs_CR13-CR14_2mA_1Hz_1050us_1_R13-R14.mat'
% 111 +  	'/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0015ROT/SEEG/StimLF_2016-05-23/SEQ_BM_ACPCHIP_F_DCM/PI01-PI02_4mA_1Hz_1050us_1/step2/DCM_averaged_zs_PI01-PI02_4mA_1Hz_1050us_1_SM14-SM15.mat'
% 135     	'/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0018GRE/SEEG/StimLF_2017-04-06/SEQ_BM_ACPCHIP_F_DCM/X01-X02_3mA_1Hz_1025us_1/step2/DCM_averaged_zs_X01-X02_3mA_1Hz_1025us_1_G01-G02.mat'

interval=[55 60];
% 10        '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0003GRE/SEEG/StimLF_2015-05-12/SEQ_BM_ACPCHIP_F_DCM/R07-R08_3mA_1Hz_1000us_1/step2/DCM_averaged_zs_R07-R08_3mA_1Hz_1000us_1_X06-X07.mat'
% 35        '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0010LIL/SEEG/StimLF_2016-06-16/SEQ_BM_ACPCHIP_F_DCM/Rp05-Rp06_3mA_1Hz_1050us_1/step2/DCM_averaged_zs_Rp05-Rp06_3mA_1Hz_1050us_1_ORp01-ORp02.mat'
% 37        '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0011GRE/SEEG/StimLF_2013-11-19/SEQ_BM_ACPCHIP_F_DCM/Ap01-Ap02_3mA_1Hz_1000us_1/step2/DCM_averaged_zs_Ap01-Ap02_3mA_1Hz_1000us_1_Tp02-Tp03.mat'
% 38    +    '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0011GRE/SEEG/StimLF_2013-11-19/SEQ_BM_ACPCHIP_F_DCM/Ep08-Ep09_3mA_1Hz_1000us_1/step2/DCM_averaged_zs_Ep08-Ep09_3mA_1Hz_1000us_1_Bp01-Bp02.mat'
% 43    +    '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0014LIL/SEEG/StimLF_2017-01-10/SEQ_BM_ACPCHIP_F_DCM/GPH07-GPH08_3mA_1Hz_1050us_1/step2/DCM_averaged_zs_GPH07-GPH08_3mA_1Hz_1050us_1_GPH12-GPH13.mat'
% 86    +   '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0023LIL/SEEG/StimLF_2017-09-12/SEQ_BM_ACPCHIP_F_DCM/Ap03-Ap04_2mA_1Hz_1050us_1/step2/DCM_averaged_zs_Ap03-Ap04_2mA_1Hz_1050us_1_Bp04-Bp05.mat'
% 100   +   '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0031NAN/SEEG/StimLF_2015-12-14/SEQ_BM_ACPCHIP_F_DCM/L02-L03_4mA_1Hz_1050us_1/step2/DCM_averaged_zs_L02-L03_4mA_1Hz_1050us_1_Q12-Q13.mat'
% 113   +   '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0036GRE/SEEG/StimLF_2011-03-08/SEQ_BM_ACPCHIP_F_DCM/Bp03-Bp04_3mA_1Hz_1050us_1/step2/DCM_averaged_zs_Bp03-Bp04_3mA_1Hz_1050us_1_Op07-Op08.mat'
% 191   ++   '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0109ROT/SEEG/StimLF_2014-06-23/SEQ_BM_ACPCHIP_F_DCM/PT05-PT06_4mA_1Hz_1050us_1/step2/DCM_averaged_zs_PT05-PT06_4mA_1Hz_1050us_1_TB01-TB02.mat'



max_peak_dist   = 2;
min_acc         = 99 ;
min_fs          = 500 ;
bsl_e           = 0.005 ;
max_bsl_z       = 1 ; % 0.5 ;


% in the good interval
id_1 = arrayfun( @(d) d.peakLat>interval(1)&&d.peakLat<interval(2), allDCMs );
length(find(id_1))

% with peak alignment
id_2 = arrayfun( @(d) abs(d.peakLat-d.s2_peak)<max_peak_dist, allDCMs );
length(find(id_2))

% accurate
id_3 = arrayfun( @(d) d.s2_acc>min_acc, allDCMs );
length(find(id_3))


id_123 = id_1 & id_2 & id_3 ;
length(find(id_123))



goodDCMs = allDCMs(id_123) ;
% good sampling rate and baseline ok
% for d=1:length(goodDCMs)
for d=length(goodDCMs):-1:1
% for d=2170:-1:1
% for d=150:-1:1
    dcmFname    = goodDCMs(d).dcmFname ;
    DCM         = load(dcmFname);
    DCM         = DCM.DCM ;
    D           = spm_eeg_load(DCM.xY.Dfile);
    
    goodDCMs(d).fsample = D.fsample ;
    goodDCMs(d).max_bsl = max(abs(D(indsample(D,0):indsample(D,bsl_e)))) ;
    
    if goodDCMs(d).fsample > min_fs
        d
        goodDCMs(d).dcmFname
        H = plotFit( goodDCMs(d).dcmFname, [1000 1078 560 420] ) ;
%         close(H);
    end
end



% id_4 = arrayfun( @(d) d.fsample>min_fs, goodDCMs );
% length(find(id_4))
% 
% id_5 = arrayfun( @(d) d.max_bsl<max_bsl_z, goodDCMs );
% length(find(id_5))

% id_45 = id_4 & id_5 ;
% length(find(id_45))

% 
% idx = find(id_45) ;
% for d=
%     plotFit( goodDCMs(d).dcmFname ) ;
%     
% end

end



function plotGoodFit()


figD    = fullfile( figDir(), 'fig2' ) ;
system( [ 'mkdir -p ' figD ] ) ;

saveFig = true ;

% interval=[10 15]; 
dcmFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0001GRE/SEEG/StimLF_2014-11-25/SEQ_BM_ACPCHIP_F_DCM/Ep02-Ep03_3mA_1Hz_1000us_1/step2/DCM_averaged_zs_Ep02-Ep03_3mA_1Hz_1000us_1_Gp07-Gp08.mat';
H = plotFit( dcmFname, [1400 1150 370 200] ) ;
if saveFig, saveas( H, fullfile( figD, 'fig2_A.svg' ) ) ; end

% interval=[25 30]; 
% dcmFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0001GRE/SEEG/StimLF_2014-11-25/SEQ_BM_ACPCHIP_F_DCM/Cp07-Cp08_3mA_1Hz_1000us_1/step2/DCM_averaged_zs_Cp07-Cp08_3mA_1Hz_1000us_1_ETp07-ETp08.mat';
% 2226
% dcmFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0144GRE/SEEG/StimLF_2017-06-23/SEQ_BM_ACPCHIP_F_DCM/A05-A06_3mA_1Hz_1025us_1/step2/DCM_averaged_zs_A05-A06_3mA_1Hz_1025us_1_Y02-Y03.mat';
% 2193
% '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0141ROT/SEEG/StimLF_2016-11-21/SEQ_BM_ACPCHIP_F_DCM/OM01-OM02_4mA_1Hz_1050us_1/step2/DCM_averaged_zs_OM01-OM02_4mA_1Hz_1050us_1_OM03-OM04.mat'
% 2170
% dcmFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0140GRE/SEEG/StimLF_2017-04-03/SEQ_BM_ACPCHIP_F_DCM/Up11-Up12_3mA_1Hz_1025us_1/step2/DCM_averaged_zs_Up11-Up12_3mA_1Hz_1025us_1_Cp13-Cp14.mat'
% 1870
% dcmFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0119ROT/SEEG/StimLF_2016-11-07/SEQ_BM_ACPCHIP_F_DCM/OP01-OP02_4mA_1Hz_1050us_1/step2/DCM_averaged_zs_OP01-OP02_4mA_1Hz_1050us_1_PA15-PA16.mat' ;
% 1641
dcmFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0105GRE/SEEG/StimLF_2014-09-16/SEQ_BM_ACPCHIP_F_DCM/D05-D06_3mA_1Hz_1000us_1/step2/DCM_averaged_zs_D05-D06_3mA_1Hz_1000us_1_U07-U08.mat' ;
H = plotFit( dcmFname, [1750 1150 370 200] ) ;
if saveFig, saveas( H, fullfile( figD, 'fig2_B.svg' ) ) ; end

% interval=[40 45];
dcmFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0013LIL/SEEG/StimLF_2016-12-15/SEQ_BM_ACPCHIP_F_DCM/CR13-CR14_2mA_1Hz_1050us_1/step2/DCM_averaged_zs_CR13-CR14_2mA_1Hz_1050us_1_R13-R14.mat';
H = plotFit( dcmFname, [1400 930 370 200] ) ;
if saveFig, saveas( H, fullfile( figD, 'fig2_C.svg' ) ) ; end

% interval=[55 60];
% dcmFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0109ROT/SEEG/StimLF_2014-06-23/SEQ_BM_ACPCHIP_F_DCM/PT05-PT06_4mA_1Hz_1050us_1/step2/DCM_averaged_zs_PT05-PT06_4mA_1Hz_1050us_1_TB01-TB02.mat';
dcmFname = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/dcm/PATS__V1/0023LIL/SEEG/StimLF_2017-09-12/SEQ_BM_ACPCHIP_F_DCM/Ap03-Ap04_2mA_1Hz_1050us_1/step2/DCM_averaged_zs_Ap03-Ap04_2mA_1Hz_1050us_1_Bp04-Bp05.mat' ;
H = plotFit( dcmFname, [1750 930 370 200] ) ;
if saveFig, saveas( H, fullfile( figD, 'fig2_D.svg' ) ) ; end

end


