function atlasDCMs = buildDCMAtlas_V14( atlasId, s1Selection_config )
% % atlasId = 'AALDilate' ;
% atlasId = 'MarsAtlas' ;
% % atlasId = 'BrodmannDilate' ;
% % atlasId = 'Brodmann' ;

V = 'V14' ;

[ s1Selection, s1Selection_txt ] = s1Selection_config_V14( s1Selection_config ) ;

sequence_id             = 'ArtefactCorrectionPCHIP' ; 
[ dataDir, ~, ~ ]       = simuDirs() ;
patientsDir             = [ dataDir '/PATS__'  sequence_id ] ;
if strcmp( sequence_id, 'ArtefactCorrectionPCHIP' )
    sequence_avg_dir    = 'SEQ_BM_ACPCHIP_F_ComputeAverage' ;
end

stims_valid             = eval( [ 'validStims_' V '( sequence_id ) ;' ] ) ;
stimsCount              = length(stims_valid) ;




pF_T            = [ 8 16 ];
pF_Te           = pF_T(1) ;
pF_Ti           = pF_T(2) ;



fitID               = [ V '/' s1Selection_txt ] ;

[ ~, dcmDir, ~ ]    = simuDirs() ;
outputDir           = [ dcmDir '/PATS__' sequence_id '/ALL/2steps/' fitID '/' atlasId ] ;
system( [ 'mkdir -p ' outputDir ] ) ;




% config_s1   = struct( 'step', 1 ) ;
config_s2   = struct( 'step', 2, 'avg_mode', 'bpa_nocond' ) ;
config_s2.s1Selection = s1Selection ;



% read patients age after retreived from FTRACT DB
fd  = fopen( 'patients_birth_date.txt', 'rt' ) ;
t   = textscan(fd,'%s\t%s\n') ;
fclose(fd) ;

names   = t{1} ;
birth   = t{2} ;


atlasDCMs               = cell(size(stims_valid));
% conduction_delays_s1    = cell(size(stims_valid));
% conduction_delays_s2    = cell(size(stims_valid));

parfor s=1:stimsCount
% for s=1:length(stims_valid)
%     s
    
    stim                        = stims_valid(s);
    [ stimData, cceps_valid ]   = loadStim_V14( sequence_id, stim.patient, stim.crf, stim.stimContact ) ;
    if isempty(cceps_valid), continue ; end
    
    try
        [ stimParcelName, ~ ] = parseAtlasParcel( stimData.stimParcel{1,1}, atlasId ) ;
    catch ME
        if strcmp( ME.identifier, 'MATLAB:AtlasNotFound' ) || strcmp( ME.identifier, 'MATLAB:ParcelNotDefined' )
            fprintf( 1, [ 'Stim parcel ' ME.message '\n' ] ) ;
            continue
        else
            rethrow(ME)
        end
    end
    
    p 	= find( cellfun( @(i) ~isempty(i), strfind(names,stim.patient) ) ) ;
    if ~isequal( names{p}, stim.patient ), error('invalid patient') ; end
    d1  = datetime( birth{p}, 'InputFormat', 'yyyy-MM-dd' ) ;
    d2  = datetime( strrep( stim.crf, 'StimLF_','' ), 'InputFormat', 'yyyy-MM-dd' ) ;
    age = d2 - d1 ;
    
    stimDir                     = [ patientsDir '/' stim.patient '/SEEG/' stim.crf '/' sequence_avg_dir '/' stim.stimContact ] ;
    ccepsDir                    = [ stimDir '/cceps' ] ;
    ccepContactNames            = stimData.sensorName(cceps_valid) ;
             
    % all DCMs computed for this stim
    stimDCMs = cell(size(cceps_valid));
    
    for c=1:length(cceps_valid)
        
        ccep = cceps_valid(c);
        try
            [ ccepParcelName, ~ ]    = parseAtlasParcel( stimData.sensorParcel{1,ccep}, atlasId ) ;
        catch ME
            if strcmp( ME.identifier, 'MATLAB:AtlasNotFound' ) || strcmp( ME.identifier, 'MATLAB:ParcelNotDefined' )
                fprintf( 1, [ '\tCcep parcel: ' ME.message '\n' ] ) ;
                continue
            else
                rethrow(ME)
            end
        end
        
        dataFname               = [ ccepsDir '/' stim.stimContact '__' ccepContactNames{c} '.mat' ] ;
        
%         [ cd, te ]      = cd_te_from_pl( stim.peak_latency(c) ) ;
%         pF_D            = [] ; % struct( 'id', '0_ADJ', 'pF_D', [ 0 cd ] ) ;
%         k2              = pF_Te/te ; % te = Te / k2 => k2 = Te / te
%         pE_T            = [ -log(k1) -log(k1); -log(k2) -log(k2) ] ;
%         pE_G            = [  log(k1)  log(k1);  log(k2)  log(k2) ] ;
        
%         dcmFname        = buildDCMFname_V14( dataFname, pF_D, pC_D, pE_T, pE_G, pC_T, pC_G, pC_S, pC_H, config_s2, fit_duration_id ) ;
        dcmFname        = buildDCMFname_V14( dataFname, [], [], [], [], [], [], [], [], config_s2, [] ) ;
            
        if ~exist(dcmFname,'file')
            %             fprintf( 1, [ dcmFname ': DCM not found' '\n' ] ) ;
            
            % this is not an error as soon as the check has been done during review_stimDCM_V14 
            continue ;
%             error( [ dcmFname ': not found' ] ) ; 
        end
        DCM_s2_bpa_nocond               = load( dcmFname ) ;
        DCM_s2_bpa_nocond               = DCM_s2_bpa_nocond.DCM ;
        [ ~, ~, ~, R2_c_m0_bpa_nocond ] = compute_accuracy( DCM_s2_bpa_nocond, [] ) ;

        [~,locs]                      	= findpeaks(abs(DCM_s2_bpa_nocond.H{1})) ;
        if ~isempty(locs)
            s2_peak         = DCM_s2_bpa_nocond.xY.pst(locs(1)) ;
        else
            s2_peak        = nan ;
        end

        stimDCMs{c}                 = struct ;
        stimDCMs{c}.age             = years(age) ;
        stimDCMs{c}.dcmFname        = dcmFname ;
        stimDCMs{c}.stimParcelName  = stimParcelName ;
        stimDCMs{c}.ccepParcelName  = ccepParcelName ;
        
        stimDCMs{c}.acc             = R2_c_m0_bpa_nocond ;
        stimDCMs{c}.peak            = s2_peak ;
        
        Cp                          = spm_unvec(diag(DCM_s2_bpa_nocond.Cp),DCM_s2_bpa_nocond.Ep) ;
        [ ~, lognormal_mode ]       = lognormal( DCM_s2_bpa_nocond.Ep.D(2,1) + log( DCM_s2_bpa_nocond.M.pF.D(2) ), Cp.D(2,1) ) ;
        stimDCMs{c}.conductionDelay = lognormal_mode ;
        [ ~, lognormal_mode ]       = lognormal( DCM_s2_bpa_nocond.Ep.T(2,1) + log( pF_Te ), Cp.T(2,1) ) ;
        stimDCMs{c}.synapticTe      = lognormal_mode ;
        [ ~, lognormal_mode ]       = lognormal( DCM_s2_bpa_nocond.Ep.T(2,2) + log( pF_Ti ), Cp.T(2,2) ) ;
        stimDCMs{c}.synapticTi      = lognormal_mode ;
        
        stimDCMs{c}.energy          = stimData.energy{ccep} ;
        stimDCMs{c}.peakLatency     = stimData.mat_delay{ccep}(5) ;
        stimDCMs{c}.peakOnset       = stimData.mat_onset_delay{ccep}(5) ;
        stimDCMs{c}.peakAmpl        = stimData.mat_ampl{ccep}(5) ;
        stimDCMs{c}.peakSlope       = stimData.mat_slope{ccep}(5) ;
        stimDCMs{c}.peakDuration    = stimData.mat_duration{ccep}(1) ;
        stimDCMs{c}.peakIntegral    = stimData.mat_integral{ccep}(1) ;
        stimDCMs{c}.peakLatStart    = stimData.mat_lat_start{ccep}(1) ;
        
        
%         if stimDCMs{c}.conductionDelay > 30
%             H5=figure('Position', [672 758 560 302]) ;
%             H6=figure('Position', [1234 758 560 302]) ;
% %             H56=figure('Position', [1796 758 560 302]);
%             figure(H5); spm_dcm_erp_results(DCM_s2_bpa_nocond,'Response',H5);          suptitle( [ 's' num2str(s) ' c' num2str(c) ] ) ; % ' s2 ' config.avg_mode ' - R2 10-55: ' num2str(R2_c_bpa_nocond) '- m0: ' num2str(R2_c_m0_bpa_nocond) ] ) ;
%             figure(H6); spm_dcm_erp_results(DCM_s2_bpa_nocond,'ERPs (sources)',H6);    suptitle( [ 's' num2str(s) ' c' num2str(c) ] ) ; %  ' s2 ' config.avg_mode ' - R2 10-55: ' num2str(R2_c_bpa_nocond) '- m0: ' num2str(R2_c_m0_bpa_nocond) ] ) ;
% %             figure(H56); bar([DCM_s2_bpa_nocond.Ep.T(1,1) DCM_s2_bpa_nocond.Ep.T(1,2) DCM_s2_bpa_nocond.Ep.G(1,1) DCM_s2_bpa_nocond.Ep.G(1,2) DCM_s2_bpa_nocond.Ep.S(1) DCM_s2_bpa_nocond.Ep.S(2) DCM_s2_bpa_nocond.Ep.C(1) DCM_s2_bpa_nocond.Ep.H(1) DCM_s2_bpa_nocond.Ep.H(2) DCM_s2_bpa_nocond.Ep.H(3) DCM_s2_bpa_nocond.Ep.H(4) ]) ; ylim( [-3.5 3.5] ) ;
% %             close all ;
%         end
        stimDCMs{c}.dist_stim_rec   = norm( stimData.stimPos{1,ccep} - stimData.sensorPos{1,ccep}, 2 ) ;

    end
%     close all ;

    atlasDCMs{s} = stimDCMs ;
    
end


fname = [ outputDir '/' atlasId '_DCMs_' V '_' s1Selection_txt '.mat' ] ;
fprintf( 1, [ 'Write ' fname '\n' ] ) ;
save( fname, 'atlasDCMs', '-v7.3' );


end


