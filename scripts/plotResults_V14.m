
V       = 'V14';
atlasId = 'MarsAtlas' ;
s1Selection_config = 1 ;

symmetric   = 1 ;
[ s1Selection, s1Selection_txt ] = s1Selection_config_V14( s1Selection_config ) ;
s2Selection     = struct ;
s2Selection.R2  = 70 ;
s2Selection.MPD = 5 ;
s2Selection_txt = [ 'R2' num2str(s2Selection.R2) '_MPD' num2str(s2Selection.MPD) ] ;
% id_txt = sprintf( '%s %s sym%d %s %s %s', V, atlasId, symmetric, s1Selection_txt, s2Selection_txt ) ;

age_limit = 10 ;


%% PL and CD

age_min     = 0 ;
age_max     = age_limit ;
fname       = [ '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm/dcm/PATS__ArtefactCorrectionPCHIP/ALL/2steps/V14/minPL0_maxPL80_R2NaN_MPDNaN/MarsAtlas/R270_MPD5/age_' num2str(age_min) '_' num2str(age_max) '/mat/median_peakLatency_parcels_sym' num2str(symmetric) '.mat' ] ;
S           = load( fname ) ;
pl_0_AL_sym = S.median_peakLatency_parcels ;

age_min     = age_limit ;
age_max     = Inf ;
fname       = [ '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm/dcm/PATS__ArtefactCorrectionPCHIP/ALL/2steps/V14/minPL0_maxPL80_R2NaN_MPDNaN/MarsAtlas/R270_MPD5/age_' num2str(age_min) '_' num2str(age_max) '/mat/median_peakLatency_parcels_sym' num2str(symmetric) '.mat' ] ;
S           = load( fname ) ;
pl_AL_inf_sym = S.median_peakLatency_parcels ;


histo_normalization = 'count' ;
if strcmp( histo_normalization, 'count' )
    ymax = 140 ;
elseif strcmp( histo_normalization, 'probability' )
    ymax = 0.5;
end

% common_parcels = (~isnan(pl_AL_inf_sym)) & (~isnan(pl_0_AL_sym)) ;



H_close                 = figure ; hold on ;
histo_normalization   	= 'cdf' ;
h_pl_AL_inf_sym_cum    	= histogram( pl_AL_inf_sym(~isnan(pl_AL_inf_sym)), 0:4:80, 'Normalization', histo_normalization ) ;
h_pl_AL_inf_sym_cum.FaceColor  = [0.5 0.5 0.5] ; h_pl_AL_inf_sym_cum.FaceAlpha = 1 ;
h_pl_0_AL_sym_cum              = histogram( pl_0_AL_sym(~isnan(pl_0_AL_sym)), 0:4:80, 'Normalization', histo_normalization ) ;
h_pl_0_AL_sym_cum.FaceColor    = [0 0 0] ; h_pl_0_AL_sym_cum.FaceAlpha = 1 ;
legend( { '>10yo' '<10yo' } ) ;



H                       = figure( 'Position', [912 1165 304 304] ) ;
histo_normalization   	= 'count' ; ymax = 140 ;
h_pl_AL_inf_sym       	= histogram( pl_AL_inf_sym(~isnan(pl_AL_inf_sym)), 0:4:80, 'Normalization', histo_normalization ) ;
h_pl_AL_inf_sym.FaceColor   = [0.5 0.5 0.5] ;
ylabel( '#parcels pairs' ) ; ylim([ 0 ymax]) ;
hold on ;
yyaxis right ;
plot( h_pl_AL_inf_sym_cum.BinEdges(1:end-1)+h_pl_AL_inf_sym_cum.BinWidth/2, h_pl_AL_inf_sym_cum.Values, 'Color', [0.5 0.5 0.5], 'LineWidth', 2 ) ;
plot( h_pl_AL_inf_sym_cum.BinEdges(1:end-1)+h_pl_AL_inf_sym_cum.BinWidth/2, h_pl_0_AL_sym_cum.Values, 'Color', [0 0  0 ], 'LineWidth', 2, 'LineStyle', '-' ) ;
ylabel( 'cumul. density' ) ; ymax = 1 ; ylim([ 0 ymax]) ;

legend( { '>10yo' '>10yo' '<10yo' } ) ;
xlabel( 'peak latencies (ms)') ; xlim([0 80]);
title( [ 'PL >10yo'  ] ) ;










