function analyzeAtlasDCMs_V14( atlasDCMs, atlasId, s1Selection_config, histo_normalization )

age_limit = 10 ;

% symmetric = 0 ;
% analyzeAtlasDCMs_V14_( atlasDCMs, atlasId, symmetric, 0, Inf, s1Selection_config, histo_normalization )
% analyzeAtlasDCMs_V14_( atlasDCMs, atlasId, symmetric, 0, age_limit, s1Selection_config, histo_normalization )
% analyzeAtlasDCMs_V14_( atlasDCMs, atlasId, symmetric, age_limit, Inf, s1Selection_config, histo_normalization )

symmetric = 1 ;
% analyzeAtlasDCMs_V14_( atlasDCMs, atlasId, symmetric, 0, age_limit, s1Selection_config, histo_normalization )
analyzeAtlasDCMs_V14_( atlasDCMs, atlasId, symmetric, age_limit, Inf, s1Selection_config, histo_normalization )

end




function analyzeAtlasDCMs_V14_( atlasDCMs, atlasId, symmetric, age_min, age_max, s1Selection_config, histo_normalization )


icm_screen = true ;
% icm_screen = false ;


V           = 'V14' ;

[ s1Selection, s1Selection_txt ] = s1Selection_config_V14( s1Selection_config ) ;

% acc_th_s2           = 70 ;
% max_diff_peak_s2    = 5 ;
s2Selection     = struct ;
s2Selection.R2  = 70 ;
% s2Selection.R2  = 75 ;
% s2Selection.R2  = 80 ;
s2Selection.MPD = 5 ;
s2Selection_txt = [ 'R2' num2str(s2Selection.R2) '_MPD' num2str(s2Selection.MPD) ] ;


% id = sprintf( '%s %s sym%d R2 %dpc MAX PK DIF %d minPL %d', V, atlasId, symmetric, s2Selection.R2, s2Selection.MPD, minPL ) ;
id_txt = sprintf( '%s %s sym%d %s %s %s', V, atlasId, symmetric, s1Selection_txt, s2Selection_txt ) ;


if isempty(atlasDCMs)
    atlasDCMs = loadAtlasDCMs(atlasId) ;
end


sequence_id         = 'ArtefactCorrectionPCHIP' ; 


fitID               = [ V '/' s1Selection_txt ] ;

output_tmp          = [ 'PATS__' sequence_id '/ALL/2steps/' fitID '/' atlasId '/' s2Selection_txt '/age_' num2str(age_min) '_' num2str(age_max) ] ;

[ ~, dcmDir, ~ ]    = simuDirs() ;
% outputDir           = [ dcmDir '/' output_tmp ] ;
% system( [ 'mkdir -p ' outputDir ] ) ;

outputDir_fig       = [ dcmDir '/../screenshots/' output_tmp ] ;
system( [ 'mkdir -p ' outputDir_fig ] ) ;

outputDir_mat       = [ dcmDir '/' output_tmp '/mat' ] ;
system( [ 'mkdir -p ' outputDir_mat ] ) ;



%% following data are independent from the cceps threshold
computed = 1 ;
if ~computed
    [ data_parcels, data_all ]  = atlasParcels( atlasId, atlasDCMs, symmetric, age_min, age_max, s1Selection, s2Selection ) ;
    data_atlas                  = struct ;
    data_atlas.data_parcels     = data_parcels ;
    data_atlas.data_all         = data_all ;
    fname                       = [ 'data_atlas.mat' ] ;
    save( fname, 'data_atlas' ) ;
else
    fname                       = [ 'data_atlas.mat' ] ;
    data_atlas                  = load( fname ) ;
    data_parcels                = data_atlas.data_atlas.data_parcels ;
    data_all                    = data_atlas.data_atlas.data_all ;
end

sigCCEPs_parcels           = data_parcels.sigCCEPs_parcels ;
peakLatency_parcels        = data_parcels.peakLatency_parcels ;
conduction_delays_parcels  = data_parcels.conduction_delays_parcels ;
synapticTe_parcels         = data_parcels.synapticTe_parcels ;
synapticTi_parcels         = data_parcels.synapticTi_parcels ;
distance_parcels           = data_parcels.distance_parcels ;
energy_parcels             = data_parcels.energy_parcels ;

stimParcel                     = data_all.stimParcel ;
recParcel                      = data_all.recParcel ;
conductionDelay                = data_all.conductionDelay ;
synapticTe                     = data_all.synapticTe ;
synapticTi                     = data_all.synapticTi ;
energy                         = data_all.energy ;
peakLatency                    = data_all.peakLatency ;
peakOnset                      = data_all.peakOnset ;
peakAmpl                       = data_all.peakAmpl ;
peakSlope                      = data_all.peakSlope ;
peakDuration                   = data_all.peakDuration ;
peakIntegral                   = data_all.peakIntegral ;
peakLatStart                   = data_all.peakLatStart ;
dist_stim_rec                  = data_all.dist_stim_rec ;



% PLOT DISTRIBUTIONS !!!

plot_distributions = 0 ;
% plot_distributions = 1 ;

if plot_distributions
    
    figure ;
    subplot(1,5,1);
    h = histogram( peakLatency, 0:4:80, 'Orientation', 'horizontal' ) ;
    h.FaceColor=[0.5 0.5 0.5] ;
    xticks(0:3000:6000); xlim([0 7000 ]);
    yticks(0:8:80); ylim([0 80]) ;
    set(get(h,'Parent'),'xdir','r')
    
    subplot(1,5,2);
    h = histogram( conductionDelay, 0:2.5:40 ) ;
    h.FaceColor=[0.5 0.5 0.5] ;
    xlim([0 40]) ;
    ylim([0 7000]) ;
    xlabel('conduction delays (ms)');
    title([num2str(age_min) ' - ' num2str(age_max) ' y.o.' ])
    
    subplot(1,5,3);
    h = histogram( synapticTe, 0:1:20 ) ;
    h.FaceColor=[0.5 0.5 0.5] ;
    xlim([0 20]) ;
    ylim([0 7000]) ;
    xlabel('synaptic excitatory delays (ms)');
    title([num2str(age_min) ' - ' num2str(age_max) ' y.o.' ])
    
    subplot(1,5,4);
    h = histogram( synapticTi, 0:1:20 ) ;
    h.FaceColor=[0.5 0.5 0.5] ;
    xlim([0 20]) ;
    ylim([0 7000]) ;
    xlabel('synaptic inhibitory delays (ms)');
    title([num2str(age_min) ' - ' num2str(age_max) ' y.o.' ])
    
    subplot(1,5,5);
    h = histogram( dist_stim_rec ) ;
    h.FaceColor=[0.5 0.5 0.5] ;
    xlabel('dist stim rec (mm)');
    
    suptitle([num2str(age_min) ' - ' num2str(age_max) ' y.o.' ])
    
    
    H = figure( 'Position', [1186 328 732 572] ) ;
    h = histogram2( conductionDelay, synapticTe, 0:1:40, 0:1:20, 'DisplayStyle', 'tile', 'FaceColor', 'flat' ) ;
    xlabel('conduction delays (ms)');
    ylabel('synaptic excitatory delays (ms)');
    caxis([0 300])
    colorbar
    
end


% % correlation peak latency & conduction delay
% [r,p] = corrcoef(peakLatency,conductionDelay);
% [r,p] = corrcoef(peakLatency,synapticTe);
% [r,p] = corrcoef(peakLatency,synapticTi);
% 
% % linear model
% X = [ synapticTe' synapticTi' ] ;
% y = (peakLatency-conductionDelay)' ;
% mdl = fitlm(X,y)
% plotDiagnostics(mdl)
% plotDiagnostics(mdl,'cookd')



%% comment expliquer le gros pic vers 0 dans les distributions marginales
% id = find( peakLatency < 24 ) ;
% 100 / length(id) * length( find( conductionDelay(id) < 2 ) )
% 100 / length(id) * length( find( conductionDelay(id) > 2 ) )
% 100 / length(id) * length( find( synapticTe(id) < 1 ) )
% 100 / length(id) * length( find( synapticTe(id) > 1 ) )
% 
% id = find( conductionDelay < 2 ) ;
% 100 / length(id) * length( find( peakLatency(id) < 12 ) )
% 100 / length(id) * length( find( peakLatency(id) > 12 ) )
% median( peakLatency(id) )
% median(dist_stim_rec(id))
% 
% id = find( conductionDelay > 2 ) ;
% median(dist_stim_rec(id))
% 
% id = find( synapticTe < 1 ) ;
% median(dist_stim_rec(id))



%% correlation distances / peak latency
% figure ; scatter( distances_stim_rec, peakLatency )
% xlabel( 'distance stim rec' ) ; ylabel( 'peak latency' )
% compute_correlation_distance = true ;
compute_correlation_distance = false ;
if compute_correlation_distance
    
    % histogram of distance
    H = figure ; histogram( dist_stim_rec ) ; xlabel( 'distance' ) ; ylabel( '#cceps' ) ;
    title( 'Eucl. distance to the stimulation ' )
    fname = [ dcmDir '/../screenshots/histo_distance_stim.png' ] ;
    saveas( H, fname ) ;

    
    conductionDelay_d   = [] ;
    synapticTe_d        = [] ;
    peakLatency_d       = [] ;
    peakOnset_d         = [] ;
    peakAmpl_d          = [] ;
    peakSlope_d         = [] ;
    peakDuration_d      = [] ;
    peakIntegral_d      = [] ;
    peakLatStart_d      = [] ;
    
    % same using a maximum distance
%     max_stim_rec_distance = max(dist_stim_rec);
%     max_stim_rec_distance = 40 ;
%     max_stim_rec_distance = 50 ;
%     max_stim_rec_distance = 60 ;
%     max_stim_rec_distance = 80 ;
%     max_stim_rec_distance = 90 ;
%     max_stim_rec_distance = 100 ;
   max_stim_rec_distance = 150 ;

    ds=10;
    dr=5:ds:max_stim_rec_distance ;
    for di=5:ds:max_stim_rec_distance
        id                      = find( ( dist_stim_rec < max_stim_rec_distance ) & ( dist_stim_rec > di - ds/2 ) & ( dist_stim_rec < di + ds/2 ) ) ;
        conductionDelay_d(end+1)= median( conductionDelay(id) ) ;
        synapticTe_d(end+1)     = median( synapticTe(id) ) ;
        peakLatency_d(end+1)    = median( peakLatency(id) ) ;
        peakOnset_d(end+1)      = median( peakOnset(id) ) ;
        peakAmpl_d(end+1)       = median( peakAmpl(id) ) ;
        peakSlope_d(end+1)      = median( peakSlope(id) ) ;
        peakDuration_d(end+1)   = median( peakDuration(id), 'omitnan' ) ;
        peakIntegral_d(end+1)   = median( peakIntegral(id), 'omitnan' ) ;
        peakLatStart_d(end+1)   = median( peakLatStart(id), 'omitnan' ) ;
    end
    
    if icm_screen
        H = figure ( 'Position', [664 797 1399 666] ) ;
    else
        H = figure ( 'Position', [103 182 1399 666] ) ;
    end
    
    subplot( 3, 4, 1 ) ; plot( dr, peakLatency_d ) ;       xlabel('distance') ; ylabel('peakLatency') ;            title('peakLatency')
    subplot( 3, 4, 2 ) ; plot( dr, peakOnset_d ) ;         xlabel('distance') ; ylabel('peakOnset') ;              title('peakOnset')
    subplot( 3, 4, 3 ) ; plot( dr, peakAmpl_d ) ;          xlabel('distance') ; ylabel('peakAmpl') ;               title('peakAmpl')
    subplot( 3, 4, 4 ) ; plot( dr, peakSlope_d ) ;         xlabel('distance') ; ylabel('peakSlope') ;              title('peakSlope')
    subplot( 3, 4, 5 ) ; plot( dr, peakDuration_d ) ;      xlabel('distance') ; ylabel('peakDuration') ;           title('peakDuration')
    subplot( 3, 4, 6 ) ; plot( dr, peakIntegral_d ) ;      xlabel('distance') ; ylabel('peakIntegral') ;           title('peakIntegral')
    subplot( 3, 4, 7 ) ; plot( dr, peakLatStart_d ) ;      xlabel('distance') ; ylabel('peakLatStart') ;           title('peakLatStart')
    subplot( 3, 4, 8 ) ; plot( dr, conductionDelay_d ) ;   xlabel('distance') ; ylabel('conductionDelay') ;        title('conductionDelay')
    subplot( 3, 4, 9 ) ; plot( dr, synapticTe_d ) ;        xlabel('distance') ; ylabel('synapticTe') ;             title('synapticTe')
    subplot( 3, 4, 10 ) ; histogram( dist_stim_rec, 0:5:max_stim_rec_distance ) ; xlabel('distance') ;
    suptitle( [ 'effect of distance - maximum distance ' num2str(max_stim_rec_distance) ] );
    
    fname = [ dcmDir '/../screenshots/histo_ALL_versus_stim_distance.png' ] ;
    saveas( H, fname ) ;

end


%% correlation peakLatency and conductionDelay
% correlation_pl_cd = true ;
correlation_pl_cd = false ;
if correlation_pl_cd
    stepPL  = 2 ;
    minPL = minPeakLatency ;
    maxPL = maxPeakLatency ;
    conductionDelay_d   = [] ;   
    for pl=minPL:stepPL:maxPL-stepPL
%         pl
        id = find( ( peakLatency > pl ) & ( peakLatency < pl + stepPL ) ) ;
        conductionDelay_d(end+1) = median( conductionDelay(id) ) ;
    end
    if icm_screen
        figure( 'Position', [109 356 1666 476] ) ;
    else
        figure( 'Position', [198 368 1018 420] ) ;
    end
    subplot(1,3,1);
    plot( minPL+stepPL/2:stepPL:maxPL-stepPL/2, conductionDelay_d ) ;
    xlabel('peak latency') ;
    ylabel('conduction delay');
    subplot(1,3,2);
    histogram(conductionDelay);
    xlabel('conduction delay');xlim([0 maxPeakLatency]);
    subplot(1,3,3);
    histogram(peakLatency,minPL:stepPL:maxPL);
    xlabel('peak latency');%xlim([0 150]);
    yyaxis right
%     plot( minPL:stepPL:maxPL, conductionDelay_d, 'Color', 'r' ) ; ylabel( 'conduction delay' )
    plot( minPL+stepPL/2:stepPL:maxPL-stepPL/2, conductionDelay_d, 'Color', 'r' ) ; ylabel( 'conduction delay' )
    
    suptitle( [ 'age [' num2str(age_min) ' - ' num2str(age_max) '] - Min PL: ' num2str(minPeakLatency) ' - Max PL: ' num2str(maxPeakLatency) ] ) ;
    
    fname = [ dcmDir '/../screenshots/CD_versus_PL.png' ] ;
    saveas( H, fname ) ;

end



%%

cceps_th = 20 ;
% cceps_th = 10 ;
% cceps_th = 5 ;

parcel_pairs_invalid                                                = cellfun( @(x) length(x) < cceps_th, conduction_delays_parcels ) ;

median_peakLatency_parcels                                          = cellfun( @(x) median(full(x)), peakLatency_parcels ) ;
median_peakLatency_parcels              ( parcel_pairs_invalid )    = nan ;
 
median_conduction_delays_parcels                                    = cellfun( @(x) median(full(x)), conduction_delays_parcels ) ;
median_conduction_delays_parcels        ( parcel_pairs_invalid )    = nan ;
    
if symmetric
    synapticTe_parcels_ipsi                                         = synapticTe_parcels(:,1:size(synapticTe_parcels,2)/2 ) ;
    synapticTe_parcels_contra                                       = synapticTe_parcels(:,  size(synapticTe_parcels,2)/2 + 1:end ) ;
    synapticTe_parcels_sym                                          = cellfun( @(i,c) [ i c ], synapticTe_parcels_ipsi, synapticTe_parcels_contra, 'UniformOutput', false ) ;
    invalid_parcels                                                 = cellfun( @(x) length(x) < cceps_th, synapticTe_parcels_sym ) ;
    median_synapticTe_parcels                                       = cellfun( @(x) median(full(x)), synapticTe_parcels_sym ) ;
    median_synapticTe_parcels           ( invalid_parcels )         = nan ;
else
    median_synapticTe_parcels                                    	= cellfun( @(x) median(full(x)), synapticTe_parcels ) ;
    median_synapticTe_parcels           ( parcel_pairs_invalid )   	= nan ;
end

if symmetric
    synapticTi_parcels_ipsi                                         = synapticTi_parcels(:,1:size(synapticTi_parcels,2)/2 ) ;
    synapticTi_parcels_contra                                       = synapticTi_parcels(:,  size(synapticTi_parcels,2)/2 + 1:end ) ;
    synapticTi_parcels_sym                                          = cellfun( @(i,c) [ i c ], synapticTi_parcels_ipsi, synapticTi_parcels_contra, 'UniformOutput', false ) ;
    invalid_parcels                                                 = cellfun( @(x) length(x) < cceps_th, synapticTi_parcels_sym ) ;
    median_synapticTi_parcels                                       = cellfun( @(x) median(full(x)), synapticTi_parcels_sym ) ;
    median_synapticTi_parcels           ( invalid_parcels )         = nan ;
else
    median_synapticTi_parcels                                       = cellfun( @(x) median(full(x)), synapticTi_parcels ) ;
    median_synapticTi_parcels           ( parcel_pairs_invalid )    = nan ;
end

median_distance_parcels                                             = cellfun( @(x) median(full(x)), distance_parcels ) ;
median_distance_parcels                 ( parcel_pairs_invalid )    = nan ;

median_peakLatency_velocity_parcels                                 = cellfun( @(d,pl) median( d./pl ), distance_parcels, peakLatency_parcels ) ;
median_peakLatency_velocity_parcels     ( parcel_pairs_invalid )    = nan ;

median_conductionDelay_velocity_parcels                             = cellfun( @(d,cd) median( d./cd ), distance_parcels, conduction_delays_parcels ) ;
median_conductionDelay_velocity_parcels ( parcel_pairs_invalid )    = nan ;


% tmp = { 'median_peakLatency_parcels', 'median_conduction_delays_parcels', 'median_synapticTe_parcels', 'median_synapticTi_parcels', 'median_distance_parcels', 'median_peakLatency_velocity_parcels', 'median_conductionDelay_velocity_parcels'       } ; 
% for v=1:length(tmp)
%     vname = tmp{v} ;
%     fname = [ outputDir_mat '/' vname '_sym' num2str(symmetric) '.mat' ] ;
%     fprintf( 1, [ 'Save ' fname '\n' ] ) ;
%     save( fname, vname ) ;
% end



[ parcelsName, ~ ]  = atlasParcels_NameTexture(atlasId) ;



plot_conduction_delays  = true ;
conduction_delay_max    = 40 ;
% plot_conduction_delays = false ;
if plot_conduction_delays
    

    saveFigures = true ;
%     saveFigures = false ;
    
    outputDir_fig = [ outputDir_fig '/ccepsMin_' num2str(cceps_th) ] ;
    system( [ 'mkdir -p ' outputDir_fig ] ) ;
    
    

    % number of significant CCEPs for each connection (independent from
    % cceps_th, directly computed from original data)
    cceps_sig           = cellfun( @(x) length(x), sigCCEPs_parcels ) ;
    cceps_sig( cceps_sig == 0 ) = nan ;
    H_sig   = figure( 'Position', [63 1124 711 376] ) ;
    imagescwithnan( log(cceps_sig), colormap, [0.6 0.6 0.6], 0, 8 ) ;
    axis equal
    if symmetric
        xlabel( 'rec ipsi /contra' ) ;
        ylabel( 'stim' ) ;
        set(gca,'XLim',[0.5 size(median_conduction_delays_parcels,2)+0.5] ) ;
        set(gca,'YLim',[0.5 size(median_conduction_delays_parcels,1)+0.5] ) ;
    else
        xlabel( 'rec L/R' ) ;
        ylabel( 'stim L/R' )
    end
    title( [ 'Sig CCEPs: sym' num2str(symmetric) ' ' num2str(age_min) ' ' num2str(age_max) ] );
    
    
    
    % number of CCEPs used in each estimation (using the threshold)
    cceps_estimated     = cellfun( @(x) length(x), conduction_delays_parcels ) ;
    cceps_estimated( cceps_estimated < cceps_th ) = nan ;
    H_est   = figure( 'Position', [63 667 711 376] ) ;
    imagescwithnan( log(cceps_estimated), colormap, [0.6 0.6 0.6], 0, 8 ) ;
    axis equal
    if symmetric
        xlabel( 'rec ipsi /contra' ) ;
        ylabel( 'stim' ) ;
        set(gca,'XLim',[0.5 size(median_conduction_delays_parcels,2)+0.5] ) ;
        set(gca,'YLim',[0.5 size(median_conduction_delays_parcels,1)+0.5] ) ;
    else
        xlabel( 'rec L/R' ) ;
        ylabel( 'stim L/R' )
    end
    title( [ 'Sig CCEPs used for cceps th ' num2str(cceps_th) ': sym' num2str(symmetric) ' ' num2str(age_min) ' ' num2str(age_max) ] );
    
    
    
    % median CD using cceps_th
    H = figure( 'Position', [58 206 716 380] ) ;
    imagescwithnan( median_conduction_delays_parcels, colormap, [0.6 0.6 0.6], 0, 30 ) ;
    axis equal
    if symmetric
        xlabel( 'rec ipsi /contra' ) ;
        ylabel( 'stim' ) ;
        set(gca,'XLim',[0.5 size(median_conduction_delays_parcels,2)+0.5] ) ;
        set(gca,'YLim',[0.5 size(median_conduction_delays_parcels,1)+0.5] ) ;
    else
        xlabel( 'rec L/R' ) ;
        ylabel( 'stim L/R' )
    end
    title( [ 'Median CD: sym' num2str(symmetric) ' ' num2str(age_min) ' ' num2str(age_max) ' cceps min=' num2str(cceps_th) ] );
    
    
    parcels_hemi      	= size( median_conduction_delays_parcels, 1 ) ;
    pairs_hemi          = parcels_hemi.^2 ;
    pairs_sig_ipsi      = 100 / pairs_hemi * length( find( ~isnan( cceps_sig(:,1:parcels_hemi) ) ) ) ;
    pairs_sig_contra    = 100 / pairs_hemi * length( find( ~isnan( cceps_sig(:,parcels_hemi+1:end)) ) ) ;
    pairs_est_ipsi      = 100 / pairs_hemi * length( find( ~isnan( median_conduction_delays_parcels(:,1:parcels_hemi) ) ) ) ;
    pairs_est_contra    = 100 / pairs_hemi * length( find( ~isnan( median_conduction_delays_parcels(:,parcels_hemi+1:end)) ) ) ;
    
    
    if saveFigures
        fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_conductionDelays_mat.txt' ] ;
        fprintf( 1, [ 'Save ' fname '\n' ] ) ;
        dlmwrite( fname, median_conduction_delays_parcels, 'delimiter', '\t', 'precision', 3 );

        fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_conductionDelays_mat_info.txt' ] ;
        fprintf( 1, [ 'Save ' fname '\n' ] ) ;
        fd  = fopen( fname, 'w' ) ;
        fprintf( fd, [ 'sig. pairs (CCEPs>=1) ipsi: ' num2str(pairs_sig_ipsi) '%%\n' ] ) ;
        fprintf( fd, [ 'sig. pairs (CCEPs>=1) contra: ' num2str(pairs_sig_contra) '%%\n' ] ) ;
        fprintf( fd, [ 'est. pairs (CCEPs>=' num2str(cceps_th) ') ipsi: ' num2str(pairs_est_ipsi) '%%\n' ] ) ;
        fprintf( fd, [ 'est. pairs (CCEPs>=' num2str(cceps_th) ') contra: ' num2str(pairs_est_contra) '%%\n' ] ) ;
        fclose(fd);
        
        
        fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_conductionDelays_mat.svg' ] ;
        fprintf( 1, [ 'Save ' fname '\n' ] ) ;
        saveas( H, fname ) ;
        fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_conductionDelays_mat.png' ] ;
        fprintf( 1, [ 'Save ' fname '\n' ] ) ;
        saveas( H, fname ) ;
    end
    
    
    if strcmp( histo_normalization, 'count' )
        ymax = 140 ;
    elseif strcmp( histo_normalization, 'probability' )
        ymax = 0.5;
    end
    
         
    H = figure( 'Position', [912 1165 304 304] ) ;
%     h = histogram( median_peakLatency_parcels(:), 0:4:80, 'Normalization', histo_normalization ) ; 
    h = histogram( median_peakLatency_parcels(~isnan(median_peakLatency_parcels)), 0:4:80, 'Normalization', histo_normalization ) ; 
    if cceps_th == 20
        ylim( [ 0 180 ] ) ;
    else
        ylim( [ 0 ymax]) ;
    end
    xlim([0 80]);
    h.FaceColor = [0.5 0.5 0.5] ;
    xlabel( 'peak latencies (ms)') ; 
    ylabel( '#parcels pairs' ) ; 
%     title( [ 'using parcels median: ' strrep(id_txt,'_',' ') ] ) ;
    title( [ 'sym' num2str(symmetric) ' ' num2str(age_min) ' ' num2str(age_max) ] );
    
    if saveFigures
        fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_peakLatency_histo_parcels_' histo_normalization '.txt' ] ;
        fprintf( 1, [ 'Save ' fname '\n' ] ) ;
        dlmwrite( fname,h.Values,'delimiter','\t');
        
        fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_peakLatency_histo_parcels_' histo_normalization '.svg' ] ;
        fprintf( 1, [ 'Save ' fname '\n' ] ) ;
        saveas( H, fname ) ;
        fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_peakLatency_histo_parcels_' histo_normalization '.png' ] ;
        fprintf( 1, [ 'Save ' fname '\n' ] ) ;
        saveas( H, fname ) ;
    end
    
    
    summary_txt = [] ;
    

    
    H = figure( 'Position', [1526 1162 304 304] ) ;
%     h = histogram( median_conduction_delays_parcels(:), 0:2:conduction_delay_max, 'Normalization', histo_normalization ) ; 
    
    if cceps_th == 20
        h = histogram( median_conduction_delays_parcels(~isnan(median_conduction_delays_parcels)), 0:1:conduction_delay_max, 'Normalization', histo_normalization ) ; 
        xlim( [ 0 20 ] );
        ylim( [ 0 180 ] ) ;
    else
        h = histogram( median_conduction_delays_parcels(~isnan(median_conduction_delays_parcels)), 0:2:conduction_delay_max, 'Normalization', histo_normalization ) ; 
        xlim( [ 0 40 ] );
        ylim( [ 0 ymax ] ) ;
    end
    h.FaceColor = [0.5 0.5 0.5] ;
    xlabel( 'conduction delays (ms)') ; 
    ylabel( '#parcels pairs' ) ; 
    
%     title( [ 'using parcels median: ' strrep(id_txt,'_',' ') ] ) ;
    title( [ 'sym' num2str(symmetric) ' ' num2str(age_min) ' ' num2str(age_max) ] );
%     fprintf( 1, [ 'Median conduction delay: ' num2str(median(median_conduction_delays_parcels(:),'omitnan')) 'ms\n' ] ) ;
    summary_txt = [ summary_txt 'Median conduction delay: ' num2str( median(median_conduction_delays_parcels(:),'omitnan')) 'ms\n' ] ;
    summary_txt = [ summary_txt 'Mean conduction delay: ' num2str( mean(median_conduction_delays_parcels(:),'omitnan')) 'ms\n' ] ;
    
    tmp         = median_conduction_delays_parcels(:,1:41) ;
    summary_txt = [ summary_txt 'Median conduction delay IPSI: ' num2str( median(tmp(:),'omitnan')) 'ms\n' ] ;
    summary_txt = [ summary_txt 'Mean conduction delay IPSI: ' num2str( mean(tmp(:),'omitnan')) 'ms\n' ] ;
    tmp         = median_conduction_delays_parcels(:,42:end) ;
    summary_txt = [ summary_txt 'Median conduction delay CONTRA: ' num2str( median(tmp(:),'omitnan')) 'ms\n' ] ;
    summary_txt = [ summary_txt 'Mean conduction delay CONTRA: ' num2str( mean(tmp(:),'omitnan')) 'ms\n' ] ;
    
    
    tmp1        = median_conduction_delays_parcels(:,1:41) ;
    [ y1, i1 ]  = max( tmp1(:) ) ;
    [ s1, r1 ]  = ind2sub( size(tmp1), i1 ) ;
    summary_txt = [ summary_txt 'Largest conduction delay IPSI: ' num2str(y1) 'ms from ' parcelsName{s1} ' to ' parcelsName{r1} '\n' ] ;
        
    tmp2        = median_conduction_delays_parcels(:,42:end) ;
    [ y2, i2 ]  = max( tmp2(:) ) ;
    [ s2, r2 ]  = ind2sub( size(tmp2), i2 ) ;
    summary_txt = [ summary_txt 'Largest conduction delay CONTRA: ' num2str(y2) 'ms from ' parcelsName{s2} ' to ' parcelsName{r2+41} '\n' ] ;
    
    
    for diz_ms=1:2
        cd_limit            = diz_ms * 10 ; % ms
        bin_limit_id        = find( h.BinEdges==cd_limit ) ;
        count_under_limit   = sum( h.Values(1:bin_limit_id-1) ) ;
        count_over_limit    = sum( h.Values(bin_limit_id:end) ) ;
%         fprintf( 1, [ 'Perc. of median conduction delay shorter than ' num2str(cd_limit) 'ms : ' num2str( 100 /  sum( h.Values ) * count_under_limit ) '%%\n' ] ) ;
%         fprintf( 1, [ 'Perc. of median conduction delay longer than ' num2str(cd_limit) 'ms : ' num2str( 100 /  sum( h.Values ) * count_over_limit ) '%%\n' ] ) ;
        summary_txt = [ summary_txt 'Perc. of median conduction delay shorter than ' num2str(cd_limit) 'ms : ' num2str( 100 /  sum( h.Values ) * count_under_limit ) '%%\n' ] ;
        summary_txt = [ summary_txt 'Perc. of median conduction delay longer than ' num2str(cd_limit) 'ms : ' num2str( 100 /  sum( h.Values ) * count_over_limit ) '%%\n' ] ; 
    end
    
    if saveFigures
        fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_conductionDelays_histo_parcels_' histo_normalization '.txt' ] ;
        fprintf( 1, [ 'Save ' fname '\n' ] ) ;
        dlmwrite( fname,h.Values,'delimiter','\t');
        
        fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_conductionDelays_histo_parcels_' histo_normalization '.svg' ] ;
        fprintf( 1, [ 'Save ' fname '\n' ] ) ;
        saveas( H, fname ) ;
        fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_conductionDelays_histo_parcels_' histo_normalization '.png' ] ;
        fprintf( 1, [ 'Save ' fname '\n' ] ) ;
        saveas( H, fname ) ;
    end
    
    
    
    
    
    H = figure( 'Position', [914 782 304 304] ) ;
%     h = histogram( median_peakLatency_velocity_parcels, 0:.2:4, 'Normalization', histo_normalization ) ; 
    h = histogram( median_peakLatency_velocity_parcels(~isnan(median_peakLatency_velocity_parcels)), 0:.2:4, 'Normalization', histo_normalization ) ; 
    if cceps_th == 20
        xlim( [ 0 4 ] ) ;
        ylim( [ 0 80 ] ) ;
    else
        xlim( [ 0 4 ] ) ;
        ylim( [ 0 ymax ] ) ;
    end
    h.FaceColor = [0.5 0.5 0.5] ;
    xlabel( 'peak latency velocity (m/s)') ; 
    ylabel( '#parcels pairs' ) ; 
%     title( [ 'using parcels median: ' strrep(id_txt,'_',' ') ] ) 
    title( [ 'sym' num2str(symmetric) ' ' num2str(age_min) ' ' num2str(age_max) ] );
    summary_txt = [ summary_txt 'Median peak latency velocity: ' num2str( median(median_peakLatency_velocity_parcels(:),'omitnan')) 'm/s\n' ] ;
    
    if saveFigures
        fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_PLVelocity_histo_parcels_' histo_normalization '.txt' ] ;
        fprintf( 1, [ 'Save ' fname '\n' ] ) ;
        dlmwrite( fname,h.Values,'delimiter','\t');
        
        fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_PLVelocity_histo_parcels_' histo_normalization '.svg' ] ;
        fprintf( 1, [ 'Save ' fname '\n' ] ) ;
        saveas( H, fname ) ;
        fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_PLVelocity_histo_parcels_' histo_normalization '.png' ] ;
        fprintf( 1, [ 'Save ' fname '\n' ] ) ;
        saveas( H, fname ) ;
    end    
    
    
    H = figure( 'Position', [1526 779 304 304] ) ;
    if cceps_th == 20
        h = histogram( median_conductionDelay_velocity_parcels(~isnan(median_conductionDelay_velocity_parcels)), 0:5:100 ) ; 
        xlim( [ 0 100 ] ) ;
        ylim( [ 0 80 ] ) ;
%         h = histogram( median_conductionDelay_velocity_parcels(~isnan(median_conductionDelay_velocity_parcels)), 0:2.5:50 ) ; 
%         xlim( [ 0 50 ] ) ;
%         ylim( [ 0 80 ] ) ;
    else
        h = histogram( median_conductionDelay_velocity_parcels(~isnan(median_conductionDelay_velocity_parcels)), 0:2:40, 'Normalization', histo_normalization ) ; 
        xlim( [ 0 40 ] ) ;
        ylim( [ 0 ymax]) ;
    end
    h.FaceColor = [0.5 0.5 0.5] ;
    xlabel( 'conduction delay velocity (m/s)') ; 
    ylabel( '#parcels pairs' ) ; 
%     title( [ 'using parcels median: ' strrep(id_txt,'_',' ') ] ) ;
    title( [ 'sym' num2str(symmetric) ' ' num2str(age_min) ' ' num2str(age_max) ] );
    
%     fprintf( 1, [ 'Median conduction delay velocity: ' num2str(median(median_conductionDelay_velocity_parcels(:),'omitnan')) 'm/s\n' ] ) ;
    summary_txt = [ summary_txt 'Median conduction delay velocity: ' num2str( median(median_conductionDelay_velocity_parcels(:),'omitnan')) 'm/s\n' ] ;
        
    if saveFigures
        fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_CDVelocity_histo_parcels_' histo_normalization '.txt' ] ;
        fprintf( 1, [ 'Save ' fname '\n' ] ) ;
        dlmwrite( fname,h.Values,'delimiter','\t');
        
        fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_CDVelocity_histo_parcels_' histo_normalization '.svg' ] ;
        fprintf( 1, [ 'Save ' fname '\n' ] ) ;
        saveas( H, fname ) ;
        fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_CDVelocity_histo_parcels_' histo_normalization '.png' ] ;
        fprintf( 1, [ 'Save ' fname '\n' ] ) ;
        saveas( H, fname ) ;
    end    
    
    
    H = figure( 'Position', [1220 1007 304 304] ) ;
%     h = histogram( median_distance_parcels, 0:5:100, 'Normalization', histo_normalization ) ; 
    h = histogram( median_distance_parcels(~isnan(median_distance_parcels)), 0:5:100, 'Normalization', histo_normalization ) ; 
    h.FaceColor = [0.5 0.5 0.5] ;
    xlabel( 'distances between parcels (mm)') ; xlim([0 100]); xticks( 0:20:100);
    ylabel( '#parcels pairs' ) ; ylim( [ 0 80]) ;
%     title( [ 'using parcels median: ' strrep(id_txt,'_',' ') ] ) ;
    title( [ 'sym' num2str(symmetric) ' ' num2str(age_min) ' ' num2str(age_max) ] );
    
    if saveFigures
        fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_distance_histo_parcels_' histo_normalization '.txt' ] ;
        fprintf( 1, [ 'Save ' fname '\n' ] ) ;
        dlmwrite( fname,h.Values,'delimiter','\t');
        
        fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_distance_histo_parcels_' histo_normalization '.svg' ] ;
        fprintf( 1, [ 'Save ' fname '\n' ] ) ;
        saveas( H, fname ) ;
        fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_distance_histo_parcels_' histo_normalization '.png' ] ;
        fprintf( 1, [ 'Save ' fname '\n' ] ) ;
        saveas( H, fname ) ;
    end       
    
    
    
    
    %% plot synaptic Te and Ti
    if strcmp( histo_normalization, 'count' )
        if symmetric
            ymax_Te_Ti = 160 ;
        else
            ymax_Te_Ti = 30 ;
        end
    else
        ymax_Te_Ti = 0.5 ;
    end
    
    H = figure( 'Position', [1910 1060 304 304] ) ;
%     h = histogram( median_synapticTe_parcels(~isnan(median_synapticTe_parcels)), 0:1:20, 'Normalization', histo_normalization ) ; 
    data = mean( median_synapticTe_parcels, 'omitnan' ) ;
    h = histogram( data(~isnan(data)), 0:1:20, 'Normalization', histo_normalization ) ; 
    
    h.FaceColor = [0.5 0.5 0.5] ;
    xlabel( 'synaptic excitatory delays (ms)') ; xlim([0 15]) ; % xlim([0 conduction_delay_max]);
    ylabel( '#parcels' ) ; ylim( [ 0 ymax_Te_Ti]) ;
%     title( [ 'using parcels median: ' strrep(id_txt,'_',' ') ] ) ;
    title( [ 'sym' num2str(symmetric) ' ' num2str(age_min) ' ' num2str(age_max) ] );
    
%     fprintf( 1, [ 'Median synaptic excitatory delays: ' num2str(median(median_synapticTe_parcels(:),'omitnan')) 'ms\n' ] ) ;
    summary_txt = [ summary_txt 'Median synaptic excitatory delays: ' num2str( median(median_synapticTe_parcels(:),'omitnan')) 'ms\n' ] ;
        
    if saveFigures
        fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_TE_histo_parcels_' histo_normalization '.txt' ] ;
        fprintf( 1, [ 'Save ' fname '\n' ] ) ;
        dlmwrite( fname,h.Values,'delimiter','\t');
        
        fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_TE_histo_parcels_' histo_normalization '.svg' ] ;
        fprintf( 1, [ 'Save ' fname '\n' ] ) ;
        saveas( H, fname ) ;
        fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_TE_histo_parcels_' histo_normalization '.png' ] ;
        fprintf( 1, [ 'Save ' fname '\n' ] ) ;
        saveas( H, fname ) ;
    end    
    
    
    H = figure( 'Position',  [2216 1060 304 304] ) ;
%     h = histogram( median_synapticTi_parcels(~isnan(median_synapticTi_parcels)), 0:1:20, 'Normalization', histo_normalization ) ; 
    data = mean( median_synapticTi_parcels, 'omitnan' ) ;
    h = histogram( data(~isnan(data)), 0:1:20, 'Normalization', histo_normalization ) ;
    
    h.FaceColor = [0.5 0.5 0.5] ;
    xlabel( 'synaptic inhibitory delays (ms)') ; xlim([0 15]) ; % xlim([0 conduction_delay_max]);
    ylabel( '#parcels' ) ; ylim( [ 0 ymax_Te_Ti]) ;
%     title( [ 'using parcels median: ' strrep(id_txt,'_',' ') ] ) ;
    title( [ 'sym' num2str(symmetric) ' ' num2str(age_min) ' ' num2str(age_max) ] );
    
%     fprintf( 1, [ 'Median synaptic inhibitory delays: ' num2str(median(median_synapticTi_parcels(:),'omitnan')) 'ms\n' ] ) ;
    summary_txt = [ summary_txt 'Median synaptic inhibitory delays: ' num2str(median(median_synapticTi_parcels(:),'omitnan')) 'ms\n' ] ;
        
    if saveFigures
        fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_TI_histo_parcels_' histo_normalization '.txt' ] ;
        fprintf( 1, [ 'Save ' fname '\n' ] ) ;
        dlmwrite( fname,h.Values,'delimiter','\t');
        
        fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_TI_histo_parcels_' histo_normalization '.svg' ] ;
        fprintf( 1, [ 'Save ' fname '\n' ] ) ;
        saveas( H, fname ) ;
        fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_TI_histo_parcels_' histo_normalization '.png' ] ;
        fprintf( 1, [ 'Save ' fname '\n' ] ) ;
        saveas( H, fname ) ;
    end    
    
    fprintf( 1, [ '\n' 'Summary:\n' summary_txt ] ) ;
    
    fname = [ outputDir_fig '/' strrep(id_txt,' ','_') '_summary.txt' ] ;
    fd = fopen( fname, 'wt' ) ;
    fprintf( fd, summary_txt ) ;
    fclose(fd);
    
    fprintf( 1, [ '\n' 'All mat files saved in ' outputDir_mat '\n' ] ) ;
    fprintf( 1, [ '\n' 'All figures and summary saved in ' outputDir_fig '\n' ] ) ;
    
end
    


return 



plot_synapticTe = true ;
if plot_synapticTe
   
    % cceps_th = 3 ;
    cceps_th = 5 ;
    median_synapticTe_parcels    = cellfun( @(x) median(full(x)), synapticTe_parcels ) ;
    count_synapticTe_parcels     = cellfun( @(x) length(x), synapticTe_parcels ) ;
    median_synapticTe_parcels( count_synapticTe_parcels < cceps_th ) = nan ;

    figure( 'Position', [207 788 1140 592] ) ;
    subplot( 2,3,1) ;
    imagesc(median_synapticTe_parcels) ;
    if symmetric
        xlabel( 'rec ipsi /contra' ) ;
        ylabel( 'stim' ) ;
    else
        xlabel( 'rec L/R' ) ;
        ylabel( 'stim L/R' )
    end
    colorbar ; % caxis([0 12]);  colorbar
   
    subplot(2,3,2);
    histogram(median_synapticTe_parcels, 0:.5:max(median_synapticTe_parcels(:))) ; 
    xlabel( 'posterior') ; ylabel( '#cceps' )
    title('using parcels') ;
    
    subplot(2,3,3);
    histogram(synapticTe, 0:.5:max(synapticTe)) ; 
    xlabel( 'posterior') ; ylabel( '#cceps' )
    title('using all') ;
    
    
    % concat Te through all stim for each rec parcel
    recParcels_count        = size(synapticTe_parcels,2) ;
    synapticTe_recParcels   = cell(1,recParcels_count) ;
    for r=1:recParcels_count
        synapticTe_recParcels{1,r} = horzcat(synapticTe_parcels{:,r});
    end
    subplot(2,3,4);
    median_synapticTe_recParcels = cellfun( @(x) median(full(x)), synapticTe_recParcels ) ;
    count_synapticTe_recParcels  = cellfun( @(x) length(x), synapticTe_recParcels ) ;
    cceps_th = 50 ;
    median_synapticTe_recParcels( count_synapticTe_recParcels < cceps_th ) = nan ;
    bar(median_synapticTe_recParcels);
    
    suptitle( [ 'synapticTe - #ccep >= ' num2str(cceps_th) ] )
    
    
    % correlation between synaptic Te and energy (charge level)
    figure
    scatter( energy, synapticTe ) ;
    xlabel('energy'); ylabel('synapticTe');
    
    [c,ia,ic] = unique(energy) ;
    synapticTe_energy = arrayfun( @(i) synapticTe(find(ic==i)), 1:length(c), 'UniformOutput', false ) ;
    
    
    median_synapticTe_energy = cellfun( @(x) median(x), synapticTe_energy ) ;
    figure ;
    plot( c,median_synapticTe_energy ) ;
    
    i_1000 = find(1000<=c & c<=1050) ;
    i_2000 = find(2000<=c & c<=2100) ;
    i_3000 = find(3000<=c & c<=3150) ;
    i_4000 = find(4200<=c & c<=4200) ;
    
    median_synapticTe_energy_1000 = median(horzcat(synapticTe_energy{i_1000})) ;
    median_synapticTe_energy_2000 = median(horzcat(synapticTe_energy{i_2000})) ;
    median_synapticTe_energy_3000 = median(horzcat(synapticTe_energy{i_3000})) ;
    median_synapticTe_energy_4000 = median(horzcat(synapticTe_energy{i_4000})) ;
    
    
    figure ;
    scatter( dist_stim_rec, synapticTe ) ;
    
    
    
    %% stim and rec in the same parcels (or at least close enough : < 40)
    % number of recs
    rec_count = cellfun( @(s) length(s), synapticTe_parcels ) ;
    figure
    imagesc(rec_count); colorbar ; caxis([0 100])
    title( '#cceps in each parcel' )
    
    
    %% pair with maximum cceps
    % BA48 - 1120
    [y,i]=max(rec_count(:));
    [s,r]=ind2sub(size(rec_count),i);
    energy_parcels{s,r}
    
    s = 48 ; r = 48 ;
%     figure ; histogram(energy_parcels{s,r})
    i_3000 = find(3000<=energy_parcels{s,r} & energy_parcels{s,r}<=3150) ;
%     median(conduction_delays_parcels{s,r}(i_3000))
%     median(synapticTe_parcels{s,r}(i_3000))
%     median(distance_parcels{s,r}(i_3000))
    conduction_delays_parcels_BA48  = conduction_delays_parcels{s,r}(i_3000) ;
    synapticTe_parcels_BA48         = synapticTe_parcels{s,r}(i_3000) ;
    distance_parcels_BA48           = distance_parcels{s,r}(i_3000) ;
    
    
    % BA20  - 575
    s = 20 ; r = 20 ;
%     figure ; histogram(energy_parcels{s,r})
    i_3000 = find(3000<=energy_parcels{s,r} & energy_parcels{s,r}<=3150) ;
%     median(conduction_delays_parcels{s,r}(i_3000))
%     median(synapticTe_parcels{s,r}(i_3000))
%     median(distance_parcels{s,r}(i_3000))
    conduction_delays_parcels_BA20  = conduction_delays_parcels{s,r}(i_3000) ;
    synapticTe_parcels_BA20         = synapticTe_parcels{s,r}(i_3000) ;
    distance_parcels_BA20           = distance_parcels{s,r}(i_3000) ;
    
    
    % BA6 - 566
    s = 6 ; r = 6 ;
%     figure ; histogram(energy_parcels{s,r})
    i_3000 = find(3000<=energy_parcels{s,r} & energy_parcels{s,r}<=3150) ;
%     median(conduction_delays_parcels{s,r}(i_3000))
%     median(synapticTe_parcels{s,r}(i_3000))
%     median(distance_parcels{s,r}(i_3000))
    conduction_delays_parcels_BA6  = conduction_delays_parcels{s,r}(i_3000) ;
    synapticTe_parcels_BA6         = synapticTe_parcels{s,r}(i_3000) ;
    distance_parcels_BA6           = distance_parcels{s,r}(i_3000) ;
    
    
    
    
    mean(distance_parcels_BA48)
    mean(distance_parcels_BA20)
    mean(distance_parcels_BA6)
    median(distance_parcels_BA48)
    median(distance_parcels_BA20)
    median(distance_parcels_BA6)
    [h,p]=ttest2(distance_parcels_BA48,distance_parcels_BA20)
    [h,p]=ttest2(distance_parcels_BA48,distance_parcels_BA6)
    [h,p]=ttest2(distance_parcels_BA20,distance_parcels_BA6)
    
    mean(conduction_delays_parcels_BA48)
    mean(conduction_delays_parcels_BA20)
    mean(conduction_delays_parcels_BA6)
    [h,p]=ttest2(conduction_delays_parcels_BA48,conduction_delays_parcels_BA20)
    [h,p]=ttest2(conduction_delays_parcels_BA48,conduction_delays_parcels_BA6)
    [h,p]=ttest2(conduction_delays_parcels_BA20,conduction_delays_parcels_BA6)
    
    figure ; 
    subplot(1,2,1);
    histogram(conduction_delays_parcels_BA20,0:1:20) ;
    subplot(1,2,2);
    histogram(conduction_delays_parcels_BA6,0:1:20) ;
    
    
    mean(synapticTe_parcels_BA48)
    mean(synapticTe_parcels_BA20)
    mean(synapticTe_parcels_BA6)
    median(synapticTe_parcels_BA48)
    median(synapticTe_parcels_BA20)
    median(synapticTe_parcels_BA6)
    [h,p]=ttest2(synapticTe_parcels_BA48,synapticTe_parcels_BA20)
    [h,p]=ttest2(synapticTe_parcels_BA48,synapticTe_parcels_BA6)
    [h,p]=ttest2(synapticTe_parcels_BA20,synapticTe_parcels_BA6)
    
    
    
    
    
    figure ;
    subplot(1,2,1) ; scatter( distance_parcels_BA6, conduction_delays_parcels_BA6 ) ;
    subplot(1,2,2) ; scatter( distance_parcels_BA6, synapticTe_parcels_BA6 ) ;
    
    
    
    median_dist = cellfun( @(x) median(full(x)), distance_parcels ) ;
    median_dist( median_dist > 40 ) = nan ;
    figure
    imagesc(median_dist); colorbar
    title( 'median distance between parcel' )
    
    
    
    
%     stim_rec_close_i = find( dist_stim_rec < 40 ) ;
%     figure ;
%     scatter( dist_stim_rec(stim_rec_close_i), conductionDelay(stim_rec_close_i) ) ;
    

    
end


return 

%% correlation L/R
if ~symmetric
    figure ;
    left    = median_conduction_delays_parcels(1:parcelsCount/2,1:parcelsCount/2) ;
    right   = median_conduction_delays_parcels(parcelsCount/2+1:end,parcelsCount/2+1:end) ;
    scatter( left(:), right(:) ) ;
    xlabel('left'); ylabel('right');
    title('correlation left/right');
end



%%
% + CORRELATION conduction delay versus peak latency




%% correlation conduction delay versus distance
median_dist_stim_rec_parcels    = cellfun( @(x) median(full(x)), distance_parcels ) ;
median_dist_stim_rec_parcels( count_conduction_delays_parcels < cceps_th ) = nan ;

plot_corr = 1 ;
if plot_corr
    figure
    scatter( median_dist_stim_rec_parcels(:), median_conduction_delays_parcels(:) ) ;
    xlabel('distance') ; ylabel('conduction delay') ;
end



%%





dcmCount = length(dcms);



%% stats on stim parcels sorted wrt the number of stimulations
[c,ia,ic]   = unique( stimParcelNames ) ;
tmp         = arrayfun( @(p) length(find(ic==p)), 1:length(c) ) ;
[y,i]       = sort(tmp,'descend') ;
sorted_stimParcelNames = c(i);
sorted_stimParcelStims = tmp(i);
for p=1:length(sorted_stimParcelNames)
    fprintf( 1, [ sorted_stimParcelNames{p} '\t' num2str(sorted_stimParcelStims(p)) '\n' ] ) ;
end

% same as before symmetrical
tmp = stimParcelNames ;
if strcmp( atlasId, 'AALDilate' )
    tmp = cellfun( @(s) s(1:end-2), tmp, 'UniformOutput', 0 ) ;
    [c,ia,ic]   = unique( tmp ) ;
    tmp         = arrayfun( @(p) length(find(ic==p)), 1:length(c) ) ;
    [y,i]       = sort(tmp,'descend') ;
    sorted_stimParcelNames = c(i);
    sorted_stimParcelStims = tmp(i);
    for p=1:length(sorted_stimParcelNames)
        fprintf( 1, [ sorted_stimParcelNames{p} '\t' num2str(sorted_stimParcelStims(p)) '\n' ] ) ;
    end
end

% stats on rec parcels
% [c,ia,ic] = unique( ccepParcelNames ) ;


%%
plot_acc_histo = 0 ;
if plot_acc_histo
    edges = -5:5:105 ;
    H = figure( 'Position', [677 613 1470 867] ) ;
    subplot( 2,4,1 ); histogram(accuracy_0_25ms_R2,edges); title(strrep('accuracy_0_25ms_R2','_','\_')); xlim([0 100]);
    subplot( 2,4,2 ); histogram(accuracy_0_50ms_R2,edges); title(strrep('accuracy_0_50ms_R2','_','\_')); xlim([0 100]);
    subplot( 2,4,3 ); histogram(accuracy_50_100ms_R2,edges); title(strrep('accuracy_50_100ms_R2','_','\_')); xlim([0 100]);
    subplot( 2,4,4 ); histogram(accuracy_0_100ms_R2,edges); title(strrep('accuracy_0_100ms_R2','_','\_')); xlim([0 100]);
    subplot( 2,4,5 ); histogram(accuracy_0_25ms_R2_c,edges); title(strrep('accuracy_0_25ms_R2_c','_','\_')); xlim([0 100]);
    subplot( 2,4,6 ); histogram(accuracy_0_50ms_R2_c,edges); title(strrep('accuracy_0_50ms_R2_c','_','\_')); xlim([0 100]);
    subplot( 2,4,7 ); histogram(accuracy_50_100ms_R2_c,edges); title(strrep('accuracy_50_100ms_R2_c','_','\_')); xlim([0 100]);
    subplot( 2,4,8 ); histogram(accuracy_0_100ms_R2_c,edges); title(strrep('accuracy_0_100ms_R2_c','_','\_')); xlim([0 100]);
    suptitle( [ 'gpCount=' num2str(gpCount) ] ) ;
    saveas( H, [ dcmDir '/../screenshots/histo_accuracy_R2.png' ] ) ;
    
    H = figure ;
    subplot(2,1,1); histogram(accuracy_15_50ms_R2,edges); title( 'accuracy 15 50ms R2'); xlim([0 100]);
    subplot(2,1,2); histogram(accuracy_15_50ms_R2_c,edges); title('accuracy 15 50ms R2 c'); xlim([0 100]);
    H = figure ;
    subplot(2,1,1); histogram(accuracy_20_50ms_R2,edges); title( 'accuracy 20 50ms R2'); xlim([0 100]);
    subplot(2,1,2); histogram(accuracy_20_50ms_R2_c,edges); title('accuracy 20 50ms R2 c'); xlim([0 100]);
end


% input_delay
plot_input_delay = 0 ;
if plot_input_delay
    H = figure ; 
    histogram( input_delay ) ; title( 'input delay' )
    saveas( H, [ dcmDir '/../screenshots/histo_input_delay.png' ] ) ;
end


% good = (1:dcmCount);
good = find( ( input_delay > -2 ) & ( input_delay < 2 ) ) ;
% good = find( sum( ( accuracy_0_50ms_R2_c > 80 ) & ( accuracy_50_100ms_R2_c > 80 ), 2 ) > 8 ) ;
% good = find( sum( ( accuracy_0_25ms_R2_c > 80 ) & ( accuracy_0_50ms_R2_c > 80 ) & ( accuracy_50_100ms_R2_c > 80 ), 2 ) > 8 ) ;
% good = find( sum( ( accuracy_0_25ms_R2_c > 70 ) & ( accuracy_0_50ms_R2_c > 70 ) & ( accuracy_50_100ms_R2_c > 70 ), 2 ) > 6 ) ;
% good = find( sum( ( accuracy_0_50ms_R2_c > 90 ) & ( accuracy_50_100ms_R2_c > 90 ), 2 ) > 8 ) ;

plot_good_fit = 1 ;
good_chs = 0 ;
if plot_good_fit
    
    plot_single_fit = 1 ;
    
    delays_good         = [] ;
    synaptic_Te_good    = [] ;
    distances_good      = [] ;

    for g=1:length(good)
        
%         good_ch = (1:gpCount);
%         good_ch = find( ( accuracy_0_25ms_R2_c(good(g),:) > 80 ) & ( accuracy_0_50ms_R2_c(good(g),:) > 80 ) & ( accuracy_50_100ms_R2_c(good(g),:) > 80 ) ) ;
%         good_ch = find( ( accuracy_0_25ms_R2_c(good(g),:) > 70 ) & ( accuracy_0_50ms_R2_c(good(g),:) > 70 ) & ( accuracy_50_100ms_R2_c(good(g),:) > 70 ) ) ;
        

%         good_ch = find( ( accuracy_0_50ms_R2_c(good(g),:) > 80 ) ) ;
%         good_ch = find( ( accuracy_0_25ms_R2_c(good(g),:) > 80 ) & ( accuracy_0_50ms_R2_c(good(g),:) > 80 ) ) ;
%         good_ch = find( ( accuracy_15_50ms_R2_c(good(g),:) > 80 ) ) ;
        good_ch = find( ( accuracy_20_50ms_R2_c(good(g),:) > 80 ) ) ;
        
        
        delays_good         = [ delays_good conduction_delays(good(g),good_ch) ] ;
        synaptic_Te_good    = [ synaptic_Te_good synaptic_delays_Te(good(g),good_ch+1) ] ;
        %     synaptic_Ti_good = [] ;
        distances_good      = [ distances_good distances_stim_rec(good(g),good_ch) ] ;

        
%         stim_parcel_id = find( cellfun( @(x) strcmp(x,), parcelsName ) ) ;
        for c=1:length(good_ch)
            gc              = good_ch(c);
%             rec_parcel_id   = find( cellfun( @(x) strcmp(x,), parcelsName ) ) ;
            
            if isempty(ccepParcelNames{good(g),gc})
               continue 
            end
            good_chs = good_chs + 1 ;
            
            [ stim_parcel_id, rec_parcel_id ] = parcelsId( atlasId, parcelsName, stimParcelNames{good(g)}, ccepParcelNames{good(g),gc}, symmetric ) ;
            
            conduction_delays_parcels{stim_parcel_id,rec_parcel_id}(end+1)     = conduction_delays(good(g),gc) ;
            synaptic_delays_Te_parcels{stim_parcel_id,rec_parcel_id}(end+1)    = synaptic_delays_Te(good(g),gc+1) ;
            distance_parcels{stim_parcel_id,rec_parcel_id}(end+1)              = distances_stim_rec(good(g),gc) ;
        end
        
        
        
        if ~plot_single_fit
            continue
        end
        
        
        if all(conduction_delays(good(g),good_ch) < 14)
            continue
        end
        
        
        
        fprintf( 1, [ 'Stim parcel: ' stimParcelNames{ good(g) } '\n' ] ) ;
        
        DCM = load( dcms{good(g)} ) ;
        DCM = DCM.DCM ;
        
        H1 = figure ;
        spm_dcm_erp_results( DCM, 'Response', H1 ) ;
        
        H2 = figure(  'Position', [482 27 560 835] ) ;
        spm_dcm_erp_results( DCM, 'ERPs (sources)', H2 ) ;
        
        
        xY      = DCM.xY;                   % data
        t       = xY.pst;                   % PST
        U       = DCM.M.U';
        i       = 1;
        tmpO    = (DCM.H{i} + DCM.R{i})*U ;
        tmpP    = (DCM.H{i})*U ;
        
        colors  = get(gca,'defaultAxesColorOrder') ;
        
        H3   = figure( 'Position', [1000 503 1142 995] ) ;
        leg = {} ;
        clf
        hold on ;
        for c=1:length(good_ch)
            gc=good_ch(c) ;
            fprintf( 1, [ '-> ' ccepParcelNames{good(g),gc} '\tR2_0_50ms ' num2str(accuracy_50_100ms_R2_c(good(g),gc)) '\tdelay ' num2str(conduction_delays(good(g),gc)) '\tTe ' num2str(synaptic_delays_Te(good(g),gc+1)) '\tTi ' num2str(synaptic_delays_Ti(good(g),gc+1)) '\tdist ' num2str(distances_stim_rec(good(g),gc)) '\n' ] ) ; % '\t' num2str(accuracy_0_50ms_R2_c(good(g),gc)) '\n' ] ) ;
            plot(t,tmpO(:,gc),'Color',colors(mod(gc-1,size(colors,1))+1,:));
            leg{end+1} = strrep( [ 'ccep' num2str(gc) ' - ' ccepParcelNames{good(g),gc} ], '_', '\_' ) ;
            plot(t,tmpP(:,gc),'Color',colors(mod(gc-1,size(colors,1))+1,:),'LineStyle','--');
            leg{end+1} = strrep( [ 'ccep' num2str(gc) ' - ' ccepParcelNames{good(g),gc} ], '_', '\_' ) ;
        end
        
        legend(leg) ;
        title(strrep(stimParcelNames{good(g)},'_','\_'));
        
        
        saveas( H1, [ dcmDir '/../screenshots/DCM_good' num2str(good(g)) '_fit.png' ] ) ;
        saveas( H2, [ dcmDir '/../screenshots/DCM_good' num2str(good(g)) '_sources.png' ] ) ;
        saveas( H3, [ dcmDir '/../screenshots/DCM_good' num2str(good(g)) '.png' ] ) ;
        
        close( [ H1, H2, H3 ] ) ;
    end
    

    
    
    %% plot results
    
    cceps_th = 5 ;
    
    % conduction delays
    median_conduction_delays_parcels = cellfun( @(x) median(x), conduction_delays_parcels ) ;
    count_conduction_delays_parcels = cellfun( @(x) length(x), conduction_delays_parcels ) ;
    median_conduction_delays_parcels( count_conduction_delays_parcels < cceps_th ) = nan ;
    
    plot_conduction_delays = 1 ;
    if plot_conduction_delays
        %     figure( 'Position', [624 339 1861 1088] ) ;
        figure( 'Position', [207 788 1140 592] ) ;
        imagesc(median_conduction_delays_parcels) ;
        xlabel( 'rec ipsi /contra' ) ;     ylabel( 'stim' ) ;    title( 'conduction delays' ) ; caxis([0 12]);  colorbar
        suptitle( [ '#ccep >= ' num2str(cceps_th) ] )
        
        figure ;
        %     subplot(1,2,1) ; histogram(conduction_delays_parcels) ;
        %     subplot(1,2,2) ; histogram(median_conduction_delays_parcels) ;
        histogram(median_conduction_delays_parcels) ;
        title( 'conduction delays' )
    end
    
    
    % synaptic delays
    median_synaptic_delays_Te_parcels = cellfun( @(x) median(x), synaptic_delays_Te_parcels ) ;
    median_synaptic_delays_Te_parcels( count_conduction_delays_parcels < cceps_th ) = nan ;

    
    plot_synaptic_delays = 0 ;
    if plot_synaptic_delays
        %     figure( 'Position', [624 339 1861 1088] ) ;
        figure( 'Position', [207 788 1140 592] ) ;
        imagesc(median_synaptic_delays_Te_parcels) ;
        xlabel( 'rec ipsi /contra' ) ;     ylabel( 'stim' ) ;    title( 'synaptic delays Te' ) ; colorbar ; caxis([1 6]);
        suptitle( [ '#ccep >= ' num2str(cceps_th) ] )
    end
    
    
    % distance
    median_distance_parcels = cellfun( @(x) median(x), distance_parcels ) ;
    median_distance_parcels( count_conduction_delays_parcels < cceps_th ) = nan ;

    
    
    plot_nb_cceps = 0 ;
    if plot_nb_cceps
        % number of cceps
        %     figure( 'Position', [624 339 1861 1088] ) ;
        figure( 'Position', [1281 788 1143 593] ) ;
        imagesc(count_conduction_delays_parcels) ;
        xlabel( 'rec ipsi /contra' ) ;     ylabel( 'stim' ) ;    title( '#cceps' ) ; caxis([0 40]); colorbar
        suptitle( [ '#ccep >= ' num2str(cceps_th) ] )
    end
    
    
    
    
    %% asymmetry
    plot_asymmetry = 0 ;
    if plot_asymmetry
        
        tmp = median_conduction_delays_parcels(:,1:41);
        tmp = tmp - tmp' ;
        
        figure( 'Position', [624 339 1861 1088] ) ;
        imagesc(abs(tmp)) ;
        xlabel( 'rec ipsi' ) ;     ylabel( 'stim' ) ;    title( 'asymetry conduction delays' ) ;colorbar
        suptitle( [ '#ccep >= ' num2str(cceps_th) ] )
        
        
        %% correlation between median conduction delays and synaptic Te
        tmp1 = median_conduction_delays_parcels(~isnan(median_conduction_delays_parcels)) ;
        tmp2 = median_synaptic_delays_Te_parcels(~isnan(median_synaptic_delays_Te_parcels)) ;
        
    end
    
    
    
      
    
    %% correlations
    plot_corr = 0 ;
    if plot_corr
        
        figure( 'Position', [181 919 1213 472] );
        subplot(1,3,1);
        scatter( delays_good, synaptic_Te_good ) ;
        xlabel('conduction') ; ylabel('syn Te') ;
        subplot(1,3,2);
        scatter( distances_good, synaptic_Te_good ) ;
        xlabel('distance') ; ylabel('syn Te') ;
        subplot(1,3,3);
        scatter( distances_good, delays_good ) ;
        xlabel('distance') ; ylabel('conduction') ;
        suptitle('all cceps')
        
        figure( 'Position', [181 363 1213 475] ) ;
        subplot(1,3,1);
        scatter( median_conduction_delays_parcels(:), median_synaptic_delays_Te_parcels(:) ) ;
        xlabel('conduction') ; ylabel('syn Te') ;
        subplot(1,3,2);
        scatter( median_distance_parcels(:), median_synaptic_delays_Te_parcels(:) ) ;
        xlabel('distance') ; ylabel('syn Te') ;
        subplot(1,3,3);
        scatter( median_distance_parcels(:), median_conduction_delays_parcels(:) ) ;
        xlabel('distance') ; ylabel('conduction') ;
        suptitle('parcel median')
        
        
        figure( 'Position', [1396 919 791 472] ) ;
        subplot(1,3,1);
        histogram(delays_good)
        title('delays good')
        subplot(1,3,2);
        histogram(synaptic_Te_good)
        title('synaptic Te good')
        subplot(1,3,3);
        histogram(distances_good)
        title('distances good')
        suptitle('all cceps')
        
        figure( 'Position', [1396 363 792 475] ) ;
        subplot(1,3,1);
        histogram(median_conduction_delays_parcels(:))
        title('delays good')
        subplot(1,3,2);
        histogram(median_synaptic_delays_Te_parcels(:))
        title('synaptic Te good')
        subplot(1,3,3);
        histogram(median_distance_parcels(:))
        title('distances good')
        suptitle('parcel median')
        
    end
    

end


end





function [ stimId, recId ] = parcelsId( atlasId, parcelsName, stimParcel, recParcel, symmetric )

if ~symmetric
    stimId  = find( cellfun( @(x) strcmp(x,stimParcel), parcelsName ) ) ;
    recId   = find( cellfun( @(x) strcmp(x,recParcel), parcelsName ) ) ;
else
    if strcmp( atlasId, 'AALDilate' )
        parcelsName_sym = unique( cellfun( @(s) s(1:end-2), parcelsName, 'UniformOutput', false ), 'stable' ) ;
        stimId          = find( cellfun( @(x) strcmp(x,stimParcel(1:end-2)), parcelsName_sym ) ) ;
        recId           = find( cellfun( @(x) strcmp(x,recParcel(1:end-2)), parcelsName_sym ) ) ;
        if stimParcel(end) ~= recParcel(end), recId = recId + length(parcelsName_sym); end
    
    elseif strcmp( atlasId, 'MarsAtlas' )
    
        parcelsName_sym = unique( cellfun( @(s) s(3:end), parcelsName, 'UniformOutput', false ), 'stable' ) ;
        stimId          = find( cellfun( @(x) strcmp(x,stimParcel(3:end)), parcelsName_sym ) ) ;
        recId           = find( cellfun( @(x) strcmp(x,recParcel(3:end)), parcelsName_sym ) ) ;
        if stimParcel(1) ~= recParcel(1), recId = recId + length(parcelsName_sym); end
    
    else
        error('atlas not implemented')
    end
    
end

end






function [ data_parcels, data_all ] = atlasParcels( atlasId, atlasDCMs, symmetric, age_min, age_max, s1Selection, s2Selection )


parcelsName                     = getParcelsName( atlasId, symmetric ) ;
parcelsCount                    = length(parcelsName) ;

if symmetric
    sigCCEPs_parcels            = cell(parcelsCount/2,parcelsCount) ;
    peakLatency_parcels         = cell(parcelsCount/2,parcelsCount) ;
    conduction_delays_parcels   = cell(parcelsCount/2,parcelsCount) ;
    synapticTe_parcels          = cell(parcelsCount/2,parcelsCount) ;
    synapticTi_parcels          = cell(parcelsCount/2,parcelsCount) ;
    distance_parcels            = cell(parcelsCount/2,parcelsCount) ;
    energy_parcels              = cell(parcelsCount/2,parcelsCount) ;
else
    sigCCEPs_parcels            = cell(parcelsCount,parcelsCount) ;
    peakLatency_parcels         = cell(parcelsCount,parcelsCount) ;
    conduction_delays_parcels   = cell(parcelsCount,parcelsCount) ;
    synapticTe_parcels          = cell(parcelsCount,parcelsCount) ;
    synapticTi_parcels          = cell(parcelsCount,parcelsCount) ;
    distance_parcels            = cell(parcelsCount,parcelsCount) ;
    energy_parcels              = cell(parcelsCount,parcelsCount) ;
end

stimParcel      = {} ;
recParcel       = {} ;
conductionDelay = [] ;
synapticTe      = [] ;
synapticTi      = [] ;
energy          = [] ;
peakLatency     = [] ;
peakOnset       = [] ;
peakAmpl        = [] ;
peakSlope       = [] ;
peakDuration    = [] ;
peakIntegral    = [] ;
peakLatStart    = [] ;
dist_stim_rec   = [] ;

minPL       	= s1Selection.minPL ;
maxPL        	= s1Selection.maxPL ;


for s=1:length(atlasDCMs)
%    s
    stimDCMs = atlasDCMs{s} ;
    for d=1:length(stimDCMs)

       if isempty(stimDCMs{d}), continue ; end
       if ~ ( minPL <= stimDCMs{d}.peakLatency && stimDCMs{d}.peakLatency <= maxPL ), continue ; end
       if ~ ( age_min <= stimDCMs{d}.age && stimDCMs{d}.age <= age_max ), continue ; end
       
       
       [ stim_parcel_id, rec_parcel_id ]                        = parcelsId( atlasId, parcelsName, stimDCMs{d}.stimParcelName, stimDCMs{d}.ccepParcelName, symmetric ) ;
      
       
       sigCCEPs_parcels       {stim_parcel_id,rec_parcel_id}(end+1) 	= stimDCMs{d}.peakLatency ;
       
       
       if ~ ( stimDCMs{d}.acc > s2Selection.R2 ), continue ; end
       if ~ ( abs( stimDCMs{d}.peak - stimDCMs{d}.peakLatency ) < s2Selection.MPD ), continue ; end
       

       peakLatency_parcels       {stim_parcel_id,rec_parcel_id}(end+1) 	= stimDCMs{d}.peakLatency ;
       conduction_delays_parcels {stim_parcel_id,rec_parcel_id}(end+1) 	= stimDCMs{d}.conductionDelay ;
       synapticTe_parcels        {stim_parcel_id,rec_parcel_id}(end+1) 	= stimDCMs{d}.synapticTe ;
       synapticTi_parcels        {stim_parcel_id,rec_parcel_id}(end+1)	= stimDCMs{d}.synapticTi ;
       distance_parcels          {stim_parcel_id,rec_parcel_id}(end+1) 	= stimDCMs{d}.dist_stim_rec ;
       energy_parcels            {stim_parcel_id,rec_parcel_id}(end+1) 	= str2num( stimDCMs{d}.energy );
              
       stimParcel       {end+1}     = stimDCMs{d}.stimParcelName ;
       recParcel        {end+1}   	= stimDCMs{d}.ccepParcelName ;
       conductionDelay  (end+1)     = stimDCMs{d}.conductionDelay ;
       synapticTe       (end+1)  	= stimDCMs{d}.synapticTe ;
       synapticTi       (end+1)  	= stimDCMs{d}.synapticTi ;
       energy           (end+1)  	= str2num( stimDCMs{d}.energy );
       peakLatency      (end+1)   	= stimDCMs{d}.peakLatency ;
       peakOnset        (end+1)  	= stimDCMs{d}.peakOnset ;
       peakAmpl         (end+1)  	= stimDCMs{d}.peakAmpl ;
       peakSlope        (end+1)   	= stimDCMs{d}.peakSlope ;
       peakDuration     (end+1)   	= stimDCMs{d}.peakDuration ;
       peakIntegral     (end+1)   	= stimDCMs{d}.peakIntegral ;
       peakLatStart     (end+1)  	= stimDCMs{d}.peakLatStart ;
       dist_stim_rec    (end+1)     = stimDCMs{d}.dist_stim_rec ;
       
    end
    
end

data_parcels                            = struct ;
data_parcels.sigCCEPs_parcels           = sigCCEPs_parcels ;
data_parcels.peakLatency_parcels        = peakLatency_parcels ;
data_parcels.conduction_delays_parcels  = conduction_delays_parcels ;
data_parcels.synapticTe_parcels         = synapticTe_parcels ;
data_parcels.synapticTi_parcels         = synapticTi_parcels ;
data_parcels.distance_parcels           = distance_parcels ;
data_parcels.energy_parcels             = energy_parcels ;

data_all                                = struct ;
data_all.stimParcel                     = stimParcel ;
data_all.recParcel                      = recParcel ;
data_all.conductionDelay                = conductionDelay ;
data_all.synapticTe                     = synapticTe ;
data_all.synapticTi                     = synapticTi ;
data_all.energy                         = energy ;
data_all.peakLatency                    = peakLatency ;
data_all.peakOnset                      = peakOnset ;
data_all.peakAmpl                       = peakAmpl ;
data_all.peakSlope                      = peakSlope ;
data_all.peakDuration                   = peakDuration ;
data_all.peakIntegral                   = peakIntegral ;
data_all.peakLatStart                   = peakLatStart ;
data_all.dist_stim_rec                  = dist_stim_rec ;

end





function parcelsName = getParcelsName( atlasId, symmetric )

[ parcelsName, ~ ]  = atlasParcels_NameTexture(atlasId) ;
% reorganization of parcelNames
if strcmp( atlasId, 'AALDilate' )
    parcelsName=parcelsName( ( cellfun( @(x) ...
        isempty(strfind(x,'Caudate')) & ...
        isempty(strfind(x,'Putamen')) & ...
        isempty(strfind(x,'Pallidum')) & ...
        isempty(strfind(x,'Thalamus')) & ...
        isempty(strfind(x,'Cerebelum')) & ...
        isempty(strfind(x,'Vermis')), parcelsName ) ) );
    % check all names ends with _L or _R
    if ~all( arrayfun( @(s) endsWith(s,'_L') || endsWith(s,'_R'), parcelsName ) ), error( 'invalid parcel names' ); end
    parcelsName_sym = unique( cellfun( @(s) s(1:end-2), parcelsName, 'UniformOutput', false ), 'stable' ) ;
    parcelsName     = [ cellfun( @(s) [ s '_L' ], parcelsName_sym, 'UniformOutput', false ) cellfun( @(s) [ s '_R' ], parcelsName_sym, 'UniformOutput', false ) ] ;

elseif strcmp( atlasId, 'MarsAtlas' )

    % check all names starts with L_ or R_
    if ~all( arrayfun( @(s) startsWith(s,'L_') || startsWith(s,'R_'), parcelsName ) ), error( 'invalid parcel names' ); end
    % and that L and R are well ordered
    tmpL        = arrayfun( @(s) startsWith(s,'L_'), parcelsName ) ;
    parcelsL    = parcelsName( tmpL ) ;
    parcelsL    = cellfun( @(s) s(3:end), parcelsL, 'UniformOutput', false ) ;
    tmpR        = arrayfun( @(s) startsWith(s,'R_'), parcelsName ) ;
    parcelsR    = parcelsName( tmpR ) ;
    parcelsR    = cellfun( @(s) s(3:end), parcelsR, 'UniformOutput', false ) ;
    if ~isequal( parcelsL, parcelsR ), error( 'invalid parcel names' ); end
    tmp = [ cellfun( @(s) [ 'L_' s ], parcelsL, 'UniformOutput', false ) cellfun( @(s) [ 'R_' s ], parcelsR, 'UniformOutput', false ) ] ;
    if ~isequal( parcelsName, tmp ), error( 'invalid parcel names' ); end

elseif strcmp( atlasId, 'Brodmann' )
%     parcelsName_sym
end

end










function atlasDCMs = loadAtlasDCMs( atlasId, s1Selection_config )

V                   = 'V14' ;
sequence_id         = 'ArtefactCorrectionPCHIP' ; 
[ ~, s1Selection_txt ]         = s1Selection_config_V14( s1Selection_config ) ;
fitID               = [ V '/' s1Selection_txt ] ;

[ ~, dcmDir, ~ ]    = simuDirs() ;
outputDir           = [ dcmDir '/PATS__' sequence_id '/ALL/2steps/' fitID '/' atlasId ] ;

fname = [ outputDir '/' atlasId '_DCMs_' V '_' s1Selection_txt '.mat' ] ;
fprintf( 1, [ 'Loading ' fname '\n' ] ) ;
atlasDCMs       = load(fname) ;
atlasDCMs       = atlasDCMs.atlasDCMs ;

end





