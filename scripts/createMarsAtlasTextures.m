function createMarsAtlasTextures ( age_min, age_max, symmetric )
%( texture_data, textureOutputDir, textureId, symmetric )


atlasId     = 'MarsAtlas' ;
% age_min     = 15 ;
% age_max     = 100 ;
% symmetric   = false ;


bd          = baseDir() ;
fname       = fullfile( bd, [ 'dcm/' atlasId '_' num2str(age_min) '-' num2str(age_max) '/atlas.mat' ] ) ;
atlas       = load(fname);
atlas       = atlas.atlas ;



% left_MarsAtlasParcels_Texture   = gifti( [ bd '/gii/Gre_2016_MNI1_Lwhite_parcels_marsAtlas.gii' ] );
% right_MarsAtlasParcels_Texture  = gifti( [ bd '/gii/Gre_2016_MNI1_Rwhite_parcels_marsAtlas.gii' ] );
% [ parcelsName, parcelsTexture ] = atlasParcels_NameTexture( atlasId ) ;



% synaptic Te
textureOutputDir = fullfile( fileparts(fname), 'synapticTe' );
system( [ 'mkdir -p ' fileparts(fname) ] );
createTexture( atlas.median_synapticTe_parcels, textureOutputDir, 'synapticTe', symmetric ) ;

% synaptic Ti
textureOutputDir = fullfile( fileparts(fname), 'synapticTi' );
system( [ 'mkdir -p ' fileparts(fname) ] );
createTexture( atlas.median_synapticTi_parcels, textureOutputDir, 'synapticTi', symmetric ) ;


end






function createTexture( texture_data, textureOutputDir, textureId, symmetric )

atlasId     = 'MarsAtlas' ;

if symmetric
    textureOutputDir = [ textureOutputDir '/sym' ] ;
else
    textureOutputDir = [ textureOutputDir '/nosym' ] ;
end
system( [ 'mkdir -p ' textureOutputDir ] ) ;

bd       	= baseDir() ;

left_MarsAtlasParcels_Texture             	= gifti( [ bd '/gii/Gre_2016_MNI1_Lwhite_parcels_marsAtlas.gii' ] );
right_MarsAtlasParcels_Texture            	= gifti( [ bd '/gii/Gre_2016_MNI1_Rwhite_parcels_marsAtlas.gii' ] );
[ parcelsName, parcelsTexture ]             = atlasParcels_NameTexture( atlasId ) ;


for stimParcelId=1:size(texture_data,1)
    
    if strcmp( textureId, 'conductionDelays' )
        fprintf( 1, [ 'Write stimParcel ' parcelsName{1,stimParcelId} '...\n' ] ) ;
    else
        fprintf( 1, [ 'Write ' textureId '...\n' ] ) ;
    end
    
    % stimParcelId to left parcels
    gii_left_cdata = -4 * ones( size( left_MarsAtlasParcels_Texture.cdata ) ) ;
    for ccepParcelId=1:41
%         gii_left_cdata( left_MarsAtlasParcels_Texture.cdata == parcelsTexture(1,ccepParcelId), 1 ) = texture_data( ccepParcelId, stimParcelId ) ;
        gii_left_cdata( left_MarsAtlasParcels_Texture.cdata == parcelsTexture(1,ccepParcelId), 1 ) = texture_data( stimParcelId, ccepParcelId ) ;
    end
    
    if strcmp( textureId, 'conductionDelays' )
        fname = [ textureOutputDir '/' parcelsName{1,stimParcelId} '__' textureId '__left.gii'] ;
    else
        fname = [ textureOutputDir '/' textureId '__left.gii'] ;
    end
    fprintf( 1, [ 'Write ' fname '\n' ] ) ;
    save( gifti(gii_left_cdata), fname, 'Base64Binary' );

    if startsWith( textureId, 'synapticT' ) && symmetric
        break
    end
    
    % stimParcelId to right parcels
    gii_right_cdata = -4 * ones( size( right_MarsAtlasParcels_Texture.cdata ) ) ;
    for ccepParcelId=42:82
%         gii_right_cdata( right_MarsAtlasParcels_Texture.cdata == parcelsTexture(1,ccepParcelId), 1 ) = texture_data( ccepParcelId, stimParcelId ) ;
        gii_right_cdata( right_MarsAtlasParcels_Texture.cdata == parcelsTexture(1,ccepParcelId), 1 ) = texture_data( stimParcelId, ccepParcelId ) ;
    end
    
    if strcmp( textureId, 'conductionDelays' )
        fname = [ textureOutputDir '/' parcelsName{1,stimParcelId} '__' textureId '__right.gii'] ;
    else
        fname = [ textureOutputDir '/' textureId '__right.gii'] ;
    end
    fprintf( 1, [ 'Write ' fname '\n' ] ) ;
    save( gifti(gii_right_cdata), fname, 'Base64Binary' );
    
end


end



