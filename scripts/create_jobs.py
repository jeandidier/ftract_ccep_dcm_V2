import os
import os.path as op
import tempfile
import numpy as np
import pickle



def create_jobs( params ) :
    
    if type(params) is dict :
        pass
    elif type(params) is str :
        params = pickle.load(open(params,'rb'))


    outputdir   = params['output_directory']
    # output directories
    jobs_dir    = op.join(outputdir,'jobs')
    logs_dir    = op.join(outputdir,'logs')

    print( 'Jobs dir :', jobs_dir )
    print( 'Logs dir :', logs_dir )
    
    if not op.exists(jobs_dir): os.mkdir(jobs_dir)
    if not op.exists(logs_dir): os.mkdir(logs_dir)



    # files for execution of jobs
    do_all_local_file   = jobs_dir + '/do_all_local.bash'
    do_qsub_file        = jobs_dir + '/do_qsub.bash'
    do_job_array_file   = jobs_dir + '/do_job_array.bash'

    # write the jobs to be submitted
    fd_do_all_local     = open( do_all_local_file, 'w' ) ;
    fd_do_all_local.write( '# /export/data/opt/CENIR/bin/parallel -j 60 < ' + do_all_local_file + '\n' )


    commands    = params['commands']
    for j,c in enumerate(commands,1) :
        job_file	= jobs_dir + '/j' + str(j) + '.bash'
        log_file    = logs_dir + '/log-X_' + str(j)
        err_file    = logs_dir + '/err-X_' + str(j)
           
        txt = [ '#!/bin/bash',
                        '#SBATCH -m block:block', 
                        '#SBATCH --mail-type=ALL',
                        '#SBATCH -p ' + params['cluster_queue'],
                        '#SBATCH -n ' + str(params['cpus_per_task']), ]
        if 'mem_G' in params :
            txt.append( '#SBATCH --mem=' + str(params['mem_G']) + 'G' )
            
        txt.append('\n'+c)

        # write the job
        fd_j = open( job_file, 'w' )
        fd_j.write( '\n'.join( txt ) )
        fd_j.close()
        command = command = ' '.join( [ 'chmod', '+x',  job_file ] )
        #print( command )
        os.system( command )

        # add the job in do_all_local_file
        fd_do_all_local.write( 'bash ' + job_file + ' > ' + log_file + ' 2> ' + err_file + '\n' )


    # do_all_local.sh 
    fd_do_all_local.close()
    command = ' '.join( [ 'chmod', '+x', do_all_local_file ] )
    #print( command )
    os.system( command )


    # write number of jobs
    jobs_count_file = jobs_dir + '/jobs_count.txt'
    fd = open( jobs_count_file, 'w' )
    fd.write( str(len(commands)) + '\n' )
    fd.close()


    # do_qsub.sh
    fd = open( do_qsub_file, 'w' )
    # fprintf( fd, [ 'export jobid=`sbatch -p ' cluster_queue ' --qos=' cluster_queue ' -N 1 --cpus-per-task=' num2str(cpus_per_task) ] ) ;
    # no more --qos option...
    fd.write( 'export jobid=`sbatch -p ' + params['cluster_queue'] )

    # walltime required
    fd.write( ' -t ' + params['walltime'] )
    #
    fd.write( ' -N 1 --cpus-per-task=' + str(params['cpus_per_task']) )


    #if recent_nodes
    #    fprintf( fd, [ ' -w node[51-61]' ] ) ;
    #end
    if 'mem_G' in params :
        fd.write( ' --mem=' + str(params['mem_G']) +'G')
    
    fd.write( ' --job-name=' + params['job_name'] + ' -o ' + logs_dir + '/log-%A_%a  -e ' + logs_dir + '/err-%A_%a --array=1-' + str(len(commands)) )

    #if simultaneous_tasks
    #    fprintf( fd, [ '%%' num2str(simultaneous_tasks) ] ) ;
    #end

    fd.write( " " + do_job_array_file + " |awk '{print $4}'` \n" )

    fd.write( 'echo submitted job $jobid\n' )
    fd.close()

    os.system( 'chmod +x ' + do_qsub_file )


    # do_job_array
    fd = open( do_job_array_file, 'w' )
    fd.write( "#!/bin/bash\n\n" )
    fd.write( "echo started on $HOSTNAME\n" )
    fd.write( "date\n" )
    fd.write( "tic=\"$(date +%s)\"\n" )
    #% fprintf( fd, [ 'cmd=$( printf "j%%d_' jobname '.bash" ${SLURM_ARRAY_TASK_ID})\n' ] );
    fd.write( "cmd=$( printf \"j%d" ".bash\" ${SLURM_ARRAY_TASK_ID})\n" )
    fd.write( "bash " + jobs_dir + "/$cmd\n" )
    fd.write( "toc=\"$(date +%s)\";\n" )
    fd.write( "sec=\"$(expr $toc - $tic)\";\n" )
    fd.write( "min=\"$(expr $sec / 60)\";\n" )
    fd.write( "heu=\"$(expr $sec / 3600)\";\n" )
    fd.write( "echo Elapsed time: $min min $heu H \n" )
    fd.close()
    os.system( 'chmod +x ' + do_job_array_file )

    print( len(commands), 'jobs created in', jobs_dir , '\n' )








