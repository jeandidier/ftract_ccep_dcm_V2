import os.path as op
import glob
import numpy as np


jdirs = [ 
'/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/scripts/jobs/BIC/tmpkn8mx1nk', 
'/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/scripts/jobs/BUC/tmptfa87444', 
'/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/scripts/jobs/GRE/tmpzccywavu', 
'/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/scripts/jobs/LIL/tmp6dqv_r6y', 
'/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/scripts/jobs/LYO/tmpv963craa', 
'/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/scripts/jobs/NAN/tmprtwuu7xa', 
'/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2/scripts/jobs/ROT/tmpki_qoi11', 
]


def missing_jobs_all() :
    ja=0
    jm=0
    for jdir in jdirs :
        print(jdir)
        (jcount,js)= missing_jobs(jdir)
        ja+=jcount
        jm+=len(js)
    print('all',ja,'missing',jm)


def missing_jobs( jdir ) :

    fd = open(op.join(jdir,'jobs','jobs_count.txt'),'r')    
    jobs_count = fd.readlines()
    fd.close()
    jcount = int(jobs_count[0].strip())

    js=[]
    for j in np.arange(1,jcount+1) :
        log_file = glob.glob(op.join(jdir,'logs','log-*_'+str(j)))
        err_file = glob.glob(op.join(jdir,'logs','err-*_'+str(j)))
        if not ( log_file and err_file ) :
            js.append(j)

    #print(js)
    print('Missing jobs #',len(js),'/',jcount)

    return jcount,js
