import os
import os.path as op
from collections import OrderedDict


baseDir     = '/network/lustre/iss01/cenir/analyse/meeg/00_jd/NEW/these/FTRACT/ftract_ccep_dcm_V2'


#def get_pats( center ) :
#    path    = op.join(baseDir,'data','PATS__V1')
#    pats    = os.listdir(path)
#    pats    = [ p for p in pats if center in p ]
#    return pats


def get_crfs_on_disk( center = None, write = False ) :

    crfs    = dict()
    path    = op.join(baseDir,'data','PATS__V1')
    pats    = os.listdir(path)
    if center :
        pats    = [ p for p in pats if center in p ]
    for p in pats :
        path    = op.join(baseDir,'data','PATS__V1',p,'SEEG')
        crfs[p] = os.listdir(path)

    if write :
        tmp = 'crfs_exported2icm_'
        if center : tmp += center
        else      : tmp += 'ALL'
        fname = op.join(baseDir,'data',tmp+'.txt')
        print('Write',fname)
        fd = open(fname,'w')
        for p_cs in sorted(crfs.items()) :
            for cs in p_cs[1] :
                fd.write( ' '.join( [ p_cs[0], cs ] ) + '\n' )
        fd.close()
    return crfs



# check that new crfs are not yet present
# to be done before rsync
def check_new_crfs( center ) :

    new_crfs = []
    path = op.join(baseDir,'data','PATS__'+center)
    pats = os.listdir(path)
    for p in pats :
        p_path = op.join(path,p,'SEEG')
        for crf in os.listdir(p_path) :
            crf_path = op.join(baseDir,'data','PATS__V1',p,'SEEG',crf)
            assert not op.exists( crf_path )
            new_crfs.append( crf_path )

    return new_crfs



# 'running'
# 'ok'
# 'nok'
# crf is 0014ROT/SEEG/StimLF_2013-03-01
def get_crf_status( crf ) :
    pass



def stim_dcm_status( stim_dcm_path ) :
    """
    Try to infer the status of a fit
    """

    fname       = op.join( stim_dcm_path, 'cceps_significant.txt' )
    if not op.exists(fname) : return 'UNKNOWN Missing '+fname
    fd          = open(fname,'r')
    cceps_sig   = fd.readlines()
    fd.close()
    if not cceps_sig :
        return 'OK'

    fname       = op.join( stim_dcm_path, 'cceps_significant_step1.txt' )
    if not op.exists(fname) : return 'UNKNOWN Missing '+fname
    fd          = open(fname,'r')
    cceps_sig_s1   = fd.readlines()
    fd.close()

    fname       = op.join( stim_dcm_path, 'cceps_significant_step2.txt' )
    if not op.exists(fname) : return 'UNKNOWN Missing '+fname
    fd          = open(fname,'r')
    cceps_sig_s2   = fd.readlines()
    fd.close()
       
    stimId = op.basename(stim_dcm_path)

    # assert that cceps_sig == cceps_sig_s1 == cceps_sig_s2
    for c, ccep in enumerate( cceps_sig ) :
        #print(c,ccep)
        fname_s1_mat = op.join(stim_dcm_path,'step1','DCM_averaged_zs_'+stimId+'_'+ccep.strip()+'.mat')
        fname_s2_mat = op.join(stim_dcm_path,'step2','DCM_averaged_zs_'+stimId+'_'+ccep.strip()+'.mat')
        assert ( cceps_sig_s1[c].strip() == fname_s1_mat ) and ( cceps_sig_s2[c].strip() == fname_s2_mat )
        assert op.exists(fname_s1_mat) and op.exists(fname_s2_mat)
        fname_s1_log = op.splitext(fname_s1_mat)[0]+'.log'
        fname_s2_log = op.splitext(fname_s2_mat)[0]+'.log'
        assert op.exists(fname_s1_log) and op.exists(fname_s2_log)
        fd = open(fname_s1_log,'r')
        s1 = fd.readlines()
        fd.close()
        #assert 'convergence' in s1[-2]
        if not 'convergence' in s1[-2] : return 'NOK: no conv in '+fname_s1_log
        fd = open(fname_s2_log,'r')
        s2 = fd.readlines()
        fd.close()
        #assert 'convergence' in s2[-2]
        if not 'convergence' in s2[-2] : return 'NOK: no conv in '+fname_s2_log


    return 'OK'




def stims_dcm_status( center = None ) :
    """
    Returns all stims which status is not OK
    """

    # running/pending => cluster
    # ok/nok => disk
    # attention avec les fichiers de logs du cluster (log* et err*) car ils sont potentiellement temporaires
    #       => si on les efface, il faut quand meme pouvoir retrouver les infos => utiliser uniquement les fichiers pour savoir si OK

    # ok si le fichier cceps_significant.txt existe, les fichiers _step1.txt et _step2.txt aussi avec les memes cceps, les fichiers DCM.mat et DCM.log aussi
    # sinon, running/pending/nok
    
    stims_nok = []
    crfs = get_crfs_on_disk( center = center )
    for pat, pat_crfs in crfs.items() :
        for pat_crf in pat_crfs :
            crf_path = op.join(baseDir,'data','PATS__V1',pat,'SEEG',pat_crf,'SEQ_BM_ACPCHIP_F_ComputeAverage')
            stims = os.listdir(crf_path)
            for stim in stims :
                stim_dcm_path   = op.join(baseDir,'dcm','PATS__V1',pat,'SEEG',pat_crf,'SEQ_BM_ACPCHIP_F_DCM',stim)
                status          = stim_dcm_status( stim_dcm_path )
                if not status == 'OK' :
                    print(stim_dcm_path,'\n','\t>>', status)
                    stims_nok.append( [stim,stim_dcm_path,status] )
    return stims_nok





def check_segmentation( center = None ) :
    recSeg_fnames = []
    crfs = get_crfs_on_disk( center = center )
    for pat, pat_crfs in crfs.items() :
        print(pat)
        for pat_crf in pat_crfs :
            #print(pat_crf)
            crf_path = op.join(baseDir,'data','PATS__V1',pat,'SEEG',pat_crf,'SEQ_BM_ACPCHIP_F_ComputeAverage')
            stims = os.listdir(crf_path)
            for stim in stims :
                stim_path   = op.join(crf_path,stim)
                recSeg_fname    = op.join(stim_path,'recContactSeg.txt')
                if not op.exists(recSeg_fname) : raise Exception('regSeg_fname not found:',recSeg_fname)
                recSeg_fnames.append(recSeg_fname)
    print('Analyzing...')
    for f in recSeg_fnames :
        fd = open( f, 'r' )
        lines = fd.readlines()
        fd.close()
        for l in lines :
            if not l.strip() in [ 'WhiteMatter', 'GreyMatter', 'not in brain matter' ] :
                print( l )


    return recSeg_fnames




