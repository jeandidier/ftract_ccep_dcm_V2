function createAtlasTextures_V14( atlasId, s1Selection_config )

age_limit = 10 ;

symmetric = 0 ;
% createAtlasTextures_V14_( atlasId, symmetric, 0, age_limit, s1Selection_config ) ;
createAtlasTextures_V14_( atlasId, symmetric, age_limit, Inf, s1Selection_config ) ;

symmetric = 1 ;
% createAtlasTextures_V14_( atlasId, symmetric, 0, age_limit, s1Selection_config ) ;
% createAtlasTextures_V14_( atlasId, symmetric, age_limit, Inf, s1Selection_config ) ;

end


function createAtlasTextures_V14_( atlasId, symmetric, age_min, age_max, s1Selection_config )

sequence_id         = 'ArtefactCorrectionPCHIP' ; 

V                   = 'V14' ;


[ ~, s1Selection_txt ] = s1Selection_config_V14( s1Selection_config ) ;
s2Selection     = struct ;
s2Selection.R2  = 70 ;
% s2Selection.R2  = 75 ;
% s2Selection.R2  = 80 ;
s2Selection.MPD = 5 ;
s2Selection_txt = [ 'R2' num2str(s2Selection.R2) '_MPD' num2str(s2Selection.MPD) ] ;


fitID               = [ V '/' s1Selection_txt ] ;

output_tmp          = [ 'PATS__' sequence_id '/ALL/2steps/' fitID '/' atlasId '/' s2Selection_txt '/age_' num2str(age_min) '_' num2str(age_max) ] ;
[ ~, dcmDir, ~ ]    = simuDirs() ;
outputDir_mat       = [ dcmDir '/' output_tmp '/mat' ] ;

outputDir_tex       = [ dcmDir '/' output_tmp '/textures' ] ;
system( [ 'mkdir -p ' outputDir_tex ] ) ;


vname = 'median_conduction_delays_parcels' ;
fname = [ outputDir_mat '/' vname '_sym' num2str(symmetric) '.mat' ] ;
fprintf( 1, [ 'Load ' fname '\n' ] ) ;
S = load( fname ) ;
median_conduction_delays_parcels = S.median_conduction_delays_parcels ;

vname = 'median_synapticTe_parcels' ;
fname = [ outputDir_mat '/' vname '_sym' num2str(symmetric) '.mat' ] ;
fprintf( 1, [ 'Load ' fname '\n' ] ) ;
S = load( fname ) ;
median_synapticTe_parcels = S.median_synapticTe_parcels ;

vname = 'median_synapticTi_parcels' ;
fname = [ outputDir_mat '/' vname '_sym' num2str(symmetric) '.mat' ] ;
fprintf( 1, [ 'Load ' fname '\n' ] ) ;
S = load( fname ) ;
median_synapticTi_parcels = S.median_synapticTi_parcels ;





createTextures = true ;
if createTextures
    
    if ~strcmp(atlasId,'MarsAtlas')
        error('invalid configuration for textures' ) ;
    end
    
    textureOutputDir    = [ outputDir_tex '/conductionDelays' ] ;
    textureId           = 'conductionDelays' ;
    createConductionDelaysTextures( median_conduction_delays_parcels, textureOutputDir, textureId, symmetric ) ;
    
    textureOutputDir    = [ outputDir_tex '/synapticTe' ] ;
    textureId           = 'synapticTe' ;
%     createConductionDelaysTextures( median(median_synapticTe_parcels,'omitnan'), textureOutputDir, textureId, symmetric ) ;
    createConductionDelaysTextures( mean(median_synapticTe_parcels,'omitnan'), textureOutputDir, textureId, symmetric ) ;
    
    textureOutputDir    = [ outputDir_tex '/synapticTi' ] ;
    textureId           = 'synapticTi' ;
%     createConductionDelaysTextures( median(median_synapticTi_parcels,'omitnan'), textureOutputDir, textureId, symmetric ) ;
    createConductionDelaysTextures( mean(median_synapticTi_parcels,'omitnan'), textureOutputDir, textureId, symmetric ) ;
    
end




end