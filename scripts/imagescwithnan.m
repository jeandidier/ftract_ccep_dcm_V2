function [h hcb] = imagescwithnan(a,cm,nanclr,dmin,dmax)
% IMAGESC with NaNs assigning a specific color to NaNs

%# find minimum and maximum
%amin=min(a(:));
%amax=max(a(:));
amin=dmin;
amax=dmax;
%# size of colormap
n = size(cm,1);
%# color step
dmap=(amax-amin)/n;

%# standard imagesc
him = imagesc(a);
%# add nan color to colormap
colormap(gca,[nanclr; cm]);
%# changing color limits
caxis([amin-dmap amax]);
% caxis([amin amax]);
%# place a colorbar
hcb = colorbar;
%# change Y limit for colorbar to avoid showing NaN color
ylim(hcb,[amin amax])

if nargout > 0
    h = him;
end
% 
% Here caxis statement assigns the first color of the color map not to the minimum value amin, but to the amin-dmap. So the first color get assigned specifically to NaNs.
% 
% Try this function with:
% 
% a=peaks;
% a(a < 0.5) = nan;
% imagescwithnan(a,hot,[0 1 1]) %# [0 1 1] is cyan
