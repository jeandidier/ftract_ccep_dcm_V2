function [ parcelsName, parcelsTexture ] = getParcelsName( atlasId, symmetric )

[ parcelsName, parcelsTexture ]  = atlasParcels_NameTexture(atlasId,symmetric) ;


% reorganization of parcelNames
if strcmp( atlasId, 'AALDilate' )
    parcelsName=parcelsName( ( cellfun( @(x) ...
        isempty(strfind(x,'Caudate')) & ...
        isempty(strfind(x,'Putamen')) & ...
        isempty(strfind(x,'Pallidum')) & ...
        isempty(strfind(x,'Thalamus')) & ...
        isempty(strfind(x,'Cerebelum')) & ...
        isempty(strfind(x,'Vermis')), parcelsName ) ) );
    % check all names ends with _L or _R
    if ~all( arrayfun( @(s) endsWith(s,'_L') || endsWith(s,'_R'), parcelsName ) ), error( 'invalid parcel names' ); end
    parcelsName_sym = unique( cellfun( @(s) s(1:end-2), parcelsName, 'UniformOutput', false ), 'stable' ) ;
    parcelsName     = [ cellfun( @(s) [ s '_L' ], parcelsName_sym, 'UniformOutput', false ) cellfun( @(s) [ s '_R' ], parcelsName_sym, 'UniformOutput', false ) ] ;

elseif strcmp( atlasId, 'MarsAtlas' )

    % check all names starts with L_ or R_
    if ~all( arrayfun( @(s) startsWith(s,'L_') || startsWith(s,'R_'), parcelsName ) ), error( 'invalid parcel names' ); end
    % and that L and R are well ordered
    tmpL        = arrayfun( @(s) startsWith(s,'L_'), parcelsName ) ;
    parcelsL    = parcelsName( tmpL ) ;
    parcelsL    = cellfun( @(s) s(3:end), parcelsL, 'UniformOutput', false ) ;
    tmpR        = arrayfun( @(s) startsWith(s,'R_'), parcelsName ) ;
    parcelsR    = parcelsName( tmpR ) ;
    parcelsR    = cellfun( @(s) s(3:end), parcelsR, 'UniformOutput', false ) ;
    if ~isequal( parcelsL, parcelsR ), error( 'invalid parcel names' ); end
    tmp = [ cellfun( @(s) [ 'L_' s ], parcelsL, 'UniformOutput', false ) cellfun( @(s) [ 'R_' s ], parcelsR, 'UniformOutput', false ) ] ;
    if ~isequal( parcelsName, tmp ), error( 'invalid parcel names' ); end

elseif strcmp( atlasId, 'Brodmann' )
%     parcelsName_sym

elseif strcmp( atlasId, 'HCP' ) || contains( atlasId, 'Lausanne2008' )
    
    % check symmetric
    if symmetric && ~strcmp( atlasId, 'Lausanne2008-60' )
        
        lp = parcelsName(1:length(parcelsName)/2);
        rp = parcelsName(length(parcelsName)/2+1:end);
            
        lp = cellfun( @(p) strrep(p,'lh.',''), lp, 'UniformOutput', false ) ;
        lp = cellfun( @(p) strrep(p,'Left-',''), lp, 'UniformOutput', false ) ;
        
        rp = cellfun( @(p) strrep(p,'rh.',''), lp, 'UniformOutput', false ) ;
        rp = cellfun( @(p) strrep(p,'Right-',''), lp, 'UniformOutput', false ) ;
        
        if ~isequal( lp, rp ), error('Invalid symmetric'); end
        
    end
    
    
    
end


end







function [ parcelsName, parcelsTexture ] = atlasParcels_NameTexture( atlasId, symmetric )

bd = baseDir() ;

if strcmp( atlasId, 'MarsAtlas' )
    
    parcels_MA 	= readtable( [ bd '/gii/MarsAtlas_parcels_label_name.txt' ] ) ;
    
    parcelsName_MA      = cellstr( parcels_MA{:,2} )' ;
    parcelsTexture_MA   = parcels_MA{:,1}' ;
    
    parcelsName         = parcelsName_MA ;
    parcelsTexture      = parcelsTexture_MA ;
    
    % occipital     1-5
    % temporal      6-11
    % parietal      12-19
    % cingular      20-23
    % frontal       24-36
    % orbito-front  37-40
    % insula        41
    
    
    
elseif strcmp( atlasId, 'AALDilate' )
    
    % from Lena paper
    AALDilateList_H = { ...
        'Fusiform', 'Lingual', 'Occipital_Inf', 'Occipital_Mid', 'Cuneus',  'Calcarine', 'Occipital_Sup', ...   % occipital 1-7
        'Temporal_Inf', 'Temporal_Pole_Mid', 'Temporal_Mid', 'Temporal_Pole_Sup', 'Heschl','Temporal_Sup',...   % temporal  8-13
        'SupraMarginal', 'Parietal_Inf', 'Parietal_Sup', 'Precuneus', 'Postcentral','Angular', ...              % parietal  14-19
        'Hippocampus', 'ParaHippocampal', 'Cingulum_Post', 'Cingulum_Mid', 'Cingulum_Ant',...                 	% cingular  20-24
        'Precentral', 'Paracentral_Lobule',  'Rolandic_Oper', ...                                               % frontal   25 -34
        'Frontal_Inf_Oper', 'Frontal_Inf_Tri', ...                                                              % frontal
        'Frontal_Mid', 'Frontal_Sup', 'Supp_Motor_Area', 'Frontal_Sup_Medial', 'Frontal_Sup_Med',  ...          % frontal
        'Frontal_Inf_Orb', 'Frontal_Mid_Orb','Frontal_Sup_Orb', 'Frontal_Med_Orb', 'Rectus',  ...               % orbito-frontal 35-39
        'Insula', ...                                                                                           % 40
        'Amygdala', ...                                                                                         % 41
        } ;
    
    AALDilateList_LH    = cellfun( @(p) [ p '_L' ], AALDilateList_H, 'UniformOutput', false );
    AALDilateList_RH    = cellfun( @(p) [ p '_R' ], AALDilateList_H, 'UniformOutput', false );
    AALDilateList       = [ AALDilateList_LH AALDilateList_RH ] ;
    
    
%     AALDilateList_2 = { 'Precentral_L' 'Precentral_R' 'Frontal_Sup_L' 'Frontal_Sup_R' 'Frontal_Sup_Orb_L' 'Frontal_Sup_Orb_R' 'Frontal_Mid_L' 'Frontal_Mid_R' ...
%         'Frontal_Mid_Orb_L' 'Frontal_Mid_Orb_R' 'Frontal_Inf_Oper_L' 'Frontal_Inf_Oper_R' 'Frontal_Inf_Tri_L' 'Frontal_Inf_Tri_R' 'Frontal_Inf_Orb_L' ...
%         'Frontal_Inf_Orb_R' 'Rolandic_Oper_L' 'Rolandic_Oper_R' 'Supp_Motor_Area_L' 'Supp_Motor_Area_R' 'Olfactory_L' 'Olfactory_R' 'Frontal_Sup_Medial_L' ...
%         'Frontal_Sup_Medial_R' 'Frontal_Med_Orb_L' 'Frontal_Med_Orb_R' 'Rectus_L' 'Rectus_R' 'Insula_L' 'Insula_R' 'Cingulum_Ant_L' 'Cingulum_Ant_R' ...
%         'Cingulum_Mid_L' 'Cingulum_Mid_R' 'Cingulum_Post_L' 'Cingulum_Post_R' 'Hippocampus_L' 'Hippocampus_R' 'ParaHippocampal_L' 'ParaHippocampal_R' ...
%         'Amygdala_L' 'Amygdala_R' 'Calcarine_L' 'Calcarine_R' 'Cuneus_L' 'Cuneus_R' 'Lingual_L' 'Lingual_R' 'Occipital_Sup_L' 'Occipital_Sup_R' ...
%         'Occipital_Mid_L' 'Occipital_Mid_R' 'Occipital_Inf_L' 'Occipital_Inf_R' 'Fusiform_L' 'Fusiform_R' 'Postcentral_L' 'Postcentral_R' 'Parietal_Sup_L' ...
%         'Parietal_Sup_R' 'Parietal_Inf_L' 'Parietal_Inf_R' 'SupraMarginal_L' 'SupraMarginal_R' 'Angular_L' 'Angular_R' 'Precuneus_L' 'Precuneus_R' ...
%         'Paracentral_Lobule_L' 'Paracentral_Lobule_R' 'Caudate_L' 'Caudate_R' 'Putamen_L' 'Putamen_R' 'Pallidum_L' 'Pallidum_R' 'Thalamus_L' 'Thalamus_R' ...
%         'Heschl_L' 'Heschl_R' 'Temporal_Sup_L' 'Temporal_Sup_R' 'Temporal_Pole_Sup_L' 'Temporal_Pole_Sup_R' 'Temporal_Mid_L' 'Temporal_Mid_R' 'Temporal_Pole_Mid_L' ...
%         'Temporal_Pole_Mid_R' 'Temporal_Inf_L' 'Temporal_Inf_R' 'Cerebelum_Crus1_L' 'Cerebelum_Crus1_R' 'Cerebelum_Crus2_L' 'Cerebelum_Crus2_R' 'Cerebelum_3_L' ...
%         'Cerebelum_3_R' 'Cerebelum_4_5_L' 'Cerebelum_4_5_R' 'Cerebelum_6_L' 'Cerebelum_6_R' 'Cerebelum_7b_L' 'Cerebelum_7b_R' 'Cerebelum_8_L' 'Cerebelum_8_R' ...
%         'Cerebelum_9_L' 'Cerebelum_9_R' 'Cerebelum_10_L' 'Cerebelum_10_R' 'Vermis_1_2' 'Vermis_3' 'Vermis_4_5' 'Vermis_6' 'Vermis_7' 'Vermis_8' 'Vermis_9' ...
%         'Vermis_10' } ;
    
    
%    AALList             = AALDilateList ;
    
% /export/data/users/jeandidier.lemarecha/these/F-TRACT/F-TRACT_CCEP/code_pierre_deman_ftract_mesh_parcels/MNI_Atlases


    parcelsName         = AALDilateList ;
    parcelsTexture      = {} ;

    
%     roi = load('/export/data/users/jeandidier.lemarecha/these/F-TRACT/F-TRACT_CCEP/code_pierre_deman_ftract_mesh_parcels/MNI_Atlases/ROI_MNI_V4_List.mat');
%     roi = roi.ROI ;
%     tmp = arrayfun( @(x) x.Nom_L, roi, 'UniformOutput', false ) ;
%     isequal( AALDilateList, tmp ) ; % yes
%     
%     tmp = nifti('/export/data/users/jeandidier.lemarecha/these/F-TRACT/F-TRACT_CCEP/code_pierre_deman_ftract_mesh_parcels/MNI_Atlases/AALSEEG12Dilate.nii');
    
% elseif strcmp( atlas, 'AALSeeg' )
 
elseif strcmp( atlasId, 'Brodmann' ) || strcmp( atlasId, 'BrodmannDilate' )
 
    BrodmannList    = arrayfun( @(i) [ num2str(i) '.0' ], [ 1:49 101:149 ], 'UniformOutput', false ) ;
    parcelsName     = BrodmannList ;
    parcelsTexture 	= {} ;

elseif strcmp( atlasId, 'HCP-MMP1' ) || contains( atlasId, 'Lausanne2008' )
    
    if symmetric
        fname       = fullfile( bd, 'scripts/parcellations/anatomy_sorted', [ atlasId '-sym.txt' ] ) ;
    else
        fname       = fullfile( bd, 'scripts/parcellations/anatomy_sorted', [ atlasId '.txt' ] ) ;
    end
    parcelsName = fileread(fname);
    parcelsName = strtrim(parcelsName);
    parcelsName = strsplit(parcelsName,'\n');
    
    parcelsTexture      = {} ;
    
else
    
    error('unknown atlas') ;
    
end

end