#! /bin/bash

SRCDIR="/Users/jd/mnt/lustre/ftract_ccep_dcm/screenshots"
DESTDIR="/Users/jd/these/F-TRACT/ftract_ccep_dcm/documents/Dynamic causal modeling of cortico-cortical evoked potentials/screenshots/"

# fig1: models



# fig2: fit et data
# alignement : 
#			fit en bas a droite => y=0 alignes avec y=0 de histo
#			fit en haut a droite => dessous de xscale aligne avec 50 de histo

# 
# cp /Users/jd/mnt/lustre/ftract_ccep_dcm/scripts/review_stimDCM_V13_R2_80pc_MAX_PK_DIF_5_minPL_0.txt "$DESTDIR"
# cp $SRCDIR/histo_fits_all_s1_s2_versus_peakLatency_V13_R2_80pc_MAX_PK_DIF_5_minPL_0.svg "$DESTDIR"

# minPL0_maxPL80_R2NaN_MPDNaN_R270_MPD5
cp /Users/jd/mnt/lustre/ftract_ccep_dcm/scripts/review_stimDCM_V14_minPL0_maxPL80_R2NaN_MPDNaN_R270_MPD5.txt "$DESTDIR"
cp $SRCDIR/histo_fits_all_s1_s2_versus_peakLatency_V14_minPL0_maxPL80_R2NaN_MPDNaN_R270_MPD5.svg "$DESTDIR"

cp $SRCDIR/data_fitted_16_20_5203.{svg,txt}		"$DESTDIR"
cp $SRCDIR/data_fitted_32_36_10402.{svg,txt}	"$DESTDIR"
cp $SRCDIR/data_fitted_48_52_970.{svg,txt}		"$DESTDIR"
cp $SRCDIR/data_fitted_64_68_47957.{svg,txt}	"$DESTDIR"


# fig3: estimation of CD/Te/Ti et histo of PL/CD/Te/Ti
cp $SRCDIR/histo_CD_V14_minPL0_maxPL80_R2NaN_MPDNaN_R270_MPD5.{svg,txt} "$DESTDIR"
cp $SRCDIR/histo_TE_V14_minPL0_maxPL80_R2NaN_MPDNaN_R270_MPD5.{svg,txt} "$DESTDIR"
cp $SRCDIR/histo_TI_V14_minPL0_maxPL80_R2NaN_MPDNaN_R270_MPD5.{svg,txt} "$DESTDIR"
cp $SRCDIR/histo_PL_V14_minPL0_maxPL80_R2NaN_MPDNaN_R270_MPD5.svg "$DESTDIR"
cp $SRCDIR/cd_te_ti_wrt_PL_V14_minPL0_maxPL80_R2NaN_MPDNaN_R270_MPD5.svg "$DESTDIR"



# fig4: CD on parcellation and histo of PL/CD, distances, PL/CD velocities
SRCDIR="/Users/jd/mnt/lustre/ftract_ccep_dcm/screenshots/PATS__ArtefactCorrectionPCHIP/ALL/2steps/V14/minPL0_maxPL80_R2NaN_MPDNaN/MarsAtlas/R270_MPD5/age_10_Inf/"

# matrice conduction delays between parcels
cp $SRCDIR/V14_MarsAtlas_sym1_minPL0_maxPL80_R2NaN_MPDNaN_R270_MPD5__conductionDelays_mat.svg "$DESTDIR"
# histo peak latencies
cp $SRCDIR/V14_MarsAtlas_sym1_minPL0_maxPL80_R2NaN_MPDNaN_R270_MPD5__peakLatency_histo_parcels_count.svg "$DESTDIR"
# histo conduction delays
cp $SRCDIR/V14_MarsAtlas_sym1_minPL0_maxPL80_R2NaN_MPDNaN_R270_MPD5__conductionDelays_histo_parcels_count.svg "$DESTDIR"
# histo distances
cp $SRCDIR/V14_MarsAtlas_sym1_minPL0_maxPL80_R2NaN_MPDNaN_R270_MPD5__distance_histo_parcels_count.svg "$DESTDIR"
# histo peak latencies velocity
cp $SRCDIR/V14_MarsAtlas_sym1_minPL0_maxPL80_R2NaN_MPDNaN_R270_MPD5__PLVelocity_histo_parcels_count.svg "$DESTDIR"
# histo conduction delays velocity
cp $SRCDIR/V14_MarsAtlas_sym1_minPL0_maxPL80_R2NaN_MPDNaN_R270_MPD5__CDVelocity_histo_parcels_count.svg "$DESTDIR"


# summary txt files
cp $SRCDIR/V14_MarsAtlas_sym1_minPL0_maxPL80_R2NaN_MPDNaN_R270_MPD5__summary.txt "$DESTDIR"
cp $SRCDIR/V14_MarsAtlas_sym0_minPL0_maxPL80_R2NaN_MPDNaN_R270_MPD5__summary.txt "$DESTDIR"



# fig6: histo of Te/Ti and 3D brain
SRCDIR="/Users/jd/mnt/lustre/ftract_ccep_dcm/screenshots/PATS__ArtefactCorrectionPCHIP/ALL/2steps/V14/minPL0_maxPL80_R2NaN_MPDNaN/MarsAtlas/R270_MPD5/age_10_Inf/"

# histo synaptic Te
#cp $SRCDIR/V14_MarsAtlas_sym1_minPL0_maxPL80_R2NaN_MPDNaN_R270_MPD5__TE_histo_parcels_count.svg "$DESTDIR"
cp $SRCDIR/V14_MarsAtlas_sym0_minPL0_maxPL80_R2NaN_MPDNaN_R270_MPD5__TE_histo_parcels_count.svg "$DESTDIR"
# histo synaptic Ti
#cp $SRCDIR/V14_MarsAtlas_sym1_minPL0_maxPL80_R2NaN_MPDNaN_R270_MPD5__TI_histo_parcels_count.svg "$DESTDIR"
cp $SRCDIR/V14_MarsAtlas_sym0_minPL0_maxPL80_R2NaN_MPDNaN_R270_MPD5__TI_histo_parcels_count.svg "$DESTDIR"

# 3D brain 
SRCDIR="/Users/jd/mnt/lustre/ftract_ccep_dcm/dcm/PATS__ArtefactCorrectionPCHIP/ALL/2steps/V14/minPL0_maxPL80_R2NaN_MPDNaN/MarsAtlas/R270_MPD5/age_10_Inf/"
#cp $SRCDIR/textures/synapticTe/sym/*png "$DESTDIR"
#cp $SRCDIR/textures/synapticTi/sym/*png "$DESTDIR"
cp $SRCDIR/textures/synapticTe/nosym/*png "$DESTDIR"
cp $SRCDIR/textures/synapticTi/nosym/*png "$DESTDIR"


# supplementary data

# fig 1 : repartition du nb de cceps a travers les stim
SRCDIR="/Users/jd/mnt/lustre/ftract_ccep_dcm/screenshots/histo_stims_cceps.svg"
cp $SRCDIR "$DESTDIR"

# fig 2 : peak latency wrt conduction and synaptic delay
SRCDIR="/Users/jd/mnt/lustre/ftract_ccep_dcm/screenshots/pl_wrt_cd_te.svg"
cp $SRCDIR "$DESTDIR"



