% default
He = 4 ;
Te = 8 ;
Hi = 32 ;
Ti = 16 ;
config_default  = struct( 'He', He, 'Te', Te, 'Hi', Hi, 'Ti', Ti ) ;

% used
k  = 2 ;
He = k   * 4 ;
Te = 1/k * 8 ;
Hi = k   * 32 ;
Ti = 1/k * 16 ;
config_used     = struct( 'He', He, 'Te', Te, 'Hi', Hi, 'Ti', Ti ) ;


convolution_kernel = @(t,H,T) H/T*t.*exp(-t/T) ;


t=0:1000 ; 
configs = { config_default config_used } ;
for c=1:2
    config = configs{c} ;
    figure ;
    plot( t, convolution_kernel(t,config.He,config.Te) ) ;
    hold on ;
    plot( t, convolution_kernel(t,config.Hi,config.Ti) ) ;
    legend({ 'exc' 'inh'})
    xlim([0 200]); ylim([0 25]);
    
    pow_exc = integral( @(t) convolution_kernel(t,config.He,config.Te), t(1), t(end) ) ;
    pow_inh = integral( @(t) convolution_kernel(t,config.Hi,config.Ti), t(1), t(end) ) ;
    title( [ 'He:' num2str(config.He) ' Te:' num2str(config.Te) ' Hi:' num2str(config.Hi) ' Ti:' num2str(config.Ti) ' - Power Exc: ' num2str(pow_exc) ' Inh: ' num2str(pow_inh) ] ) ;
end


